﻿using System;
using System.Data;
using System.Data.Common;
using Statnco.FW.Dac;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;

namespace Statnco.DA.Racket
{
    public class Racket_DA : DataAccessBase
    {
        public Racket_DA(Database DB) : base(DB) { }

        public Racket_DA(Database DB, DbTransaction DbTrans) : base(DB, DbTrans) { }

        #region 공통 코드

        #region 공통 코드 조회
        public DataTable GetComCodeList(string pDIVISION)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COM_CODE_LIST_NTR");

            DB.AddInParameter(cmd, "@pDIVISION", DbType.String, pDIVISION);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 공통 코드 저장
        public int SetComCode(string division, byte code, string codeName)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COM_CODE_TRX");

            DB.AddInParameter(cmd, "@division", DbType.String, division);
            DB.AddInParameter(cmd, "@code", DbType.String, code);
            DB.AddInParameter(cmd, "@codeName", DbType.String, codeName);

            DB.AddOutParameter(cmd, "@returnNo", DbType.Int32, 4);

            DB.ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@returnNo"));
        }
        #endregion

        #endregion

        #region 리전 코드 조회
        public DataTable fnGetRegionList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_REGION_LIST");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable fnGetAreaList(int pREGION_CODE)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_AREA_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.String, pREGION_CODE);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 리스트 조회
        public DataTable GetCompetitionList(string pCOMPETITION_NAME, byte pORDER_OPT)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_LIST");

            DB.AddInParameter(cmd, "@pCOMP_NAME", DbType.String, pCOMPETITION_NAME);
            DB.AddInParameter(cmd, "@pORDER_OPT", DbType.Byte, pORDER_OPT);

            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion

        #region 대회 마스터 저장
        public int SetCompetition(
            int competitionIdx,
            string pNAME, byte pTYPE_CODE, byte pGRADE_CODE, 
            DateTime pAPP_SRT_DATE, DateTime pAPP_END_DATE, 
            DateTime pSRT_DATE, DateTime pEND_DATE,
            DateTime pLIST_DATE, byte pAPP_TEAMS_CODE, string pLOCATION_CODE_SET, 
            int pADMIN_IDX
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.String, competitionIdx);
            DB.AddInParameter(cmd, "@pNAME", DbType.String, pNAME);
            DB.AddInParameter(cmd, "@pTYPE_CODE", DbType.Byte, pTYPE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Byte, pGRADE_CODE);
            DB.AddInParameter(cmd, "@pAPP_SRT_DATE", DbType.DateTime, pAPP_SRT_DATE);
            DB.AddInParameter(cmd, "@pAPP_END_DATE", DbType.DateTime, pAPP_END_DATE);
            DB.AddInParameter(cmd, "@pSRT_DATE", DbType.DateTime, pSRT_DATE);
            DB.AddInParameter(cmd, "@pEND_DATE", DbType.DateTime, pEND_DATE);
            DB.AddInParameter(cmd, "@pLIST_DATE", DbType.DateTime, pLIST_DATE);
            DB.AddInParameter(cmd, "@pAPP_TEAMS_CODE", DbType.Byte, pAPP_TEAMS_CODE);
            DB.AddInParameter(cmd, "@pLOCATION_CODE_SET", DbType.String, pLOCATION_CODE_SET);
            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oCOMPETITION_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (DB.GetParameterValue(cmd, "@oRETURN_NO").ToString() == "0")
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oCOMPETITION_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 대회 상세 조회 
        public DataTable GetCompetition(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_NTR");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 장소 저장
        public int SetCompetitionStadium(DataTable pSTADIUM_TABLE)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_TRX");


            SqlParameter param = new SqlParameter("@pSTADIUM_TABLE", pSTADIUM_TABLE);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 룰 저장
        public int SetCompetitionRules(int competitionIdx, int pUSE_LIVE_SCORE, 
            string pRANK_RULE, byte pTHIRD_RANK_RULE_CODE, 
            byte pSUB_WIN_GAME_CNT, byte pSUB_WIN_SCORES, byte pSUB_MAX_SCORES, byte pSUB_IS_DEUCE,
            byte pMAIN_WIN_GAME_CNT, byte pMAIN_WIN_SCORES, byte pMAIN_MAX_SCORES, byte pMAIN_IS_DEUCE,
            byte pTIME_PER_MATCH
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_RULES_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pUSE_LIVE_SCORE", DbType.Boolean, pUSE_LIVE_SCORE);

            DB.AddInParameter(cmd, "@pRANK_RULE", DbType.String, pRANK_RULE);
            DB.AddInParameter(cmd, "@pTHIRD_RANK_RULE_CODE", DbType.Byte, pTHIRD_RANK_RULE_CODE);

            DB.AddInParameter(cmd, "@pSUB_WIN_GAME_CNT", DbType.Byte, pSUB_WIN_GAME_CNT);
            DB.AddInParameter(cmd, "@pSUB_WIN_SCORES", DbType.Byte, pSUB_WIN_SCORES);
            DB.AddInParameter(cmd, "@pSUB_MAX_SCORES", DbType.Byte, pSUB_MAX_SCORES);
            DB.AddInParameter(cmd, "@pSUB_IS_DEUCE", DbType.Boolean, pSUB_IS_DEUCE);

            DB.AddInParameter(cmd, "@pMAIN_WIN_GAME_CNT", DbType.Byte, pMAIN_WIN_GAME_CNT);
            DB.AddInParameter(cmd, "@pMAIN_WIN_SCORES", DbType.Byte, pMAIN_WIN_SCORES);
            DB.AddInParameter(cmd, "@pMAIN_MAX_SCORES", DbType.Byte, pMAIN_MAX_SCORES);
            DB.AddInParameter(cmd, "@pMAIN_IS_DEUCE", DbType.Boolean, pMAIN_IS_DEUCE);

            DB.AddInParameter(cmd, "@pTIME_PER_MATCH", DbType.Byte, pTIME_PER_MATCH);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 종목 조회 
        public DataTable fnGetCompetitionEventsList(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_LIST");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 종목 저장
        public int SetCompEvents(int pEVENT_IDX, int competitionIdx, int pORDER,
            byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, byte pAPP_TEAM_CNT,
            int pADMIN_IDX
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_TRX");

            DB.AddInParameter(cmd, "@pEVENT_IDX", DbType.Int32, pEVENT_IDX);
            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pORDER", DbType.Int32, pORDER);

            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Int32, pEVENT_CODE);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Int32, pAGE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Int32, pGRADE_CODE);
            DB.AddInParameter(cmd, "@pAPP_TEAM_CNT", DbType.Int32, pAPP_TEAM_CNT);

            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 종목 삭제
        public int DelCompEvents(int competitionIdx, string pEVENT_IDX_SET, int pADMIN_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_DEL");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pEVENT_IDX_SET", DbType.String, pEVENT_IDX_SET);
            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 경기장 메인 리스트 조회 
        public DataTable GetCompetitionStadiumMainList(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_MAIN_LIST");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 경기장 코트 수 저장
        public int SetCompetitionStadiumCourtCnt(int competitionIdx, int pSTADIUM_NO, 
                int pCOURT_CNT
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_MAIN_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, pSTADIUM_NO);
            DB.AddInParameter(cmd, "@pCOURT_CNT", DbType.Int32, pCOURT_CNT);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 저장
        public int SetCompetitionStadiumSchedule(int competitionIdx, int pSTADIUM_NO,
                string pTIME_TYPE, DateTime pTIME
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_SCHEDULE_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, pSTADIUM_NO);
            DB.AddInParameter(cmd, "@pTIME_TYPE", DbType.String, pTIME_TYPE);
            DB.AddInParameter(cmd, "@pTIME", DbType.DateTime, pTIME);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 참가현황 그룹 보기
        public DataTable GetCompetitionGroup(int competitionIdx, byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_GROUP");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Byte, pEVENT_CODE);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Byte, pAGE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Byte, pGRADE_CODE);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 종목별 팀리스트 조회
        public DataTable GetCompetitionTeamsDetail(int competitionIdx, int pGROUP_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_TEAMS_DETAIL");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, pGROUP_IDX);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 그룹 설정 저장
        public int SetCompetitionGroupConfig(int competitionIdx, int groupIdx,
                byte competitionType, byte leagueGroupCount, byte tournementMinRank, int stadiumNo
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_GROUP_CONFIG_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            DB.AddInParameter(cmd, "@pCOMPETITION_TYPE", DbType.Byte, competitionType);
            DB.AddInParameter(cmd, "@pLEAGUE_GROUP_COUNT", DbType.Byte, leagueGroupCount);
            DB.AddInParameter(cmd, "@pTOURNEMENT_MIN_RANK", DbType.Byte, tournementMinRank);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, stadiumNo);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 리그 팀리스트 조회
        public DataTable GetCompetitionLeagueSchedule(int competitionIdx, int pGROUP_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_LEAGUE_SCHEDULE");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, pGROUP_IDX);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 운영 방식 조회 
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_RULES_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 코트 타임테이블 조회
        public DataTable GetCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_COURT_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Byte, stadiumNo);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 매치 리스트 조회
        public DataTable GetCompetitionMatches(int competitionIdx, int groupIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_MATCHES_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 토너먼트 생성
        public int InitMatches(int competitionIdx, int groupIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_TOURNAMENT_INIT_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 배정된 팀끼리 스왑
        public int SwapMatches(int competitionIdx, int groupIdx, int matchIdx1, int matchIdx2, byte team1Num, byte team2Num)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_SWAP_MATCHES_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX1", DbType.Int32, matchIdx1);
            DB.AddInParameter(cmd, "@pMATCH_IDX2", DbType.Int32, matchIdx2);
            DB.AddInParameter(cmd, "@pTEAM1_NUM", DbType.Byte, team1Num);
            DB.AddInParameter(cmd, "@pTEAM2_NUM", DbType.Byte, team2Num);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 회원 관리
        public DataTable GetMemberDetail(string type, int idx, string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_DETAIL_NTR");
           if(type == "info")
            {
                DB.AddInParameter(cmd, "@pTYPE", DbType.String, 'i');
                DB.AddInParameter(cmd, "@pUSERID", DbType.Int32, null);
                DB.AddInParameter(cmd, "@pUSERIDX", DbType.Int32, idx);
            }
           else if(type == "check")
            {
                DB.AddInParameter(cmd, "@pTYPE", DbType.String, 'c');
                DB.AddInParameter(cmd, "@pUSERIDX", DbType.Int32, null);
                DB.AddInParameter(cmd, "@pUSERID", DbType.String, id);

            }

            return ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region 회원 조회
        public DataTable GetMemberList(int opt1, int opt2, String search, int page)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_LIST_NTR");

            DB.AddInParameter(cmd, "@pOPTION1", DbType.Int32, opt1);
            DB.AddInParameter(cmd, "@pOPTION2", DbType.Int32, opt2);
            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, search);
            DB.AddInParameter(cmd, "@pPAGING", DbType.Int32, page);


            return ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region 회원정보 수정
        public int ModifyMemberInfo(int memberIdx, String userNickName, String phone, String sido, String sigungu, int assosication, int modiMemberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_MODIFY_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "u");
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pUSERNICKNAME", DbType.String, userNickName);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pSIDO", DbType.String, sido);
            DB.AddInParameter(cmd, "@pSIGUNGU", DbType.String, sigungu);
            DB.AddInParameter(cmd, "@pASSOCIATION", DbType.Int32, assosication);
            DB.AddInParameter(cmd, "@pMODMEMBER", DbType.Int32, modiMemberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 회원 탈퇴
        public int IsDelMemberInfo(int memberIdx, int modiMemberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_MODIFY_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "d");
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pUSERNICKNAME", DbType.String, null);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, null);
            DB.AddInParameter(cmd, "@pSIDO", DbType.String, null);
            DB.AddInParameter(cmd, "@pSIGUNGU", DbType.String, null);
            DB.AddInParameter(cmd, "@pASSOCIATION", DbType.Int32, null);
            DB.AddInParameter(cmd, "@pMODMEMBER", DbType.Int32, modiMemberIdx);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 회원 가입
        public int InsertMemberInfo(String uid, String pw, String name, String nickName, String ph, String birth, String mtype, String sido, String sigungu, String jtype, String gender, int asso)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_SIGNUP_TRX");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pUSERNAME", DbType.String, name);
            DB.AddInParameter(cmd, "@pUSERNICKNAME", DbType.String, nickName);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, ph);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pMEMBERTYPE", DbType.String, mtype);
            DB.AddInParameter(cmd, "@pSIDO", DbType.String, sido);
            DB.AddInParameter(cmd, "@pSIGUNGU", DbType.String, sigungu);
            DB.AddInParameter(cmd, "@pJOINTYPE", DbType.String, jtype);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pASSOCIATION", DbType.Int32, asso);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 로그인(아이디체크)
        public DataTable CheckUserId(String uid)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CHECKMEMBER_TRX");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, null);
            DB.AddInParameter(cmd, "@oTYPE", DbType.String, "i");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region  로그인 (비밀번호 확인)
        public DataTable CheckUserPw(String uid , String pw)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CHECKMEMBER_TRX");
            
            DB.AddInParameter(cmd, "@pUSERID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, pw);
            DB.AddInParameter(cmd, "@oTYPE", DbType.String, "p");

            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion

        #region  로그인 (회원정보 반환)
        public DataTable UserInfo(String id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_LOGMEMBERINFO_NTR");

            DB.AddInParameter(cmd, "@pID", DbType.String, id);

            
            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion

        #region 게시글 조회
        public DataTable GetBoardList(int pid, int opt1, int opt2, string search, int page)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARDLIST_NTR");

            DB.AddInParameter(cmd, "@pPOSTIDX", DbType.Int32, pid);
            DB.AddInParameter(cmd, "@pOPTION1", DbType.Int32, opt1);
            DB.AddInParameter(cmd, "@pOPTION2", DbType.Int32, opt2);
            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, search);
            DB.AddInParameter(cmd, "@pPAGING", DbType.Int32, page);

            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion

        #region 게시글 작성
        public int InsertBoard(int pid, int type, string title,  string contents, string writer, int mbIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARDINSERT_TRX");

            DB.AddInParameter(cmd, "@pPOSTIDX", DbType.Int32, pid);
            DB.AddInParameter(cmd, "@pTYPE", DbType.Int32, type);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
            DB.AddInParameter(cmd, "@pWRITER", DbType.String, writer);
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, mbIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        
        #region 게시글 상세보기
        public DataTable GetBoardDetail(int bIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARDDETAIL_NTR");

            DB.AddInParameter(cmd, "@pBOARDIDX", DbType.Int32, bIdx);

            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion

        //#region 게시글 수정
        //public int ModifyBoard(string type, int bIdx, string title, string contents, int mbIdx)
        //{
        //    DbCommand cmd = DB.GetStoredProcCommand("USP_BOARDDETAIL_NTR");

        //    DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
        //    DB.AddInParameter(cmd, "@pBOARDIDX", DbType.Int32, bIdx);
        //    DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
        //    DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
        //    DB.AddInParameter(cmd, "@pMODMEMBERIDX", DbType.Int32, mbIdx);



        //    DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

        //    ExecuteNonQuery(cmd);

        //    return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        //}
        //#endregion

        
        #region 클럽생성
        public int CreateClub(int type, string clubName, string cAdminName, string cNickName, string birth, int cAdminIdx, string local, int gym)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CREATECLUB_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);
            DB.AddInParameter(cmd, "@pCLUBADMINN", DbType.String, cAdminName);
            DB.AddInParameter(cmd, "@pCLUBNICKNAME", DbType.String, cNickName);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pCLUBADMINI", DbType.Int32, cAdminIdx);
            DB.AddInParameter(cmd, "@pCLUBLOCAL", DbType.String, local);
            DB.AddInParameter(cmd, "@pGYMIDX", DbType.Int32, gym);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽명 중복 확인
        public DataTable CheckClubName(string clubName)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CHECKCLUBNAME_NTR");

            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 클럽 상세정보
        public DataTable ClubDetail(int id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBDETAIL_NTR");

            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, id);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion


        #region 클럽 수정
        public int ModifyClubInfo(int idx, string clubName, string AdminName, int AdminIdx, int gym)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMODIFY_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "u");
            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, idx);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);
            DB.AddInParameter(cmd, "@pCLUBADMINN", DbType.String, AdminName);
            DB.AddInParameter(cmd, "@pCLUBADMINI", DbType.Int32, AdminIdx);
            DB.AddInParameter(cmd, "@pGYM", DbType.Int32, gym);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽 삭제
        public int  DeleteClub(int idx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMODIFY_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "d");
            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, idx);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, "");
            DB.AddInParameter(cmd, "@pCLUBADMINN", DbType.String, "");
            DB.AddInParameter(cmd, "@pCLUBADMINI", DbType.Int32, 0);
            DB.AddInParameter(cmd, "@pGYM", DbType.Int32, null);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽 조회
        public DataTable ClubList(string local, int type, int opt, string search)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBLIST_NTR");

            DB.AddInParameter(cmd, "@pLOCAL", DbType.String, local);
            DB.AddInParameter(cmd, "@pTYPE", DbType.Int32, type);
            DB.AddInParameter(cmd, "@pOPTION", DbType.Int32, opt);
            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, search);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

    }
}
