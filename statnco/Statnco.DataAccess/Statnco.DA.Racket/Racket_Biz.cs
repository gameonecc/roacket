﻿using System;
using System.Data;
using Statnco.FW.Dac;

namespace Statnco.DA.Racket
{
    public class Racket_Biz : BizLogicBase
    {
        public Racket_Biz() : base("racket_DB", true) { }

        #region 공통 코드 조회
        public DataTable GetComCodeList(string pDIVISION)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetComCodeList(pDIVISION);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 공통 코드 저장
        public int SetComCode(string division, byte code, string codeName)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetComCode(division, code, codeName);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 리전 코드 조회
        public DataTable fnGetRegionList()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.fnGetRegionList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable fnGetAreaList(int pREGION_CODE)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.fnGetAreaList(pREGION_CODE);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 리스트 조회
        public DataTable GetCompetitionList(string pCOMPETITION_NAME, byte pORDER_OPT)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionList(pCOMPETITION_NAME, pORDER_OPT);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 마스터 저장
        public int SetCompetition(
            int competitionIdx, string pNAME, byte pTYPE_CODE, byte pGRADE_CODE,
            DateTime pAPP_SRT_DATE, DateTime pAPP_END_DATE,
            DateTime pSRT_DATE, DateTime pEND_DATE,
            DateTime pLIST_DATE, byte pAPP_TEAMS_CODE, string pLOCATION_CODE_SET,
            string[] pSTADIUM_DATA_SET, int pADMIN_IDX
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {

                    rtn = da.SetCompetition(competitionIdx, pNAME, pTYPE_CODE, pGRADE_CODE, pAPP_SRT_DATE,
                        pAPP_END_DATE, pSRT_DATE, pEND_DATE, pLIST_DATE, pAPP_TEAMS_CODE,
                        pLOCATION_CODE_SET, pADMIN_IDX
                    );


                    if (rtn > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add(new DataColumn("cCOMPETITION_IDX", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_NAME", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_ADDR_1", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_ADDR_2", typeof(string)));

                        for (var i = 0; i < pSTADIUM_DATA_SET.Length; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["cCOMPETITION_IDX"] = rtn;
                            dr["cSTADIUM_NAME"] = pSTADIUM_DATA_SET[i].Split('|')[0];
                            dr["cSTADIUM_ADDR_1"] = pSTADIUM_DATA_SET[i].Split('|')[1];
                            dr["cSTADIUM_ADDR_2"] = pSTADIUM_DATA_SET[i].Split('|')[2];

                            dt.Rows.Add(dr);
                        }

                        rtn = da.SetCompetitionStadium(dt);
                    }
                    else if (rtn != -1)
                    {
                        rtn = 0;
                    }


                }
                CommitTransaction();
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 상세 조회 
        public DataTable GetCompetition(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetition(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 룰 저장
        public int SetCompetitionRules(int competitionIdx, int pUSE_LIVE_SCORE,
            string pRANK_RULE, byte pTHIRD_RANK_RULE_CODE,
            byte pSUB_WIN_GAME_CNT, byte pSUB_WIN_SCORES, byte pSUB_MAX_SCORES, byte pSUB_IS_DEUCE,
            byte pMAIN_WIN_GAME_CNT, byte pMAIN_WIN_SCORES, byte pMAIN_MAX_SCORES, byte pMAIN_IS_DEUCE,
            byte pTIME_PER_MATCH
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionRules(competitionIdx, pUSE_LIVE_SCORE,
                            pRANK_RULE, pTHIRD_RANK_RULE_CODE,
                            pSUB_WIN_GAME_CNT, pSUB_WIN_SCORES, pSUB_MAX_SCORES, pSUB_IS_DEUCE,
                            pMAIN_WIN_GAME_CNT, pMAIN_WIN_SCORES, pMAIN_MAX_SCORES, pMAIN_IS_DEUCE,
                            pTIME_PER_MATCH
                        );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 종목 조회 
        public DataTable fnGetCompetitionEventsList(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.fnGetCompetitionEventsList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion


        #region 대회 종목 저장
        public int SetCompEvents(int pEVENT_IDX, int competitionIdx, int pORDER,
            byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, byte pAPP_TEAM_CNT,
            int pADMIN_IDX
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetCompEvents(pEVENT_IDX, competitionIdx, pORDER,
                        pEVENT_CODE, pAGE_CODE, pGRADE_CODE, pAPP_TEAM_CNT,
                        pADMIN_IDX
                    );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 종목 삭제
        public int DelCompEvents(int competitionIdx, string pEVENT_IDX_SET, int pADMIN_IDX)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.DelCompEvents(competitionIdx, pEVENT_IDX_SET, pADMIN_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 경기장 메인 리스트 조회 
        public DataTable GetCompetitionStadiumMainList(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionStadiumMainList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 경기장 코트 수 저장
        public int SetCompetitionStadiumCourtCnt(int competitionIdx, int pSTADIUM_NO,
                int pCOURT_CNT
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionStadiumCourtCnt(competitionIdx, pSTADIUM_NO, pCOURT_CNT);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionStadiumSchedule(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 저장
        public int SetCompetitionStadiumSchedule(int competitionIdx, int pSTADIUM_NO,
                string pTIME_TYPE, DateTime pTIME
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionStadiumSchedule(competitionIdx, pSTADIUM_NO, pTIME_TYPE, pTIME);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 참가현황 그룹 보기
        public DataTable GetCompetitionGroup(int competitionIdx, byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionGroup(competitionIdx, pEVENT_CODE, pAGE_CODE, pGRADE_CODE);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 종목별 팀리스트 조회
        public DataTable GetCompetitionTeamsDetail(int competitionIdx, int pGROUP_IDX)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionTeamsDetail(competitionIdx, pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 그룹 설정 저장
        public int SetCompetitionGroupConfig(int competitionIdx, int groupIdx,
                byte competitionType, byte leagueGroupCount, byte tournementMinRank, int stadiumNo
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionGroupConfig(competitionIdx, groupIdx, competitionType,
                            leagueGroupCount, tournementMinRank, stadiumNo
                        );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 리그 팀리스트 조회
        public DataTable GetCompetitionLeagueSchedule(int competitionIdx, int pGROUP_IDX)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionLeagueSchedule(competitionIdx, pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 운영 방식 조회 
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    dt = da.GetCompetitionRules(competitionIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 코트 타임테이블 조회
        public DataTable GetCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_DA(DB))
                {
                    dt = biz.GetCourtSchedule(competitionIdx, stadiumNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 매치 리스트 조회
        public DataTable GetCompetitionMatches(int competitionIdx, int groupIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetCompetitionMatches(competitionIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 토너먼트 생성
        public int InitMatches(int competitionIdx, int groupIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.InitMatches(competitionIdx, groupIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 배정된 팀끼리 스왑
        public int SwapMatches(int competitionIdx, int groupIdx, int matchIdx1, int matchIdx2, byte team1Num, byte team2Num)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.SwapMatches(competitionIdx, groupIdx, matchIdx1, matchIdx2, team1Num, team2Num);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 회원 관리
        public DataTable GetMemberDetail(string type, int idx, string id)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetMemberDetail(type, idx, id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 회원검색
        public DataTable GetMemberList(int opt1, int opt2, String search, int page)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetMemberList(opt1, opt2, search, page);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 회원정보 수정
        public int ModifyMemberInfo(int memberIdx, String userNickName, String phone, String sido, String sigungu, int assosication, int modiMemberIdx)
        {

            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.ModifyMemberInfo(memberIdx, userNickName, phone, sido, sigungu, assosication, modiMemberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 회원 탈퇴
        public int IsDelMemberInfo(int memberIdx, int modiMemberIdx)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.IsDelMemberInfo(memberIdx, modiMemberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 회원 가입
        public int InsertMemberInfo(String uid, String pw, String name, String nickName, String ph, String birth, String mtype, String sido, String sigungu, String jtype, String gender, int asso)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.InsertMemberInfo(uid, pw, name, nickName, ph, birth, mtype, sido, sigungu, jtype, gender, asso);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 로그인 (아이디확인)
        public DataTable CheckUserId(String uid)
        {
            DataTable dt = null;
            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.CheckUserId(uid);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;

        }
        #endregion

        #region  로그인 (비밀번호 확인)
        public DataTable CheckUserPw(String uid, String pw)
        {
            DataTable dt = null;
            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.CheckUserPw(uid, pw);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;

        }
        #endregion

        #region  로그인 (회원정보)
        public DataTable UserInfo(String id)
        {
            DataTable dt = null;
            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.UserInfo(id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;

        }
        #endregion

        #region 게시글 조회
        public DataTable GetBoardList(int pid, int opt1, int opt2, string search, int page)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetBoardList(pid, opt1, opt2, search, page);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }
        #endregion


        #region 게시글 작성
        public int InsertBoard(int pid, int type, string title, string contents, string writer, int mbIdx)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.InsertBoard(pid, type, title, contents, writer, mbIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion


        #region 게시글 상세보기
        public DataTable GetBoardDetail(int bIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.GetBoardDetail(bIdx);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return dt;

        }
        #endregion

        //#region 게시글 수정
        //public int ModifyBoard(string type, int bIdx, string title, string contents)
        //{
        //    int rtn = 0;
        //    try
        //    {
        //        BeginTransaction();
        //        using (var da = new Racket_DA(DB, DbTrans))
        //        {
        //            rtn = da.ModifyBoard(type, bIdx, title, contents);
        //        }
        //    }
        //    catch (Exception exp)
        //    {
        //        throw exp;
        //    }

        //    return rtn;
        //}
        //#endregion

        #region 클럽생성
        public int CreateClub(int type, string clubName, string cAdminName, string cNickName, string birth, int cAdminIdx, string local, int gym)
        {
            int rtn = 0;
           
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB, DbTrans))
                {
                    rtn = da.CreateClub(type, clubName, cAdminName, cNickName, birth, cAdminIdx, local, gym);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 클럽명 중복 확인
        public DataTable CheckClubName(string clubName)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Racket_DA(DB))
                {
                    dt = da.CheckClubName(clubName);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion


        #region 클럽 상세정보
        public DataTable ClubDetail(int id)
        {
            DataTable dt = null;
            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.ClubDetail(id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion


        #region 클럽 수정
        public int ModifyClubInfo(int idx, string clubName, string AdminName, int AdminIdx, int gym)
        {
            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB,DbTrans))
                {
                    dt = da.ModifyClubInfo(idx, clubName, AdminName, AdminIdx, gym);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion

        #region 클럽 삭제
        public int DeleteClub(int idx)
        {
            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Racket_DA(DB,DbTrans))
                {
                    dt = da.DeleteClub(idx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion

        #region 클럽 조회
        public DataTable ClubList(string local, int type, int opt, string search)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Racket_DA(DB))
                {
                    dt = da.ClubList(local, type, opt, search);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion
    }
}
