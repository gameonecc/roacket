﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionMatches
    {
        public class SwapInfo
        {
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
            public int matchIdx1 { get; set; }
            public int matchIdx2 { get; set; }
            public byte team1Num { get; set; }
            public byte team2Num { get; set; }
        }
    }
}