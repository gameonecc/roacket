﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Statnco.Web.Racket.API.Models
{
    public class CommonCodes
    {
        public class CommonCode
        {
            public string division { get; set; }
            public byte code { get; set; }
            public string codeName { get; set; }
        }
    }
}