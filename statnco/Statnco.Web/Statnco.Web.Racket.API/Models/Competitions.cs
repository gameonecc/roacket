﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Statnco.Web.Racket.API.Models
{
    public class Competitions
    {
        public class SearchOption
        {
            public string competitionName { get; set; }
            public byte orderOption { get; set; }
        }

        public class Competition
        {
            public string competitionName { get; set; }
            public byte typeCode { get; set; }
            public byte gradeTypeCode { get; set; }
            public string applicationStartDate { get; set; }
            public string applicationEndDate { get; set; }
            public string competitionStartDate { get; set; }
            public string competitionEndDate { get; set; }
            public string listDate { get; set; }
            public byte applicationTeamsCode { get; set; }
            public string stadiumCodeSet { get; set; }
            public string[] stadiumArray { get; set; }

            public int adminIdx { get; set; }

        }


       
        public class StadiumSchedule
        {
            public int pCOMP_IDX { get; set; }
            public int pSTADIUM_NO { get; set; }
            public string pTIME_TYPE { get; set; }
            public DateTime pTIME { get; set; }
        }
        
        public class TeamsDetail
        {
            public int pCOMP_IDX { get; set; }
            public int pGROUP_IDX { get; set; }
        }
        
        public class GroupSearch
        {
            public int pCOMP_IDX { get; set; }
            public byte pEVENT_CODE { get; set; }
            public byte pAGE_CODE { get; set; }
            public byte pGRADE_CODE { get; set; }
        }
    }
}