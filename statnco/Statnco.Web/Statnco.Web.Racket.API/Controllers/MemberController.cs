﻿using Statnco.DA.Racket;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MemberController : ApiController
    {

        //회원 정보 조회
        public object Get(String type, String id, int idx)
        {
            DataTable dt = null;
           

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetMemberDetail(type,idx, id);
            }

            if(type == "info" && dt.Rows.Count == 0)
            {
                return "등록되지 않은 회원입니다";
            } 
            else if (type == "check" && dt.Rows.Count == 0)
            {
                return "사용 가능한 아이디 입니다";
            }
            else if (type == "check" && dt.Rows.Count > 0)
            {
                return "이미 사용중인 아이디 입니다";
            }

            return dt;

        }

        // 조회 리스트
        public object Get(int option1, int option2, String search, int page)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetMemberList(option1, option2, search, page);
            }

            return dt;



        }
        // 회원정보 수정
        public int Patch(int memberIdx, String userNickName, String phone, String sido, String sigungu, int assosication, int modiMemberIdx)
        {
            int rtn = 0;

            using (var biz = new Racket_Biz())
            {
                rtn = biz.ModifyMemberInfo(memberIdx, userNickName, phone, sido, sigungu, assosication, modiMemberIdx);
            }

            return rtn;
        }

        //회원정보 삭제
        public int Put(int memberIdx, int modiMemberIdx)
        {
        int rtn = 0; //정상처리

        using (var biz = new Racket_Biz())
        {
            rtn = biz.IsDelMemberInfo(memberIdx, modiMemberIdx);
        }
        //오류 발생시 -1 retrun 
        return rtn;
        }

        // 회원가입
        public int Post(String uid, String pw, String name, String nickName, String ph, String birth, String mtype, String sido, String sigungu, String jtype, String gender, int asso)
        {
            int rtn = 0; //정상처리

            using (var biz = new Racket_Biz())
            {
                rtn = biz.InsertMemberInfo(uid, pw, name, nickName, ph, birth, mtype, sido, sigungu, jtype, gender, asso);
            }
            //오류 발생시 -1 retrun 
            return rtn;
        }

        

    }
}
