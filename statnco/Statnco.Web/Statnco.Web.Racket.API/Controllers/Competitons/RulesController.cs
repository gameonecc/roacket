﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers.Competitons
{
    public class RulesController : ApiController
    {
        #region 대회 리스트 조회
        [HttpGet]
        [ActionName("GetCompetitionList")]
        public string GetCompetitionList([FromUri] Competitions.SearchOption cs)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetCompetitionList(cs.competitionName, cs.orderOption);
            }

            return JsonConvert.SerializeObject(dt);
        }
        #endregion
    }
}
