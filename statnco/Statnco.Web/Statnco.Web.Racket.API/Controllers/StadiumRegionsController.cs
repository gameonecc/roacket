﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumRegionsController : ApiController
    {
        // GET: api/Region
        public DataTable Get()
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.fnGetRegionList();
            }

            return dt;
        }

    }
}
