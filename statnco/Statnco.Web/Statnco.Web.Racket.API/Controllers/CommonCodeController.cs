﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CommonCodeController : ApiController
    {
        // GET: api/CommonCode
        public DataTable Get(string division)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetComCodeList(division);
            }

            return dt;
        }

        public int Post([FromBody] CommonCodes.CommonCode cc)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Racket_Biz())
                {
                    rtn = biz.SetComCode(cc.division, cc.code, cc.codeName);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
