﻿using Statnco.DA.Racket;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class ClubController : ApiController
    {
        //클럽생성
        public int Post(int type, string clubName, string cAdminName, string cNickName, string birth, int cAdminIdx, string local, int gym)
        {
            int dt = 0;

            using (var biz = new Racket_Biz())
            {
                dt = biz.CreateClub(type, clubName, cAdminName, cNickName, birth, cAdminIdx, local, gym);
            }

            return dt;
        }
       
        //클럽 검색
        public object Get(string local, int type, int opt, string search)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.ClubList(local, type, opt, search);
            }
            if (dt.Rows.Count == 0)
            {
                return null;
            }

            return dt.Rows[0].DataRowToJson();

        }

        // 클럽 상세 조회
        public object Get(int id)
        {
            DataTable dt = null;

            
            using (var biz = new Racket_Biz())
            {
                dt = biz.ClubDetail(id);
            }

            if (dt.Rows.Count == 0)
            {
                return null;
            }
             
          
            return dt.Rows[0].DataRowToJson();

        }

        //클럽 수정
        public int Patch( int idx, string clubName, string AdminName, int AdminIdx, int gym)
        {
            int dt = 0;

          
            using (var biz = new Racket_Biz())
            {
                dt = biz.ModifyClubInfo(idx, clubName, AdminName, AdminIdx, gym);
            }

            return dt;
        }

        //클럽 삭제
        public int Put(int idx)
        {
            int dt = 0;

            using (var biz = new Racket_Biz())
            {
                dt = biz.DeleteClub(idx);
            }

            return dt;
        }

        



    }
}
