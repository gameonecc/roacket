﻿using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumSchedulesController : ApiController
    {
        // GET: api/StadiumSchedules
        public IEnumerable<DataTable> Get()
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionStadiumSchedule(id);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }
    }
}
