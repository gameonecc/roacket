﻿using Statnco.DA.Racket;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionSwapMatchesController : ApiController
    {
        // PUT: api/CompetitionSwapMatches/5
        public int Patch([FromBody]CompetitionMatches.SwapInfo cs)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Racket_Biz())
                {
                    rtn = biz.SwapMatches(cs.competitionIdx, cs.groupIdx, cs.matchIdx1, cs.matchIdx2, cs.team1Num, cs.team2Num);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
