﻿using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionMatchesController : ApiController
    {
        public DataTable Get(int competitonIdx, int groupIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionMatches(competitonIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw null;
            }

            return dt;
        }

        // GET: api/CompetitionMatches/5
        public int Post(int competitionIdx, int groupIdx)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Racket_Biz())
                {
                    rtn = biz.InitMatches(competitionIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }


    }
}
