﻿using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class BoardController : ApiController
    {
        //게시글 조회
        public object Get(int pid, int opt1, int opt2, string search, int page)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetBoardList(pid, opt1, opt2, search, page);
            }

            if (dt.Rows.Count == 0)
            {
                return "게시글이 없습니다";
            }
            return dt;
        }

        //게시글 수정
        public int Patch(int pid, int type, string title, string contents, string writer, int mbIdx)
        {
            int dt = 0;

            using (var biz = new Racket_Biz())
            {
                dt = biz.InsertBoard(pid, type, title, contents, writer, mbIdx);
            }
            
           return dt;
        }

        //public int Put(string type, int bIdx, string title, string contents, int modIdx)
        //{

        //    int dt = 0;

        //    using (var biz = new Racket_Biz())
        //    {
        //        dt = biz.ModifyBoard(type, bIdx, title, contents, modIdx);
        //    }

        //    return dt;
        //}
        
    }
}
