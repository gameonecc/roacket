﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Statnco.DA.Racket;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionsController : ApiController
    {
        public DataTable Get()
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetCompetitionList("", 0);
            }

            return dt;
        }

        // GET: api/Competitions/5
        public object Get(int id)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetition(id);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt.Rows[0].DataRowToJson();
        }

        // POST: api/CompetitionRules
        public int Post([FromBody] Competitions.Competition cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompetition(0, cc.competitionName, cc.typeCode, cc.gradeTypeCode,
                            Convert.ToDateTime(cc.applicationStartDate), Convert.ToDateTime(cc.applicationEndDate),
                            Convert.ToDateTime(cc.competitionStartDate), Convert.ToDateTime(cc.competitionEndDate),
                            Convert.ToDateTime(cc.listDate), cc.applicationTeamsCode, cc.stadiumCodeSet,
                            cc.stadiumArray, cc.adminIdx
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return 0;
        }


        // PUT: api/CompetitionRules/5
        public int Patch(int id, [FromBody] Competitions.Competition cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompetition(id, cc.competitionName, cc.typeCode, cc.gradeTypeCode,
                            Convert.ToDateTime(cc.applicationStartDate), Convert.ToDateTime(cc.applicationEndDate),
                            Convert.ToDateTime(cc.competitionStartDate), Convert.ToDateTime(cc.competitionEndDate),
                            Convert.ToDateTime(cc.listDate), cc.applicationTeamsCode, cc.stadiumCodeSet,
                            cc.stadiumArray, cc.adminIdx
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

        /*

        #region 종목별 팀리스트 조회
        [HttpGet]
        [ActionName("GetCompetitionTeamsDetail")]
        public string GetCompetitionTeamsDetail([FromUri]Competitions.TeamsDetail ct)
        {
            
        }
        #endregion

        #region 리그 팀 리스트 조회
        [HttpGet]
        [ActionName("GetCompetitionLeagueSchedule")]
        public string GetCompetitionLeagueSchedule([FromUri]Competitions.TeamsDetail ct)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionLeagueSchedule(ct.pCOMP_IDX, ct.pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                return exp.Message;
            }

            return JsonConvert.SerializeObject(dt);
        }
        #endregion
        */
    }
}