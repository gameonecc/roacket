﻿using Statnco.DA.Racket;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionTeamsController : ApiController
    {
        public DataTable Get([FromUri] CompetitionTeams.TeamList tl)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionTeamsDetail(tl.competitionIdx, tl.groupIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }
    }
}
