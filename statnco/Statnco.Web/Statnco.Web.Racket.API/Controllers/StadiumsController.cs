﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumsController : ApiController
    {
        // GET: api/Stadiums
        public DataTable Get(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionStadiumMainList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }
    }
}
