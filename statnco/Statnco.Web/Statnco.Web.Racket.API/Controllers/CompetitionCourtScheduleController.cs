﻿using Statnco.DA.Racket;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionCourtScheduleController : ApiController
    {
        public DataTable Get(int competitionIdx, byte stadiumNo)
        {
            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                dt = biz.GetCourtSchedule(competitionIdx, stadiumNo);
            }

            return dt;
        }
    }
}
