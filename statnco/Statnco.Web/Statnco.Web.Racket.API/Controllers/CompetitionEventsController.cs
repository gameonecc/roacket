﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionEventsController : ApiController
    {
        // GET: api/CompetitionEvents
        public DataTable Get(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.fnGetCompetitionEventsList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                
            }

            return dt;
        }

        // POST: api/CompetitionEvents
        public int Post([FromBody] CompetitionEvents.Events ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompEvents(0, ce.competitionIdx, ce.order,
                        ce.eventCode, ce.ageCode, ce.gradeCode, ce.applicationTeamCount, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return 0;
        }

        // PUT: api/CompetitionEvents/5
        public int Patch(int id, [FromBody] CompetitionEvents.Events ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompEvents(id, ce.competitionIdx, ce.order,
                        ce.eventCode, ce.ageCode, ce.gradeCode, ce.applicationTeamCount, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

        // DELETE: api/CompetitionEvents/5
        public int Delete(CompetitionEvents.EventsDel ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.DelCompEvents(ce.competitionIdx, ce.eventIdxSet, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
