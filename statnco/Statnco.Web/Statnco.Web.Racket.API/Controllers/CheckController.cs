﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using Statnco.DA.Racket;
using Statnco.Web.Racket.API.Models;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CheckController : ApiController
    {
        //클럽명 중복체크
        public int Get([FromUri]CommonCheck.ClubNameCheck cs)
        {
            DataTable dt = null;

            int rtn = 0;
           
            using (var biz = new Racket_Biz())
            {
                dt = biz.CheckClubName(cs.clubName);
            }

            if (dt.Rows.Count != 0)
            {
                return -1;
            }
          
            return rtn;
        }

        //회원가입시 id 중복체크
        public int Get(string id)
        {
            DataTable dt = null;

            int rtn = 0;

            using (var biz = new Racket_Biz())
            {
                dt = biz.CheckUserId(id);
            }

            if (dt.Rows.Count != 0)
            {
                return -1;
            }
            return rtn;
        }
    }
}
