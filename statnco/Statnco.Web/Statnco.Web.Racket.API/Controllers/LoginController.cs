﻿using Statnco.DA.Racket;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class LoginController : ApiController
    {
        // 로그인
        public object Get(String uid, String pw)
        {

            DataTable check1 = null;
            DataTable check2 = null;

            DataTable dt = null;

            using (var biz = new Racket_Biz())
            {
                check1 = biz.CheckUserId(uid);
                check2 = biz.CheckUserPw(uid, pw);
            }

            if(check1.Rows.Count == 0)
            {
                return "no id";
            }
            else if( check2.Rows.Count == 0)
            {
                return "wrong pw";
            }
            if(check2.Rows.Count != 0)
            {
                using (var biz2 = new Racket_Biz())
                {
                    dt = biz2.UserInfo(uid);
                   
                }
            }
            return dt;
        }
           
    }
}
