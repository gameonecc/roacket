﻿using Statnco.DA.Racket;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumScheduleController : ApiController
    {

        public DataTable Get(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionStadiumSchedule(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }

        // PUT: api/StadiumSchedule/5
        public int Patch([FromBody] Stadiums.StadiumSchedule css)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompetitionStadiumSchedule(
                        css.competitionIdx, css.stadiumNo, css.timeType, css.time);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
