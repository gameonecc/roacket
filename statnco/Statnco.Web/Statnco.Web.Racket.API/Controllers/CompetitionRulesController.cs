﻿using Newtonsoft.Json;
using Statnco.DA.Racket;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionRulesController : ApiController
    {
        //// GET: api/CompetitionRules
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        // GET: api/CompetitionRules/5
        public object Get(int id)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionRules(id);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt.Rows[0].DataRowToJson();
        }

        // POST: api/CompetitionRules
        public int Post([FromBody] CompetitionRules.Rules cr)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompetitionRules(cr.competitionIdx, cr.useLiveScore,
                            cr.rankRule, cr.thirdRankRule,
                            cr.subWinGameCount, cr.subWinScores, cr.subMaxScores, cr.subIsDeuce,
                            cr.mainWinGameCount, cr.mainWinScores, cr.mainMaxScores, cr.mainIsDeuce,
                            cr.timePerMatch
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
