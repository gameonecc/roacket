﻿using Statnco.DA.Racket;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionGroupsController : ApiController
    {
        // GET: api/CompetitionGroup
        public DataTable Get([FromUri] CompetitionGroups.GroupList gl)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Racket_Biz())
                {
                    dt = biz.GetCompetitionGroup(gl.competitionIdx, gl.eventCode, gl.ageCode, gl.gradeCode);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }

        public int Patch([FromBody] CompetitionGroups.GroupConfig cg)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Racket_Biz())
                {
                    rtn = biz.SetCompetitionGroupConfig(cg.competitionIdx, cg.groupIdx,
                            cg.competitionType, cg.leagueGroupCount, cg.tournementMinRank, 
                            cg.stadiumNo
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
