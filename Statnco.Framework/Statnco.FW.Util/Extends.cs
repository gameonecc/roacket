﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Web;
using System.Web.Script.Serialization;

namespace Statnco.FW.Util
{
    /// <summary>
    /// 확장 메서드 모음
    /// </summary>
    public static class Extends
    {
        #region System.Collections.Generic.Dictionary<string, object>

        public static DataSet ToDataSet(this Dictionary<string, object> dic)
        {
            DataSet ds = new DataSet();

            // 테이블 루프
            foreach (KeyValuePair<string, object> tables in dic)
            {
                DataTable dt = ds.Tables.Add(tables.Key);

                // Row 루프
                foreach (object row in (object[])tables.Value)
                {
                    DataRow dr = dt.NewRow();

                    foreach (KeyValuePair<string, object> cols in (Dictionary<string, object>)row)
                    {
                        // 컬럼 추가
                        if (dt.Columns[cols.Key] == null) dt.Columns.Add(cols.Key, Type.GetType("System.String"));

                        if (cols.Value == null)
                            dr[cols.Key] = System.DBNull.Value;
                        else
                            dr[cols.Key] = cols.Value;

                    }

                    dt.Rows.Add(dr);
                }
            }

            return ds;
        }

        #endregion

        #region object[]
        public static DataTable ToDataTable(this object[] obj)
        {

            // 테이블 루프
            DataTable dt = new DataTable();

            // Row 루프
            foreach (object row in obj)
            {
                DataRow dr = dt.NewRow();

                foreach (KeyValuePair<string, object> cols in (Dictionary<string, object>)row)
                {
                    // 컬럼 추가
                    if (dt.Columns[cols.Key] == null) dt.Columns.Add(cols.Key, Type.GetType("System.String"));

                    if (cols.Value == null)
                        dr[cols.Key] = System.DBNull.Value;
                    else
                        dr[cols.Key] = cols.Value;

                }

                dt.Rows.Add(dr);
            }

            return dt;
        }

        #endregion

        public static object ToJson(this DataTable value)

        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            serializer.MaxJsonLength = 2147483647;

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

            Dictionary<string, object> row;

            foreach (DataRow dr in value.Rows)

            {

                row = new Dictionary<string, object>();

                foreach (DataColumn col in value.Columns)
                {
                    row.Add(col.ColumnName, dr[col]);
                }

                rows.Add(row);
            }

            return new JavaScriptSerializer().DeserializeObject(serializer.Serialize(rows));

        }

        public static string ToJson(this DataSet value)

        {

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            serializer.MaxJsonLength = 2147483647;

            Dictionary<string, List<Dictionary<string, object>>> dsList = new Dictionary<string, List<Dictionary<string, object>>>();

            List<Dictionary<string, object>> rows;

            Dictionary<string, object> row;



            foreach (DataTable dt in value.Tables)

            {

                rows = new List<Dictionary<string, object>>();

                foreach (DataRow dr in dt.Rows)

                {

                    row = new Dictionary<string, object>();

                    foreach (DataColumn col in dt.Columns)

                    {

                        row.Add(col.ColumnName, dr[col]);

                    }

                    rows.Add(row);

                }

                dsList.Add(dt.TableName, rows);

            }

            return serializer.Serialize(dsList);

        }

        #region System.Data.DataSet

        /// <summary>
        /// 데이터 셋을 IDictionary<string, object> 객체로 변환
        /// </summary>
        /// <param name="ds">데이터 셋</param>
        /// <returns></returns>
        public static Dictionary<string, object> ToJSONObject(this DataSet ds)
        {
            Dictionary<string, object> dtDic = new Dictionary<string, object>();

            foreach (DataTable dt in ds.Tables)
            {
                List<object> rowDic = new List<object>();

                foreach (DataRow row in dt.Rows)
                {
                    Dictionary<string, object> colDic = new Dictionary<string, object>();

                    foreach (DataColumn col in row.Table.Columns)
                    {
                        colDic.Add(col.ColumnName, row[col]);
                    }

                    rowDic.Add(colDic);

                }

                dtDic.Add(dt.TableName, rowDic);
            }

            return dtDic;
        }

        /// <summary>
        /// 데이터 셋을 Dictionary로 변경한다. (=ToJSONObject)
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static Dictionary<string, object> ToDictionary(this DataSet ds)
        {
            return ds.Tables.Cast<DataTable>().ToDictionary(t => t.TableName, t => t.RowsToDictionary());
        }

        /// <summary>
        /// 데이터 셋을 JSON 스트링으로 변환한다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetJSONString(DataSet ds)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(ds.ToDictionary());
        }

        /// <summary>
        /// 데이터 셋을 JSON 스트링으로 변환한다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string ToJSONString(this DataSet ds)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(ds.ToDictionary());
        }


        /// <summary>
        /// 데이터 테이블을 합친다.
        /// </summary>
        /// <param name="ds">소스 데이터셋</param>
        /// <param name="tableName">테이블명</param>
        /// <param name="dt">데이터 테이블</param>
        /// <returns>데이터셋</returns>
        public static DataSet MergeTable(this DataSet ds, string tableName, DataTable dt)
        {
            dt.TableName = tableName;
            ds.Tables.Add(dt.Copy());
            //dt.Dispose();

            return ds;
        }

        /// <summary>
        /// 데이터 테이블을 합친다.
        /// </summary>
        /// <param name="ds">소스 데이터셋</param>
        /// <param name="tableName">테이블명</param>
        /// <param name="tds">합칠 데이터셋</param>
        /// <returns>데이터셋</returns>
        public static DataSet MergeTable(this DataSet ds, string tableName, DataSet tds)
        {
            for (int i = 0; i < tds.Tables.Count; i++)
            {
                tds.Tables[i].TableName = (i > 0) ? tableName + (i + 1) : "";
                ds.Tables.Add(tds.Tables[i].Copy());
            }

            tds.Dispose();

            return ds;
        }

        #endregion

        #region System.Data.DataTable

        /// <summary>
        /// 테이블 Row를 Dictionary 객체로 변경한다.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        static object RowsToDictionary(this DataTable table)
        {
            var columns = table.Columns.Cast<DataColumn>().ToArray();
            return table.Rows.Cast<DataRow>().Select(r => columns.ToDictionary(c => c.ColumnName, c => r[c]));
        }

        /// <summary>
        /// 테이블을 Dictnoary로 변경한다.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        static Dictionary<string, object> ToDictionary(this DataTable table)
        {
            return new Dictionary<string, object> { { table.TableName, table.RowsToDictionary() } };
        }

        /// <summary>
        /// 테이블의 JSON 스트링을 얻는다.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string GetJSONString(DataTable table)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(table.ToDictionary());
        }

        /// <summary>
        /// 테이블의 JSON 스트링을 얻는다.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string ToJSONString(this DataTable table)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(table.ToDictionary());
        }

        /// <summary>
        /// 테이블을 List로 변경한다.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static List<Dictionary<string, object>> ToList(this DataTable table)
        {
            //return (List<Dictionary<string, object>>)table.RowsToDictionary();
            var columns = table.Columns.Cast<DataColumn>().ToArray();
            var _list = new List<Dictionary<string, object>>();

            foreach (DataRow dr in table.Rows)
            {
                _list.Add(new Dictionary<string, object>());

                foreach (var col in columns)
                {
                    _list[_list.Count - 1][col.ColumnName] = dr[col.ColumnName];
                }
            }

            return _list;

        }
        #endregion

        #region List To DataTable
        public static DataTable ToDataTable<T>(this List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);
            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        #endregion

        #region String

        /// <summary>
        /// Base64로 인코딩 한다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static String EncodeBase64(this String data)
        {
            try
            {
                byte[] encData_byte = new byte[data.Length];
                encData_byte = System.Text.Encoding.UTF8.GetBytes(data);
                string encodedData = System.Convert.ToBase64String(encData_byte);
                return encodedData;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Encode" + e.Message);
            }
        }

        /// <summary>
        /// Base64로 디코딩 한다.
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static String DecodeBase64(this String data)
        {
            try
            {
                System.Text.UTF8Encoding encoder = new System.Text.UTF8Encoding();
                System.Text.Decoder utf8Decode = encoder.GetDecoder();

                byte[] todecode_byte = System.Convert.FromBase64String(data);
                int charCount = utf8Decode.GetCharCount(todecode_byte, 0, todecode_byte.Length);
                char[] decoded_char = new char[charCount];
                utf8Decode.GetChars(todecode_byte, 0, todecode_byte.Length, decoded_char, 0);
                string result = new String(decoded_char);
                return result;
            }
            catch (Exception e)
            {
                throw new Exception("Error in base64Decode" + e.Message);
            }

        }
        /// <summary>
        /// 문자 배열을 Sql 문자열로 변환
        /// </summary>
        /// <param name="arrString"></param>
        /// <returns></returns>
        public static String ConvertToSqlString(this String[] arrString)
        {
            String rtnStr = String.Empty;
            for (int i = 0; i < arrString.Length; i++)
            {
                arrString[i] = "'" + arrString[i].Replace("'", "''") + "'";

                if (rtnStr != String.Empty) rtnStr += ",";
                rtnStr += arrString[i];
            }

            return rtnStr;

        }

        #region 문자열을 숫자로 반환
        /// <summary>
        /// 문자열을 숫자로 반환
        /// (변환할 값이 없거나 오류시 -1 반환)
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static int? ToInt(this string arg)
        {
            int rtnVal = -1;

            if (!String.IsNullOrEmpty(arg))
            {
                int.TryParse(arg.Replace(",", ""), out rtnVal);
            }

            if (rtnVal == -1)
                return null;
            else
                return rtnVal;
        }
        /// <summary>
        /// 문자열을 숫자로 반환
        /// (변환할 값이 없거나 오류시 -1 반환)
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static long? ToLong(this string arg)
        {
            long rtnVal = -1;

            if (!String.IsNullOrEmpty(arg))
            {
                long.TryParse(arg.Replace(",", ""), out rtnVal);
            }

            if (rtnVal == -1)
                return null;
            else
                return rtnVal;
        }
        /// <summary>
        /// 문자열을 숫자로 반환
        /// (변환할 값이 없거나 오류시 -1 반환)
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static decimal? ToDecimal(this string arg)
        {
            decimal rtnVal = -1;

            if (!String.IsNullOrEmpty(arg))
            {
                decimal.TryParse(arg.Replace(",", ""), out rtnVal);
            }

            if (rtnVal == -1)
                return null;
            else
                return rtnVal;
        }
        #endregion

        #region 문자열을 DateTime 형식으로 변환
        /// <summary>
        /// 문자열(일정한 양식이 있는)을 DateTime 형식으로 변환
        /// </summary>
        /// <param name="arg"></param>
        /// <returns></returns>
        public static DateTime? ToDate(this string arg)
        {
            if (!String.IsNullOrEmpty(arg))
            {
                if (arg.Trim().Count() == 8 || arg.Trim().Count() == 10)
                {
                    string year = "";
                    string month = "";
                    string day = "";
                    string predate = "";
                    if (arg.Trim().Count() == 10)
                    {
                        year = arg.Trim().Substring(0, 4);
                        month = arg.Trim().Substring(5, 2);
                        day = arg.Trim().Substring(8, 2);
                        predate = year + "-" + month + "-" + day;
                    }
                    else
                    {
                        year = arg.Trim().Substring(0, 4);
                        month = arg.Trim().Substring(4, 2);
                        day = arg.Trim().Substring(6, 2);
                        predate = year + "-" + month + "-" + day;
                    }

                    return DateTime.Parse(predate);
                }
                else
                    return null;
            }
            else
                return null;
        }
        #endregion

        #region 문자열 포맷
        /// <summary>
        /// string format 적용
        /// format : yyyy-MM-dd, yyyy/MM/dd, ...
        /// </summary>
        /// <param name="val"></param>
        /// <param name="format"></param>
        /// <returns></returns>
        public static string ToString(this object val, string format)
        {
            string rtn = val.ToString();
            if (String.IsNullOrEmpty(rtn) || String.IsNullOrEmpty(format)) return rtn;

            switch (format)
            {
                case "yyyy-MM-dd":
                    if (rtn.Length == 8)
                        rtn = rtn.Substring(0, 4) + "-" + rtn.Substring(4, 2) + "-" + rtn.Substring(6, 2);

                    break;

                case "yyyy/MM/dd":
                    if (rtn.Length == 8)
                        rtn = rtn.Substring(0, 4) + "/" + rtn.Substring(4, 2) + "/" + rtn.Substring(6, 2);

                    break;

                case "hh:mm":
                    if (rtn.Length == 4)
                        rtn = rtn.Substring(0, 2) + ":" + rtn.Substring(3, 2);
                    break;

                case "hh:mm:ss":
                    if (rtn.Length == 6)
                        rtn = rtn.Substring(0, 2) + ":" + rtn.Substring(3, 2) + ":" + rtn.Substring(6, 2);
                    break;

                default:
                    break;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region Newtonsoft.Json.Linq.JObject
        public static DataSet ToDataSet(this Newtonsoft.Json.Linq.JObject objJson)
        {
            DataSet ds = new DataSet();

            // 테이블 루프
            foreach (KeyValuePair<string, JToken> tables in objJson)
            {
                DataTable dt = ds.Tables.Add(tables.Key);

                // Row 루프
                foreach (JObject row in tables.Value)
                {
                    DataRow dr = dt.NewRow();

                    foreach (KeyValuePair<string, JToken> cols in row)
                    {
                        // 컬럼 추가
                        if (dt.Columns[cols.Key] == null) dt.Columns.Add(cols.Key, Type.GetType("System.String"));

                        if (((JValue)cols.Value).Value == null)
                            dr[cols.Key] = System.DBNull.Value;
                        else
                            dr[cols.Key] = ((JValue)cols.Value).Value;

                    }

                    dt.Rows.Add(dr);
                }
            }

            return ds;
        }

        public static DataTable ToDataTable(this JArray objJson)
        {
            DataTable dt = new DataTable();

            // Row 루프
            foreach (JObject row in objJson)
            {
                DataRow dr = dt.NewRow();

                foreach (KeyValuePair<string, JToken> cols in row)
                {
                    // 컬럼 추가
                    if (dt.Columns[cols.Key] == null) dt.Columns.Add(cols.Key, Type.GetType("System.String"));

                    if (((JValue)cols.Value).Value == null)
                        dr[cols.Key] = System.DBNull.Value;
                    else
                        dr[cols.Key] = ((JValue)cols.Value).Value;

                }

                dt.Rows.Add(dr);
            }


            return dt;
        }

        public static Dictionary<string, object> ToDictionary(this JObject objJson)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            // 테이블 루프
            foreach (KeyValuePair<string, JToken> tables in objJson)
            {
                List<object> rowsList = new List<object>();

                // Row 루프
                foreach (JObject row in tables.Value)
                {
                    Dictionary<string, object> colsDic = new Dictionary<string, object>();

                    foreach (KeyValuePair<string, JToken> cols in row)
                    {

                        if (((JValue)cols.Value).Value == null)
                            colsDic.Add(cols.Key, System.DBNull.Value);
                        else
                            colsDic.Add(cols.Key, ((JValue)cols.Value).Value);

                    }

                    rowsList.Add(colsDic);
                }

                dic.Add(tables.Key, rowsList);
            }

            return dic;
        }

        public static Dictionary<string, object> ToDictionaryRow(this JObject objJson)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            foreach (KeyValuePair<string, JToken> cols in objJson)
            {

                if (((JValue)cols.Value).Value == null)
                    dic.Add(cols.Key, System.DBNull.Value);
                else
                    dic.Add(cols.Key, ((JValue)cols.Value).Value);

            }

            return dic;
        }

        public static List<Dictionary<string, object>> ToList(this JArray objJson)
        {
            List<Dictionary<string, object>> _list = new List<Dictionary<string, object>>();

            // Row 루프
            foreach (JObject row in objJson)
            {
                Dictionary<string, object> _listRow = new Dictionary<string, object>();

                foreach (KeyValuePair<string, JToken> cols in row)
                {
                    if (((JValue)cols.Value).Value == null)
                        _listRow[cols.Key] = System.DBNull.Value;
                    else
                        _listRow[cols.Key] = ((JValue)cols.Value).Value;

                }

                _list.Add(_listRow);
            }

            return _list;
        }
        #endregion

        #region Dictionary<string, object>
        public static object Value(this Dictionary<string, object> dic, string key)
        {
            return dic.ContainsKey(key) ? dic[key] : null;
        }
        #endregion

        #region 유저 IP 가져오기
        public static String getUserIP()
        {
            string userIp = "";

            string VisitorsIPAddr = string.Empty;

            if (HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                userIp = VisitorsIPAddr;
            }
            else if (HttpContext.Current.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = HttpContext.Current.Request.UserHostAddress;
                userIp = VisitorsIPAddr;
            }

            return userIp;
        }
        #endregion

        #region 구글 메일 전송
        public static void fnSendGoogleMail(string pSENDER_MAIL, string pSENDER_PW, string pSENDER_NAME, string pRECIEVER, string pTITLE, string pMESSAGE)
        {
            SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
            client.UseDefaultCredentials = false;
            client.EnableSsl = true;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.Credentials = new NetworkCredential(pSENDER_MAIL, pSENDER_PW);

            // 보내는 메일 표시 이름
            MailAddress from = new MailAddress(pSENDER_MAIL, pSENDER_NAME, System.Text.Encoding.UTF8);

            // 받는 사람 메일 주소
            MailAddress to = new MailAddress(pRECIEVER);

            // 메세지 방향
            MailMessage message = new MailMessage(from, to);

            message.Body = pMESSAGE;

            message.BodyEncoding = System.Text.Encoding.UTF8;
            message.IsBodyHtml = true;
            message.Subject = pTITLE;
            message.SubjectEncoding = System.Text.Encoding.UTF8;

            try
            {
                // 동기로 메일을 보낸다.
                client.Send(message);
            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                message.Dispose();
            }
        }
        #endregion


    }
}
