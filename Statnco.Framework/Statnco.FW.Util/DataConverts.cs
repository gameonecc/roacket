﻿using System.Collections.Generic;
using System.Data;
using System.Web.Script.Serialization;

namespace Statnco.FW.Util
{
    public static class DataConverts
    {
        public static object DataRowToJson(this DataRow datarow)
        {
            var dict = new Dictionary<string, object>();
            foreach (DataColumn col in datarow.Table.Columns)
            {
                dict.Add(col.ColumnName, datarow[col]);
            }

            var jsSerializer = new JavaScriptSerializer();

            return new JavaScriptSerializer().DeserializeObject(jsSerializer.Serialize(dict));
        }
    }
}
