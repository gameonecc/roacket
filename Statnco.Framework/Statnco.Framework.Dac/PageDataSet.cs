﻿using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;

namespace Statnco.FW.Dac
{
    public class PageDataSet
    {
        private int? totalCount;
        private int totalCount2;
        DataSet ds;

        //[XmlAttribute(DataType="int")]
        public int? TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; totalCount2 = Convert.ToInt32(value); }
        }

        [XmlAttribute("TotalCount")]
        public int TotalCount2
        {
            get { return totalCount2; }
        }


        [XmlAnyElement]
        public DataSet Ds
        {
            get { return ds; }
            set { ds = value; }
        }

        public PageDataSet()
        {
            ds = new DataSet("NewDataSet");
            totalCount = -1;
        }

        public PageDataSet(DataSet pDs, int? pTotalCount)
        {
            ds = pDs;
            totalCount = pTotalCount;
        }

        /// <summary>
        /// Json Object로 변환
        /// </summary>
        public Dictionary<string, object> ToJSONObject()
        {
            Dictionary<string, object> jsonObj = Ds.ToJSONObject();
            jsonObj.Add("totalCount", TotalCount);
            return jsonObj;
        }
    }
}
