﻿using Statnco.FW.Dac;
using System;
using System.Data;

namespace Statnco.DA.Roacket.Web
{

    public class Roacket_Web_Biz : BizLogicBase
    {
        public Roacket_Web_Biz() : base("roacket_DB", true) { }

        #region 비밀번호변경
        public int EditPassWd(string id, string pw)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Web_DA(DB, DbTrans))
                {
                    rtn = da.EditPassWd(id, pw);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 미성년자 부모 정보 저장
        public int SetMemberParent(int memberIdx, string parentName, string parentGender,
                string parentBirth, string parentPhone
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Web_DA(DB, DbTrans))
                {
                    rtn = da.SetMemberParent(memberIdx, parentName, parentGender, parentBirth, parentPhone);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 로그인
        public DataTable Login(String uid, string password)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    dt = da.Login(uid, password);
                }
            }
            catch (Exception exe)
            {

                throw exe;
            }

            return dt;
        }
        #endregion

        #region 복식해Duo
        public DataSet GetPlayerInfo(int idx)
        {
            DataSet ds = null;


            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    ds = da.GetPlayerInfo(idx);
                }
            }
            catch (Exception e)
            {

                throw e;
            }


            return ds;
        }
        #endregion

        #region 복식할거Gyo
        public int AcceptCompetitionDuo(int aIdx, int ply2)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Web_DA(DB, DbTrans))
                {
                    rtn = da.AcceptCompetitionDuo(aIdx, ply2);
                    CommitTransaction();
                }
            }
            catch (Exception e)
            {
                RollBackTransaction();
                throw e;
            }
            return rtn;
        }
        #endregion

        #region PASS인증

        #region 아이디찾기
        public DataTable ConfirmDI(string DI)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    dt = da.ConfirmDI(DI);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return dt;
        }
        #endregion



        #region 인증번호비교(아이디찾기)
        public DataTable CompareAuthCodeId(string code, string phone)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    dt = da.CompareAuthCodeId(code, phone);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return dt;
        }
        #endregion

        #region 인증번호비교(비밀번호찾기)
        public int CompareAuthCodePw(string id, string code, string phone)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    rtn = da.CompareAuthCodePw(id, code, phone);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 인증번호생성 후 가져오기
        public int CreateAuthCode(string phone)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Web_DA(DB, DbTrans))
                {
                    rtn = da.CreateAuthCode(phone);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 아이디 중복체크
        public int IdCheck(string id)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    rtn = da.IdCheck(id);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;

        }
        #endregion

        #region 아이디 중복체크
        public int NickCheck(string type, string NickName)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    rtn = da.NickCheck(type, NickName);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;

        }
        #endregion

         #region 아이디 중복체크
        public string PASSDICheck(string id)
        { 
           string rtn =null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    rtn = da.PASSDICheck(id);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;

        }
        #endregion

        #region 회원 가입
        public int InsertMemberInfo(
            String uid, String pw, String name,
            String ph, String birth,
            String mtype, int regionCode, int areaCode,
            String jtype, String gender, bool isAss,
            int clubIdx, bool isAuth, String DI
        )
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Web_DA(DB, DbTrans))
                {
                    rtn = da.InsertMemberInfo(uid, pw, name, ph, birth,
                        mtype, regionCode, areaCode, jtype, gender, isAss, clubIdx, isAuth, DI);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 권한 조회 
        public DataTable GetMenuPower(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    dt = da.GetMenuPower(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region 약관
        public DataSet GetTermList()
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Web_DA(DB))
                {
                    ds = da.GetTermList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion
    }
}
