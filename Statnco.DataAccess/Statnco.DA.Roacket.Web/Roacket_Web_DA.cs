﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Statnco.FW.Dac;
using System;
using System.Data;
using System.Data.Common;

namespace Statnco.DA.Roacket.Web
{
    public class Roacket_Web_DA : DataAccessBase
    {
        public Roacket_Web_DA(Database DB) : base(DB) { }

        public Roacket_Web_DA(Database DB, DbTransaction DbTrans) : base(DB, DbTrans) { }

        #region 비밀번호변경
        public int EditPassWd(string id, string pw)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_EDIT_PASSWD_TRX");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, id);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, pw);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 미성년자 부모 정보 저장
        public int SetMemberParent(int memberIdx, string parentName, string parentGender,
                string parentBirth, string parentPhone
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_PARENT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.String, memberIdx);
            DB.AddInParameter(cmd, "@pPARENT_NAME", DbType.String, parentName);
            DB.AddInParameter(cmd, "@pPARENT_GENDER", DbType.String, parentGender);

            DB.AddInParameter(cmd, "@pPARENT_BIRTH", DbType.String, parentBirth);
            DB.AddInParameter(cmd, "@pPARENT_PHONE", DbType.String, parentPhone);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 로그인
        public DataTable Login(string uid, string password)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_LOGIN_NTR");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, password);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 인증번호비교(아이디찾기)
        public DataTable CompareAuthCodeId(string code, string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_COMPARE_ID_NTR");


            DB.AddInParameter(cmd, "@pAUTHCODE", DbType.String, code);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 인증번호비교(비밀번호찾기)
        public int CompareAuthCodePw(string id, string code, string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_COMPARE_PW_NTR");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, id);
            DB.AddInParameter(cmd, "@pAUTHCODE", DbType.String, code);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 아이디찾기
        public DataTable ConfirmDI(string DI)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_DI_NTR");


            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 복식해Duo
        public DataSet GetPlayerInfo(int idx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_PLAYER_INFO_NTR");


            DB.AddInParameter(cmd, "@pIDX", DbType.Int32, idx);


            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 복식할거Gyo
        public int AcceptCompetitionDuo(int aIdx, int ply2)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_ACCEPT_AGREE_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, aIdx);
            DB.AddInParameter(cmd, "@pACCEPT_IDX", DbType.Int32, ply2);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 인증번호생성 후 가져오기
        public int CreateAuthCode(string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_CREATE_TRX");


            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            DB.AddOutParameter(cmd, "@oAUTH_CODE", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oAUTH_CODE"));
            }
            else
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
            }

        }
        #endregion

        #region 아이디 중복체크
        public int IdCheck(string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CHECK_NTR");

            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, id);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);
            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 닉네임 중복체크
        public int NickCheck(string type, string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CHECK_NTR");

            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, id);
            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion


        #region PASS ID 중복체크
        public string PASSDICheck(string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_DI_CHECK_NTR");

            DB.AddInParameter(cmd, "@pPASSDI", DbType.String, id);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0) 
            {
                return "";
            }
            else 
            {
                return "already";
            }
          

        }
        #endregion

        #region 회원 가입
        public int InsertMemberInfo(
            String uid, String pw, String name,
            String ph, String birth,
            String mtype, int regionCode, int areaCode,
            String jtype, String gender, bool isAss,
            int clubIdx, bool isAuth, String DI
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_SIGNUP_TRX");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASS_WD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pUSER_NAME", DbType.String, name);

            DB.AddInParameter(cmd, "@pPHONE", DbType.String, ph);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);

            DB.AddInParameter(cmd, "@pMEMBER_TYPE", DbType.String, mtype);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);

            DB.AddInParameter(cmd, "@pJOIN_TYPE", DbType.String, jtype);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pIS_ASS", DbType.Boolean, isAss);

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pIS_AUTH", DbType.Boolean, isAuth);
            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);


            DB.AddOutParameter(cmd, "@oMEMBER_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {

                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oMEMBER_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion



        #region 메뉴 권한 조회 
        public DataTable GetMenuPower(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MENU_POWER_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 약관
        public DataSet GetTermList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_TERMS_LIST_NTR");

            //DB.AddInParameter(cmd, "@pINDEX", DbType.String, termsIdx);

            return ExecuteDataSet(cmd);
        }
        #endregion
    }
}
