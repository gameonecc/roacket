﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Statnco.FW.Dac;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Statnco.DA.Roacket.Admin
{
    public class Roacket_Admin_DA : DataAccessBase
    {
        public Roacket_Admin_DA(Database DB) : base(DB) { }
        public Roacket_Admin_DA(Database DB, DbTransaction DbTrans) : base(DB, DbTrans) { }

        #region 공용

        #region 리전 코드 조회
        public DataTable GetRegionList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_REGION_LIST");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable GetAreaList(int regionCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_AREA_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.String, regionCode);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 공통 코드 조회
        public DataTable GetComCodeList(string division)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COM_CODE_LIST_NTR");

            DB.AddInParameter(cmd, "@pDIVISION", DbType.String, division);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #endregion

        #region 회원

        #region Admin 로그인
        public DataTable GetAdminLoginInfo(string uid, string password)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_LOGIN_NTR");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pUSER_PW", DbType.String, password);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 아이디 중복 체크
        public int ChkUserId(string userId)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MEMBER_ID_CHECK_NTR");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, userId);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 메뉴 권한 조회 
        public DataTable GetMenuPower(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_POWER_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #endregion

        #region 회원 관리

        #region 회원 리스트 조회

        public DataSet GetMemberList(int regionCode, int areaCode, string isTax, int option, string value, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MEMBER_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pIS_TAX", DbType.String, isTax);
            DB.AddInParameter(cmd, "@pOPTION", DbType.Int32, option);
            DB.AddInParameter(cmd, "@pVALUE", DbType.String, value);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 회원 정보
        public DataTable GetMemberDetail(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MEMBER_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 회원 등급 변경
        public int SetMemberGrade(int memberIdx, byte gradeType, byte gradeCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("ASP_MEMBER_GRADE_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pGRADE_TYPE", DbType.Int32, gradeType);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Int32, gradeCode);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 클럽 관리

        #region 클럽 리스트 조회
        public DataSet GetClubList(int regionCode, int areaCode, string clubName, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_CLUB_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pCLUB_NAME", DbType.String, clubName);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 클럽 회원 리스트 조회
        public DataSet GetClubMemberList(int clubIdx, int option, string value, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_CLUB_MEMBER_LIST_NTR");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pOPTION", DbType.Int32, option);
            DB.AddInParameter(cmd, "@pVALUE", DbType.String, value);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }

        #endregion

        #region 클럽 회원 등급 변경
        public int SetClubMemberGrade(int clubIdx, int memberIdx, int memberGrade, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_CLUB_MEMBER_GRAGE_TRX");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pMEMBER_GRADE", DbType.Int32, memberGrade);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 협회 회비 일괄 납부
        public int SetClubMemberTax(int clubIdx, string memberList, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_CLUB_MEMBER_TAX_TRX");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pMEMBER_LIST", DbType.String, memberList);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 협회 관리 

        #region 협회 리스트 조회
        public DataTable GetAssociationList(int regionCode, int areaCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_ASSOCIATION_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 관리자 리스트 조회
        public DataSet GetManagerList(int option, string value, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MANAGER_LIST_NTR");

            DB.AddInParameter(cmd, "@pOPTION", DbType.Int32, option);
            DB.AddInParameter(cmd, "@pVALUE", DbType.String, value);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 관리자 상세 조회 
        public DataTable GetManagerDetail(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MANAGER_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 관리자 추가/수정
        public int SetManager(int memberIdx, string userId, string userPw, string userName, string gender, string birth, string phone,
            int regionCode, int areaCode, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MANAGER_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, userId);
            DB.AddInParameter(cmd, "@pUSER_PW", DbType.String, userPw);
            DB.AddInParameter(cmd, "@pUSER_NAME", DbType.String, userName);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 관리자 삭제
        public int DelManager(int memberIdx, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MANAGER_DEL_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 협회 삭제
        public int DelAssociation(int associationIdx, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_ASSOCIATION_DEL_TRX");

            DB.AddInParameter(cmd, "@pASS_IDX", DbType.Int32, associationIdx);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 권한관리

        #region 메뉴 모듈 리스트 조회
        public DataTable GetMenuModuleList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_MODULE_LIST_NTR");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 메뉴 모듈 생성
        public int SetMenuModule(string moduleName)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_MODULE_TRX");

            DB.AddInParameter(cmd, "@pMODULE_NAME", DbType.String, moduleName);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 메뉴 모듈 권한 조회
        public DataTable GetMenuModulePower(int moduleIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_MODULE_POWER_NTR");

            DB.AddInParameter(cmd, "@pMODULE_IDX", DbType.Int32, moduleIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 메뉴 모듈 권한 세팅
        public int SetMenuModulePower(int moduleIdx, int menuIdx, int menuSubIdx, string power)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_MODULE_POWER_TRX");

            DB.AddInParameter(cmd, "@pMODULE_IDX", DbType.Int32, moduleIdx);
            DB.AddInParameter(cmd, "@pMENU_IDX", DbType.Int32, menuIdx);
            DB.AddInParameter(cmd, "@pMENU_SUB_IDX", DbType.Int32, menuSubIdx);
            DB.AddInParameter(cmd, "@pPOWER", DbType.String, power);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 메뉴 모듈 유저 복사
        public int SetMenuModuleToMember(int moduleIdx, int memberIdx, int editUser) 
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_MODULE_TO_MEMBER_TRX");

            DB.AddInParameter(cmd, "@pMODULE_IDX", DbType.Int32, moduleIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.String, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 메뉴 권한 저장
        public int SetMenuPower(int memberIdx, int menuIdx, int menuSubIdx, string power)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_MENU_POWER_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pMENU_IDX", DbType.Int32, menuIdx);
            DB.AddInParameter(cmd, "@pMENU_SUB_IDX", DbType.Int32, menuSubIdx);
            DB.AddInParameter(cmd, "@pPOWER", DbType.String, power);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 대회 관리

        #region 대회 리스트 조회
        public DataSet GetCompetitionList(int regionCode, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 대회 상세 조회
        public DataSet GetCompetitionDetail(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 대회 추가/수정
        public int SetCompetition(int competitionIdx, string competitionName, byte typeCode, bool isDivision, 
            DateTime appSrtDate, DateTime appEndDate, DateTime srtDate, DateTime endDate, DateTime listDate,
            int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pCOMPETITION_NAME", DbType.String, competitionName);
            DB.AddInParameter(cmd, "@pTYPE_CODE", DbType.Byte, typeCode);
            DB.AddInParameter(cmd, "@pIS_DIVISION", DbType.Boolean, isDivision);
            DB.AddInParameter(cmd, "@pAPP_SRT_DATE", DbType.DateTime, appSrtDate);
            DB.AddInParameter(cmd, "@pAPP_END_DATE", DbType.DateTime, appEndDate);
            DB.AddInParameter(cmd, "@pSRT_DATE", DbType.DateTime, srtDate);
            DB.AddInParameter(cmd, "@pEND_DATE", DbType.DateTime, endDate);
            DB.AddInParameter(cmd, "@pLIST_DATE", DbType.DateTime, listDate);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oCOMPETITION_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oCOMPETITION_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 대회참가 가능 지역 추가/수정
        public int SetCompetitionAreas(int competitionIdx, DataTable areasDt)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_AREAS_TRX");

            SqlParameter param = new SqlParameter("@pAREAS_TABLE", areasDt);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 지역 추가/수정
        public int SetCompetitionStadiums(int competitionIdx, DataTable stadiumsDt)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_STADIUMS_TRX");

            SqlParameter param = new SqlParameter("@pSTADIUM_TABLE", stadiumsDt);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 규칙 조회
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("ASP_COMPETITION_RULES_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 규칙 추가/수정
        public int SetCompetitionRules(int competitionIdx, bool useLiveScore, string rankRule, 
            byte thirdRankRule, byte giveupRuleCode, byte disqualRuleCode, byte timePerMatch,
            byte subWinGameCnt, byte subWinScore, byte subMaxScore, bool subIsDeuce,
            byte mainWinGameCnt, byte mainWinScore, byte mainMaxScore, bool mainIsDeuce
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_RULES_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pUSE_LIVE_SCORE", DbType.Boolean, useLiveScore);
            DB.AddInParameter(cmd, "@pRANK_RULE", DbType.String, rankRule);

            DB.AddInParameter(cmd, "@pTHIRD_RANK_RULE_CODE", DbType.Byte, thirdRankRule);
            DB.AddInParameter(cmd, "@pGIVE_UP_RULE_CODE", DbType.Byte, giveupRuleCode);
            DB.AddInParameter(cmd, "@pDISQUAL_RULE_CODE", DbType.Byte, disqualRuleCode);
            DB.AddInParameter(cmd, "@pTIME_PER_MATCH", DbType.Byte, timePerMatch);

            DB.AddInParameter(cmd, "@pSUB_WIN_GAME_CNT", DbType.Byte, subWinGameCnt);
            DB.AddInParameter(cmd, "@pSUB_WIN_SCORES", DbType.Byte, subWinScore);
            DB.AddInParameter(cmd, "@pSUB_MAX_SCORES", DbType.Byte, subMaxScore);
            DB.AddInParameter(cmd, "@pSUB_IS_DEUCE", DbType.Boolean, subIsDeuce);

            DB.AddInParameter(cmd, "@pMAIN_WIN_GAME_CNT", DbType.Byte, mainWinGameCnt);
            DB.AddInParameter(cmd, "@pMAIN_WIN_SCORES", DbType.Byte, mainWinScore);
            DB.AddInParameter(cmd, "@pMAIN_MAX_SCORES", DbType.Byte, mainMaxScore);
            DB.AddInParameter(cmd, "@pMAIN_IS_DEUCE", DbType.Boolean, mainIsDeuce);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 그룹 리스트 조회 
        public DataSet GetCompetitionGroupList(int competitionIdx, byte eventCode, byte ageCode, byte gradeCode, int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_GROUP_LIST_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Byte, eventCode);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Byte, ageCode);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Byte, gradeCode);
            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Byte, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 대회 경기장 리스트 조회
        public DataTable GetCompetitionStadiumList(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_STADIUM_LIST_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region 대회 경기장 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx, int stadiumNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_COMPETITION_STADIUM_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, stadiumNo);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #endregion

        #region 기타 관리

        #region 광고 리스트 조회
        public DataSet GetAdvertisementList(int pageNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.ASP_ADVERTISEMENT_LIST_NTR");

            DB.AddInParameter(cmd, "@pPAGE_NO", DbType.Int32, pageNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #endregion
    }
}
