﻿using Statnco.FW.Dac;
using System;
using System.Data;

namespace Statnco.DA.Roacket.Admin
{
    public class Roacket_Admin_Biz : BizLogicBase
    {
        public Roacket_Admin_Biz() : base("roacket_DB", true) { }

        #region 공용

        #region 리전 코드 조회
        public DataTable GetRegionList()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetRegionList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable GetAreaList(int regionCode)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetAreaList(regionCode);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 공통 코드 조회
        public DataTable GetComCodeList(string division)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetComCodeList(division);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region 회원

        #region Admin 로그인
        public DataTable GetAdminLoginInfo(string uid, string password)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetAdminLoginInfo(uid, password);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 아이디 중복 체크
        public int ChkUserId(string userId)
        {
            int rtn = 0;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    rtn = da.ChkUserId(userId);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 권한 조회 
        public DataTable GetMenuPower(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetMenuPower(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region 회원 관리

        #region 회원 리스트 조회

        public DataSet GetMemberList(int regionCode, int areaCode, string isTax, int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetMemberList(regionCode, areaCode, isTax, option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 회원 정보
        public DataTable GetMemberDetail(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetMemberDetail(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 회원 등급 변경
        public int SetMemberGrade(int memberIdx, byte gradeType, byte gradeCode)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetMemberGrade(memberIdx, gradeType, gradeCode);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 클럽 관리

        #region 클럽 리스트 조회
        public DataSet GetClubList(int regionCode, int areaCode, string clubName, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetClubList(regionCode, areaCode, clubName, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 클럽 회원 리스트 조회
        public DataSet GetClubMemberList(int clubIdx, int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetClubMemberList(clubIdx, option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 클럽 회원 등급 변경
        public int SetClubMemberGrade(int clubIdx, int memberIdx, int memberGrade, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetClubMemberGrade(clubIdx, memberIdx, memberGrade, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 협회 회비 일괄 납부
        public int SetClubMemberTax(int clubIdx, string memberList, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetClubMemberTax(clubIdx, memberList, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 협회 관리 

        #region 협회 리스트 조회
        public DataTable GetAssociationList(int regionCode, int areaCode)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetAssociationList(regionCode, areaCode);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 관리자 리스트 조회
        public DataSet GetManagerList(int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetManagerList(option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 관리자 상세 조회 
        public DataTable GetManagerDetail(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetManagerDetail(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 관리자 추가/수정
        public int SetManager(int memberIdx, string userId, string userPw, string userName, string gender, string birth, string phone,
            int regionCode, int areaCode, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans)) 
                {
                    rtn = da.SetManager(memberIdx, userId, userPw, userName, gender, birth, phone, regionCode, areaCode, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 관리자 삭제
        public int DelManager(int memberIdx, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.DelManager(memberIdx, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 협회 삭제

        public int DelAssociation(int associationIdx, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.DelAssociation(associationIdx, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 권한 관리

        #region 메뉴 모듈 리스트 조회
        public DataTable GetMenuModuleList()
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetMenuModuleList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 메뉴 모듈 생성
        public int SetMenuModule(string moduleName)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans)) 
                {
                    rtn = da.SetMenuModule(moduleName);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 모듈 권한 조회
        public DataTable GetMenuModulePower(int moduleIdx)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetMenuModulePower(moduleIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 메뉴 모듈 권한 세팅
        public int SetMenuModulePower(int moduleIdx, int menuIdx, int menuSubIdx, string power)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetMenuModulePower(moduleIdx, menuIdx, menuSubIdx, power);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 모듈 유저 복사
        public int SetMenuModuleToMember(int moduleIdx, int memberIdx, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetMenuModuleToMember(moduleIdx, memberIdx, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 권한 저장
        public int SetMenuPower(int memberIdx, int menuIdx, int menuSubIdx, string power)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans)) 
                {
                    rtn = da.SetMenuPower(memberIdx, menuIdx, menuSubIdx, power);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 대회 관리

        #region 대회 리스트 조회
        public DataSet GetCompetitionList(int regionCode, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetCompetitionList(regionCode, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 대회 상세 조회
        public DataSet GetCompetitionDetail(int competitionIdx)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetCompetitionDetail(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 대회 추가/수정
        public int SetCompetition(int competitionIdx, string competitionName, byte typeCode, bool isDivision,
            DateTime appSrtDate, DateTime appEndDate, DateTime srtDate, DateTime endDate, DateTime listDate,
            int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetition(competitionIdx, competitionName, typeCode, isDivision,
                        appSrtDate, appEndDate, srtDate, endDate, listDate, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회참가 가능 지역 추가/수정
        public int SetCompetitionAreas(int competitionIdx, DataTable areasDt)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionAreas(competitionIdx, areasDt);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 지역 추가/수정
        public int SetCompetitionStadiums(int competitionIdx, DataTable stadiumsDt)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionStadiums(competitionIdx, stadiumsDt);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 규칙 조회
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetCompetitionRules(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 규칙 추가/수정
        public int SetCompetitionRules(int competitionIdx, bool useLiveScore, string rankRule,
            byte thirdRankRule, byte giveupRuleCode, byte disqualRuleCode, byte timePerMatch,
            byte subWinGameCnt, byte subWinScore, byte subMaxScore, bool subIsDeuce,
            byte mainWinGameCnt, byte mainWinScore, byte mainMaxScore, bool mainIsDeuce
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionRules(competitionIdx, useLiveScore, rankRule,
                        thirdRankRule, giveupRuleCode, disqualRuleCode, timePerMatch,
                        subWinGameCnt, subWinScore, subMaxScore, subIsDeuce,
                        mainWinGameCnt, mainWinScore, mainMaxScore, mainIsDeuce
                    );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 그룹 리스트 조회 
        public DataSet GetCompetitionGroupList(int competitionIdx, byte eventCode, byte ageCode, byte gradeCode, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetCompetitionGroupList(competitionIdx, eventCode, ageCode, gradeCode, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 대회 경기장 리스트 조회
        public DataTable GetCompetitionStadiumList(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    dt = da.GetCompetitionStadiumList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 경기장 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx, int stadiumNo)
        {
            DataTable dt = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB, DbTrans))
                {
                    dt = da.GetCompetitionStadiumSchedule(competitionIdx, stadiumNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region 기타 관리

        #region 광고 리스트 조회
        public DataSet GetAdvertisementList(int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var da = new Roacket_Admin_DA(DB))
                {
                    ds = da.GetAdvertisementList(pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #endregion
    }
}
