﻿using Statnco.FW.Dac;
using System;
using System.Data;

namespace Statnco.DA.Roacket.Api
{
    public class Roacket_Api_Biz : BizLogicBase
    {
        public Roacket_Api_Biz() : base("roacket_DB", true) { }

        #region 공통 코드 조회
        public DataTable GetComCodeList(string pDIVISION)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetComCodeList(pDIVISION);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 공통 코드 저장
        public int SetComCode(string division, byte code, string codeName)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetComCode(division, code, codeName);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 리전 코드 조회
        public DataTable fnGetRegionList()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.fnGetRegionList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable fnGetAreaList(int pREGION_CODE)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.fnGetAreaList(pREGION_CODE);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 솔루션

        #region 단식 참가 신청

        public int SetCompetitonAccept(int competitionIdx, int memberIdx, int eventCode, int ageCode, int gradeCode)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitonAccept(competitionIdx, memberIdx, eventCode, ageCode, gradeCode);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 참가 접수 대기(복식)
        public int SetCompetitionAcceptWait(int competitionIdx, int memberIdx, int eventCode, int ageCode, int gradeCode)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionAcceptWait(competitionIdx, memberIdx, eventCode, ageCode, gradeCode);
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 리스트 조회
        public DataSet GetCompetitionList(string pCOMPETITION_NAME, int regionCode, int page, string option)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetCompetitionList(pCOMPETITION_NAME, regionCode, page, option);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 대회 마스터 저장
        public int SetCompetition(
            int competitionIdx, string pNAME, byte pTYPE_CODE, byte pGRADE_CODE,
            DateTime pAPP_SRT_DATE, DateTime pAPP_END_DATE,
            DateTime pSRT_DATE, DateTime pEND_DATE,
            DateTime pLIST_DATE, byte pAPP_TEAMS_CODE, string pLOCATION_CODE_SET,
            string[] pSTADIUM_DATA_SET, int pADMIN_IDX
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {

                    rtn = da.SetCompetition(competitionIdx, pNAME, pTYPE_CODE, pGRADE_CODE, pAPP_SRT_DATE,
                        pAPP_END_DATE, pSRT_DATE, pEND_DATE, pLIST_DATE, pAPP_TEAMS_CODE,
                        pLOCATION_CODE_SET, pADMIN_IDX
                    );


                    if (rtn > 0)
                    {
                        DataTable dt = new DataTable();
                        dt.Columns.Add(new DataColumn("cCOMPETITION_IDX", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_NAME", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_ADDR_1", typeof(string)));
                        dt.Columns.Add(new DataColumn("cSTADIUM_ADDR_2", typeof(string)));

                        for (var i = 0; i < pSTADIUM_DATA_SET.Length; i++)
                        {
                            DataRow dr = dt.NewRow();
                            dr["cCOMPETITION_IDX"] = rtn;
                            dr["cSTADIUM_NAME"] = pSTADIUM_DATA_SET[i].Split('|')[0];
                            dr["cSTADIUM_ADDR_1"] = pSTADIUM_DATA_SET[i].Split('|')[1];
                            dr["cSTADIUM_ADDR_2"] = pSTADIUM_DATA_SET[i].Split('|')[2];

                            dt.Rows.Add(dr);
                        }

                        rtn = da.SetCompetitionStadium(dt);
                    }
                    else if (rtn != -1)
                    {
                        rtn = 0;
                    }


                }
                CommitTransaction();
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 상세 조회 
        public DataTable GetCompetition(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetition(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 룰 저장
        public int SetCompetitionRules(int competitionIdx, int pUSE_LIVE_SCORE,
            string pRANK_RULE, byte pTHIRD_RANK_RULE_CODE,
            byte pSUB_WIN_GAME_CNT, byte pSUB_WIN_SCORES, byte pSUB_MAX_SCORES, byte pSUB_IS_DEUCE,
            byte pMAIN_WIN_GAME_CNT, byte pMAIN_WIN_SCORES, byte pMAIN_MAX_SCORES, byte pMAIN_IS_DEUCE,
            byte pTIME_PER_MATCH
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionRules(competitionIdx, pUSE_LIVE_SCORE,
                            pRANK_RULE, pTHIRD_RANK_RULE_CODE,
                            pSUB_WIN_GAME_CNT, pSUB_WIN_SCORES, pSUB_MAX_SCORES, pSUB_IS_DEUCE,
                            pMAIN_WIN_GAME_CNT, pMAIN_WIN_SCORES, pMAIN_MAX_SCORES, pMAIN_IS_DEUCE,
                            pTIME_PER_MATCH
                        );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 종목 조회 
        public DataSet fnGetCompetitionEventsList(int competitionIdx, int start, int end)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.fnGetCompetitionEventsList(competitionIdx, start, end);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion


        #region 대회 종목 저장
        public int SetCompEvents(int pEVENT_IDX, int competitionIdx, int pORDER,
            byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, byte pAPP_TEAM_CNT,
            int pADMIN_IDX
        )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompEvents(pEVENT_IDX, competitionIdx, pORDER,
                        pEVENT_CODE, pAGE_CODE, pGRADE_CODE, pAPP_TEAM_CNT,
                        pADMIN_IDX
                    );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 대회 종목 삭제
        public int DelCompEvents(int competitionIdx, string pEVENT_IDX_SET, int pADMIN_IDX)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DelCompEvents(competitionIdx, pEVENT_IDX_SET, pADMIN_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 경기장 메인 리스트 조회 
        public DataTable GetCompetitionStadiumMainList(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionStadiumMainList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 경기장 코트 수 저장
        public int SetCompetitionStadiumCourtCnt(int competitionIdx, int pSTADIUM_NO,
                int pCOURT_CNT
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionStadiumCourtCnt(competitionIdx, pSTADIUM_NO, pCOURT_CNT);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionStadiumSchedule(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 저장
        public int SetCompetitionStadiumSchedule(int competitionIdx, int pSTADIUM_NO,
                string pTIME_TYPE, DateTime pTIME
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionStadiumSchedule(competitionIdx, pSTADIUM_NO, pTIME_TYPE, pTIME);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 참가현황 그룹 보기
        public DataTable GetCompetitionGroup(int competitionIdx, byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, int start, int end)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionGroup(competitionIdx, pEVENT_CODE, pAGE_CODE, pGRADE_CODE, start, end);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 종목별 팀리스트 조회
        public DataTable GetCompetitionTeamsDetail(int competitionIdx, int pGROUP_IDX)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionTeamsDetail(competitionIdx, pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 그룹 설정 저장
        public int SetCompetitionGroupConfig(int competitionIdx, int groupIdx,
                byte competitionType, byte leagueGroupCount, byte tournementMinRank, int stadiumNo
            )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionGroupConfig(competitionIdx, groupIdx, competitionType,
                            leagueGroupCount, tournementMinRank, stadiumNo
                        );
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 리그 팀리스트 조회
        public DataTable GetCompetitionLeagueSchedule(int competitionIdx, int pGROUP_IDX)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionLeagueSchedule(competitionIdx, pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 운영 방식 조회 
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.GetCompetitionRules(competitionIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 코트 타임테이블 조회
        public DataTable GetCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCourtSchedule(competitionIdx, stadiumNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 대회 코트 타임테이블 조회(라이브 스코어)
        public DataSet GetLiveScoreCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetLiveScoreCourtSchedule(competitionIdx, stadiumNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 대회 코트 타임테이블 상세 조회
        public DataSet GetCourtScheduleDetail(int competitionIdx, int groupIdx, int matchIdx)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetCourtScheduleDetail(competitionIdx, groupIdx, matchIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 매치 리스트 조회
        public DataTable GetCompetitionMatches(string type, int competitionIdx, int groupIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionMatches(type, competitionIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 매치 시간 세팅
        public int SetMatchTime(int competitionIdx, int groupIdx, int matchIdx, string timeType, DateTime setTime)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetMatchTime(competitionIdx, groupIdx, matchIdx, timeType, setTime);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 매치 스코어 조회
        public DataTable GetCompetitionMatchScore(int competitionIdx, int groupIdx, int matchIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionMatchScore(competitionIdx, groupIdx, matchIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion


        #region 토너먼트 생성
        public int InitMatches(int competitionIdx, int groupIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.InitMatches(competitionIdx, groupIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 배정된 팀끼리 스왑
        public int SwapMatches(int competitionIdx, int groupIdx, int matchIdx1, int matchIdx2, byte team1Num, byte team2Num)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SwapMatches(competitionIdx, groupIdx, matchIdx1, matchIdx2, team1Num, team2Num);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 대회 게시판

        #region 대회게시판 리스트
        public DataTable GetCompetitionBoardsList()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompettionBoardsList();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }
        #endregion

        #region 대회게시판 상세조회
        public DataTable GetCompetitionBoardDetail(int compIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetCompetitionBoardDetail(compIdx);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return dt;
        }

        #endregion

        #region 대회 게시판 새글 생성하기

        public int CreateCompetitionBoard(int competitionIdx,
                                            string boardTitle,
                                            string compType,
                                            string title,
                                            string content,
                                            int memberIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CreateCompetitionBoard(competitionIdx, boardTitle, compType, title, content, memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }

        #endregion


        #region 대회 게시판 글 수정하기

        public int ModifyCompetitionBoard(int compIdx,
                                          string title,
                                          string contents,
                                          DateTime editDate,
                                          byte isActive,
                                          byte isDel,
                                          int memberIdx)
        {

            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.ModifyCompetitionBorad(compIdx, title, contents, editDate,
                                                   isActive, isDel, memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;

        }

        #endregion

        #region 대회게시판 글 삭제하기

        public int DeleteCompetitionBoard(int compIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DelAdvertisement(compIdx);
                    CommitTransaction();
                }

            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }

        #endregion

        #endregion


        #endregion

        #region 검증

        #region 비밀번호 2차확인
        public int CheckPassword(int idx, String password)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.CheckPassword(idx, password);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;
        }
        #endregion

        #region 인증번호생성 후 가져오기
        public int CreateAuthCode(string phone)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CreateAuthCode(phone);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 인증번호비교(아이디찾기)
        public DataTable CompareAuthCodeId(string code, string phone)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.CompareAuthCodeId(code, phone);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return dt;
        }
        #endregion

        #region 인증번호비교(비밀번호찾기)
        public int CompareAuthCodePw(string id, string code, string phone)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.CompareAuthCodePw(id, code, phone);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;
        }
        #endregion





        #region 회원가입(클럽리스트조회)
        public DataTable GetClubList(int regionCode, int areaCode, string search)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetClubList(regionCode, areaCode, search);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 회원검색
        public DataSet GetMemberList(int memberIdx, int associaitionIdx, int si, int gu, int tax, string option, string content, int start, int end, string sort1, string sort2)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetMemberList(memberIdx, associaitionIdx, si, gu, tax, option, content, start, end, sort1, sort2);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return ds;
        }
        #endregion



        #endregion

        #region 회원 관리


        #region 회원 관리
        public DataTable GetMemberDetail(int id)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetMemberDetail(id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 회원 가입
        public int InsertMemberInfo(
            String uid, String pw, String name,
            String ph, String birth,
            String mtype, int regionCode, int areaCode,
            String jtype, String gender, bool isAss,
            int clubIdx, bool isAuth, String DI
        )
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.InsertMemberInfo(uid, pw, name, ph, birth,
                        mtype, regionCode, areaCode, jtype, gender, isAss, clubIdx, isAuth, DI);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 아이디 중복체크
        public int IdCheck(string id)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.IdCheck(id);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;

        }
        #endregion


        #endregion

        #region 아이디 중복체크
        public int NickCheck(string type, string NickName)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.NickCheck(type, NickName);
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            return rtn;

        }
        #endregion

        #region 로그인

        #region 로그인
        public DataSet Login(String uid, string password)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.Login(uid, password);
                }
            }
            catch (Exception exe)
            {

                throw exe;
            }

            return ds;
        }
        #endregion

        #region 광고로그인
        public DataTable AdvertisementLogin(int adverLoginIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.AdvertisementLogin(adverLoginIdx);
                }
            }
            catch (Exception exe)
            {

                throw exe;
            }

            return dt;
        }
        #endregion

        #region Admin 로그인
        public DataSet AdminLogn(string uid, string password)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.AdminLogn(uid, password);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 회원정보 수정
        public int SetMember(int memberIdx, string si, string gu, int editUser)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.SetMember(memberIdx, si, gu, editUser);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 회원 핸드폰 번호 수정
        public int SetMemberPhone(int memberIdx, string phone, string password, int editUser)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.SetMemberPhone(memberIdx, phone, password, editUser);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 회원 비밀 번호 수정
        public int SetMemberPassword(int memberIdx, string checkPw, string newPw, int editUser)
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    rtn = da.SetMemberPassword(memberIdx, checkPw, newPw);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 회원 삭제
        public int DeleteMember(int memberIdx)
        {
            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.DeleteMember(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;

        }
        #endregion



        #endregion

        #region 핸드폰으로 회원번호조회
        public int GetMemberForPhone(string ph)
        {
            int mbIdx = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    mbIdx = da.GetMemberForPhone(ph);
                }
            }
            catch (Exception e)
            {

                throw e;
            }

            return mbIdx;

        }
        #endregion

        #region 게시판 하다말았지

        #region 게시글 조회
        public DataSet GetBoardList(string type, string search, int start, int end)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetBoardList(type, search, start, end);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            return ds;
        }
        #endregion


        #region 게시글 작성
        public int InsertBoard(string type, int id, int clubIdx, string title, string contents, string ph1, string ph2, string ph3, string ph4, string ph5)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.InsertBoard(type, id, clubIdx, title, contents, ph1, ph2, ph3, ph4, ph5);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion


        #region 게시글 상세보기
        public DataSet GetBoardDetail(int bIdx)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetBoardDetail(bIdx);
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return ds;

        }
        #endregion

        #region 게시글 수정
        public int EditBoard(int bIdx, string title, string contents,
                                 string ph1, string ph2, string ph3,
                                 string ph4, string ph5, int editMember)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.EditBoard(bIdx, title, contents, ph1, ph2, ph3, ph4, ph5, editMember);
                    CommitTransaction();
                }
            }
            catch (Exception e)
            {
                RollBackTransaction();
                throw e;
            }
            return rtn;
        }
        #endregion


        #region 게시글 삭제
        public int DeleteBoard(int bIdx, int editMember)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DeleteBoard(bIdx, editMember);
                    CommitTransaction();
                }
            }
            catch (Exception e)
            {
                RollBackTransaction();
                throw e;
            }
            return rtn;
        }
        #endregion
        #endregion


        #region 클럽

        #region 클럽생성
        public int CreateClub(int type, string clubName, string masterName, string nickName, string birth, int clubMaster, int local, int associationId, string gender)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CreateClub(type, clubName, masterName, nickName, birth, clubMaster, local, associationId, gender);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion


        #region 클럽명 중복 확인
        public DataTable CheckClubName(string clubName)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.CheckClubName(clubName);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion


       


        #region 클럽 정보 수정
        public int ModifyClubInfo(int idx, string clubName, int clubMaster, int editUser)
        {
            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.ModifyClubInfo(idx, clubName, clubMaster, editUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion

        #region 클럽 삭제
        public int DeleteClub(int idx, int editUser)
        {
            int dt = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.DeleteClub(idx, editUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return dt;
        }
        #endregion

        #region 클럽 가입안한 회원 클럽 목록 조회
        public DataTable ClubList(int si, int gu, int start, int end, string order, string sort)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.ClubList(si, gu, start, end, order, sort);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 클럽 상세조회
        public DataSet ClubDetail(int clubId)
        {
            DataSet ds = null;
            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.ClubDetail(clubId);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return ds;
        }
        #endregion

        #region 클럽가입신청
        public int ClubJoinReq(int clubIdx, int memberIdx)
        {
            int dt = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.ClubJoinReq(clubIdx, memberIdx);
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 클럽가입 거절
        public int ClubJoinRefuse(int memberIdx, int clubIdx, int user)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.ClubJoinRefuse(memberIdx, clubIdx, user);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 클럽가입대기목록
        public DataSet GetWaitList(int clubIdx)
        {
            DataSet ds = null;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    ds = da.GetWaitList(clubIdx);
                    CommitTransaction();
                }
            }
            catch (Exception e)
            {
                RollBackTransaction();
                throw e;
            }
            return ds;
        }
        #endregion

        #region 클럽회원조회
        public DataSet ClubMemberList(int clubIdx, string opt, string content, int start, int end, string sort1, string sort2)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.ClubMemberList(clubIdx, opt, content, start, end, sort1, sort2);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 클럽 회원 등급 수정
        public int MemberGradeModify(int memberIdx, int clubIdx, int user, int grade)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.MemberGradeModify(memberIdx, clubIdx, user, grade);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 클럽탈퇴
        public int ClubResign(int clubIdx, int memberIdx, int editUser)
        {
            int dt = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.ClubResign(clubIdx, memberIdx, editUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 클럽원정보수정
        public int ClubMemberEdit(int memberIdx, string si, string gu, int editUser)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.ClubMemberEdit(memberIdx, si, gu, editUser);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 클럽장 확인
        public int CheckMaster(int memberIdx, int clubIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CheckMaster(memberIdx, clubIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 회비 수정
        public int MemberTaxEdit(int assIdx, int memberIdx, int tax, int editUser)
        {
            int dt = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    dt = da.MemberTaxEdit(assIdx, memberIdx, tax, editUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion  

        #endregion

        #region 협회

        #region 협회생성
        public int AssociationCreate(int regionCode, int areaCode, int memberIdx, int statnco)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.AssociationCreate(regionCode, areaCode, memberIdx, statnco);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 협회조회
        public DataSet AssociationList(int regionCode, int areaCode)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.AssociationList(regionCode, areaCode);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #region 협회상세조회
        public DataSet AssociationDetail(int id)
        {
            DataSet ds = null;
            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.AssociationDetail(id);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }
            return ds;
        }

        #endregion

        #region 협회 수정
        public int SetAssociation(int assIdx, int id, int statnco)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetAssociation(assIdx, id, statnco);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }

        #endregion

        #region 회원추가
        public int AddMember(int assIdx, int memberIdx, int eidtUser)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.AddMember(assIdx, memberIdx, eidtUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 관리자 수정
        public int SetAssociationMember(int memberIdx, string id, string pw, string name, string gender, string birth, string phone, int associationId)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetAssociationMember(memberIdx, id, pw, name, gender, birth, phone, associationId);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 관리자 상세정보
        public DataSet GetAssociationManagerInfo(int memberIdx)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.GetAssociationManagerInfo(memberIdx);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            return ds;
        }
        #endregion

        #region 관리자 삭제
        public int DelAssociationMember(int memberIdx)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DelAssociationMember(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 협회회원 추가
        public int AddAssociationMember(string signupType, string id, string pass, string name, string gender, string birth, string phone, int regionCode, int areaCode, int assId, int memberIdx)
        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.AddAssociationMember(signupType, id, pass, name, gender,
                                                  birth, phone, regionCode, areaCode,
                                                  assId, memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 관리자 목록
        public DataSet ManagerList(string opt, string content)
        {
            DataSet ds = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    ds = da.ManagerList(opt, content);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return ds;
        }
        #endregion

        #endregion

        #region 메뉴/권한

        #region 메뉴 조회
        public DataTable GetMenuPower(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetMenuPower(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 매치 스코어 저장
        public int SetCompetitionMatchScore(int competitionIdx, int groupIdx,
            int matchIdx, byte setNo, String team1Score1, String team1Score2,
            String team2Score1, String team2Score2, DateTime? setSrtTime,
            DateTime? setEndTime, int shuttleCockCnt)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetCompetitionMatchScore(competitionIdx, groupIdx, matchIdx, setNo, team1Score1, team1Score2, team2Score1, team2Score2, setSrtTime, setEndTime, shuttleCockCnt);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 권한 저장
        public int SetMenuPower(int menuIdx, int menuSubIdx, int memberIdx, string power, int adminIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetMenuPower(menuIdx, menuSubIdx, memberIdx, power, adminIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 권한모듈 생성
        public int CreateMenuPowerModule(string moduleName, int moduleIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CreateMenuPowerModule(moduleName, moduleIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 권한모듈리스트
        public DataTable GetMenuModule()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetMenuModule();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region 약관 관리

        #region 약관 리스트
        public DataTable GetTermsList(int termsIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetTermsList(termsIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 약관 저장/ 수정
        public int SetTerms(int termsIdx, string title, string contents, bool isUse)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetTerms(termsIdx, title, contents, isUse);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 약관 삭제

        public int DelTerms(int termsIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DelTerms(termsIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 광고 관리

        #region 광고 리스트 조회
        public DataTable GetAdvertisementList()
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetAdvertisementList();
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #region 광고 리스트 조회
        public DataTable GetAdvertisementWithLocation(string locationCode)
        {
            DataTable dt = null;
            

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetAdvertisemenWithLocation(locationCode);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion


        #region 광고 추가
        public int SetAdvertisement(string title, string locationCode, string linkUrl,
                                    DateTime srtDate, DateTime endDate, string imgPath
                                )
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetAdvertisement( title, locationCode, linkUrl, srtDate,
                                                endDate, imgPath
                                            );
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 광고 수정
        public int EditAdvertisement(int advIdx, string title, string locationCode, string linkUrl,
                                    DateTime srtDate, DateTime endDate, string imgPath
                                    )
        {
            int rtn = 0;

            try
            {
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.EditAdvertisement(advIdx, title, locationCode, linkUrl, srtDate, endDate, imgPath);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 광고 삭제
        public int DelAdvertisement(int advIdx)
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.DelAdvertisement(advIdx);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #endregion

        #region 앱
        #region 회원가입
        public int InsertAppUser(string id, string pw, string name,
                                 string phone, string gender, string birth,
                                 string DI, int region, int area)

        {
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.InsertAppUser(id, pw, name,
                                           phone, gender, birth,
                                           DI, region,
                                           area);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;

        }
        #endregion

        #region 미성년자 부모 정보 저장
        public int SetMemberParent(string id, string pw, string name,
                                    string phone, string gender, string birth,
                                    string DI, int isAss, int region,
                                    int area, int clubIdx,
                                    string parentName, string parentGender,
                                    string parentBirth, string parentPhone
                                  )
        {
            int rtn = 0;

            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.SetMemberParent(id, pw, name,
                                             phone, gender, birth,
                                             DI, isAss, region,
                                             area, clubIdx,
                                             parentName, parentGender,
                                             parentBirth, parentPhone);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
        }
        #endregion

        #region 마이 롸켓

        #region 마이 롸켓(클럽)
        public DataTable GetMyRoacketClub(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetMyRoacketClub(memberIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion


        #region 마이 롸켓(대회참가 내역)
        public DataTable GetMyRoacketParticipation(int memberIdx, string order, string sort)
        {
            DataTable dt = null;

            try
            {
                using (var da = new Roacket_Api_DA(DB))
                {
                    dt = da.GetMyRoacketParticipation(memberIdx,order,sort);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return dt;
        }
        #endregion

        #endregion

        #region DI중복체크
        public int CheckDI(string DI)
        {
            
            int rtn = 0;
            try
            {
                BeginTransaction();
                using (var da = new Roacket_Api_DA(DB, DbTrans))
                {
                    rtn = da.CheckDI(DI);
                    CommitTransaction();
                }
            }
            catch (Exception exp)
            {
                RollBackTransaction();
                throw exp;
            }

            return rtn;
            
        }
        #endregion

        #endregion
    }
}
