﻿using Microsoft.Practices.EnterpriseLibrary.Data;
using Statnco.FW.Dac;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace Statnco.DA.Roacket.Api
{
    public class Roacket_Api_DA : DataAccessBase
    {
        public Roacket_Api_DA(Database DB) : base(DB) { }

        public Roacket_Api_DA(Database DB, DbTransaction DbTrans) : base(DB, DbTrans) { }

        #region 공통 코드

        #region 공통 코드 조회
        public DataTable GetComCodeList(string pDIVISION)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COM_CODE_LIST_NTR");

            DB.AddInParameter(cmd, "@pDIVISION", DbType.String, pDIVISION);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 공통 코드 저장
        public int SetComCode(string division, byte code, string codeName)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COM_CODE_TRX");

            DB.AddInParameter(cmd, "@division", DbType.String, division);
            DB.AddInParameter(cmd, "@code", DbType.String, code);
            DB.AddInParameter(cmd, "@codeName", DbType.String, codeName);

            DB.AddOutParameter(cmd, "@returnNo", DbType.Int32, 4);

            DB.ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@returnNo"));
        }
        #endregion

        #endregion

        #region 리전 코드 조회
        public DataTable fnGetRegionList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_REGION_LIST");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 지역 코드 조회 
        public DataTable fnGetAreaList(int pREGION_CODE)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_AREA_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.String, pREGION_CODE);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회

        #region 참가 접수
        public int SetCompetitonAccept(int competitionIdx, int memberIdx, int eventCode, int ageCode, int gradeCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_ACCEPT_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Int32, eventCode);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Int32, ageCode);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Int32, gradeCode);

            DB.AddOutParameter(cmd, "@oTEAM_NO", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oTEAM_NO"));
            }
            else
            {
                return -1;
            }

        }
        #endregion

        #region 참가 접수 대기(복식)
        public int SetCompetitionAcceptWait(int competitionIdx, int memberIdx, int eventCode, int ageCode, int gradeCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_ACCEPT_WAIT_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Int32, eventCode);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Int32, ageCode);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Int32, gradeCode);

            DB.AddOutParameter(cmd, "@oACCEPT_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oACCEPT_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 대회 리스트 조회
        public DataSet GetCompetitionList(string pCOMPETITION_NAME,int regionCode, int page, string option)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_LIST_NTR");

            DB.AddInParameter(cmd, "@pCOMP_NAME", DbType.String, pCOMPETITION_NAME);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32,regionCode);
            DB.AddInParameter(cmd, "@pPAGE", DbType.String, page);
            DB.AddInParameter(cmd, "@pCODE", DbType.String, option);

            return ExecuteDataSet(cmd);

        }
        #endregion

        #region 대회 마스터 저장
        public int SetCompetition(
            int competitionIdx,
            string pNAME, byte pTYPE_CODE, byte pGRADE_CODE,
            DateTime pAPP_SRT_DATE, DateTime pAPP_END_DATE,
            DateTime pSRT_DATE, DateTime pEND_DATE,
            DateTime pLIST_DATE, byte pAPP_TEAMS_CODE, string pLOCATION_CODE_SET,
            int pADMIN_IDX
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.String, competitionIdx);
            DB.AddInParameter(cmd, "@pNAME", DbType.String, pNAME);
            DB.AddInParameter(cmd, "@pTYPE_CODE", DbType.Byte, pTYPE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Byte, pGRADE_CODE);
            DB.AddInParameter(cmd, "@pAPP_SRT_DATE", DbType.DateTime, pAPP_SRT_DATE);
            DB.AddInParameter(cmd, "@pAPP_END_DATE", DbType.DateTime, pAPP_END_DATE);
            DB.AddInParameter(cmd, "@pSRT_DATE", DbType.DateTime, pSRT_DATE);
            DB.AddInParameter(cmd, "@pEND_DATE", DbType.DateTime, pEND_DATE);
            DB.AddInParameter(cmd, "@pLIST_DATE", DbType.DateTime, pLIST_DATE);
            DB.AddInParameter(cmd, "@pAPP_TEAMS_CODE", DbType.Byte, pAPP_TEAMS_CODE);
            DB.AddInParameter(cmd, "@pLOCATION_CODE_SET", DbType.String, pLOCATION_CODE_SET);
            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oCOMPETITION_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (DB.GetParameterValue(cmd, "@oRETURN_NO").ToString() == "0")
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oCOMPETITION_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 대회 상세 조회 
        public DataTable GetCompetition(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_NTR");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 장소 저장
        public int SetCompetitionStadium(DataTable pSTADIUM_TABLE)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_TRX");


            SqlParameter param = new SqlParameter("@pSTADIUM_TABLE", pSTADIUM_TABLE);
            param.SqlDbType = SqlDbType.Structured;
            cmd.Parameters.Add(param);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 룰 저장
        public int SetCompetitionRules(int competitionIdx, int pUSE_LIVE_SCORE,
            string pRANK_RULE, byte pTHIRD_RANK_RULE_CODE,
            byte pSUB_WIN_GAME_CNT, byte pSUB_WIN_SCORES, byte pSUB_MAX_SCORES, byte pSUB_IS_DEUCE,
            byte pMAIN_WIN_GAME_CNT, byte pMAIN_WIN_SCORES, byte pMAIN_MAX_SCORES, byte pMAIN_IS_DEUCE,
            byte pTIME_PER_MATCH
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_RULES_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pUSE_LIVE_SCORE", DbType.Boolean, pUSE_LIVE_SCORE);

            DB.AddInParameter(cmd, "@pRANK_RULE", DbType.String, pRANK_RULE);
            DB.AddInParameter(cmd, "@pTHIRD_RANK_RULE_CODE", DbType.Byte, pTHIRD_RANK_RULE_CODE);

            DB.AddInParameter(cmd, "@pSUB_WIN_GAME_CNT", DbType.Byte, pSUB_WIN_GAME_CNT);
            DB.AddInParameter(cmd, "@pSUB_WIN_SCORES", DbType.Byte, pSUB_WIN_SCORES);
            DB.AddInParameter(cmd, "@pSUB_MAX_SCORES", DbType.Byte, pSUB_MAX_SCORES);
            DB.AddInParameter(cmd, "@pSUB_IS_DEUCE", DbType.Boolean, pSUB_IS_DEUCE);

            DB.AddInParameter(cmd, "@pMAIN_WIN_GAME_CNT", DbType.Byte, pMAIN_WIN_GAME_CNT);
            DB.AddInParameter(cmd, "@pMAIN_WIN_SCORES", DbType.Byte, pMAIN_WIN_SCORES);
            DB.AddInParameter(cmd, "@pMAIN_MAX_SCORES", DbType.Byte, pMAIN_MAX_SCORES);
            DB.AddInParameter(cmd, "@pMAIN_IS_DEUCE", DbType.Boolean, pMAIN_IS_DEUCE);

            DB.AddInParameter(cmd, "@pTIME_PER_MATCH", DbType.Byte, pTIME_PER_MATCH);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 종목 조회 
        public DataSet fnGetCompetitionEventsList(int competitionIdx, int start, int end)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_LIST");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 대회 종목 저장
        public int SetCompEvents(int pEVENT_IDX, int competitionIdx, int pORDER,
            byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, byte pAPP_TEAM_CNT,
            int pADMIN_IDX
        )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_TRX");

            DB.AddInParameter(cmd, "@pEVENT_IDX", DbType.Int32, pEVENT_IDX);
            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pORDER", DbType.Int32, pORDER);

            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Int32, pEVENT_CODE);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Int32, pAGE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Int32, pGRADE_CODE);
            DB.AddInParameter(cmd, "@pAPP_TEAM_CNT", DbType.Int32, pAPP_TEAM_CNT);

            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 종목 삭제
        public int DelCompEvents(int competitionIdx, string pEVENT_IDX_SET, int pADMIN_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_EVENT_DEL");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pEVENT_IDX_SET", DbType.String, pEVENT_IDX_SET);
            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.Int32, pADMIN_IDX);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 경기장 메인 리스트 조회 
        public DataTable GetCompetitionStadiumMainList(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_MAIN_LIST");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 경기장 코트 수 저장
        public int SetCompetitionStadiumCourtCnt(int competitionIdx, int pSTADIUM_NO,
                int pCOURT_CNT
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_MAIN_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, pSTADIUM_NO);
            DB.AddInParameter(cmd, "@pCOURT_CNT", DbType.Int32, pCOURT_CNT);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 조회
        public DataTable GetCompetitionStadiumSchedule(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 경기장 별 일자별 스케쥴 저장
        public int SetCompetitionStadiumSchedule(int competitionIdx, int pSTADIUM_NO,
                string pTIME_TYPE, DateTime pTIME
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_STADIUM_SCHEDULE_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, pSTADIUM_NO);
            DB.AddInParameter(cmd, "@pTIME_TYPE", DbType.String, pTIME_TYPE);
            DB.AddInParameter(cmd, "@pTIME", DbType.DateTime, pTIME);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 참가현황 그룹 보기
        public DataTable GetCompetitionGroup(int competitionIdx, byte pEVENT_CODE, byte pAGE_CODE, byte pGRADE_CODE, int start, int end)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_GROUP");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pEVENT_CODE", DbType.Byte, pEVENT_CODE);
            DB.AddInParameter(cmd, "@pAGE_CODE", DbType.Byte, pAGE_CODE);
            DB.AddInParameter(cmd, "@pGRADE_CODE", DbType.Byte, pGRADE_CODE);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 종목별 팀리스트 조회
        public DataTable GetCompetitionTeamsDetail(int competitionIdx, int pGROUP_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_TEAMS_DETAIL");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, pGROUP_IDX);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 그룹 설정 저장
        public int SetCompetitionGroupConfig(int competitionIdx, int groupIdx,
                byte competitionType, byte leagueGroupCount, byte tournementMinRank, int stadiumNo
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_GROUP_CONFIG_TRX");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            DB.AddInParameter(cmd, "@pCOMPETITION_TYPE", DbType.Byte, competitionType);
            DB.AddInParameter(cmd, "@pLEAGUE_GROUP_COUNT", DbType.Byte, leagueGroupCount);
            DB.AddInParameter(cmd, "@pTOURNEMENT_MIN_RANK", DbType.Byte, tournementMinRank);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Int32, stadiumNo);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 리그 팀리스트 조회
        public DataTable GetCompetitionLeagueSchedule(int competitionIdx, int pGROUP_IDX)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_LEAGUE_SCHEDULE");

            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, pGROUP_IDX);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 운영 방식 조회 
        public DataTable GetCompetitionRules(int competitionIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_RULES_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 코트 타임테이블 조회
        public DataTable GetCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_COURT_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Byte, stadiumNo);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 대회 코트 타임테이블 조회(Live Score)
        public DataSet GetLiveScoreCourtSchedule(int competitionIdx, byte stadiumNo)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_LIVESCORE_COURT_SCHEDULE_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pSTADIUM_NO", DbType.Byte, stadiumNo);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 대회 코트 타임테이블 상세 조회
        public DataSet GetCourtScheduleDetail(int competitionIdx, int groupIdx, int matchIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_COURT_SCHEDULE_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX", DbType.Int32, matchIdx);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 매치 리스트 조회
        public DataTable GetCompetitionMatches(string type, int competitionIdx, int groupIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_MATCHES_NTR");
            
            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 매치 시간 세팅
        public int SetMatchTime(int competitionIdx, int groupIdx, int matchIdx, string timeType, DateTime setTime)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_MATCH_TIME_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX", DbType.Int32, matchIdx);
            DB.AddInParameter(cmd, "@pTIME_TYPE", DbType.StringFixedLength, timeType);
            DB.AddInParameter(cmd, "@pSET_TIME", DbType.DateTime, setTime);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 매치 스코어 조회
        public DataTable GetCompetitionMatchScore(int competitionIdx, int groupIdx, int matchIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_MATCH_SCORES_NTR");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX", DbType.Int32, matchIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 매치 스코어 저장
        public int SetCompetitionMatchScore(int competitionIdx, int groupIdx,
            int matchIdx, byte setNo, String team1Score1, String team1Score2,
            String team2Score1, String team2Score2, DateTime? setSrtTime, DateTime? setEndTime, int shuttleCockCnt)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_MATCH_SCORES_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX", DbType.Int32, matchIdx);
            DB.AddInParameter(cmd, "@pSET_NO", DbType.Byte, setNo);
            DB.AddInParameter(cmd, "@pTEAM1_SCORE1", DbType.String, team1Score1);
            DB.AddInParameter(cmd, "@pTEAM1_SCORE2", DbType.String, team1Score2);
            DB.AddInParameter(cmd, "@pTEAM2_SCORE1", DbType.String, team2Score1);
            DB.AddInParameter(cmd, "@pTEAM2_SCORE2", DbType.String, team2Score2);
            DB.AddInParameter(cmd, "@pSET_SRT_TIME", DbType.DateTime, setSrtTime);
            DB.AddInParameter(cmd, "@pSET_END_TIME", DbType.DateTime, setEndTime);
            DB.AddInParameter(cmd, "@pSHUTTLE_COCK_CNT", DbType.Int32, shuttleCockCnt);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 토너먼트 생성
        public int InitMatches(int competitionIdx, int groupIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_TOURNAMENT_INIT_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 배정된 팀끼리 스왑
        public int SwapMatches(int competitionIdx, int groupIdx, int matchIdx1, int matchIdx2, byte team1Num, byte team2Num)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_SWAP_MATCHES_TRX");

            DB.AddInParameter(cmd, "@pCOMPETITION_IDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pGROUP_IDX", DbType.Int32, groupIdx);
            DB.AddInParameter(cmd, "@pMATCH_IDX1", DbType.Int32, matchIdx1);
            DB.AddInParameter(cmd, "@pMATCH_IDX2", DbType.Int32, matchIdx2);
            DB.AddInParameter(cmd, "@pTEAM1_NUM", DbType.Byte, team1Num);
            DB.AddInParameter(cmd, "@pTEAM2_NUM", DbType.Byte, team2Num);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 대회 게시판

        #region 대회 게시판 리스트
        public DataTable GetCompettionBoardsList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_BOARD_LIST");


            return ExecuteDataSet(cmd).Tables[0];
        }

        #endregion


        #region 대회 게시판 상세페이지 조회

        public DataTable GetCompetitionBoardDetail(int compIdx)
        {

            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_BOARD_DETAIL");


            DB.AddInParameter(cmd, "@pCOMPIDX", DbType.Int32, compIdx);


            return ExecuteDataSet(cmd).Tables[0];

        }
        #endregion



        #region 대회 게시판 새글 생성하기

        public int CreateCompetitionBoard(int competitionIdx,
                                            string boardTitle,
                                            string compType,
                                            string title,
                                            string content,
                                            int memberIdx)
        {

            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_COMPETITION_BOARD_CREATE_TRX");


            DB.AddInParameter(cmd, "@pCOMPETITIONIDX", DbType.Int32, competitionIdx);
            DB.AddInParameter(cmd, "@pBOARDTITLE", DbType.String, boardTitle);
            DB.AddInParameter(cmd, "@pCOMPTYPE", DbType.String, compType);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, content);
            DB.AddInParameter(cmd, "@pCOMPETITIONIDX", DbType.Int32, memberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }


        #region 대회 게시판 글 수정하기
        public int ModifyCompetitionBorad(int compIdx,
                                          string title,
                                          string contents,
                                          DateTime editDate,
                                          byte isActive,
                                          byte isDel,
                                          int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_BOARD_EDIT_TRX");


            DB.AddInParameter(cmd, "@pCOMP_IDX", DbType.Int32, compIdx);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
            DB.AddInParameter(cmd, "@pEDIT_DATE", DbType.Int32, editDate);
            DB.AddInParameter(cmd, "@pIS_ACTIVE", DbType.Byte, isActive);
            DB.AddInParameter(cmd, "@pIS_DEL", DbType.Byte, isDel);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.Int32, memberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion


        #region 대회 게시판 글 삭제
        public int DeleteCompetitionBoard(int compIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_COMPETITION_BOARD_DEL_TRX");

            DB.AddInParameter(cmd, "@pCOM_IDX", DbType.Int32, compIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region 회원

        #region 로그인
        public DataSet Login(string uid, string password)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_LOGIN_NTR");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASSWD", DbType.String, password);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 광고 로그인
        public DataTable AdvertisementLogin(int adverLoginIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ADVERTISEMENT_LOGIN_URL_NTR");

            DB.AddInParameter(cmd, "@pADVERLOGIN_IDX", DbType.String, adverLoginIdx);
        

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion


        #region Admin 로그인
        public DataSet AdminLogn(string uid, string password)
        {
            DbCommand cmd = DB.GetStoredProcCommand("ASP_LOGIN_NTR");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pUSER_PW", DbType.String, password);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 검증

        #region 비밀번호 2차확인
        public int CheckPassword(int idx, String password)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_PASS_CHECK_NTR");


            DB.AddInParameter(cmd, "@pIDX", DbType.Int32, idx);
            DB.AddInParameter(cmd, "@pPASS", DbType.String, password);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 인증번호생성 후 가져오기
        public int CreateAuthCode(string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_CREATE_TRX");


            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            DB.AddOutParameter(cmd, "@oAUTH_CODE", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oAUTH_CODE"));
            }
            else
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
            }

        }
        #endregion



        #region 인증번호비교(아이디찾기)
        public DataTable CompareAuthCodeId(string code, string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_COMPARE_ID_NTR");


            DB.AddInParameter(cmd, "@pAUTHCODE", DbType.String, code);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 인증번호비교(비밀번호찾기)
        public int CompareAuthCodePw(string id, string code, string phone)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_COMPARE_PW_NTR");

            DB.AddInParameter(cmd, "@pUSERID", DbType.String, id);
            DB.AddInParameter(cmd, "@pAUTHCODE", DbType.String, code);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region PASS인증

        #region 아이디찾기
        public DataTable ConfirmDI(string DI)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_AUTH_DI_NTR");


            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #endregion

        #endregion

        #region 회원 관리
        public DataTable GetMemberDetail(int id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, id);

            return ExecuteDataSet(cmd).Tables[0];
        }

        #endregion

        #region 회원 조회

        public DataSet GetMemberList(int memberIdx, int associaitionIdx, int si, int gu, int tax, string option, string content, int start, int end, string sort1, string sort2)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_LIST_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pASS_IDX", DbType.Int32, associaitionIdx);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, si);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, gu);
            DB.AddInParameter(cmd, "@pTAX", DbType.Int32, tax);
            DB.AddInParameter(cmd, "@pOPTION", DbType.String, option);
            DB.AddInParameter(cmd, "@pCONTENT", DbType.String, content);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);
            DB.AddInParameter(cmd, "@pORDER", DbType.String, sort1);
            DB.AddInParameter(cmd, "@pSORT", DbType.String, sort2);



            return ExecuteDataSet(cmd);
        }

        #endregion

        #region 회원정보 수정
        public int SetMember(int memberIdx, string si, string gu, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_EDIT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pSI_GRADE", DbType.String, si);
            DB.AddInParameter(cmd, "@pGU_GRADE", DbType.String, gu);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 회원정보 핸드폰 번호 수정
        public int SetMemberPhone(int memberIdx, string phone, string password, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_MEMBER_PHONE_EDIT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pPASSWORD", DbType.String, phone);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, password);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 회원정보 비밀번호 번호 변경
        public int SetMemberPassword(int memberIdx, string checkPw, string newPw)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_PASSWORD_EDIT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pCHECK_PASSWORD", DbType.String, checkPw);
            DB.AddInParameter(cmd, "@pNEW_PASSWORD", DbType.String, newPw);
         

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion




        #region 핸드폰으로 회원번호조회
        public int GetMemberForPhone(string ph)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_INFO_FOR_PHONE_NTR");


            DB.AddInParameter(cmd, "@pPHONE", DbType.String, ph);
            DB.AddOutParameter(cmd, "@oMEMBER_IDX", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oMEMBER_IDX"));


        }
        #endregion

        #region 회원 삭제
        public int DeleteMember(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MEMBER_DEL_TRX");


            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }


        #endregion



        #region 회원 가입
        public int InsertMemberInfo(
            String uid, String pw, String name,
            String ph, String birth,
            String mtype, int regionCode, int areaCode,
            String jtype, String gender, bool isAss,
            int clubIdx, bool isAuth, String DI
            )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_SIGNUP_TRX");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, uid);
            DB.AddInParameter(cmd, "@pPASS_WD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pUSER_NAME", DbType.String, name);

            DB.AddInParameter(cmd, "@pPHONE", DbType.String, ph);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);

            DB.AddInParameter(cmd, "@pMEMBER_TYPE", DbType.String, mtype);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);

            DB.AddInParameter(cmd, "@pJOIN_TYPE", DbType.String, jtype);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pIS_ASS", DbType.Boolean, isAss);

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pIS_AUTH", DbType.Boolean, isAuth);
            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);


            DB.AddOutParameter(cmd, "@oMEMBER_IDX", DbType.Int32, 4);
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            if (Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO")) == 0)
            {
                return Convert.ToInt32(DB.GetParameterValue(cmd, "@oMEMBER_IDX"));
            }
            else
            {
                return -1;
            }
        }
        #endregion

        #region 닉네임 중복체크
        public int NickCheck(string type, string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CHECK_NTR");

            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, id);
            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 회원가입(클럽리스트조회)
        public DataTable GetClubList(int regionCode, int areaCode, string search)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_CLUB_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, search);


            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 회비 수정
        public int MemberTaxEdit(int assIdx, int memberIdx, int tax, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("UPS_TAX_EDIT_TRX");
            DB.AddInParameter(cmd, "@pASS_IDX", DbType.Int32, assIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pTAX", DbType.Int32, tax);
            DB.AddInParameter(cmd, "@@pEDIT_USER", DbType.Int32, editUser);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 게시판

        #region 게시글 조회
        public DataSet GetBoardList(string type, string search, int start, int end)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARD_LIST_NTR");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, search);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);

            return ExecuteDataSet(cmd);

        }
        #endregion

        #region 게시글 작성
        public int InsertBoard(string type, int id, int clubIdx, string title, string contents, string ph1, string ph2, string ph3, string ph4, string ph5)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARD_INSERT_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, id);
            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
            DB.AddInParameter(cmd, "@pPHOTO1", DbType.String, ph1);
            DB.AddInParameter(cmd, "@pPHOTO2", DbType.String, ph2);
            DB.AddInParameter(cmd, "@pPHOTO3", DbType.String, ph3);
            DB.AddInParameter(cmd, "@pPHOTO4", DbType.String, ph4);
            DB.AddInParameter(cmd, "@pPHOTO5", DbType.String, ph5);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }

        #endregion

        #region 게시글 상세보기
        public DataSet GetBoardDetail(int bIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARD_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pBOARD_IDX", DbType.Int32, bIdx);

            return ExecuteDataSet(cmd);

        }
        #endregion

        #region 게시글 수정
        public int EditBoard(int bIdx, string title, string contents,
                                 string ph1, string ph2, string ph3,
                                 string ph4, string ph5, int editMember)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARD_UPDATE_TRX");

            DB.AddInParameter(cmd, "@pBOARD_IDX", DbType.Int32, bIdx);
            DB.AddInParameter(cmd, "@pEDIT_MEMBER", DbType.Int32, editMember);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
            DB.AddInParameter(cmd, "@pPHOTO1", DbType.String, ph1);
            DB.AddInParameter(cmd, "@pPHOTO2", DbType.String, ph2);
            DB.AddInParameter(cmd, "@pPHOTO3", DbType.String, ph3);
            DB.AddInParameter(cmd, "@pPHOTO4", DbType.String, ph4);
            DB.AddInParameter(cmd, "@pPHOTO5", DbType.String, ph5);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 게시글 삭제
        public int DeleteBoard(int bIdx, int editMember)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_BOARD_DELTE_TRX");

            DB.AddInParameter(cmd, "@pBOARD_IDX", DbType.Int32, bIdx);
            DB.AddInParameter(cmd, "@pEDIT_MEMBER", DbType.Int32, editMember);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #endregion

        #region 클럽

        #region 클럽생성
        public int CreateClub(int type, string clubName, string masterName, string nickName, string birth, int clubMaster, int local, int associationId, string gender)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUB_CREATE_TRX");
            //협회idx없으면 0으로 
            DB.AddInParameter(cmd, "@pTYPE", DbType.String, type);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);
            DB.AddInParameter(cmd, "@pMASTERNAME", DbType.String, masterName);
            DB.AddInParameter(cmd, "@pCLUBNICKNAME", DbType.String, nickName);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pCLUBMASTER", DbType.Int32, clubMaster);
            DB.AddInParameter(cmd, "@pCLUBLOCAL", DbType.Int32, local);
            DB.AddInParameter(cmd, "@pASSOCIATIONID", DbType.Int32, associationId);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);



            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽명 중복 확인
        public DataTable CheckClubName(string clubName)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CHECK_CLUBNAME_NTR");

            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

       

        #region 클럽 정보 수정
        public int ModifyClubInfo(int idx, string clubName, int clubMaster, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUB_EDIT_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "u");
            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, idx);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, clubName);
            DB.AddInParameter(cmd, "@pCLUBMASTER", DbType.Int32, clubMaster);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽 삭제
        public int DeleteClub(int idx, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUB_EDIT_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "d");
            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, idx);
            DB.AddInParameter(cmd, "@pCLUBNAME", DbType.String, null);
            DB.AddInParameter(cmd, "@pCLUBMASTER", DbType.Int32, 0);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽 가입안한 회원 클럽 목록 조회
        public DataTable ClubList(int si, int gu, int start, int end, string order, string sort)
        {

            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CLUB_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, si);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, gu);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);
            DB.AddInParameter(cmd, "@pORDER", DbType.String, order);
            DB.AddInParameter(cmd, "@pSORT", DbType.String, sort);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 클럽 상세조회
        public DataSet ClubDetail(int clubId)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CLUB_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubId);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 클럽 멤버조회
        public DataSet ClubMemberDetail(int clubId)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CLUB_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubId);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 클럽 가입신청
        public int ClubJoinReq(int clubIdx, int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUB_JOIN_TRX");

            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, memberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 클럽회원조회
        public DataSet ClubMemberList(int clubIdx, string opt, string content, int start, int end, string sort1, string sort2)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_LIST_NTR");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pOPTION", DbType.String, opt);
            DB.AddInParameter(cmd, "@pCONTENT", DbType.String, content);
            DB.AddInParameter(cmd, "@pSTART", DbType.Int32, start);
            DB.AddInParameter(cmd, "@pEND", DbType.Int32, end);
            DB.AddInParameter(cmd, "@pORDER", DbType.String, sort1);
            DB.AddInParameter(cmd, "@pSORT", DbType.String, sort2);



            return ExecuteDataSet(cmd);

        }
        #endregion

        #region 클럽 회원 등급 수정
        public int MemberGradeModify(int memberIdx, int clubIdx, int user, int grade)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_GRADE_MODIFY_TRX");

            DB.AddInParameter(cmd, "@pMASTER", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pUSER", DbType.Int32, user);
            DB.AddInParameter(cmd, "@pGRADE", DbType.Int32, grade);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 클럽가입 거절
        public int ClubJoinRefuse(int memberIdx, int clubIdx, int user)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_REFUSE_TRX");

            DB.AddInParameter(cmd, "@pMASTER", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pUSER", DbType.Int32, user);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 클럽가입대기목록
        public DataSet GetWaitList(int clubIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_WAIT_LIST_NTR");

            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);
           

            return ExecuteDataSet(cmd);
        }
        #endregion
        
        #region 클럽탈퇴
        public int ClubResign(int clubIdx, int clubMemberIdx, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_RESIGN_TRX");

            DB.AddInParameter(cmd, "@pCLUBIDX", DbType.Int32, clubIdx);
            DB.AddInParameter(cmd, "@pCLUBMEMBERIDX", DbType.Int32, clubMemberIdx);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, editUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽원정보수정
        public int ClubMemberEdit(int memberIdx, string si, string gu, int editUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_EDIT_TRX");
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pSI_GRADE", DbType.String, si);
            DB.AddInParameter(cmd, "@pGU_GRADE", DbType.String, gu);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, editUser);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 클럽장 확인
        public int CheckMaster(int memberIdx, int clubIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_CLUBMEMBER_CHECK_MASTER_NTR");
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 협회

        #region 협회생성
        public int AssociationCreate(int regionCode, int areaCode, int memberIdx, int statnco)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_CREATE_TRX");

            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pMANAGER", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, statnco);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 협회조회(협회관리)
        public DataSet AssociationList(int regionCode, int areaCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIAITON_LIST_NTR");

            DB.AddInParameter(cmd, "@pREGION", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA", DbType.Int32, areaCode);

            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 협회상세조회
        public DataSet AssociationDetail(int id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pASSOCIATIONID", DbType.Int32, id);

            return ExecuteDataSet(cmd);
        }

        #endregion

        #region 협회 수정 (관리자 변경)
        public int SetAssociation(int assIdx, int id, int statnco)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_EDIT_TRX");

            DB.AddInParameter(cmd, "@pASS_IDX", DbType.Int32, assIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, id);
            DB.AddInParameter(cmd, "@pEDIT_USER", DbType.Int32, statnco);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }

        #endregion

        #region 회원추가
        public int AddMember(int assIdx, int memberIdx, int eidtUser)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_CREATE_TRX");

            DB.AddInParameter(cmd, "@pASSIDX", DbType.Int32, assIdx);
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, eidtUser);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 관리자 수정
        public int SetAssociationMember(int memberIdx, string id, string pw, string name, string gender, string birth, string phone, int associationId)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_MANAGER_EDIT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, id);
            DB.AddInParameter(cmd, "@pPASS_WORD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pNAME", DbType.String, name);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pASSOCIATION_ID", DbType.Int32, associationId);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 관리자 상세정보
        public DataSet GetAssociationManagerInfo(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_MEMBER_DETAIL_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd);

        }
        #endregion

        #region 관리자 삭제
        public int DelAssociationMember(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_MANAGER_EDIT_TRX");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 협회 회원 추가
        public int AddAssociationMember(string signUpType, string id, string pass, string name, string gender,
                                        string birth, string phone, int regionCode, int areaCode, int assId, int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_MEMBER_ADD_TRX");

            //회원 추가 구분
            if (signUpType == "U") //회원
            {
                DB.AddInParameter(cmd, "@pINSERT_TYPE", DbType.String, "U");
            }
            else //매니저(관리자)
            {
                DB.AddInParameter(cmd, "@pINSERT_TYPE", DbType.String, "A");
            }
            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, id);
            DB.AddInParameter(cmd, "@pPASS_WD", DbType.String, pass);
            DB.AddInParameter(cmd, "@pUSERNAME", DbType.String, name);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pREGION_CODE", DbType.Int32, regionCode);
            DB.AddInParameter(cmd, "@pAREA_CODE", DbType.Int32, areaCode);
            DB.AddInParameter(cmd, "@pASSIDX", DbType.Int32, assId);
            DB.AddInParameter(cmd, "@pEDITUSER", DbType.Int32, memberIdx);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion


        #region 아이디 중복체크
        public int IdCheck(string id)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_CHECK_NTR");

            DB.AddInParameter(cmd, "@pSEARCH", DbType.String, id);


            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);
            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 관리자 목록
        public DataSet ManagerList(string opt, string content)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ASSOCIATION_MANAGER_LIST_NTR");


            DB.AddInParameter(cmd, "@pOPTION", DbType.String, opt);
            DB.AddInParameter(cmd, "@pCONTENT", DbType.String, content);


            return ExecuteDataSet(cmd);
        }
        #endregion

        #region 회원정보 수정
        public int ModifyMemberInfo(int memberIdx, String userNickName, String phone, String sido, String sigungu, int assosication, int modiMemberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_MEMBER_MODIFY_TRX");

            DB.AddInParameter(cmd, "@pTYPE", DbType.String, "u");
            DB.AddInParameter(cmd, "@pMEMBERIDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pUSERNICKNAME", DbType.String, userNickName);
            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pSIDO", DbType.String, sido);
            DB.AddInParameter(cmd, "@pSIGUNGU", DbType.String, sigungu);
            DB.AddInParameter(cmd, "@pASSOCIATION", DbType.Int32, assosication);
            DB.AddInParameter(cmd, "@pMODMEMBER", DbType.Int32, modiMemberIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion



        #region 약관 관리

        #region 약관 리스트
        public DataTable GetTermsList(int termsIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_TERMS_LIST_NTR");

            DB.AddInParameter(cmd, "@pINDEX", DbType.Int32, termsIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 약관 저장/ 수정
        public int SetTerms(int termsIdx, string title, string contents, bool isUse)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_TERMS_TRX");

            DB.AddInParameter(cmd, "@pTERM_IDX", DbType.Int32, termsIdx);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pCONTENTS", DbType.String, contents);
            DB.AddInParameter(cmd, "@pIS_USE", DbType.Boolean, isUse);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 약관 삭제

        public int DelTerms(int termsIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_TERMS_TRX");

            DB.AddInParameter(cmd, "@pTERM_IDX", DbType.Int32, termsIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 광고 관리

        #region 광고 리스트 조회
        public DataTable GetAdvertisementList()
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ADVERTISEMENT_LIST_NTR");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 위치 정보로 광고 조회
        public DataTable GetAdvertisemenWithLocation(string locationCode)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ADVERTISEMENT_WITH_LOCATION");

            DB.AddInParameter(cmd, "@pLOCATION_CODE", DbType.String, locationCode);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion



        #region 광고 추가
        public int SetAdvertisement(string title, string locationCode, string linkUrl, 
                                    DateTime srtDate, DateTime endDate, string imgPath
                                    )
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ADVERTISEMENT_CREATE_TRX");

            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pLOCATION_CODE", DbType.String, locationCode);
            DB.AddInParameter(cmd, "@pLINK_URL", DbType.String, linkUrl);
            DB.AddInParameter(cmd, "@pSRT_DATE", DbType.DateTime, srtDate);
            DB.AddInParameter(cmd, "@pEND_DATE", DbType.DateTime, endDate);
            DB.AddInParameter(cmd, "@pIMG_PATH", DbType.String, imgPath);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 광고 수정
        public int EditAdvertisement(int advIdx, string title, string locationCode, string linkUrl,
                                    DateTime srtDate, DateTime endDate, string imgPath
                                    )
        {
            DbCommand cmd = DB.GetStoredProcCommand("ASP_ADVERTISEMENT_EDIT_TRX");

            DB.AddInParameter(cmd, "@pADV_IDX", DbType.Int32, advIdx);
            DB.AddInParameter(cmd, "@pTITLE", DbType.String, title);
            DB.AddInParameter(cmd, "@pLOCATION_CODE", DbType.String, locationCode);
            DB.AddInParameter(cmd, "@pLINK_URL", DbType.String, linkUrl);
            DB.AddInParameter(cmd, "@pSRT_DATE", DbType.DateTime, srtDate);
            DB.AddInParameter(cmd, "@pEND_DATE", DbType.DateTime, endDate);
            DB.AddInParameter(cmd, "@pIMG_PATH", DbType.String, imgPath);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 광고 삭제
        public int DelAdvertisement(int advIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_ADVERTISEMENT_DEL_TRX");

            DB.AddInParameter(cmd, "@pADV_IDX", DbType.Int32, advIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #endregion

        #region 메뉴/권한

        #region 메뉴 권한 저장
        public int SetMenuPower(int menuIdx, int menuSubIdx, int memberIdx, string power, int adminIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MENU_POWER_TRX");

            DB.AddInParameter(cmd, "@pMENU_IDX", DbType.Int32, menuIdx);
            DB.AddInParameter(cmd, "@pMENU_SUB_IDX", DbType.Int32, menuSubIdx);
            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.String, memberIdx);
            DB.AddInParameter(cmd, "@pPOWER", DbType.Int32, power);
            DB.AddInParameter(cmd, "@pADMIN_IDX", DbType.String, adminIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 메뉴 조회
        public DataTable GetMenuPower(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MENU_POWER_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 권한모듈 생성
        public int CreateMenuPowerModule(string moduleName, int moduleIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MENU_MODULE_POWER_TRX");
            DB.AddInParameter(cmd, "@pMODULE_NAME", DbType.String, moduleName);
            DB.AddInParameter(cmd, "@pMODULE_IDX", DbType.Int32, moduleIdx);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 권한모듈리스트
        public DataTable GetMenuModule()
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MENU_MODULE_LIST_NTR");

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #endregion

        #region 앱
        #region 회원가입
        public int InsertAppUser(string id, string pw, string name,
                                 string phone, string gender, string birth,
                                 string DI, int region, int area)

        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_USER_JOIN_TRX");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, id);
            DB.AddInParameter(cmd, "@pPASS_WD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pNAME", DbType.String, name);

            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);

            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);

            DB.AddInParameter(cmd, "@pREGION", DbType.Int32, region);
            DB.AddInParameter(cmd, "@pAREA", DbType.Int32, area);
            

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);
            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
        }
        #endregion

        #region 미성년자 부모 정보 저장
        public int SetMemberParent(string id, string pw, string name,
                                    string phone, string gender, string birth,
                                    string DI, int isAss, int region,
                                    int area, int clubIdx,
                                    string parentName, string parentGender,
                                    string parentBirth, string parentPhone
                                  )
        {
            DbCommand cmd = DB.GetStoredProcCommand("dbo.USP_USER_PARENT_TRX");

            DB.AddInParameter(cmd, "@pUSER_ID", DbType.String, id);
            DB.AddInParameter(cmd, "@pPASS_WD", DbType.String, pw);
            DB.AddInParameter(cmd, "@pNAME", DbType.String, name);

            DB.AddInParameter(cmd, "@pPHONE", DbType.String, phone);
            DB.AddInParameter(cmd, "@pGENDER", DbType.String, gender);
            DB.AddInParameter(cmd, "@pBIRTH", DbType.String, birth);

            DB.AddInParameter(cmd, "@pDI", DbType.String, DI);
            DB.AddInParameter(cmd, "@pISASS", DbType.Int32, isAss);
            DB.AddInParameter(cmd, "@pREGION", DbType.Int32, region);

            DB.AddInParameter(cmd, "@pAREA", DbType.Int32, area);
            DB.AddInParameter(cmd, "@pCLUB_IDX", DbType.Int32, clubIdx);


            DB.AddInParameter(cmd, "@pPARENT_NAME", DbType.String, parentName);
            DB.AddInParameter(cmd, "@pPARENT_GENDER", DbType.String, parentGender);

            DB.AddInParameter(cmd, "@pPARENT_BIRTH", DbType.String, parentBirth);
            DB.AddInParameter(cmd, "@pPARENT_PHONE", DbType.String, parentPhone);

            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);


            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));

        }
        #endregion

        #region 마이 롸켓

        #region 마이 롸켓(클럽)
        public DataTable GetMyRoacketClub(int memberIdx)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MY_ROACKET_CLUB_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion

        #region 마이 롸켓(대회참가 내역)
        public DataTable GetMyRoacketParticipation(int memberIdx, string order, string sort)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_MY_ROACKET_PARTICIPATION_NTR");

            DB.AddInParameter(cmd, "@pMEMBER_IDX", DbType.Int32, memberIdx);
            DB.AddInParameter(cmd, "@pORDER", DbType.String, order);
            DB.AddInParameter(cmd, "@pSORT", DbType.String, sort);

            return ExecuteDataSet(cmd).Tables[0];
        }
        #endregion




        #endregion

        #region DI중복체크
        public int CheckDI(string DI)
        {
            DbCommand cmd = DB.GetStoredProcCommand("USP_USER_DI_CHECK_NTR");

            DB.AddInParameter(cmd, "@pDI", DbType.Int32, DI);
        
            DB.AddOutParameter(cmd, "@oRETURN_NO", DbType.Int32, 4);

            ExecuteNonQuery(cmd);

            return Convert.ToInt32(DB.GetParameterValue(cmd, "@oRETURN_NO"));
            
        }
        #endregion

        #endregion
    }
}
