﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="testAdmin.index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <script src="js/bootstrap.bundle.js"></script>
    <script src="js/bootstrap.js"></script>
    
    
    <title>Test Admin</title>
    <script>
        function fnChk() {
            if ($("#txtId").val() == "" || $("#txtPw").val() == "") {
                alert("아이디와 비밀번호 입력");
                return false;
            }

            return true;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
      
                    <img src="/img/roacket_logo.png" />
      
                    <a class="navbar-brand" id="logoText">Roacket Admin Test</a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="form-group">
              <label for="txtId">Id</label>
              <asp:TextBox CssClass="form-control input-lg" ID="txtId" runat="server"></asp:TextBox>
            </div>
            <div class="form-group" style="margin-top:20px;">
              <label for="password">Password</label>
              <asp:TextBox ID="txtPw" runat="server" CssClass="form-control input-lg" ></asp:TextBox>
            </div>
            <div class="form-group" style="text-align:right;margin-top:20px;">
                <asp:Button runat="server" ID="btnLogin" OnClientClick="return fnChk();" OnClick="btnLogin_Click" Text="Login" CssClass="btn btn-success" />
            </div>
        </div>
    </form>
</body>
</html>
