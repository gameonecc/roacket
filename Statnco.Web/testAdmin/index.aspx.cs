﻿using Statnco.DA.Roacket.Api;
using Statnco.DA.Roacket.Web;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace testAdmin
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            // 로그인
            DataSet ds = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    ds = biz.AdminLogn(txtId.Text, Crypto.fnGetSHA512(txtPw.Text));
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    // 로그인 성공

                    DataRow drMemberInfo = ds.Tables[0].Rows[0];
                    Response.Cookies["memberIdx"].Value = drMemberInfo["memberIdx"].ToString();
                    Response.Cookies["regionCode"].Value = drMemberInfo["regionCode"].ToString();
                    Response.Cookies["areaCode"].Value = drMemberInfo["areaCode"].ToString();
                    Response.Cookies["assIdx"].Value = drMemberInfo["associationId"].ToString();
                }
            }
            catch (Exception exe)
            {

                throw exe;
            }

            Response.Redirect("/main.aspx");
        }
    }
}