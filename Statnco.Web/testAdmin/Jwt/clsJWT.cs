﻿using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace testAdmin
{
    public static class clsJWT
    {
        static string plainTextSecurityKey = "0123456789abcdefghijklmnopqrstuvwxyz";
        static InMemorySymmetricSecurityKey signingKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes(plainTextSecurityKey));
        static SigningCredentials signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);
        static int lifetimeMinutes = 43200; //30일

       public static string createToken(int memberIdx, string memberId)
       {
           var claimsIdentity = new ClaimsIdentity(new List<Claim>()
           {
               new Claim("idx", memberIdx.ToString()),
               new Claim("Id", memberId)
           }, "UserInfo");

           var securityTokenDescriptor = new SecurityTokenDescriptor()
           {
               AppliesToAddress = "http://localhost:10003",
               TokenIssuerName = "Roacket",
               Subject = claimsIdentity,
               SigningCredentials = signingCredentials,
           };

            var tokenHandler = new JwtSecurityTokenHandler();
           tokenHandler.TokenLifetimeInMinutes = lifetimeMinutes;
           var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
           var signedAndEncodeToken = tokenHandler.WriteToken(plainToken);

           return signedAndEncodeToken;
       }

        public static JwtSecurityToken isValidToken(string signedAndEncodedToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudiences = new string[]
                {
                "http://localhost:10003",
                },
                ValidIssuers = new string[]
                {
                "Roacket"
                },
                IssuerSigningKey = signingKey
            };

            SecurityToken validatedToken;

            tokenHandler.ValidateToken(signedAndEncodedToken,
                tokenValidationParameters, out validatedToken);

            return validatedToken as JwtSecurityToken;
        }

        public static JwtSecurityToken readToken(string signedAndEncodedToken)
        {

            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            validatedToken = tokenHandler.ReadToken(signedAndEncodedToken);

            return validatedToken as JwtSecurityToken;

        }
    }
}