﻿var regionCode;
var areaCode;
var assIdx;

$(document).ready(function () {
    regionCode = getCookie("regionCode");
    areaCode = getCookie("areaCode");

    fnGetRegion();
});

function fnGetRegion() {
    $.ajax({
        type: "GET",
        url: "http://192.168.0.2:10002/api/Region",
        dataType: "json",
        success: function (result) {
            for (var i = 0; i < result.data.length; i++) {
                if (areaCode == result.data[i]["regionCode"]) {
                    $("#selRegion").append(
                        String.format(
                            "<option value=\"{0}\">{1}</option>",
                            result.data[i]["regionCode"],
                            result.data[i]["regionName"]
                        )
                    );
                }
            }
            
            fnGetAreaList();
        },
        error: function (a, b, c) {
            return 0;
        }
    });
}

function fnGetAreaList() {
    var regionCode = $("#selRegion").val();

    $.ajax({
        type: "GET",
        url: "http://192.168.0.2:10002/api/Area/" + regionCode,
        dataType: "json",
        success: function (result) {
            $("#selArea").empty();

            for (var i = 0; i < result.data.length; i++) {
                if (areaCode == result.data[i]["areaCode"]) {
                    $("#selArea").append(
                        String.format(
                            "<option value=\"{0}\">{1}</option>",
                            result.data[i]["areaCode"],
                            result.data[i]["areaName"]
                        )
                    );
                }
            }
        },
        error: function (a, b, c) {
            return 0;
        }
    });
}

function fnGetMemberList() {
    var url = String.format(
        "http://192.168.0.2:10002/api/Member?associationIdx={0}&regionCode={1}&areaCode={2}&option={3}&tax={4}&search={5}",
        getCookie("assIdx"),
        $("#selRegion").val(),
        $("#selArea").val(),
        $("#selOption").val(),
        $("#selTax").val(),
        $("#txtSearch").val()
    );

    $.ajax({
        type: "GET",
        url: url,
        dataType: "json",
        success: function (result) {
            $("#selArea").empty();

            for (var i = 0; i < result.data.length; i++) {
                if (areaCode == result.data[i]["areaCode"]) {
                    $("#selArea").append(
                        String.format(
                            "<option value=\"{0}\">{1}</option>",
                            result.data[i]["areaCode"],
                            result.data[i]["areaName"]
                        )
                    );
                }
            }
        },
        error: function (a, b, c) {
            return 0;
        }
    });
}