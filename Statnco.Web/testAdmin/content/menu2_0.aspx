﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="menu2_0.aspx.cs" Inherits="testAdmin.content.menu2_0" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        th{
            border:1px solid grey;
            text-align:center;
            vertical-align:middle;
        }
    </style>
    <script src="menu2_0.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div>
        <label for="selRegion">지역</label>
        <select id="selRegion" onchange="fnGetAreaList();">

        </select>

        <label for="selArea">세부지역</label>
        <select id="selArea">

        </select>

        <label for="selTax">납부상태</label>
        <select id="selTax">
            <option value="0">전체</option>
            <option value="1">납부</option>
            <option value="-1">미납</option>
        </select>
        <select id="selOption">
            <option value="0">클럽명</option>
            <option value="1">이름</option>
            <option value="2">아이디</option>
            <option value="3">핸드폰</option>
        </select>
        <input type="text" id="txtSearch" />
        <input type="button" class="btn btn-danger" onclick="fnGetMemberList();" value="검색" />
    </div>
    <br />
    <table id="tblMember" class="table" style="width:80%;">
        <tr>
            <th rowspan="2">
                <input type="checkbox" />
            </th>
            <th rowspan="2">아이디</th>
            <th rowspan="2">이름</th>
            <th rowspan="2">성별</th>
            <th rowspan="2">휴대폰</th>
            <th rowspan="2">소속클럽</th>
            <th colspan="3">시도</th>
            <th colspan="3">시군구</th>
        </tr>
        <tr>
            <th>소속협회</th>
            <th>급수</th>
            <th>회비납부</th>
            <th>소속협회</th>
            <th>급수</th>
            <th>회비납부</th>
        </tr>
    </table>
    
</asp:Content>
