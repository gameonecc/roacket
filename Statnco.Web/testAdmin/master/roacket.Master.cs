﻿using Statnco.DA.Roacket.Web;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace testAdmin.master
{
    public partial class roacket : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.Cookies["memberIdx"] != null)
            {
                int memberIdx = Convert.ToInt32(Request.Cookies["memberIdx"].Value);

                DataTable menuDt = null;

                using(var biz = new Roacket_Web_Biz())
                {
                    menuDt = biz.GetMenuPower(memberIdx);
                }

                fnSetMenu(menuDt);
            }
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Cookies["memberIdx"].Expires = DateTime.Now.AddDays(-1);

            Response.Redirect("/");
        }

        protected void fnSetMenu(DataTable menuDt)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<li class=\"nav-item\">");
            sb.Append("	<a href=\"{0}\" class=\"nav-link text-white\" aria-current=\"page\">");
            sb.Append("	{1} </a>");
            sb.Append("</li>");

            StringBuilder menuSb = new StringBuilder();

            foreach(DataRow dr in menuDt.Rows)
            {
                menuSb.Append(
                    String.Format(
                        sb.ToString(),
                        String.Format("/content/menu{0}_{1}.aspx", dr["menuIdx"], dr["menuSubIdx"]),
                        dr["menuName"]
                    )
                );
            }

            ulMenu.InnerHtml = menuSb.ToString();
        }
    }
}