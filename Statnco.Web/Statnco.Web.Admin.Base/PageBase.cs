﻿using System;
using System.IO;
using System.Web.UI;

namespace Statnco.Web.Admin.Base
{
    public class PageBase : Page
    {
        protected override void OnPreInit(EventArgs e)
        {
            base.OnPreInit(e);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            ClientScriptManager csm = Page.ClientScript;

            // PAGE 공용 스크립트 추가
            if (File.Exists(Server.MapPath(Request.Path.ToLower().Replace(".aspx", ".js"))))
                csm.RegisterStartupScript(this.GetType(), "PageScript", "<script language=\"javascript\" " +
                    "src=\"" + Request.Path.Replace(".aspx", ".js") + "\"><script>\n");

        }
    }
}