﻿namespace Statnco.App.Racket.API.Models
{
    public class Login
    {
        public string id { get; set; }
        public string pw { get; set; }
    }
}