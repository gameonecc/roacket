﻿using Statnco.DA.Roacket.Web;
using Statnco.FW.Util;
using System;

namespace Statnco.Web.Main
{
    public partial class _double : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string secret = Request["agree"].ToString();
            string apc = Crypto.Base64Decode(secret);
            string[] bpc = apc.Split(new string[] { "statnco" }, StringSplitOptions.None);

            int memberIdx = Convert.ToInt32(bpc[0].Split('_')[0]);
            string compName = bpc[0].Split('_')[1];
            string acceptNumb = bpc[0].Split('_')[2];

            Response.Cookies["memberIdx"].Value = memberIdx.ToString();

            hiddenCompName.Value = compName;
            hiddenIdx.Value = Response.Cookies["memberIdx"].Value;
            hiddenAcceptNumb.Value = acceptNumb;
        }

        protected void acceptBtn_Click(object sender, EventArgs e)
        {
            int acceptNum = Convert.ToInt32(hiddenAcceptNumb.Value);
            int player2 = Convert.ToInt32(hiddenPlayer2.Value);
            int rtn = 0;
            Response.Cookies["memberIdx"].Value = player2.ToString();
            try
            {
                using (var biz = new Roacket_Web_Biz())
                {
                    rtn = biz.AcceptCompetitionDuo(acceptNum, player2);
                }

                if (rtn == 0)
                {
                    Response.Redirect("/index.aspx");
                }
            }
            catch (Exception exe)
            {
                throw exe;
            }
            Response.Redirect("/index.aspx");
        }
    }
}