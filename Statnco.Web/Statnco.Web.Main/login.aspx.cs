﻿using Statnco.DA.Roacket.Web;
using Statnco.FW.Util;
using System;
using System.Data;

namespace Statnco.Web.Main
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["memberIdx"] == null)
            {
            }
        }

        protected void login_submit_Click(object sender, EventArgs e)
        {

            var uid = userId.Text;
            var password = Crypto.fnGetSHA512(userPw.Text);

            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Web_Biz())
                {
                    dt = biz.Login(uid, password);
                }

                if (dt.Rows.Count > 0)
                {
                    Response.Cookies["memberIdx"].Value = dt.Rows[0]["id"].ToString();
                }
                else
                {
                    Response.Redirect("/login.aspx");
                }
            }
            catch (Exception exe)
            {

                throw exe;
            }

            Response.Redirect("/loginCom.aspx");
        }
    }
}