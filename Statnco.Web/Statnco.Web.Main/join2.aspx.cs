﻿using Statnco.DA.Roacket.Web;
using Statnco.FW.Util;
using System;

namespace Statnco.Web.Main
{
    public partial class join2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            isChild.Value = Request["joinType"];

        }
        protected void join_Click(object sender, EventArgs e)
        {
            var rtn = 0;
            int clubIdx = string.IsNullOrEmpty(hidClubIdx.Value) ? 0 : Convert.ToInt32(hidClubIdx.Value);

            var child = isChild.Value.ToString();

            string gender = string.Equals(passGender.Value, "남") ? "M" : "F";

            try
            {
                int regionCode = 0;
                int areaCode = 0;

                // 라디오 처리
                if (rdoIsAss1.Checked)
                {
                    // 협회가 있어요
                    regionCode = Convert.ToInt32(Request["selRegion2"]);
                    areaCode = Convert.ToInt32(Request["selArea2"]);
                }
                else
                {
                    // 협회가 없어요
                    regionCode = Convert.ToInt32(Request["selRegion1"]);
                    areaCode = Convert.ToInt32(Request["selArea1"]);

                }




                using (var biz = new Roacket_Web_Biz())
                {

                    if (child == "2")
                    {
                        rtn = biz.InsertMemberInfo(
                        txtId.Value,
                        Crypto.fnGetSHA512(txtPw.Value),
                        txtName.Value,
                        hidPhone.Value,
                        String.Format(
                            "{0}-{1}-{2}",
                            txtYear.Value,
                            txtMonth.Value,
                            txtDay.Value
                        ),
                        "U", // 고정
                        regionCode,
                        areaCode,
                        "E", // 고정
                        gender,
                        rdoIsAss1.Checked,
                        Convert.ToInt32(clubIdx),
                        true,
                        hidDI.Value
                         );
                    }
                    else
                    {
                        rtn = biz.InsertMemberInfo(
                                txtId.Value,
                                Crypto.fnGetSHA512(txtPw.Value),
                                txtName.Value,
                                txtPhone.Value,
                                String.Format(
                                    "{0}-{1}-{2}",
                                    txtYear.Value,
                                    txtMonth.Value,
                                    txtDay.Value
                                ),
                                "U", // 고정
                                regionCode,
                                areaCode,
                                "E", // 고정
                                hiddenGender.Value,
                                rdoIsAss1.Checked,
                                Convert.ToInt32(clubIdx),
                                true,
                                hidDI.Value
                         );
                        rtn = biz.SetMemberParent(
                           rtn,
                           txtParentName.Value,
                           Request["selParentGender"],
                           String.Format(
                               "{0}-{1}-{2}",
                               txtParentYear.Value,
                               txtParentMonth.Value,
                               txtParentDay.Value
                           ),
                           txtParentMobile.Value
                       );
                    }



                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            if (rtn == -1)
            {
                Response.Write("<script>alert('" + rtn + "')</script>");
            }

            Response.Redirect("/join3.aspx");
        }

    }
}