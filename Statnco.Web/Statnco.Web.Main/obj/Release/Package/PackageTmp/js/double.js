﻿$(function () {
    var compName1 = $("#hiddenCompName").val();
    var text1 = "<br/>신청수락 페이지입니다.<br/>로그인을 하시면 참가신청이 완료됩니다.";
    $("#txt_compName1").html(compName1 + text1);
    
    var text2 = " 조의<br/>참가를 신청합니다.";
    $("#txt_compName2").html(compName1 + text2);
});

function login_btn() {
    var id = $("#txt_id").val();
    var pw = $("#txt_pw").val();
    if (id == "") {
        $("#txt_id").focus();
    } else if (pw == "") {
        $("#txt_pw").focus();
    } else {
        var fnCallback = function (data) {

            var rtn = data.d;

            if (rtn != 0) {
                
                var idx = $("#hiddenIdx").val();
                $("#hiddenPlayer2").val(rtn);
                memberInfo1(idx); //신청자
                memberInfo2(rtn); //신청받은자
                
                $("#ssoDoublesModal").modal("show");
            }
        }
        var param = {
            "id": id,
            "passwd" : pw
        };

        window.fnAjaxSend("/ws/ws.asmx/GetUserInfo", param, fnCallback);
    }
  

}



function findId() {
    location.href = "/findId.aspx";
}

function findPw() {
    location.href = "/findPw.aspx";
}

function join() {
    location.href = "/join.aspx";
}


//회원이름, 클럽정보 가져오기
function memberInfo1(param) {
    var idx = param;
    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var txt = table[0]["userName"];
        txt += " (";
        txt += table[0]["clubName"] == null ? "없음" : table[0]["clubName"];
        txt += ")";
           
        $("#player1").text(txt);
    }
    var param = {
        "idx": idx,
    };
    window.fnAjaxSend("/ws/ws.asmx/GetMbCl", param, fnCallback);
}

function memberInfo2(param) {
    var idx = param;
    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var txt = table[0]["userName"];
        txt += " (";
        txt += table[0]["clubName"] == null ? "없음" : table[0]["clubName"];
        txt += ")";

        $("#player2").text(txt);
    }
    var param = {
        "idx": idx,
    };
    window.fnAjaxSend("/ws/ws.asmx/GetMbCl", param, fnCallback);
}