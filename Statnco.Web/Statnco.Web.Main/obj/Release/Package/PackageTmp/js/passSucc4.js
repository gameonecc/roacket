﻿function exit() {

    change();
   

    window.close();
}

function change() {
    $("#hidName").val($("#hidName").val());
    $("#hidBirth", opener.document).val($("#hidBirth").val());
    $("#hidGender", opener.document).val($("#hidGender").val());
    $("#hidDI", opener.document).val($("#hidDI").val());
    $("#hidPhone", opener.document).val($("#hidPhone").val());

    $("#selMonth", opener.document).hide();
    $("#selDay", opener.document).hide();
 

    $("#txtParentName", opener.document).val($("#hidName").val()).attr('readonly', true);
    $("#txtParentGender", opener.document).val($("#hidGender").val() == '1' ? '남' : '여').attr('readonly', true).show();

    //생일나누기
    var birth = $("#hidBirth", opener.document).val();
    $("#txtParentYear", opener.document).val(birth.substring(0, 4)).attr('readonly', true);
    $("#txtParentMonth", opener.document).val(birth.substring(4, 6)).attr('readonly', true).show();
    $("#txtParentDay", opener.document).val(birth.substring(6, 8)).attr('readonly', true).show();

    $("#txtParentMobile", opener.document).val($("#hidPhone").val()).attr('readonly', true);

}