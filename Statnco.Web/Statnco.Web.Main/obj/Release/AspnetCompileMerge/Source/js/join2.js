﻿// 세부지역 전역변수
var jsonArea = {};

// 아이디 중복 체크 
var isChkId = false;

// 본인인증 체크
var isChkSelf = false;

// 미성년자 여부
var isChild = false;

// 닉네임 중복 체크
var isChkNickName = false;

var flag = false;
//소속클럽의 협회 구분
var association = 0;

$(document).ready(function () {
    

    isChild = $("#isChild").val() == "1";
   
    
    if (isChild) {
        $(".teen_input_box").show();
        $("#btnPass1").hide();
        $("#btnPass2").show();
        $("#txtName").attr('readonly', false);
    } else {
        $(".teen_input_box").hide();
        $("#btnPass2").hide();
        $("#btnPass1").show();
    }


    $("#txtMonth").hide();
    $("#txtDay").hide();
    $("#passGender").hide();
    // 일자 세팅
    fnSetDaySelect();

    $("#input_content_search").attr('placeholder', '소속클럽검색하기');
    
    //지역리스트 가지고 오기
    GetRegionList();
    test();
    $("#acticalClub").show();
    $(".assign_none").hide();

});
function passAuth() {
    if (isChild) {
    window.open("/pass/pass.aspx?findType=child", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
    } else {
    window.open("/pass/pass.aspx?findType=join", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');

    }

}


function fnSetDaySelect() {
    $("#selMonth").empty();
    $("#selParentMonth").empty();

    $("#selMonth").append('<option value="">월</option>');
    $("#selParentMonth").append('<option value="">월</option>');

    for (var i = 1; i < 13; i++) {
        $("#selMonth").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
        $("#selParentMonth").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
    }



    $("#selDay").empty();
    $("#selParentDay").empty();
   
    $("#selDay").append('<option value="">일</option>');
    $("#selParentDay").append('<option value=""></option>');

    for (var i = 1; i < 32; i++) {
        $("#selDay").append('<option value="' + lpad(i.toString(),2,"0") + '">' + i + '</option>');
        $("#selParentDay").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
    }
}

// 테스트 데이터
function test() {
    
    $("#txtId").val("test12345");
    fnCheckId();

    fnRegExpId();

    $("#txtPw").val("a12345678");
    $("#txtPw2").val("a12345678");
    fnChkPw();

   



}

//아이디 유효성검사
function fnRegExpId() {
    isChkId = false;


    var regExp = /^[a-z0-9]{6,20}$/g;

    if (regExp.test($("#txtId").val())) {
        // 유효성 검사 성공시 중복 확인 버튼 활성화
        $("#btnCheckId").attr("disabled", false).addClass("btn_green");
    }
    else {
        fnSetWarning("Id", "6~20자 영문, 숫자를 사용하세요.", false);

        // 유효성 검사 실패시 중복 확인 버튼 비활성화
        $("#btnCheckId").attr("disabled", true).removeClass("btn_green");
    }
}

function fnSetWarning(conName, text, isSuccess) {
    if (isSuccess) {
        $("#warning" + conName).text(text).attr("class", "succ_text");
    }
    else {
        $("#warning" + conName).text(text).attr("class", "fail_text");
    }
    
}

//아이디 중복체크
function fnCheckId() {
    var fnAfter = function (data) {
        var rtn = data.d;
          
        if (rtn == '0') {
            isChkId = false;
            fnSetWarning("Id", "이미 사용중이거나 탈퇴한 아이디입니다.", false);
        } else {
            isChkId = true;
            fnSetWarning("Id", "멋진 아이디입니다!.", true);
        }

    }
    var pData = {
        id: $("#txtId").val()
    };

    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/CheckId", pData, fnAfter);

}

//비밀번호 같은지 체크
function fnChkPw() {

    // 비밀번호 유효성 검사
    var regExp = /^[a-z0-9]{6,20}$/g;

    var pw = $("#txtPw").val();
    var pw2 = $("#txtPw2").val();

    if (!regExp.test(pw)) {
        fnSetWarning("Pw", "6~20자 영문,숫자를 사용하세요.", false);
    } else {
        $("#warningPw").text('');
    }

    if (pw != pw2) {
        fnSetWarning("Pw2", "비밀번호가 일치 하지 않습니다.", false);
    } else {
        $("#warningPw2").text('');
    }
};

// 지역 코드 조회
function GetRegionList() {
    $('#selRegion').children('option:not(:first)').remove();
    $('#selRegion1').children('option:not(:first)').remove();
    $('#selRegion2').children('option:not(:first)').remove();
    $('#selArea2').children('option:not(:first)').remove();
    
    var fnAfter = function (data) {
        var table = JSON.parse(data.d).Table;

        var option = "<option value=\"{0}\">{1}</option>";
        
        for (var i = 0; i < table.length; i++) {
            if (table[i]["regionName"] == '전국') {
                continue;
            }

            // 시군구협회 지역
            $("#selRegion1").append(
                String.format(
                    option,
                    table[i]["regionCode"],
                    table[i]["regionName"]
                )
            );

            // 시군구협회 세부지역
            $("#selRegion2").append(
                String.format(
                    option,
                    table[i]["regionCode"],
                    table[i]["regionName"]
                )
            );
        }
    }
   
    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/GetRegionList", {}, fnAfter);
}

//세부지역코드
function GetAreaList(regionIdx) {
    $("#warningNoAss").text("");

    var table;
    var RegionCode = $("#selRegion" + regionIdx).val();
    var area = "selArea" + regionIdx;
   
    $("#" + area).empty();

    var option = "<option value=\"{0}\">{1}</option>";

    if (jsonArea.hasOwnProperty(RegionCode)) {
        table = jsonArea[RegionCode];

        for (var i = 0; i < table.length; i++) {
            if (table[i]["areaName"] == '전체') {
                continue;
            }

            $("#" + area).append(
                String.format(
                    option,
                    table[i]["areaCode"],
                    table[i]["areaName"]
                )
            );
        }
    }
    else {

        var pData = {
            pREGION_CODE: RegionCode
        }
        var fnAfter = function (data) {
            table = JSON.parse(data.d).Table;

            jsonArea[RegionCode] = table;
                     

            for (var i = 0; i < table.length; i++) {
               $("#" + area).append(
                    String.format(
                        option,
                        table[i]["areaCode"],
                        table[i]["areaName"]
                    )
                );
            }
        }

        window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/GetAreaList", pData, fnAfter);
    }
}

//// 생년 체크
//function fnChkYear() {
//    if ($("#txtYear").val().length == 4) {
//        var nowYear = new Date().getFullYear();

//        if (nowYear - $("#txtYear").val() <= 14) {
//            $(".teen_input_box").show();

//            isChild = true;
//        }
//        else {
//            $(".teen_input_box").hide();

//            isChild = false;
//        }
//    }
//    else {
//        $(".teen_input_box").hide();

//        isChild = false;
//    }

    
//}

// 협회 소속 여부 선택 변경시
function rdoIsAssChange() {
    if ($("input[name='rdoIsAss']:checked").val() == "1") {
        // 있다면
        $(".assign_yes").show();

        $(".assign_none").hide();

        $("#clubCheck").show();
    }
    else {
        // 없다면
        $(".assign_yes").hide();

        $(".assign_none").show();

        $("#clubCheck").hide();

    }
}


// 클럽 검색창 오픈
function fnOpenSearchClub() {
    $("#autocomplete_items").empty();
    $(".del_icon").hide();
    $("#txtClubName").val("");

    if ($("input[name='rdoIsAss']:checked").val() == "0") {
        // 소속 협회가 없어요
        if ($("#selRegion1").val() == "") {
            fnSetWarning("NoAss", "소속 시/도는 필수값입니다.", false);
            return;
        }
    }

    $("#ssoSearchModal").modal('show');
}

// 클럽명 체크
function fnChkClubName() {
    if ($("#txtClubName").val() == "") {
        $(".del_icon").hide();
    }
    else {
        $(".del_icon").show();
    }
}

// 클럽명 삭제 아이콘 클릭시
function fnResetClubName() {
    $(".del_icon").hide();
    $("#txtClubName").val("");
}

//클럽검색
function fnClubSerach() {
    var pData = {};
    var rdoAssIdx = $("input[name='rdoAss']:checked").val();
   
    // 소속협회가 없어요
    if ($("input[name='rdoIsAss']:checked").val() == "0") {
        pData = {
            regionCode: $("#selRegion1").val(),
            areaCode: $("#selArea1").val(),
            search: $("#txtClubName").val()
        };
    }
    else {
        // 소속 협회가 있어요
        if (rdoAssIdx == 0) {
            pData = {
                regionCode: $("#selRegion").val(),
                areaCode: 0,
                search: $("#txtClubName").val()
            };
        }
        else {
            pData = {
                regionCode: $("#selRegion2").val(),
                areaCode: $("#selArea2").val(),
                search: $("#txtClubName").val()
            };
        }
    }

    var fnAfter = function (data) {
        var table = JSON.parse(data.d).Table;

        var pattern = "<div onclick='fnSelectClub(this, {0});' value=\"{0}\">{1}</div>";
        $("#autocomplete_items").empty();
        for (var i = 0; i < table.length ; i++) {
            $("#autocomplete_items").append(
                String.format(
                    pattern,
                    table[i]["clubIdx"],
                    table[i]["clubName"]
                )
            );
        }
    }
    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/GetClubList", pData, fnAfter);
    
}


//클럽선택시
function fnSelectClub(con, clubIdx) {
    $("#hidClubIdx").val(clubIdx);
    $("#txtClub").val($(con).text());
    $("#ssoSearchModal").modal('hide');
}

//마지막 체크
function fnBeforeSubmit() {
    // 아이디 체크
    if (!isChkId) {
        alert("아이디 중복체크를 진행하세요.");
        $("#txtId").focus();
        return false;
    }

 
    return true;
}

// 완료 팝업
function fnPopSuccess() {
    $("#ssoCertiModal").modal('show');
}