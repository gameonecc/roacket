﻿$(document).ready(function () {
    $("#check2").css('font-weight', '900').css('background-color', '#fbf5dd').css('border-radius', '5px');
    $("#check1").css('font-weight', '500').css('background-color', '#fff').css('border', '0');
    $("#txtPw").show();
    $("#ssoPwBtn").css("background-color", "#B8B9B9").attr("disabled", "disabled");
    $("#nextBtn").attr("disabled","disabled");
    option(2);
    

});

function findId() {
    location.href = "/findId.aspx";
}

function regExpId() {
    // 아이디 유효성 검사
    var regExp = /^[a-z0-9]{6,20}$/g;
    var id = $("#txtId").val();
    if (regExp.test(id) == false) {
        $("#warningId").text("아이디(영문+숫자, 6~20자리)").css("font-size", "10px").css("color","#cc2c8c");
    } else {
        $("#warningId").empty();
    }
}

function option(con) {
    if (con == 1) {
        $("#option" + con).hide();
        $("#option" + 2).show();
        $("#ssoPwBtn").hide();
    } else if (con == 2) {
        $("#option" + con).hide();
        $("#option" + 1).show();
        $("#ssoPwBtn").show();

    }

}




function inquireNum() {
    var fnAfter = function (data) {
        var rtn = data.d;

        if (rtn == '-2') {
            alert("허용 인증 횟수를 초과하였습니다.");
        }

        // 버튼 되돌리기
        $("#btnConfirm").css("background-color", "#009f59").removeAttr("disabled");

        $("#txtAuthCode").focus();
        TimerStart();
    }
    var pData = {
      
        "phone": $("#txtPhone").val()
    };


    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/AuthPhone", pData, fnAfter);
}


    var SetTime = 120;      // 최초 설정 시간(기본 : 120초)
function msg_time() {   // 1초씩 카운트      
    m = Math.floor(SetTime / 60) + ": " + lpad((SetTime % 60).toString(), 2, '0'); // 남은 시간 계산
    
        $(".num_time").html(m);     // div 영역에 보여줌
        SetTime--;                  // 1초씩 감소
        if (SetTime < 0) {          // 시간이 종료 되었으면..        
            clearInterval(tid);     // 타이머 해제
           // alert("종료");
            $(".num_time").empty();
            $("#btnConfirm").css("background-color", "#B8B9B9").attr("disabled", "disabled");
        }
}
function TimerStart() { tid = setInterval('msg_time()', 1000) };


function confirm() {
    var id = $("#txtId").val();
    var phone = $("#txtPhone").val();
    var authCode = $("#txtAuthCode").val();

    var fnAfter = function (data) {
        var rtn = data.d;
        
        if (rtn == 0) {
            $("#btnConfirm").css("background-color", "#B8B9B9").attr("disabled", "disabled");
            $("#ssoPwBtn").css("background-color", "#009f59");
            $("#ssoPwBtn").removeAttr("disabled");
            alert("인증되었습니다");
        } else if (rtn == -2) {  //가입된 아이디가 없는 경우
            alert("오류");
            $("#txtId").focus();
        } else {
            alert("만료된 인증번호입니다.");
        }

    }
    var pData = {
        "id" : id,
        "phone": phone,
        "code" : authCode
    };

    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/CompareAuthCodePw", pData, fnAfter);
}

function regPw() {
    var regExp = /^[a-z0-9]{6,20}$/g;
    var pw = $("#txtNewPw1").val();
    if (regExp.test(pw) == false) {
        $("#warningPw").text("아이디(영문+숫자, 6~20자리)").css("font-size", "10px").css("color", "#cc2c8c");
    } else {
        $("#warningPw").empty();
    }

    checkPw();
}

function checkPw() {
    var pw1 = $("#txtNewPw1").val();
    var pw2 = $("#txtNewPw2").val();
    if (pw1 == pw2) {
        $("#warningPwCheck").empty();
        $("#nextBtn").css("background-color", "#009f59").attr("disabled",false);
    }
    else {
        $("#warningPwCheck").text("달라요").css("font-size", "10px").css("color", "#cc2c8c");
        $("#nextBtn").css("background-color", "#B8B9B9");
        $("#nextBtn").attr("disabled", true);
        

    }


}

function editPw() {
    var id = $("#txtId").val();
    var pw = $("#txtNewPw2").val();

    var fnAfter = function (data) {
        var rtn = data.d;
        if (rtn == 0) {

            //효과효과
            $(".pw_change_box").hide();
            $(".pw_change_ok_box").show();

        } else {
            alert("오류오류");
        }
    }
    var pData = {
        "id": id,
        "passwd" : pw
    };

    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/EditPassWd", pData, fnAfter);

}

function openPass() {
    window.open("/pass/pass.aspx?findType=pw", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
}