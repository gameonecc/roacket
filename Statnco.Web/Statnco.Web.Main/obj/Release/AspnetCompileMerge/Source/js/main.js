$(function() {
    // agree_page : check box
    $('.check_list').on('click', '.check_all input', function() {
        $(this).parents('.check_list').find('input').prop('checked', $(this).is(':checked'));
    });
    $('.check_list').on('click', '.check_btn_small input', function() {
        var is_checked = true;
        $('.check_list .check_btn_small input').each(function() {
            is_checked = is_checked && $(this).is(':checked');
        });
        $('.check_all input').prop('checked', is_checked);
    });

    $("#fail_text").hide();
    $("#fail_text_02").hide();
    $("#succ_text").hide();
    $("#warnningTxt").show();

    

});

// Ajax Json 구현
var fnAjaxSend = function (pUrl, pData, pFunction) {
    var oXML = new XMLHttpRequest();

    oXML.responseJSON = null;
    oXML.open("post", pUrl, true);
    oXML.responseType = "text";
    oXML.setRequestHeader("Content-Type", "application/json");

    oXML.addEventListener("load", function () {
        oXML.responseJSON = JSON.parse(oXML.responseText);
        pFunction(oXML.responseJSON, oXML);
    });

    oXML.send(JSON.stringify(pData));

    return oXML;
};

String.format = function () {
    let args = arguments;
    return args[0].replace(/{(\d+)}/g, function (match, num) {
        num = Number(num) + 1;
        return typeof (args[num]) != undefined ? args[num] : match;
    });
}

/**
 * 좌측문자열채우기
 * @params
 *  - str : 원 문자열
 *  - padLen : 최대 채우고자 하는 길이
 *  - padStr : 채우고자하는 문자(char)
 */
function lpad(str, padLen, padStr) {
    if (padStr.length > padLen) {
        console.log("오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다");
        return str;
    }
    str += ""; // 문자로
    padStr += ""; // 문자로
    while (str.length < padLen)
        str = padStr + str;
    str = str.length >= padLen ? str.substring(0, padLen) : str;
    return str;
}
console.log(lpad("01", 5, "0")); // 00001
console.log(lpad("01", 5, "01")); // 01010


/**
 * 우측문자열채우기
 * @params
 *  - str : 원 문자열
 *  - padLen : 최대 채우고자 하는 길이
 *  - padStr : 채우고자하는 문자(char)
 */
function rpad(str, padLen, padStr) {
    if (padStr.length > padLen) {
        console.log("오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다");
        return str + "";
    }
    str += ""; // 문자로
    padStr += ""; // 문자로
    while (str.length < padLen)
        str += padStr;
    str = str.length >= padLen ? str.substring(0, padLen) : str;
    return str;
}

