﻿$(document).ready(function () {
    $("#check1").css('font-weight', '900').css('background-color', '#fbf5dd').css('border-radius', '5px');
    $("#check2").css('font-weight', '500').css('background-color', '#fff').css('border', '0');
    $("#txtPw").hide();
    $("#ssoPwBtn").css("background-color", "#B8B9B9").attr("disabled", "disabled");
    option(2);
  

});

function findPw() {
    location.href = "/findPw.aspx";
}

function option(con) {
    if (con == 1) {
        $("#option" + con).hide();
        $("#option" + 2).show();
        $("#ssoPwBtn").hide();
    } else if (con == 2) {
        $("#option" + con).hide();
        $("#option" + 1).show();
        $("#ssoPwBtn").show();
    }

}

function fnSetWarning(text, isSuccess) {
    var message = text;
    if (isSuccess == false) {
        alert(message);
    }

}


function inquireNum() {
    var fnAfter = function (data) {
        var rtn = data.d;
  
        if (rtn == '-2') {
            fnSetWarning("허용 인증 횟수를 초과하였습니다.", false);
        }
        else {
        // 버튼 되돌리기
        $("#btnConfirm").css("background-color", "#009f59").removeAttr("disabled");

        $("#txtAuthCode").focus();
        TimerStart();

        }
    }
    var pData = {
      
        "phone": $("#txtPhone").val()
    };

    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/AuthPhone", pData, fnAfter);
}


    var SetTime = 120;      // 최초 설정 시간(기본 : 120초)
function msg_time() {   // 1초씩 카운트      
    m = Math.floor(SetTime / 60) + ": " + lpad((SetTime % 60).toString(), 2, '0'); // 남은 시간 계산
    
        $(".num_time").html(m);     // div 영역에 보여줌
        SetTime--;                  // 1초씩 감소
        if (SetTime < 0) {          // 시간이 종료 되었으면..        
            clearInterval(tid);     // 타이머 해제
           // alert("종료");
            $(".num_time").empty();
            $("#btnConfirm").css("background-color", "#B8B9B9").attr("disabled", "disabled");

        }
}
function TimerStart() { tid = setInterval('msg_time()', 1000) };

function confirm() {
    
    var phone = $("#txtPhone").val();
    var authCode = $("#txtAuthCode").val();

    var fnAfter = function (data) {
       
        var table = JSON.parse(data.d).Table;
        var result = table[0]["result"];
      
        if (result ==1 ) {
            $("#btnConfirm").css("background-color", "#B8B9B9").attr("disabled", "disabled");
            $("#ssoPwBtn").css("background-color", "#009f59");
            $("#ssoPwBtn").removeAttr("disabled");
            alert("인증되었습니다");
            $("#user_id").text(table[0]["userId"]);
        } else if ( result == 2) {
            alert("가입된 정보가 없습니다.");
        }

    }
    var pData = {
        "phone": phone,
        "code": authCode
    };

    window.fnAjaxSend("https://www.roacket.com/ws/ws.asmx/CompareAuthCodeId", pData, fnAfter);
}

function moveFindPw() {
    location.href = "/findPw.aspx?userId=" + $("#user_id").text();
}

function moveLogin() {
    location.href = "/login.aspx";
}

function openPass() {
    window.open("/pass/pass.aspx?findType=id", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
}