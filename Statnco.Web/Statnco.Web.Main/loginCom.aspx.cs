﻿using System;

namespace Statnco.Web.Main
{
    public partial class loginCom : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void logout_Click(object sender, EventArgs e)
        {
            Response.Cookies["memberIdx"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("/index.aspx");

        }

    }
}