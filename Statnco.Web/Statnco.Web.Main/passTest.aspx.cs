﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Statnco.Web.Main
{
    public partial class passTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPCLIENTLib.KisinfoClass clsCPClient = new CPCLIENTLib.KisinfoClass();

            string sSiteCode = "BW459";
            string sSitePw = "ZrYsxKgyUGoj";

            string AuthType = "";

            string popgubun = "N";

            string sGender = "";

            string sRequestNo = "STNC"+DateTime.Now.ToString("yyyyMMddHHmmss");
            int iRtn = clsCPClient.fnRequestNO(sSiteCode);

            if(iRtn == 0)
            {
                sRequestNo = clsCPClient.bstrRandomRequestNO;
                Session["REQ_SEQ"] = sRequestNo;
            }

            string RtnURL = "http://localhost:10003/passSuccess.aspx";
            string ErrURL = "http://localhost:10003/passFail.aspx";

            string sPlainData = "7:REQ_SEQ" + sRequestNo.Length + ":" + sRequestNo +
                            "8:SITECODE" + sSiteCode.Length + ":" + sSiteCode +
                            "9:AUTH_TYPE" + AuthType.Length + ":" + AuthType +
                            "7:RTN_URL" + RtnURL.Length + ":" + RtnURL +
                            "7:ERR_URL" + ErrURL.Length + ":" + ErrURL +
                            "11:POPUP_GUBUN" + popgubun.Length + ":" + popgubun +
                            "6:GENDER" + sGender.Length + ":" + sGender;

            // 데이타 암호화
            int iDataRtn = clsCPClient.fnEncode(sSiteCode, sSitePw, sPlainData);
            
            if(iDataRtn == 0)
            {
                string sEncData = clsCPClient.bstrCipherData;
                lbl_Result.Text = "요청정보_암호화_성공[" + sEncData + "]<br>";

                Response.Write("<form name='form_chk' method='post'>");
                Response.Write("<input type='hidden' name='EncodeData' value='" + sEncData + "'/>");
                Response.Write("<input type='hidden' name='m' value='checkplusService'>");

                Response.Write("</form>");
            }
            else
            {
                lbl_Result.Text = "요청정보_암호화_실패 - 리턴코드 : " + iDataRtn + "<br>";
                /*
                 * -1 : 암호화 시스템 에러입니다.
                 * -4 : 입력 데이터 오류입니다.
                 * -5 : 복호화 해쉬 오류입니다.
                 * -6 : 복호화 데이터 오류입니다.
                 * -9 : 입력 데이터 오류입니다.
                 * -12 : 사이트 패스워드 오류입니다.
                 */
            }
        }
    }
}