﻿using Statnco.DA.Roacket.Api;
using Statnco.DA.Roacket.Web;
using Statnco.FW.Util;
using System;
using System.Data;
using System.Web.Services;
using WpfApp1.AligoSMSSample;

namespace Statnco.Web.Main.ws
{
    /// <summary>
    /// ws의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws : System.Web.Services.WebService
    {

        [WebMethod]
        public string CheckId(string id)
        {
            int rtn = 0;

            using (var biz = new DA.Roacket.Web.Roacket_Web_Biz())
            {
                rtn = biz.IdCheck(id);
            }

            return rtn.ToString();
        }

        [WebMethod]
        public string NickCheck(string NickName)
        {
            int rtn = 0;

            using (var biz = new DA.Roacket.Web.Roacket_Web_Biz())
            {
                rtn = biz.NickCheck("nick", NickName);
            }

            return rtn.ToString();
        }


        [WebMethod]
        public string GetRegionList()
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.fnGetRegionList();
            }

            return Extends.GetJSONString(dt);

        }
        [WebMethod]
        public string GetAreaList(int pREGION_CODE)
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.fnGetAreaList(pREGION_CODE);
            }

            return Extends.GetJSONString(dt);

        }

        [WebMethod]
        public string GetClubList(int regionCode, int areaCode, string search)
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.GetClubList(regionCode, areaCode, search);
            }

            return Extends.GetJSONString(dt);

        }

        [WebMethod]
        public int AuthPhone(string phone)
        {
            Curl_Send cs = new Curl_Send();

            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CreateAuthCode(phone);

                if (rtn > 0)
                {
                    // 성공
                    rtn = cs.Curl_SendEx(rtn.ToString(), phone);
                }
            }
            return rtn;
        }

        [WebMethod]
        public string CompareAuthCodeId(string code, string phone)
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.CompareAuthCodeId(code, phone);
            }

            return Extends.GetJSONString(dt);
        }

        [WebMethod]
        public int CompareAuthCodePw(string id, string code, string phone)
        {
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CompareAuthCodePw(id, code, phone);
            }

            return rtn;
        }

        [WebMethod]
        public int EditPassWd(string id, string passwd)
        {
            string password = Crypto.fnGetSHA512(passwd);

            int rtn = 0;

            using (var biz = new Roacket_Web_Biz())
            {
                rtn = biz.EditPassWd(id, password);
            }

            return rtn;
        }

        [WebMethod]
        public string GetTermList()
        {
            DataSet ds = null;

            using (var biz = new Roacket_Web_Biz())
            {
                ds = biz.GetTermList();
            }

            return Extends.GetJSONString(ds);
        }

        [WebMethod]
        public int GetUserInfo(string id, string passwd)
        {
            DataTable dt = null;
            int rtn = 0;
            using (var biz = new Roacket_Web_Biz())
            {
                dt = biz.Login(id, Crypto.fnGetSHA512(passwd));
            }
            if (dt.Rows.Count == 0)
            {
                rtn = -1;
            }
            rtn = Convert.ToInt32(dt.Rows[0][0].ToString());
            return rtn;
        }

        [WebMethod]
        public string GetMbCl(int idx)
        {
            DataSet ds = null;

            using (var biz = new Roacket_Web_Biz())
            {
                ds = biz.GetPlayerInfo(idx);
            }
            return Extends.GetJSONString(ds);
        }
    }
}
