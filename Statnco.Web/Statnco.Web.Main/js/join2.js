﻿// 세부지역 전역변수
var jsonArea = {};

// 아이디 중복 체크 
var isChkId = false;

// 본인인증 체크
var isChkSelf = false;

// 미성년자 여부
var isChild = false;

// 닉네임 중복 체크
var isChkNickName = false;

// PASS중복 인증여부
var isPASSDI = false;

var flag = false;
//소속클럽의 협회 구분
var association = 0;

$(function () {
    

    isChild = $("#isChild").val() == "1";
   
    
    if (isChild) {
        $(".teen_input_box").show();
        $("#btnPass1").hide();
        $("#btnPass2").show();
        $("#txtName").attr('readonly', false);
    } else {
        $(".teen_input_box").hide();
        $("#btnPass2").hide();
        $("#btnPass1").show();
    }


    $("#txtMonth").hide();
    $("#txtDay").hide();
    $("#passGender").hide();
    // 일자 세팅
    fnSetDaySelect();

    $("#input_content_search").attr('placeholder', '소속클럽검색하기');
    
    //지역리스트 가지고 오기
    GetRegionList();
    //test();
    $("#acticalClub").show();
    $(".assign_none").hide();

});
function passAuth() {
    if (isChild) {
     window.open("/pass/pass.aspx?findType=child", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
    } else {
     window.open("/pass/pass.aspx?findType=join", 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
    }

}


function fnSetDaySelect() {
    $("#selMonth").empty();
    $("#selParentMonth").empty();

    $("#selMonth").append('<option value="">월</option>');
    $("#selParentMonth").append('<option value="">월</option>');

    for (var i = 1; i < 13; i++) {
        $("#selMonth").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
        $("#selParentMonth").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
    }



    $("#selDay").empty();
    $("#selParentDay").empty();
   
    $("#selDay").append('<option value="">일</option>');
    $("#selParentDay").append('<option value=""></option>');

    for (var i = 1; i < 32; i++) {
        $("#selDay").append('<option value="' + lpad(i.toString(),2,"0") + '">' + i + '</option>');
        $("#selParentDay").append('<option value="' + lpad(i.toString(), 2, "0") + '">' + i + '</option>');
    }
}

//아이디 유효성검사
function fnRegExpId() {
    isChkId = false;

    var regExp = /^[a-z0-9]{6,20}$/g;

    if (regExp.test($("#txtId").val())) {
        // 유효성 검사 성공시 중복 확인 버튼 활성화
        $("#btnCheckId").attr("disabled", false).addClass("btn_green");

        $("#warningId").empty();
    }
    else {
        fnSetWarning("Id", "6~20자 영문, 숫자를 사용하세요.", false);

        // 유효성 검사 실패시 중복 확인 버튼 비활성화
        $("#btnCheckId").attr("disabled", true).removeClass("btn_green");
    }
}

function fnSetWarning(conName, text, isSuccess) {
    if (isSuccess) {
        $("#warning" + conName).text(text).attr("class", "succ_text");
    }
    else {
        $("#warning" + conName).text(text).attr("class", "fail_text");
    }
    
}

//아이디 중복체크
function fnCheckId() {
    var fnCallback = function (data) {
        var rtn = data.d;
          
        if (rtn == '0') {
            isChkId = false;
            fnSetWarning("Id", "이미 사용중이거나 탈퇴한 아이디입니다.", false);
        } else {
            isChkId = true;
            fnSetWarning("Id", "멋진 아이디입니다!.", true);
        }

    }
    var param = {
        id: $("#txtId").val()
    };

    window.fnAjaxSend("/ws/ws.asmx/CheckId", param, fnCallback);

}



// 지역 코드 조회
function GetRegionList() {
    $('#selRegion').children('option:not(:first)').remove();
    $('#selRegion1').children('option:not(:first)').remove();
    $('#selRegion2').children('option:not(:first)').remove();
    $('#selArea2').children('option:not(:first)').remove();
    
    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        var option = "<option value=\"{0}\">{1}</option>";
        
        for (var i = 0; i < table.length; i++) {
            if (table[i]["regionName"] == '전국') {
                continue;
            }

            // 시군구협회 지역
            $("#selRegion1").append(
                String.format(
                    option,
                    table[i]["regionCode"],
                    table[i]["regionName"]
                )
            );

            // 시군구협회 세부지역
            $("#selRegion2").append(
                String.format(
                    option,
                    table[i]["regionCode"],
                    table[i]["regionName"]
                )
            );
        }
    }
   
    window.fnAjaxSend("/ws/ws.asmx/GetRegionList", {}, fnCallback);
}

//세부지역코드
function GetAreaList(regionIdx) {
    $("#warningNoAss").text("");

    var table;
    var RegionCode = $("#selRegion" + regionIdx).val();
    var area = "selArea" + regionIdx;
   
    $("#" + area).empty();

    var option = "<option value=\"{0}\">{1}</option>";

    if (jsonArea.hasOwnProperty(RegionCode)) {
        table = jsonArea[RegionCode];

        for (var i = 0; i < table.length; i++) {
            if (table[i]["areaName"] == '전체') {
                continue;
            }

            $("#" + area).append(
                String.format(
                    option,
                    table[i]["areaCode"],
                    table[i]["areaName"]
                )
            );
        }
    }
    else {

        var param = {
            pREGION_CODE: RegionCode
        }
        var fnCallback = function (data) {
            table = JSON.parse(data.d).Table;

            jsonArea[RegionCode] = table;
                     

            for (var i = 0; i < table.length; i++) {
               $("#" + area).append(
                    String.format(
                        option,
                        table[i]["areaCode"],
                        table[i]["areaName"]
                    )
                );
            }
        }

        window.fnAjaxSend("/ws/ws.asmx/GetAreaList", param, fnCallback);
    }
}

//// 생년 체크
//function fnChkYear() {
//    if ($("#txtYear").val().length == 4) {
//        var nowYear = new Date().getFullYear();

//        if (nowYear - $("#txtYear").val() <= 14) {
//            $(".teen_input_box").show();

//            isChild = true;
//        }
//        else {
//            $(".teen_input_box").hide();

//            isChild = false;
//        }
//    }
//    else {
//        $(".teen_input_box").hide();

//        isChild = false;
//    }

    
//}

// 협회 소속 여부 선택 변경시
function rdoIsAssChange() {
    if ($("input[name='rdoIsAss']:checked").val() == "1") {
        // 있다면
        $(".assign_yes").show();

        $(".assign_none").hide();

        $("#clubCheck").show();
    }
    else {
        // 없다면
        $(".assign_yes").hide();

        $(".assign_none").show();

        $("#clubCheck").hide();

    }
}


// 클럽 검색창 오픈
function fnOpenSearchClub() {
    $("#autocomplete_items").empty();
    $(".del_icon").hide();
    $("#txtClubName").val("");

    if ($("input[name='rdoIsAss']:checked").val() == "0") {
        // 소속 협회가 없어요
        if ($("#selRegion1").val() == "") {
            fnSetWarning("NoAss", "소속 시/도는 필수값입니다.", false);
            return;
        }
    }
    if ($("#selRegion2").val() != "") {
        $("#ssoSearchModal").modal('show');
    }
}

// 클럽명 체크
function fnChkClubName() {
    if ($("#txtClubName").val() == "") {
        $(".del_icon").hide();
    }
    else {
        $(".del_icon").show();
    }
}

// 클럽명 삭제 아이콘 클릭시
function fnResetClubName() {
    $(".del_icon").hide();
    $("#txtClubName").val("");
}



//클럽검색
function fnClubSerach() {
    
    var param = {};
    var rdoAssIdx = $("input[name='rdoAss']:checked").val();
   
   
    // 소속협회가 없어요
    if ($("input[name='rdoIsAss']:checked").val() == "0") {
        param = {
            regionCode: $("#selRegion1").val(),
            areaCode: $("#selArea1").val(),
            search: $("#txtClubName").val()
        };
    }
    else {
        // 소속 협회가 있어요
        if (rdoAssIdx == 0) {
            param = {
                regionCode: $("#selRegion").val(),
                areaCode: 0,
                search: $("#txtClubName").val()
            };
        }
        else {
            param = {
                regionCode: $("#selRegion2").val(),
                areaCode: $("#selArea2").val(),
                search: $("#txtClubName").val()
            };
        }
    }

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        var pattern = "<div onclick='fnSelectClub(this, {0});' value=\"{0}\">{1}</div>";
        $("#autocomplete_items").empty();
        for (var i = 0; i < table.length ; i++) {
            $("#autocomplete_items").append(
                String.format(
                    pattern,
                    table[i]["clubIdx"],
                    table[i]["clubName"]
                )
            );
        }
    }
    window.fnAjaxSend("/ws/ws.asmx/GetClubList", param, fnCallback);
    
}


//클럽선택시
function fnSelectClub(con, clubIdx) {
    $("#hidClubIdx").val(clubIdx);
    $("#txtClub").val($(con).text());
    $("#ssoSearchModal").modal('hide');
}

//마지막 체크
function fnBeforeSubmit() {
    // 아이디 체크
    if (!isChkId) {
        alert("아이디 중복체크를 진행하세요.");
        $("#txtId").focus();
        return false;
    }

    // 비밀번호 입력 여부 체크
    if ($("#txtPw").val() == "") {
        alert("비밀번호를 입력하세요");
        return false;
    }

    if (!isChkSelf) {
        alert("패스 인증을 진행해주세요.");
        return false;
    }

    if ($("#txtDIcheck").val() != "")
    {
        alert("이미 가입한 회원 입니다.");
        $("#btnPass1").focus();
        return false;
    }
    return true;
}

function isChkSelfChange(value) {
    if ($("#txtDIcheck").val() != "") {
        alert("이미 가입한 회원 입니다.");
        return;
    }

    isChkSelf = value;


}


// 완료 팝업
function fnPopSuccess() {
    $("#ssoCertiModal").modal('show');
}