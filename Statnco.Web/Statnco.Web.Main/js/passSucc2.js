﻿function regPw() {
    var regExp = /^[a-z0-9]{6,20}$/g;
    var pw = $("#txtNewPw1").val();
    if (regExp.test(pw) == false) {
        $("#warningPw").text("아이디(영문+숫자, 6~20자리)").css("font-size", "10px").css("color", "#cc2c8c");
    } else {
        $("#warningPw").empty();
    }

    checkPw();
}

function checkPw() {
    var pw1 = $("#txtNewPw1").val();
    var pw2 = $("#txtNewPw2").val();
    if (pw1 == pw2) {
        $("#warningPwCheck").empty();
        $("#nextBtn").css("background-color", "#009f59").attr("disabled", false);
    }
    else {
        $("#warningPwCheck").text("달라요").css("font-size", "10px").css("color", "#cc2c8c");
        $("#nextBtn").css("background-color", "#B8B9B9");
        $("#nextBtn").attr("disabled", true);
    }
}



function editPw() {
    var id = $("#txtId", opener.document).val();
    var pw = $("#txtNewPw2").val();

    var fnCallback = function (data) {
        var rtn = data.d;
        if (rtn == 0) {

            //효과효과
            $(".pw_change_box").hide();
            $(".pw_change_ok_box").show();

        } else {
            alert("오류오류");
        }
    }
    var param = {
        "id": id,
        "passwd": pw
    };

    window.fnAjaxSend("/ws/ws.asmx/EditPassWd", param, fnCallback);

}




function moveLogin() {
    opener.location.href = "/login.aspx";
    window.close();
}