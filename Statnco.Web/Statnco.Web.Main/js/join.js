﻿$(function () {
    $("#check_btn_01").click();
    GetTermList();
});


//쩌거 가져오기
function GetTermList() {
    
    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        $("#term1").text(table[0]["contents"]);
        $("#term2").text(table[1]["contents"]);
        $("#term3").text(table[2]["contents"]);
        $("#term4").text(table[4]["contents"]);
        
    }
    var param = {"1":"1"};
    
    window.fnAjaxSend("/ws/ws.asmx/GetTermList", param, fnCallback);
    
}

function okClick() {
    var check1 = $("#check_btn_02").is(":checked");
    var check2 = $("#check_btn_03").is(":checked");


    if (!(check1) || !(check2)) {
        alert("필수요소 체크");
        return false;
    }
    return true;
}