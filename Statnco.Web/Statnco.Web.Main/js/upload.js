﻿function openTextFile() {
    var input = document.createElement("input");

    input.type = "file";
    input.accept = "image/*";
    input.id = "uploadInput";

    input.click();
    input.onchange = function (event) {
        processFile(event.target.files[0]);
    };

}

function processFile(file) {
    var reader = new FileReader();

    reader.onload = function () {
        var result = reader.result;
        output.innerText = result;
        //document.getElementById('tartgetImg').setAttribute('src', result);
        $("#tartgetImg").attr('src', result);
    };
    reader.readAsDataURL(file);
}




