﻿using System;
using System.Collections;

namespace Statnco.Web.Main.pass
{
    public partial class passFail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPCLIENTLib.KisinfoClass clsCPClient = new CPCLIENTLib.KisinfoClass();

            string sSiteCode = "BW459";
            string sSitePw = "ZrYsxKgyUGoj";           // NICE에서 부여받은 사이트코드


            // NICE로부터 암호화된 인증결과 리턴
            string sEncData = Request["EncodeData"];

            Response.Write("sEncData : " + sEncData + "<br><br><br>");

            int iRtn = clsCPClient.fnDecode(sSiteCode, sSitePw, sEncData);

            if (iRtn == 0)
            {
                string sPlain = clsCPClient.bstrPlainData;
                string sCipherTime = clsCPClient.bstrCipherDateTime;

                Response.Write("<b>ERR_CODE 는 xls 파일을 참조하세요.</b><br>");
                lbl_Result.Text = "인증결과_복호화_성공_원문 [" + sPlain + "]<br>";

                Hashtable element = new Hashtable();
                Split s = new Split();
                element = s.parse(sPlain);

                lbl_Result2.Text = sCipherTime;
                lbl_Result3.Text = (string)element["REQ_SEQ"];
                lbl_Result4.Text = (string)element["ERR_CODE"];
                lbl_Result5.Text = (string)element["AUTH_TYPE"];
            }
            else
            {
                lbl_Result.Text = "요청정보_암호화_오류 : " + iRtn + "<br>";
                /*
                 * -1 : 암호화 시스템 에러입니다.
                 * -4 : 입력 데이터 오류입니다.
                 * -5 : 복호화 해쉬 오류입니다.
                 * -6 : 복호화 데이터 오류입니다.
                 * -9 : 입력 데이터 오류입니다.
                 * -12 : 사이트 패스워드 오류입니다.
                 */
            }
        }
    }
}