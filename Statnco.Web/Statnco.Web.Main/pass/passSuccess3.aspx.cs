﻿using Statnco.DA.Roacket.Web;
using System;
using System.Collections;
using System.Text;
using System.Web.UI;

namespace Statnco.Web.Main.pass
{
    public partial class passSuccess3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            CPCLIENTLib.KisinfoClass clsCPClient = new CPCLIENTLib.KisinfoClass();

            string sSiteCode = "BW459";
            string sSitePw = "ZrYsxKgyUGoj";

            // NICE로부터 암호화된 인증결과 리턴
            string sEncData = Request["EncodeData"];

            int iRtn = clsCPClient.fnDecode(sSiteCode, sSitePw, sEncData);

            if (iRtn == 0)
            {
                string sPlain = clsCPClient.bstrPlainData;
                string sCipherTime = clsCPClient.bstrCipherDateTime;

                //lbl_Result.Text = "인증결과_복호화_성공_원문 [" + sPlain + "]<br>";

                Hashtable element = new Hashtable();
                Split s = new Split();
                element = s.parse(sPlain);

                if (element["DI"] != null)
                {
                    //lbl_Result2.Text = sCipherTime;
                    //lbl_Result3.Text = (string)element["REQ_SEQ"];
                    //lbl_Result4.Text = (string)element["RES_SEQ"];
                    //lbl_Result5.Text = (string)element["AUTH_TYPE"];
                    //lbl_Result6.Text = (string)element["NAME"];
                    ////lbl_Result6.Text = (string)element["UTF8_NAME"]; //charset utf8 사용시 주석 해제 후 사용
                    //lbl_Result7.Text = (string)element["BIRTHDATE"];
                    //lbl_Result8.Text = (string)element["GENDER"];
                    //lbl_Result9.Text = (string)element["NATIONALINFO"];
                    //lbl_Result10.Text = (string)element["DI"];
                    //lbl_Result11.Text = (string)element["CI"];
                    //lbl_Result12.Text = (string)element["MOBILE_NO"];
                    //lbl_Result13.Text = (string)element["MOBILE_CO"];


                    

                    hidName.Value = (string)element["NAME"]; //이름
                    hidBirth.Value = (string)element["BIRTHDATE"]; //생일
                    hidGender.Value = (string)element["GENDER"]; //성별
                    hidDI.Value = (string)element["DI"];  //몰라 이거중요
                    hidPhone.Value = (string)element["MOBILE_NO"]; //핸드폰번호

                    var biz = new Roacket_Web_Biz();
                    string checkvalue = biz.PASSDICheck(hidDI.Value);
                    hidDICheckDB.Value = checkvalue;



                    StringBuilder sb = new StringBuilder();

                        
                        sb.Append("<script>");
                        sb.Append("    const onMessage = {");
                        sb.Append(String.Format("        sRtnMSG: '{0}',", "인증 성공"));
                        sb.Append(String.Format("        requestnumber: '{0}',", (string)element["REQ_SEQ"]));
                        sb.Append(String.Format("        responsenumber: '{0}',", (string)element["REQ_SEQ"]));
                        sb.Append(String.Format("        authtype: '{0}',", (string)element["AUTH_TYPE"]));
                        sb.Append(String.Format("        name: '{0}',", (string)element["NAME"]));
                        //sb.Append(String.Format("        dupinfockeck: '{0}',", checkvalue));
                        sb.Append(String.Format("        birthdate: '{0}',", (string)element["BIRTHDATE"]));
                        sb.Append(String.Format("        gender: '{0}',", (string)element["GENDER"]));
                        sb.Append(String.Format("        nationalinfo: '{0}',", (string)element["NATIONALINFO"]));
                        sb.Append(String.Format("        dupinfo: '{0}',", (string)element["DI"]));
                        sb.Append(String.Format("        conninfo: '{0}',", (string)element["CI"]));
                        sb.Append(String.Format("        mobileno: '{0}',", (string)element["MOBILE_NO"]));
                        sb.Append(String.Format("        mobileco: '{0}'", (string)element["MOBILE_CO"]));
                     sb.Append(String.Format("        mobileco: '{0}'", (string)element["MOBILE_CO"]));


                        sb.Append("    };");
                        sb.Append("    window.ReactNativeWebView.postMessage(JSON.stringify({ onMessage }));");
                        sb.Append("</script>");

                        ClientScriptManager csm = Page.ClientScript;

                        // PAGE 공용 스크립트 추가
                        csm.RegisterStartupScript(this.GetType(), "PageScript", sb.ToString());


                    
                }
                else
                {
                    Response.Write("세션값 오류 입니다.<br>");
                }
            }
            else
            {
                //lbl_Result.Text = "요청정보_암호화_오류 : " + iRtn + "<br>";
                /*
                 * -1 : 암호화 시스템 에러입니다.
                 * -4 : 입력 데이터 오류입니다.
                 * -5 : 복호화 해쉬 오류입니다.
                 * -6 : 복호화 데이터 오류입니다.
                 * -9 : 입력 데이터 오류입니다.
                 * -12 : 사이트 패스워드 오류입니다.
                 */
            }
        }
    }
}


