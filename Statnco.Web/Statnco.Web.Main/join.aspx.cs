﻿using System;

namespace Statnco.Web.Main
{
    public partial class join : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void cancel_btn_Click(object sender, EventArgs e)
        {
            Response.Redirect("/index.aspx");
        }

        protected void ok_btn_Click(object sender, EventArgs e)
        {
            int agree1 = 0;
            int agree2 = 0;
            string joinType = "";
            if (check_btn_05.Checked)
            {
                agree1 = 1;

            }
            else
            {
                agree1 = 0;
            }


            if (check_btn_06.Checked)
            {
                agree2 = 1;
            }
            else
            {
                agree2 = 0;
            }
            if (radio_btn_1.Checked)
            {
                joinType = "1";
            }
            else
            {
                joinType = "2";
            }

            Response.Cookies["agree1"].Value = agree1.ToString();
            Response.Cookies["agree2"].Value = agree2.ToString();

            Response.Redirect("/join2.aspx?joinType=" + joinType);
        }
    }
}