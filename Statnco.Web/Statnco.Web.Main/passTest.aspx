﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="passTest.aspx.cs" Inherits="Statnco.Web.Main.passTest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script>
        window.name = 'Parent_window';

        function fnPopup() {

            window.open('', 'popupChk', 'width=500, height=550, top=100, left=100, fullscreen=no, menubar=no, status=no, toolbar=no, titlebar=yes, location=no, scrollbar=no');
            document.form_chk.target = 'popupChk';
            document.form_chk.action = "https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb";
            document.form_chk.submit();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <a href="javascript:fnPopup();">CheckPlus 안심본인인증 Click<BR></a>
            <asp:Label ID="lbl_Result" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
