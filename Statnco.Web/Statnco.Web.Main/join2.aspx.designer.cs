﻿//------------------------------------------------------------------------------
// <자동 생성됨>
//     이 코드는 도구를 사용하여 생성되었습니다.
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다. 
// </자동 생성됨>
//------------------------------------------------------------------------------

namespace Statnco.Web.Main
{


    public partial class join2
    {

        /// <summary>
        /// form1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlForm form1;

        /// <summary>
        /// btnNull 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnNull;

        /// <summary>
        /// isChild 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField isChild;

        /// <summary>
        /// hidClubIdx 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidClubIdx;

        /// <summary>
        /// hidName 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidName;

        /// <summary>
        /// hidBirth 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidBirth;

        /// <summary>
        /// hidGender 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidGender;

        /// <summary>
        /// hidDI 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidDI;

        /// <summary>
        /// hidDICheckDB 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidDICheckDB;

        /// <summary>
        /// hidPhone 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hidPhone;

        /// <summary>
        /// txtId 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtId;

        /// <summary>
        /// txtPw 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputPassword txtPw;

        /// <summary>
        /// btnPass1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnPass1;

        /// <summary>
        /// txtName 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtName;

        /// <summary>
        /// txtDIcheck 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputHidden txtDIcheck;

        /// <summary>
        /// selGender 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selGender;

        /// <summary>
        /// passGender 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText passGender;

        /// <summary>
        /// hiddenGender 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.HiddenField hiddenGender;

        /// <summary>
        /// txtYear 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtYear;

        /// <summary>
        /// selMonth 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selMonth;

        /// <summary>
        /// txtMonth 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtMonth;

        /// <summary>
        /// selDay 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selDay;

        /// <summary>
        /// txtDay 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtDay;

        /// <summary>
        /// txtPhone 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtPhone;

        /// <summary>
        /// txtParentName 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentName;

        /// <summary>
        /// txtParentGender 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentGender;

        /// <summary>
        /// txtParentYear 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentYear;

        /// <summary>
        /// txtParentMonth 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentMonth;

        /// <summary>
        /// txtParentDay 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentDay;

        /// <summary>
        /// txtParentMobile 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputText txtParentMobile;

        /// <summary>
        /// rdoIsAss1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rdoIsAss1;

        /// <summary>
        /// rdoIsAss2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlInputRadioButton rdoIsAss2;

        /// <summary>
        /// selRegion1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selRegion1;

        /// <summary>
        /// selArea1 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selArea1;

        /// <summary>
        /// selRegion2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selRegion2;

        /// <summary>
        /// selArea2 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.HtmlControls.HtmlSelect selArea2;

        /// <summary>
        /// join 컨트롤입니다.
        /// </summary>
        /// <remarks>
        /// 자동 생성 필드입니다.
        /// 수정하려면 디자이너 파일에서 코드 숨김 파일로 필드 선언을 이동하세요.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button join;
    }
}
