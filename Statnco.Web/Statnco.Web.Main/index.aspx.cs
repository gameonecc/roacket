﻿using System;

namespace Statnco.Web.Main
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["memberIdx"] != null)
            {
                Response.Redirect("/loginCom.aspx");
            }
            else
            {

            }
        }

        protected void btn_login_Click(object sender, EventArgs e)
        {
            Response.Redirect("/login.aspx");

        }

        protected void join_Click(object sender, EventArgs e)
        {
            Response.Redirect("/join.aspx");
        }


    }
}