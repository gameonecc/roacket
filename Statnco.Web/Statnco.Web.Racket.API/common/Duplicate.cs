﻿using System;

namespace Statnco.Web.Racket.API.common
{
    public class Duplicate
    {

        //중복된 파일명 처리
        public string FileUploadName(String dirPath, String fileN)
        {
            string fileName = fileN;

            if (fileN.Length > 0)
            {
                int indexOfDot = fileName.LastIndexOf(".");
                string strName = fileName.Substring(0, indexOfDot);
                string strExt = fileName.Substring(indexOfDot);

                bool bExist = true;
                int fileCount = 0;

                string dirMapPath = string.Empty;

                while (bExist)
                {
                    dirMapPath = dirPath;
                    string pathCombine = System.IO.Path.Combine(dirMapPath, fileName);

                    if (System.IO.File.Exists(pathCombine))
                    {
                        fileCount++;
                        fileName = strName + "(" + fileCount + ")" + strExt;
                    }
                    else
                    {
                        bExist = false;
                    }
                }
            }

            return fileName;

        }
    }
}