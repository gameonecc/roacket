﻿using System.Web;
using System.Web.Mvc;

namespace Statnco.Web.Racket.API
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
