﻿using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;

namespace Statnco.Web.Racket.API.Jwt
{
    public static class clsJWT1
    {
        static string plainTextSecurityKey = "0123456789abcdefghijklmnopqrstuvwxyz";
        static InMemorySymmetricSecurityKey signingKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes(plainTextSecurityKey));
        static SigningCredentials signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);
        static int lifetimeMinutes = 43200; //30일

        public static string createToken(int memberIdx,
                                         string id,
                                         string birth,
                                         string sidoGrade,
                                         string gunguGrade,
                                         int sidoCode,
                                         int gunguCode,
                                         string sidoName,
                                         string gunguName,
                                         string phone,
                                         string gender,
                                         int clubIdx,
                                         string name,
                                         string profilePath
                                         )
        {
            var claimsIdentity = new ClaimsIdentity(new List<Claim>()
           {
               new Claim("id", memberIdx.ToString()),
               new Claim("userId", id),
               new Claim("birth",birth.ToString()),
               new Claim("sido", sidoGrade),
               new Claim("gungu", gunguGrade),
               new Claim("sidoCode", sidoCode.ToString()),
               new Claim("gunguCode", gunguCode.ToString()),
               new Claim("sidoName", sidoName.ToString()),
               new Claim("gunguName", gunguName.ToString()),
               new Claim("phone", phone),
               new Claim("gender", gender),
               new Claim("clubIdx", clubIdx.ToString()),
               new Claim("name", name.ToString()),
               new Claim("profilePath", profilePath.ToString())
           }, "UserInfo");


            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                AppliesToAddress = ConfigurationManager.AppSettings["MainUrl"],
                TokenIssuerName = "Roacket",
                Subject = claimsIdentity,
                SigningCredentials = signingCredentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            tokenHandler.TokenLifetimeInMinutes = lifetimeMinutes;
            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodeToken = tokenHandler.WriteToken(plainToken);

            return signedAndEncodeToken;
        }

        public static JwtSecurityToken isValidToken(string signedAndEncodedToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudiences = new string[]
                {
                ConfigurationManager.AppSettings["MainUrl"],
                },
                ValidIssuers = new string[]
                {
                "Roacket"
                },
                IssuerSigningKey = signingKey
            };

            SecurityToken validatedToken;

            tokenHandler.ValidateToken(signedAndEncodedToken,
                tokenValidationParameters, out validatedToken);

            return validatedToken as JwtSecurityToken;
        }

        public static JwtSecurityToken readToken(string signedAndEncodedToken)
        {

            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            validatedToken = tokenHandler.ReadToken(signedAndEncodedToken);

            return validatedToken as JwtSecurityToken;

        }
    }
}