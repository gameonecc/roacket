﻿using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;

namespace Statnco.Web.Racket.API.Jwt
{
    public static class clsJWT
    {
        static string plainTextSecurityKey = "0123456789abcdefghijklmnopqrstuvwxyz";
        static InMemorySymmetricSecurityKey signingKey = new InMemorySymmetricSecurityKey(Encoding.UTF8.GetBytes(plainTextSecurityKey));
        static SigningCredentials signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256Signature, SecurityAlgorithms.Sha256Digest);
        static int lifetimeMinutes = 43200; //30일

        public static string createToken(int memberIdx, string memberType, int associaitionIdx, string memberId, string power, string manager, string region, string area)
        {
            var claimsIdentity = new ClaimsIdentity(new List<Claim>()
           {
               new Claim("idx", memberIdx.ToString()),
               new Claim("memberType", memberType.ToString()),
               new Claim("associaitionIdx", associaitionIdx.ToString()),
               new Claim("Id", memberId),
               new Claim("power", power.ToString()),
               new Claim("manager", manager.ToString()),
               new Claim("region", region.ToString()),
               new Claim("area", area.ToString()),
           }, "UserInfo");

            var securityTokenDescriptor = new SecurityTokenDescriptor()
            {
                AppliesToAddress = ConfigurationManager.AppSettings["MainUrl"],
                TokenIssuerName = "Roacket",
                Subject = claimsIdentity,
                SigningCredentials = signingCredentials,
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            tokenHandler.TokenLifetimeInMinutes = lifetimeMinutes;
            var plainToken = tokenHandler.CreateToken(securityTokenDescriptor);
            var signedAndEncodeToken = tokenHandler.WriteToken(plainToken);

            return signedAndEncodeToken;
        }

        public static JwtSecurityToken isValidToken(string signedAndEncodedToken)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters()
            {
                ValidAudiences = new string[]
                {
                ConfigurationManager.AppSettings["MainUrl"],
                },
                ValidIssuers = new string[]
                {
                "Roacket"
                },
                IssuerSigningKey = signingKey
            };

            SecurityToken validatedToken;

            tokenHandler.ValidateToken(signedAndEncodedToken,
                tokenValidationParameters, out validatedToken);

            return validatedToken as JwtSecurityToken;
        }

        public static JwtSecurityToken readToken(string signedAndEncodedToken)
        {

            var tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken validatedToken;

            validatedToken = tokenHandler.ReadToken(signedAndEncodedToken);

            return validatedToken as JwtSecurityToken;

        }
    }
}