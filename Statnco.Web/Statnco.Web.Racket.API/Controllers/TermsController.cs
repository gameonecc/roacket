﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class TermsController : ApiController
    {
        [Route("Terms/{id}")]
        public object Get(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetTermsList(id);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "query error";

            }
            if (dt.Rows.Count == 0)
            {
                dic["success"] = false;
                dic["message"] = "데이터가 없습니다.";
            }

            dic["success"] = true;
            dic["data"] = dt;

            return dic;
        }

      /*  [Route("Terms")]
        public int Post([FromBody] Terms ts)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetTerms(0, ts.title, ts.contents, ts.isUse);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;

        }*/

      /*  [Route("Terms/{id}")]
        public int Patch(int id, [FromBody] Terms ts)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetTerms(id, ts.title, ts.contents, ts.isUse);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }*/

        [Route("Terms/{id}")]
        public int Delete(int id)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.DelTerms(id);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

    }
}
