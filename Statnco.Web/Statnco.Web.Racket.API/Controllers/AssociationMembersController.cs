﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Roacket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AssociationMembersController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 관리자 리스트 조회(statnco 관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 :
        ///    
        ///                 {
        ///                     search : {
        ///                       option : "userId",
        ///                       content: ""
        ///                       }
        ///                 }
        ///         
        ///        
        ///         
        ///     
        /// </remarks>
        /// <returns></returns>
        //관리자목록 조회
        [Route("AssociationMembers")]
        public object Get(string filter = null)
        {
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            string opt;
            string content;
            if (filter == null)
            {
                opt = "userId";
                content = string.Empty;
            }
            else
            {
                JObject root = JObject.Parse(filter);

                JObject search = (JObject)root["search"];
                opt = search["option"].ToString();
                content = search["content"].ToString() == "" ? string.Empty : search["content"].ToString();

            }


            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.ManagerList(opt, content);
            }
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic["data"] = dt;
            dic["total"] = Convert.ToInt32(dt.Rows.Count);

            return dic;
        }

        /// <summary>
        /// 협회 관리자 상세 조회(statnco 관리자만 가능)
        /// </summary>
        /// <returns></returns>
        //협회 관리자 상세 조회
        [Route("AssociationMembers/{id}")]
        public object Get(int id)
        {
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }

            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetAssociationManagerInfo(id);
            }
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count == 0)
            {
                return "result : -1";
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt.Rows[0].DataRowToJson();

            return dic;

        }

        /// <summary>
        /// 협회 관리자 정보 수정(statnco 관리자만 가능)
        /// </summary>
        /// <returns></returns>

        //협회 관리자 정보 수정
        [Route("AssociationMembers")]
        public object Patch([FromBody] AssociationMemberEdit assm)
        {
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }

            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.SetAssociationMember(assm.memberIdx, assm.userId, Crypto.fnGetSHA512(assm.password), assm.name, assm.gender, assm.birth, assm.phone, assm.associationId);
            }

            return rtn;

        }


        /// <summary>
        /// 협회 관리자 정보 삭제(statnco 관리자만 가능)
        /// </summary>
        /// <returns></returns>
        /// 
        //협회 관리자 정보 삭제
        [Route("AssociationMembers")]
        public object Delete([FromBody] AssociationMemberDel assd)
        {
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.DelAssociationMember(assd.memberIdx);
            }

            return rtn;
        }

        /// <summary>
        /// 협회 관리자 생성 (statnco관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 :
        ///    
        ///       {
        ///         "signupType": "U" or "A",  (U는 일반유저  A는 협회소속유저 입니다.)
        ///         "userId": "string",
        ///         "password": "string",
        ///         "name": "string",
        ///         "gender": "string",
        ///         "birth": "string",
        ///         "phone": "string",  
        ///         "regionCode": 0,        (지역코드)
        ///         "areaCode": 0,          (세부지역 코드)
        ///         "assId": 0,             (협회 Idx 번호)
        ///         "memberIdx": 0          (statnco 관리자 계정 memberIdx번호)
        ///        }
        ///         
        ///     
        /// </remarks>
        /// <returns></returns>
        //협회 관리자 생성
        [Route("AssociationMembers")]
        public object Post([FromBody] AssociationMemberAdd add)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.AddAssociationMember(add.signupType, add.userId, Crypto.fnGetSHA512(add.password),
                                               add.name, add.gender, add.birth, add.phone,
                                               add.regionCode, add.areaCode, add.assId, add.memberIdx);
            }

            return rtn;
        }

    }
}
