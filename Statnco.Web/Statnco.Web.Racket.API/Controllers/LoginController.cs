﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    [Route("Login")]
    public class LoginController : ApiController
    {
        // 로그인
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///   {
        ///    "id" : "test123",
        ///    "password" : "dlwnsgus12"
        ///    }
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        public object Post([FromBody] AdminLogin al)
        {
            DataSet ds = null;
            //string pw = Crypto.fnGetSHA512(al.password);
            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.Login(al.id, Crypto.fnGetSHA512(al.password));
            }
            
            DataTable dt = ds.Tables[0];
           
            if (dt.Rows.Count > 0)
            {
                Dictionary<string, object> dicLogin = new Dictionary<string, object>();
                dicLogin["MemberInfo"] = dt.Rows[0].DataRowToJson();
                dicLogin["memberToken"] = clsJWT1.createToken(Convert.ToInt32(dt.Rows[0]["id"]),
                                                                 dt.Rows[0]["userId"].ToString(),
                                                                 dt.Rows[0]["birth"].ToString(),
                                                                 dt.Rows[0]["sidoGrade"].ToString(),
                                                                 dt.Rows[0]["gunguGrade"].ToString(),
                                                                 Convert.ToInt32(dt.Rows[0]["sidoCode"]),
                                                                 Convert.ToInt32(dt.Rows[0]["gunguCode"]),
                                                                 dt.Rows[0]["sidoName"].ToString(),
                                                                 dt.Rows[0]["gunguName"].ToString(),
                                                                 dt.Rows[0]["phone"].ToString(),
                                                                 dt.Rows[0]["gender"].ToString(),
                                                                 Convert.ToInt32(dt.Rows[0]["clubIdx"]),
                                                                 dt.Rows[0]["userName"].ToString(),
                                                                 dt.Rows[0]["profilePath"].ToString()
                                                                 );
                return dicLogin;
            }
            else
            {
                return "{return : -1}";
            }
        }

    }
}
