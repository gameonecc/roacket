﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CheckController : ApiController
    {
        //클럽명 중복체크
        [Route("Check")]
        public int Get([FromUri] CommonCheck.ClubNameCheck cs)
        {
            DataTable dt = null;

            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.CheckClubName(cs.clubName);
            }

            if (dt.Rows.Count != 0)
            {
                return -1;
            }

            return rtn;
        }

        ////회원가입시 id 중복체크
        //public int Get(string id)
        //{
        //    DataTable dt = null;

        //    int rtn = 0;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.CheckUserId(id);
        //    }

        //    if (dt.Rows.Count != 0)
        //    {
        //        return -1;
        //    }
        //    return rtn;
        //}
    }
}
