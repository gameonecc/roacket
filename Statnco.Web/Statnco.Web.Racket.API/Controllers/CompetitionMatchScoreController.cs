﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using System;
using System.Data;
using System.Web.Http;
using static Statnco.Web.Racket.API.Models.CompetitionMatchScores;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionMatchScoreController : ApiController
    {

        /// <summary>
        /// 경기스코어 보기
        /// </summary>
        /// <remarks>
        /// 
        /// 
        /// 예제 : 
        ///     
        ///       {
        ///         "competitionIdx" : "45",
        ///         "groupIdx" : "1",
        ///         "matchIdx" : "2"
        ///        }
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>


        [Route("CompetitionMatchScore")]
        public DataTable Get(string filter)
        {

            JObject root = JObject.Parse(filter);

            int competitionIdx = Convert.ToInt32(root["competitionIdx"].ToString());
            int groupIdx = Convert.ToInt32(root["groupIdx"].ToString());
            int matchIdx = Convert.ToInt32(root["matchIdx"].ToString());



            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionMatchScore(competitionIdx, groupIdx, matchIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }

        [Route("CompetitionMatchScore")]
        public object Post([FromBody] MatchScore ms)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetitionMatchScore(ms.competitionIdx, ms.groupIdx, ms.matchIdx,
                            ms.setNo, ms.team1Score1, ms.team1Score2, ms.team2Score1, ms.team2Score2,
                            ms.setSrtTime, ms.setEndTime, ms.shuttleCockCnt
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
