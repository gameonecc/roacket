﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AdminLoginController : ApiController
    {
        /// <summary>
        /// 로그인
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        ///      현재 관리자 아이디로 등록
        ///     {
        ///       "id":"test123",
        ///       "password" : "dlwnsgus12"
        ///     }
        ///     
        ///   
        /// </remarks>
        /// <returns></returns>

        [Route("AdminLogin")]
        public object Post([FromBody] AdminLogin al)
        {
            //jToken = Request.Headers.Authorization.ToString();

            DataSet ds = null;
            Dictionary<string, object> dicLogin = new Dictionary<string, object>();

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    ds = biz.AdminLogn(al.id, Crypto.fnGetSHA512(al.password));
                }
            }
            catch (Exception exp)
            {
                return "";
            }

            // 0번은 로그인 정보 + 토큰
            DataTable dt = ds.Tables[0];

            string manager = "";

            // 1번은 메뉴 및 메뉴 권한
            DataTable dt1 = ds.Tables[1];

            // 2번은 매니저
            DataTable dt2 = ds.Tables[2];
            if (dt2.Rows.Count == 0)
            {
                manager = "empty";
            }
            else
            {
                manager = dt2.Rows[0]["manager"].ToString();
            }

            if (dt.Rows.Count > 0)
            {
                
                dicLogin["memberInfo"] = dt.Rows[0].DataRowToJson();
                dicLogin["memberToken"] = clsJWT.createToken(Convert.ToInt32(dt.Rows[0]["memberIdx"]),
                                                                             dt.Rows[0]["memberType"].ToString(),
                                                             Convert.ToInt32(dt.Rows[0]["associationId"]),
                                                                             dt.Rows[0]["userId"].ToString(),
                                                                             manager,
                                                                             dt2.Rows[0]["manager"].ToString(),
                                                                             dt.Rows[0]["regionCode"].ToString(),
                                                                             dt.Rows[0]["areaCode"].ToString()) ;

                dicLogin["menuInfo"] = dt1;

               
            }
            else
            {

                dicLogin["result"] = "fail";
                dicLogin["message"] = "아이디, 비밀번호 오류";
            }

            return dicLogin;
        }
    }
}
