﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class UsersController : ApiController
    {
        //회원가입
        /// <summary>
        /// 앱 사용자 회원가입
        /// </summary>
        /// <remarks>
        ///  
        /// 예제 입니다: 
        /// 
        ///        {
        ///        "id": "test456",
        ///         "pw": "dlwnsgus12",
        ///         "name": "이준현님",
        ///         "gender": "M",
        ///         "birth": "1992-09-25",
        ///         "phone": "01051216647",
        ///         "DI": "d$D19ueijhfAF",
        ///         "isAss": 0,
        ///         "region": 14,
        ///         "area": 12,
        ///        }
        ///   
        /// </remarks>
        /// <returns></returns>
        public object Post([FromBody] User us)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.InsertAppUser(us.id, Crypto.fnGetSHA512(us.pw), us.name,
                                            us.phone, us.gender, us.birth,
                                            us.DI,  us.region,
                                            us.area);
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (rtn == 0)
            {
                dic["success"] = true;
            }
            else
            {
                dic["success"] = false;
                dic["error_no"] = rtn;
            }

            return dic;
        }
        
    }
}
