﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class ClubsController : ApiController
    {
        //클럽생성 [2022-01-22 현재 사용x]
        //public int Post(int type, string clubName, string masterName, string nickName, string birth, int clubMaster, int local, int associationId, string gender)
        //{
        //    int dt = 0;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.CreateClub(type, clubName, masterName, nickName, birth, clubMaster, local, associationId, gender);
        //    }

        //    return dt;
        //}

        public static string jToken = string.Empty;
        //클럽 검색
        /// <summary>
        /// 클럽 리스트 조회 API (협회관리자 + statnco관리자만 사용 가능)
        /// </summary>
        /// <remarks>
        ///  
        /// 예제 입니다: 
        /// 
        ///        예시 :
        ///     
        ///         {
        ///             "page" : 1
        ///         }
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        [Route("Clubs")]
        public object Get(string filter)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int si = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["sidoCode"]);
            int gu = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["gunguCode"]);

            JObject root = JObject.Parse(filter);

            int page = Convert.ToInt32(root["page"].ToString());

            int start = ((page - 1) * 10) + 1;
            int end = page * 10;
            string sort1 = "clubName";
            string sort2 = "ASC";

            if (si == null && gu == null)
                {
                    si = 1;
                    gu = 1;
                }

             
            DataTable dt = null;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.ClubList(si, gu, start, end, sort1, sort2);

                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            if (dt.Rows.Count == 0)
            {
                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }

            //DataTable dt = ds.Tables[0];
            //DataRow dr = ds.Tables[1].Rows[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }

            dic["success"] = true;
            dic["data"] = dt;
            //dic["total"] = dr["total"];


            return dic;

        }
        /// <summary>
        /// 클럽멤버 조회하는 API
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 : 클럽 인덱스 보내 주시면 됩니다.
        ///     
        ///      
        ///           
        ///           
        ///              
        ///                 
        ///        id = 1
        ///        
        ///        
        ///     
        /// 
        /// 
        /// 
        /// </remarks>
        /// <returns></returns>



        // 클럽 상세 조회
        [Route("Clubs/{id}")]
        public object Get(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            
          
            DataSet ds = null;
           // DataTable dt1 = null;
            DataTable dt2 = null;


            try
            {



                using (var biz = new Roacket_Api_Biz())
                {
                    ds = biz.ClubDetail(id);
                }
               // dt1 = ds.Tables[0];
                dt2 = ds.Tables[1];

            }
            catch(Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            if (dt2.Rows.Count == 0)
            {
                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }

            dic["success"] = true;
           // dic["clubInfo"] = dt1;
            dic["clubMembers"] = dt2;


            return dic;

        }
        //[2022-01-26 현재 사용x]
        ////클럽 정보 수정
        //public int Patch(int idx, string clubName, int clubMaster, int editUser)
        //{
        //    int dt = 0;


        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.ModifyClubInfo(idx, clubName, clubMaster, editUser);
        //    }

        //    return dt;
        //}

        //[2022-01-26 현재 사용x]
        ////클럽 삭제
        //public int Put(int idx, int editUser)
        //{
        //    int dt = 0;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.DeleteClub(idx, editUser);
        //    }

        //    return dt;
        //}





    }
}
