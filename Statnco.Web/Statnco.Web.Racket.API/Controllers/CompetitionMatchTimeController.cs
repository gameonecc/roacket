﻿using Statnco.DA.Roacket.Api;
using System;
using System.Web.Http;
using static Statnco.Web.Racket.API.Models.CompetitionMatchTime;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionMatchTimeController : ApiController
    {
        [Route("CompetitionMatchTime")]
        public int Post([FromBody] MatchTime mt)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetMatchTime(mt.competitionIdx, mt.groupIdx, mt.matchIdx,
                        mt.timeType, mt.setTime);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
