﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AdminClubsController : ApiController
    {
        //클럽생성 [2022-01-22 현재 사용x]
        //public int Post(int type, string clubName, string masterName, string nickName, string birth, int clubMaster, int local, int associationId, string gender)
        //{
        //    int dt = 0;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.CreateClub(type, clubName, masterName, nickName, birth, clubMaster, local, associationId, gender);
        //    }

        //    return dt;
        //}

        public static string jToken = string.Empty;
        /// <summary>
        /// 클럽 리스트 조회 API (협회관리자 + statnco관리자만 사용 가능)
        /// </summary>
        /// <remarks>
        ///  
        /// 예제 입니다: 
        /// 
        ///       {
        ///        "search": { "option" : "clubName",
        ///                     "content" : "" 
        ///                   }
        ///               }
        ///      range = 0 9
        ///      sort = clubIdx DESC
        ///           
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        [Route("AdminClubs")]
        public object Get(string filter = null, [FromUri] int[] range = null, [FromUri] string[] sort = null)
        {   
            

            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();
            int si = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["region"]);
            int gu = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["area"]);


            string option = "";
            string content = "";
            int range1 = 0;
            int range2 = 0;
            string sort1 = "";
            string sort2 = "";

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }



            if (filter == null)
            {
                option = "clubName";
                content = string.Empty;

                if (range == null)
                {
                    range1 = 0;
                    range2 = 9;
                }
                else
                {
                    range1 = range[0];
                    range2 = range[1];
                }


                if (sort == null)
                {
                    sort1 = "clubIdx";
                    sort2 = "DESC";
                }
                else
                {
                    sort1 = sort[0];
                    sort2 = sort[1];
                }
            }
            else
            {
                JObject root = JObject.Parse(filter);

                JObject region = (JObject)root["region"];
                JObject search = (JObject)root["search"];

                if (memberIdx == 5)
                {
                    si = Convert.ToInt32(region["si"]);
                    gu = Convert.ToInt32(region["gu"]);
                }


                option = search["option"].ToString();
                content = search["content"].ToString();

                if (range.Length == 0)
                {
                    range1 = 0;
                    range2 = 9;
                }
                else
                {
                    range1 = range[0];
                    range2 = range[1];
                }


                if (sort.Length == 0)
                {
                    sort1 = "clubIdx";
                    sort2 = "DESC";
                }
                else
                {
                    sort1 = sort[0];
                    sort2 = sort[1];
                }
            }





            DataSet ds = null;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                   // ds = biz.ClubList(memberIdx, si, gu, option, content, range1, range2, sort1, sort2);

                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            if (ds.Tables.Count == 0)
            {
                // 에러메세지 적으면 댐
                return "데이터가 없습니다";

            }

            DataTable dt = ds.Tables[0];
            DataRow dr = ds.Tables[1].Rows[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt;
            dic["total"] = dr["total"];


            return dic;

        }

        //클럽 검색
        /// <summary>
        /// 클럽 리스트 조회 API (협회관리자 + statnco관리자만 사용 가능)
        /// </summary>
        // 클럽 상세 조회
        [Route("AdminClubs/{id}")]
        public object Get(int id)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }
            DataSet ds = null;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.ClubDetail(id);
            }
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt;

            return dic;

        }
        //[2022-01-26 현재 사용x]
        ////클럽 정보 수정
        //public int Patch(int idx, string clubName, int clubMaster, int editUser)
        //{
        //    int dt = 0;


        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.ModifyClubInfo(idx, clubName, clubMaster, editUser);
        //    }

        //    return dt;
        //}

        //[2022-01-26 현재 사용x]
        ////클럽 삭제
        //public int Put(int idx, int editUser)
        //{
        //    int dt = 0;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        dt = biz.DeleteClub(idx, editUser);
        //    }

        //    return dt;
        //}





    }
}
