﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;


namespace Statnco.Web.Racket.API.Controllers
{
    public class BoardsController : ApiController
    {
        //토큰 가져오기
        public static string jToken = String.Empty;

        //게시판 작성
        /// <summary>
        /// 게시판 작성하기(로그인 필수)
        /// </summary>
        /// <remarks>
        /// 
        ///     예제 : 
        ///     
        ///         {
        ///             "type" : "market",
        ///             "clubIdx" : "0",
        ///             "title": "test",
        ///             "contents" : "test입니다",
        ///             "ph1" : "",
        ///             "ph2" : "",
        ///             "ph3" : "",
        ///             "ph4" : "",
        ///             "ph5" : ""
        ///         }
        /// </remarks>
        /// <returns></returns>
        [Route("Boards")]
        public object Post([FromBody] Boards bs)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            
            if(Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "로그인!!!!";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();

            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);

            int rtn = 0;

            // 이미지 확인
            byte[] arrByte = Convert.FromBase64String("abcd");
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.InsertBoard(
                                          bs.type, memberIdx, bs.clubIdx, bs.title, bs.contents,
                                          bs.ph1 == "" ? string.Empty : bs.ph1,
                                          bs.ph2 == "" ? string.Empty : bs.ph2,
                                          bs.ph3 == "" ? string.Empty : bs.ph3,
                                          bs.ph4 == "" ? string.Empty : bs.ph4,
                                          bs.ph5 == "" ? string.Empty : bs.ph5
                                          );
                }
            }
            catch (Exception exp)
            {

                throw exp;
            }
            if (rtn == 0)
            {
                dic["success"] = true;
            }
            else
            {
                dic["success"] = false;
                dic["error no"] = rtn.ToString();

            }

            return dic;
        }

        /// <summary>
        /// 게시판 리스트
        /// </summary>
        /// <remarks>
        /// 
        ///     예시 :
        ///     
        ///         {
        ///             "search" : "",
        ///             "type" : "market",
        ///             "page" : 1
        ///         }
        /// </remarks>
        /// <returns></returns>
        [Route("Boards")]
        public object Get(string filter)
        {
            //토큰 필요 없음
            JObject root = JObject.Parse(filter);

            DataSet ds = null;

            string search = root["search"].ToString() == "" ? string.Empty : root["search"].ToString();
            string type = root["type"].ToString();
            int page = Convert.ToInt32(root["page"].ToString());
            int start = ((page - 1) * 10) + 1;
            int end = page * 10;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetBoardList(type, search, start, end);
            }

            DataTable dt1 = ds.Tables[0];
            if (dt1.Rows.Count == 0)
            {
                return "데이터가 없습니다";
            }
            DataTable dt2 = ds.Tables[1];

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt1;
            dic["totalCnt"] = dt2;

            return dic;
        }
        /// <summary>
        /// 게시글 상세보기
        /// </summary>
        /// <returns></returns>
        [Route("Boards/{id}")]
        public object Get(int id)
        {
            DataSet ds = null;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetBoardDetail(id);
            }
            //게시글 내용
            DataTable dt1 = ds.Tables[0];
            //사진 리스트
            DataTable dt2 = ds.Tables[1];
            //댓글에 대한 내용
            DataTable dt3 = ds.Tables[2];

            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (ds.Tables.Count == 0)
            {
                return "데이터가 없습니다";
            }
            else
            {
                dic["data"] = dt1.Rows[0].DataRowToJson();

            }


            string[] photos = new string[5];
            if (dt2.Rows.Count == 0)
            {           
              
                dic["photo"] = "사진이 없습니다.";
            }
            else
            {

                for (var i = 0; i < dt2.Columns.Count; i++)
                {
                    photos[i] = Convert.ToString(dt2.Rows[0][i]);
                }

                dic["photos"] = photos;
            }

            if (dt3.Rows.Count == 0)
            {
                dic["repy"] = "댓글이 없습니다";
            }
            else
            {
                dic["reply"] = dt3;
            }



            return dic;
        }

        /// <summary>
        /// 게시판 수정 (작성자 및 관리자만 수정 가능 - 토큰 사용) 
        /// </summary>
        /// <remarks>
        /// 
        ///     예시 :
        ///     
        ///         {
        ///             "title" : "수정하기",
        ///             "contents" : "나는 수정을 할것입니다",
        ///             "ph1"   :   "ab",
        ///             "ph2"   :   "cd",
        ///             "ph3"   :   "ef",
        ///             "ph4"   :   "gh",
        ///             "ph5"   :   "Ab"
        ///         }
        /// </remarks>
        /// <returns></returns>
        [Route("Boards/{id}")]
        public object Patch(int id, [FromBody] Boards.updateBoard bu)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "로그인!!!!";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();

            int editMember = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;

            
            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.EditBoard(id, bu.title, bu.contents,
                                    bu.ph1 == "" ? string.Empty : bu.ph1, 
                                    bu.ph2 == "" ? string.Empty : bu.ph2, 
                                    bu.ph3 == "" ? string.Empty : bu.ph3,
                                    bu.ph4 == "" ? string.Empty : bu.ph4,
                                    bu.ph5 == "" ? string.Empty : bu.ph5, 
                                    editMember);
            }
            if (rtn == -2)
            {
                dic["success"] = false;
                dic["message"] = "권한이 없습니다";
            }
            else if (rtn != 0)
            {
                dic["success"] = false;
                dic["message"] = "오류가 발생하였습니다";
            }
            else
            {
                dic["success"] = true;
            }

            return dic;
        }

        /// <summary>
        /// 게시글 삭제
        /// </summary>
        /// <returns></returns>
        [Route("Boards/{id}")]
        public object Delete(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "로그인!!!!";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();

            int editMember = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.DeleteBoard(id, editMember);
            }
            if (rtn == -2)
            {
                dic["success"] = false;
                dic["message"] = "권한이 없습니다";
            }
            else if (rtn != 0)
            {
                dic["success"] = false;
                dic["message"] = "오류가 발생하였습니다";
            }
            else
            {
                dic["success"] = true;
            }

            return dic;
        }
    }
}
