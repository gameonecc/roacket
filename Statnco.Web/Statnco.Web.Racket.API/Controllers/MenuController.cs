﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MenuController : ApiController
    {
        [Route("Menu/{id}")]
        public object Get(int id)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetMenuPower(id);
                }
            }
            catch (Exception exp)
            {

                throw;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt.Rows[0].DataRowToJson();

            return dic;
        }

        [Route("Menu")]
        public int Post([FromBody] MenuPower mp)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetMenuPower(mp.menuIdx, mp.menuSubIdx, mp.memberIdx,
                            mp.power, mp.adminIdx
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
