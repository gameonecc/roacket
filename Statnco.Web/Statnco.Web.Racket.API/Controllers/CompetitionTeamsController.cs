﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionTeamsController : ApiController
    {
        [Route("CompetitionTeams")]
        public DataTable Get([FromUri] CompetitionTeams.TeamList tl)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionTeamsDetail(tl.competitionIdx, tl.groupIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }
    }
}
