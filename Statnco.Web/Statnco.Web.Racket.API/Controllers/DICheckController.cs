﻿using Statnco.DA.Roacket.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class DICheckController : ApiController
    {
        //회원가입
        /// <summary>
        /// DI 중복체크
        /// </summary>
        /// <remarks>
        ///  
        /// 예제 입니다: 
        /// 
        ///        {
        ///        "DI": "MC0GCCqGSIb3DQIJAyEAeomOjr3pqmm93UlOlK7qLygelxMB1hpMJXUt7EpQAVo=",
        ///        }
        ///   
        /// </remarks>
        /// <returns></returns>
        [Route("DICheck/{DI}")]
        public object Get(string DI)
        {
            int rtn = 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.CheckDI(DI);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "qyery error";
            }

            if(rtn == 0)
            {
                dic["success"] = true;
                dic["message"] = "사용 가능";
            }
           else if(rtn == 1)
            {
                dic["success"] = false;
                dic["message"] = "중복가입자";
            }
            return dic;
        }
    }
}
