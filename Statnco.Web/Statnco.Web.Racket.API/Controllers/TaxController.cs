﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class TaxController : ApiController
    {
        public static string jToken = string.Empty;

        /// <summary>
        /// 회원의 회비 내역 수정 ( 토큰 필수, statnco 관리자, 협회 관리자만 수정 가능)
        /// </summary>
        //협회 회비 내역 수정
        [Route("Tax")]
        public object Patch([FromBody] Tax tax)
        {
            int statnco = 1;

            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "no statnco";
            }
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.MemberTaxEdit(tax.assIdx, tax.memberIdx, tax.tax, memberIdx);

            }

            return rtn;


        }
    }
}
