﻿using Statnco.DA.Roacket.Api;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumsController : ApiController
    {
        // GET: api/Stadiums
        [Route("Stadiums")]
        public DataTable Get(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionStadiumMainList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }
    }
}
