﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionGroupsController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        ///  참가현황 그룹 보기 (statnco관리자 + 협회관리자 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 
        /// 예제 : 
        ///     
        ///       {
        ///         "competitionIdx" : "45",
        ///         "eventCode" : "1",
        ///         "ageCode"   : "20",
        ///         "gradeCode" : "2"
        ///        }
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>
        // GET: api/CompetitionGroup
        [Route("CompetitionGroups")]
        public object Get(string filter, [FromUri] int[] range = null, [FromUri] string[] sort = null)
        {
            //statnco 관리자만 가능
            int statnco = 18;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }
            JObject root = JObject.Parse(filter);

            int competitionIdx = Convert.ToInt32(root["competitionIdx"].ToString());
            byte eventCode = Convert.ToByte(root["eventCode"].ToString());
            byte ageCode = Convert.ToByte(root["ageCode"].ToString());
            byte gradeCode = Convert.ToByte(root["gradeCode"].ToString());

            int range1 = 0;
            int range2 = 0;

            if (range.Length < 1)
            {
                range1 = 0;
                range2 = 9;
            }
            else
            {
                range1 = range[0];
                range2 = range[1];
            }



            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionGroup(competitionIdx, eventCode, ageCode, gradeCode, range1, range2);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }

        [Route("CompetitionGroups")]
        public int Patch([FromBody] CompetitionGroups.GroupConfig cg)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetitionGroupConfig(cg.competitionIdx, cg.groupIdx,
                            cg.competitionType, cg.leagueGroupCount, cg.tournementMinRank,
                            cg.stadiumNo
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
