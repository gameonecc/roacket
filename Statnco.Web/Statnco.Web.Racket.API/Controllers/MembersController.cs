﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MembersController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 협회에 등록된 회원의 리스트 조회 API (협회관리자, statnco 계정만 가능)
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///         {
        ///        "region":{ "si":"14", 
        ///                   "gu":"13"
        ///                   },
        ///        "search": { "option" : "userId",
        ///                     "content" : "" 
        ///                     },
        ///        "tax"   : { "tax" : "0"
        ///                     }
        ///               }
        ///      range = 0 9
        ///      sort = userId DESC
        ///            
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>

        [Route("Members")]
        public object Get(string filter = null, [FromUri] int[] range = null, [FromUri] string[] sort = null)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();
            int associaitionIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["associaitionIdx"]);
            int si = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["region"].ToString());
            int gu = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["area"].ToString());
            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }

            string option = "";
            string content = "";
            int range1 = 0;
            int range2 = 0;
            string sort1 = "";
            string sort2 = "";
            int taxVal;

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }



            if (filter == null)
            {
                option = "userId";
                content = string.Empty;
                taxVal = 0;
                if (range == null)
                {
                    range1 = 0;
                    range2 = 9;
                }
                else
                {
                    range1 = range[0];
                    range2 = range[1];
                }


                if (sort == null)
                {
                    //정렬 기준이 될 칼럼
                    sort1 = "userId";
                    // 정렬 방법(ASC or DESC)
                    sort2 = "DESC";
                }
                else
                {
                    sort1 = sort[0];
                    sort2 = sort[1];
                }
            }
            else
            {
                JObject root = JObject.Parse(filter);

                JObject region = (JObject)root["region"];
                JObject search = (JObject)root["search"];
                JObject tax = (JObject)root["tax"];

                //tax  여부
                taxVal = Convert.ToInt32(tax["tax"].ToString());

                if (memberIdx == 5)
                {
                    si = Convert.ToInt32(region["si"]);
                    gu = Convert.ToInt32(region["gu"]);
                }


                option = search["option"].ToString();
                content = search["content"].ToString() == "" ? string.Empty : search["content"].ToString();

                if (range.Length == 0)
                {
                    range1 = 0;
                    range2 = 9;
                }
                else
                {
                    range1 = range[0];
                    range2 = range[1];
                }


                if (sort.Length == 0)
                {
                    sort1 = "userId";
                    sort2 = "DESC";
                }
                else
                {
                    sort1 = sort[0];
                    sort2 = sort[1];
                }
            }


            //리스트
            DataSet ds = null;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetMemberList(memberIdx, associaitionIdx, si, gu, taxVal, option, content, range1, range2, sort1, sort2);
            }
            if (ds.Tables.Count == 0)
            {
                return "데이터가 없습니다";
            }
            DataTable dt = ds.Tables[0];
            DataRow dr = ds.Tables[1].Rows[0];
            if (dt.Rows.Count == 0)
            {
                return null;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic["data"] = dt;
            dic["total"] = dr["total"];

            return dic;
        }
        /// <summary>
        /// 회원 정보 조회(협회관리자, statnco 계정만 가능)
        /// </summary>
        //회원 정보 조회
        [Route("Members/{id}")]
        public object Get(int id)
        {
            //statnco 관리자만 가능
            int statnco = 18;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }


            DataTable dt = null;


            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.GetMemberDetail(id);
            }


            if (dt.Rows.Count > 0)
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["data"] = dt.Rows[0].DataRowToJson();

                return dic;
            }
            else
            {
                return "데이터가 없습니다";
            }
        }
        /// <summary>
        /// 회원정보 수정 토큰필수
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///         {
        ///         "si" : "A",
        ///         "gu" : "-"
        ///         }
        ///         
        ///            
        /// </remarks> 
        /// <returns></returns>
        // 회원정보 수정
        [Route("Members/{id}")]
        public object Patch([FromUri] int id, [FromBody] MemberRegionArea mb)
        {
            //statnco 관리자만 가능
            int statnco = 18;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }
            string si = mb.si == "" ? "-" : mb.si;
            string gu = mb.gu == "" ? "-" : mb.gu;

            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.SetMember(id, si, gu, memberIdx);
            }

            return rtn;
        }

        /// <summary>
        /// 회원정보 수정 토큰필수
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///         {
        ///         "password" : "a12345678",
        ///         "phone" : "010-0000-0000"
        ///         }
        ///         
        ///            
        /// </remarks> 
        /// <returns></returns>
        //회원 휴대폰 번호 수정
        [Route("Members/phoneEdit")]
        public object Patch([FromBody] MemberPhoneEdit mphe)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["id"]);


            int rtn = 0;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetMemberPhone(memberIdx, mphe.phone, Crypto.fnGetSHA512(mphe.password), memberIdx);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }
            if (rtn == 0)
            {
                dic["success"] = true;
                dic["message"] = "처리완료";
            }
            else
            {
                dic["success"] = false;
                dic["message"] = "query error";
                dic["error_no"] = rtn;
            }
            return dic;

        }
        /// <summary>
        /// 회원정보 수정 토큰필수
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///         {
        ///         "checkPw" : "dlwnsgus12",
        ///         "newPw" : "a12345678"
        ///         }
        ///         
        ///            
        /// </remarks> 
        /// <returns></returns>
        //회원 비밀번호 수정
        [Route("Members/passwordEdit")]
        public object Patch([FromBody] MemberPasswordEdit mpae)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["id"]);


            int rtn = 0;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetMemberPassword(memberIdx,
                                                Crypto.fnGetSHA512(mpae.checkPw), 
                                                Crypto.fnGetSHA512(mpae.newPw), memberIdx);
                }
            }
            catch (Exception exp)
            {

                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }
            if (rtn == 0)
            {
                dic["success"] = true;
                dic["message"] = "처리완료";
            }
            else
            {
                dic["success"] = false;
                dic["message"] = "현재 비밀번호가 일치하지 않습니다.";
                dic["error_no"] = rtn;
            }
            return dic;

        }


        //회원 삭제
        [Route("Members/{id}")]
        public object Delete(int id)
        {
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.DeleteMember(id);
            }

            return rtn;
        }


    }
}
