﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MenuModuleController : ApiController
    {
        [Route("MenuModule")]
        public object Get()
        {
            DataTable dt = null;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetMenuModule();
                }

            }
            catch (Exception e)
            {

                throw e;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();

            dic["data"] = dt;

            return dic;

        }

        [Route("MenuModule")]
        public int Post([FromBody] MenuModule mm)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.CreateMenuPowerModule(mm.moduleName, 0);
                }

            }
            catch (Exception e)
            {

                throw e;
            }
            return rtn;
        }
    }
}
