﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionsController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 대회 리스트 조회 (statnco관리자 + 협회관리자 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 : 공백일 경우 전체검색
        ///     
        ///       {
        ///         "search" : {
        ///           "searchWord":"대회",
        ///           "regiComp":"allComp"
        ///                 }
        ///                 
        ///         
        ///        }
        ///        
        ///     
        /// page =1
        /// 
        /// 
        /// </remarks>
        /// <returns></returns>
        //대회 리스트 조회
        [Route("Competitions")]
        public object Get(string filter, [FromUri] int page )
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            int regionCode = 0;

            if (Request.Headers.Authorization == null )
            {
                 regionCode = 0;   
            }
            else
            {
                
                jToken = Request.Headers.Authorization.ToString();
                if( jToken == null)
                {
                    dic["success"] = false;
                   dic["message"] = "회원정보가 필요합니다.";
                    return dic;
                }
                regionCode = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["sidoCode"]);

                 
            }            
          

            JObject root = JObject.Parse(filter);
            JObject search = (JObject)root["search"];   
           
            
            string searchWord = search["searchWord"].ToString();
            string regiComp = search["regiComp"].ToString();

            if (page == 0)
            {
                page = 1;
            }
            

            DataSet ds = null;
            try
            {

                using (var biz = new Roacket_Api_Biz())
                {
                    ds = biz.GetCompetitionList(searchWord == "" ? string.Empty : searchWord, regionCode, page, regiComp);
                }

            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "query error";

            }
            if (ds.Tables.Count == 0)
            {
                dic["success"] = false;
                dic["message"] = "데이터가 없습니다.";
            }
            DataTable dt = ds.Tables[0];
            //DataRow dr = ds.Tables[1].Rows[0];
            /*if (dt.Rows.Count == 0)
            {
                return "데이터가 없습니다";
            }
          */
            dic["success"] = true;
            dic["data"] = dt;
            //dic["total"] = dr["total"];

            return dic;
        }


        // GET: api/Competitions/5
        [Route("Competitions/{id}")]
        public object Get(int id)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetition(id);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt.Rows[0].DataRowToJson();
        }

        // POST: api/CompetitionRules
        [Route("Competitions")]
        public int Post([FromBody] Competitions.Competition cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetition(0,
                                             cc.competitionName, cc.typeCode, cc.gradeTypeCode,
                                             Convert.ToDateTime(cc.applicationStartDate),
                                             Convert.ToDateTime(cc.applicationEndDate),
                                             Convert.ToDateTime(cc.competitionStartDate),
                                             Convert.ToDateTime(cc.competitionEndDate),
                                             Convert.ToDateTime(cc.listDate),
                                             cc.applicationTeamsCode,
                                             cc.stadiumCodeSet,
                                             cc.stadiumArray,
                                             cc.adminIdx
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return 0;
        }


        // PUT: api/CompetitionRules/5
        [Route("Competitions/{id}")]
        public int Patch(int id, [FromBody] Competitions.Competition cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetition(id, cc.competitionName, cc.typeCode, cc.gradeTypeCode,
                            Convert.ToDateTime(cc.applicationStartDate), Convert.ToDateTime(cc.applicationEndDate),
                            Convert.ToDateTime(cc.competitionStartDate), Convert.ToDateTime(cc.competitionEndDate),
                            Convert.ToDateTime(cc.listDate), cc.applicationTeamsCode, cc.stadiumCodeSet,
                            cc.stadiumArray, cc.adminIdx
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

        /*

        #region 종목별 팀리스트 조회
        [HttpGet]
        [ActionName("GetCompetitionTeamsDetail")]
        public string GetCompetitionTeamsDetail([FromUri]Competitions.TeamsDetail ct)
        {
            
        }
        #endregion

        #region 리그 팀 리스트 조회
        [HttpGet]
        [ActionName("GetCompetitionLeagueSchedule")]
        public string GetCompetitionLeagueSchedule([FromUri]Competitions.TeamsDetail ct)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionLeagueSchedule(ct.pCOMP_IDX, ct.pGROUP_IDX);
                }
            }
            catch (Exception exp)
            {
                return exp.Message;
            }

            return JsonConvert.SerializeObject(dt);
        }
        #endregion
        */
    }
}