﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionEventsController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 대회 종목별 리스트 (statnco관리자 + 협회관리자 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 : 
        ///     
        ///       {
        ///         "competitionIdx" : "45"   
        ///        }
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>
        // GET: api/CompetitionEvents
        [Route("CompetitionEvents")]
        public object Get(string filter, [FromUri] int[] range = null)
        {
            //statnco 관리자만 가능
            int statnco = 18;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }
            JObject root = JObject.Parse(filter);

            int competitionIdx = Convert.ToInt32(root["competitionIdx"].ToString());

            int range1 = 0;
            int range2 = 0;
            if (range.Length < 1)
            {
                range1 = 0;
                range2 = 9;
            }
            else
            {
                range1 = range[0];
                range2 = range[1];
            }
            DataSet ds = null;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.fnGetCompetitionEventsList(competitionIdx, range1, range2);
            }


            if (ds.Tables.Count == 0)
            {
                return "데이터가 없습니다";
            }
            DataTable dt = ds.Tables[0];
            DataRow dr = ds.Tables[1].Rows[0];

            if (dt.Rows.Count == 0)
            {
                return "데이터가 없습니다";
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt;
            dic["total"] = dr["total"];

            return dic;

        }

        // POST: api/CompetitionEvents
        [Route("CompetitionEvents")]
        public int Post([FromBody] CompetitionEvents.Events ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompEvents(0, ce.competitionIdx, ce.order,
                        ce.eventCode, ce.ageCode, ce.gradeCode, ce.applicationTeamCount, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return 0;
        }

        // PUT: api/CompetitionEvents/5
        [Route("CompetitionEvents/{id}")]
        public int Patch(int id, [FromBody] CompetitionEvents.Events ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompEvents(id, ce.competitionIdx, ce.order,
                        ce.eventCode, ce.ageCode, ce.gradeCode, ce.applicationTeamCount, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

        // DELETE: api/CompetitionEvents/5
        [Route("CompetitionEvents")]
        public int Delete(CompetitionEvents.EventsDel ce)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.DelCompEvents(ce.competitionIdx, ce.eventIdxSet, ce.adminIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
