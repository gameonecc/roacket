﻿using Statnco.DA.Roacket.Api;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    [Route("LiveScoreCourtSchedule")]
    public class LiveScoreCourtScheduleController : ApiController
    {

        public DataSet Get(int competitionIdx, byte stadiumNo)
        {
            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetLiveScoreCourtSchedule(competitionIdx, stadiumNo);
            }

            return ds;
        }
    }
}
