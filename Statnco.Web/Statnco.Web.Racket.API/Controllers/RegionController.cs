﻿using Statnco.DA.Roacket.Api;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class RegionController : ApiController
    {
        [Route("Region")]
        public object Get()
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.fnGetRegionList();
            }

            Dictionary<string, object> dicReturn = new Dictionary<string, object>();
            dicReturn["data"] = dt;

            return dicReturn;
        }
    }
}
