﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Web.Http;
using WpfApp1.AligoSMSSample;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionAcceptController : ApiController
    {
        public static string jToken = string.Empty;
        /// <summary>
        ///  참가현황 그룹 보기 (statnco관리자 + 협회관리자 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 
        /// 예제 : 
        ///     
        ///     matchType이 1이면 teamPhone x   
        ///      {
        ///             "matchType": 1,
        ///             "eventCode": 0,
        ///             "ageCode": 0,
        ///             "gradeCode": 0,
        ///             "teamPhone": "string"
        ///             
        ///                                 }
        ///                                 
        ///       matchType이 2이면 teamPhone o  
        ///      {
        ///             "matchType": 2,
        ///             "eventCode": 0,
        ///             "ageCode": 0,
        ///             "gradeCode": 0,
        ///             "teamPhone": "string"
        ///             
        ///                                 }
        ///     
        /// </remarks>
        /// <returns></returns>
        [Route("Competitions/{id}/Accept")]
        public int Post([FromBody] Competitions.Accept ca, int id)
        {
            int rtn = 0;

            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            string name = clsJWT1.isValidToken(jToken).Payload["name"].ToString();
            var sms = new Curl_Send();
            int smsRtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    // 단식
                    if (String.IsNullOrWhiteSpace(ca.teamPhone))
                    {
                        rtn = biz.SetCompetitonAccept(id, memberIdx, ca.eventCode, ca.ageCode, ca.gradeCode);

                        if (rtn > 0)
                        {
                            rtn = 0;
                        }
                        else
                        {
                            rtn = -1;
                        }
                    }
                    else //복식
                    {
                        rtn = biz.SetCompetitionAcceptWait(id, memberIdx, ca.eventCode, ca.ageCode, ca.gradeCode);

                        if (rtn > 0)
                        {
                            // 전화번호 문자 전송
                            // 핸드폰번호로 회원 정보 조회
                            int ot_mb_idx = biz.GetMemberForPhone(ca.teamPhone);
                            string compName = biz.GetCompetition(id).Rows[0][1].ToString();
                            string acceptNum = rtn.ToString();
                            string secret = Crypto.Base64Encode(memberIdx + "_" + compName + "_" + acceptNum + "statnco");

                            smsRtn = sms.Curl_SendEx(name, compName, ca.teamPhone, secret);
                            rtn = 0;
                        }

                    }
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}