﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CommonCodeController : ApiController
    {
        // GET: api/CommonCode
        [Route("CommonCode")]
        public DataTable Get(string division)
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.GetComCodeList(division);
            }

            return dt;
        }

        [Route("CommonCode")]
        public int Post([FromBody] CommonCodes.CommonCode cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetComCode(cc.division, cc.code, cc.codeName);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
