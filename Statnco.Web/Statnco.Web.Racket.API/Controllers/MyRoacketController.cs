﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MyRoacketController : ApiController
    {
        public static string jToken = String.Empty;

        /// <remarks>
        /// 토큰만 넘기면 데이터 추출
        /// 테스트 계정 : apptest / a12345678
        /// </remarks>
        /// <summary>
        /// 마이 롸켓 > 나의 클럽
        /// </summary>
        /// <returns></returns>
   
        [Route("MyRoacket/club")]
        public object Get()
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["id"]);

            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetMyRoacketClub(memberIdx);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }
            if (dt.Rows.Count == 0)
            {

                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }


            dic["success"] = true;
            dic["data"] = dt.Rows[0].DataRowToJson();

            return dic;
        }

        /// <summary>
        /// 대회참가내역
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 : 공백일 경우 전체검색
        ///     
        ///      competitionIdx
        ///      desc
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>

 
        [Route("MyRoacket/Participation")]
        public object Get([FromUri] string[] sort = null)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["id"]);

         
            string sort1 = "";
            string sort2 = "";

            if (sort.Length < 1)
            {
                sort1 = "competitionIdx ";
                sort2 = "DESC";
            }
            else
            {
                sort1 = sort[0];
                sort2 = sort[1];
            }

            DataTable dt = null;
         

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetMyRoacketParticipation(memberIdx,sort1,sort2);
                }
            }
            catch (Exception exp)
            {

                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            dt.Columns.Add(new DataColumn("competitonState", typeof(string)));
            dt.Columns.Add(new DataColumn("registerGrade", typeof(string)));
            dt.Columns.Add(new DataColumn("registerGradeDetatil", typeof(string)));




            if (dt.Rows.Count > 0)
               {
                   for(var i=0; i<dt.Rows.Count;i++)
                   {

                    // [i][0] : no  [i][1] : competitionName
                    // [i][2] : applicationSrtDate  [i][3] : applicationEndDate
                    // [i][4] : str Date  [i][5] :endDate
                    // [i][6] :eventCode

                    DateTime today = DateTime.Today;

                    if (Convert.ToDateTime(dt.Rows[i][3]) > today ) {
                        dt.Rows[i]["competitonState"] =
                            Convert.ToDateTime(dt.Rows[i][3]) > today ? "접수중" : "접수마감";
                        dt.Rows[i]["registerGrade"] = "접수현황";
                        dt.Rows[i]["registerGradeDetatil"] = "참자확정";
                    }
                    else
                    {
                        dt.Rows[i]["competitonState"] =
                          Convert.ToDateTime(dt.Rows[i][5]) > today ? "대회중" : "대회종료";
                        dt.Rows[i]["registerGrade"] = "나의 성적";
                        dt.Rows[i]["registerGradeDetatil"] = dt.Rows[i][6]+"";
                    }

                }
               }

            if (dt.Rows.Count == 0)
            {

                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }

            dic["success"] = true;
            dic["data"] = dt;


            return dic;
        }

    }
}
