﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{

    public class StadiumCourtsController : ApiController
    {
        // PUT: api/StadiumCourts/5
        [Route("StadiumCourts")]
        public int Patch([FromBody] Stadiums.CourtCnt cc)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetitionStadiumCourtCnt(
                            cc.competitionIdx, cc.stadiumNo, cc.courtCount
                        );
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

    }
}
