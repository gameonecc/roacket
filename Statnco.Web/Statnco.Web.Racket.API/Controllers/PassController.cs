﻿using Statnco.DA.Roacket.Api;
using System;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class PassController : ApiController
    {
        //비밀번호 재확인
        [Route("Pass")]
        public int Get(int idx, String password)
        {
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CheckPassword(idx, password);
            }
            return rtn;
        }
    }
}
