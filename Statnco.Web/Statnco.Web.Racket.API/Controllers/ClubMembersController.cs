﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class ClubMembersController : ApiController
    {
        public static string jToken = String.Empty;
        //클럽가입신청
        /// <summary>
        /// 클럽가입신청 (로그인 필수)
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        ///    
        ///      {
        ///         "clubIdx" : ""
        ///      }
        ///      
        ///            
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        /// 
        [Route("ClubMember")]
        public object Post([FromBody]ClubMember cm)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰 필요";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.ClubJoinReq(cm.clubIdx, memberIdx);
            }
            if(rtn == -1)
            {
                dic["success"] = false;
            }else if (rtn == 0)
            {
                dic["success"] = true;

            }

            return dic;
        }
        /// <summary>
        /// 협회에 등록된 클럽 회원 조회 API
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        ///    
        ///      {
        ///         "clubIdx" : ""
        ///      }
        ///      
        ///            
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        /// 

        //클럽회원조회
        [Route("Clubs/{id}/Members")]
        public object Get([FromUri] int id, string filter = null, [FromUri] int[] range = null, [FromUri] string[] sort = null)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();
            if (memberIdx != statnco && manager != "manager")
            {
                return "관리자만 접근 가능합니다.";

            }
            string opt;
            string content;
            int range1;
            int range2;
            string sort1 = "";
            string sort2 = "";

            if (filter == null)
            {
                opt = "userId";
                content = string.Empty;

            }
            else
            {
                JObject root = JObject.Parse(filter);
                JObject search = (JObject)root["search"];
                opt = search["option"].ToString();
                content = search["content"].ToString() == "" ? string.Empty : search["content"].ToString();

            }
            if (range.Length < 1)
            {
                range1 = 0;
                range2 = 9;
            }
            else
            {
                range1 = range[0];
                range2 = range[1];
            }

            if (sort.Length < 1)
            {
                sort1 = "userId ";
                sort2 = "DESC";
            }
            else
            {
                sort1 = sort[0];
                sort2 = sort[1];
            }


            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.ClubMemberList(id, opt, content, range1, range2, sort1, sort2);
            }
            if (ds.Tables.Count == 0)
            {
                return "데이터가 없습니다";
            }
            DataTable dt = ds.Tables[0];
            DataRow dr = ds.Tables[1].Rows[0];

            if (dt.Rows.Count == 0)
            {
                return "데이터가 없습니다";
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt;
            dic["total"] = dr["total"];

            return dic;

        }
        /// <summary>
        /// 클럽원정보수정(협회관리자, statnco 관리자만 사용 가능)
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///     {
        ///       "memberIdx" : 7,
        ///       "si" : "A",
        ///       "gu" : "-"
        ///      }
        ///      
        ///   
        /// </remarks>
        /// <returns></returns>
        /// 
        //클럽원정보수정
        //[Route("ClubMember")]
        //public object Patch([FromBody] ClubMember cm)
        //{
        //    //statnco 관리자만 가능
        //    int statnco = 18;
        //    //token 정보 조회
        //    jToken = Request.Headers.Authorization.ToString();
        //    if (jToken == null)
        //    {
        //        return "회원정보가 필요합니다.";
        //    }
        //    int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
        //    string manager = clsJWT.isValidToken(jToken).Payload["manager"].ToString();

        //    if (memberIdx != statnco && manager != "manager")
        //    {
        //        return "관리자만 접근 가능합니다.";

        //    }

        //    int rtn = 0;

        //    string si = cm.si == "" ? "-" : cm.si;
        //    string gu = cm.gu == "" ? "-" : cm.gu;

        //    using (var biz = new Roacket_Api_Biz())
        //    {
        //        rtn = biz.ClubMemberEdit(cm.memberIdx, si, gu, memberIdx);
        //    }

        //    return rtn;
        //}


    }

}
