﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class ClubAdminsController : ApiController
    {
        public static string jToken = String.Empty;
        ///<summary>
        /// 가입대기 리스트
        ///</summary>
        /// <remarks>
        /// 예제 입니다:
        ///     
        ///    idx = 2     클럽인덱스
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        public object Get(int idx)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰 필요";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;
            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CheckMaster(memberIdx,idx);
            }

            if (rtn == -1)
            {
                dic["success"] = false;
                dic["message"] = "권한이없습니다";
                return dic;
            }
            DataSet ds = null;

            using (var biz2 = new Roacket_Api_Biz())
            {
                ds = biz2.GetWaitList(idx);
            }

            DataTable dt = ds.Tables[0];

            if(dt.Rows.Count > 0)
            {
                dic["success"] = true;
                dic["data"] = dt;
            }
            else
            {
                dic["success"] = false;
                dic["message"] = "데이터가 없습니다";
            }

            return dic;
        }
        //등업처리
        /// <summary>
        /// 등업처리 (클럽장만 사용 가능)
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///   {
        ///    "clubIdx" : 1,
        ///    "grade" : 10,
        ///    "user" : 18
        ///    }
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        [Route("ClubAdmins")]
        public object Patch(ClubAdmin ca)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰 필요";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;
            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CheckMaster(memberIdx, ca.clubIdx);
            }

            if(rtn == -1)
            {
                dic["success"] = false;
                dic["message"] = "권한이없습니다";
                return dic;
            }
            int rtn2 = 0;
            using (var biz2 = new Roacket_Api_Biz())
            {
                rtn2 = biz2.MemberGradeModify(memberIdx, ca.clubIdx, ca.clubIdx, ca.grade);
            }
            if(rtn2 == -1)
            {
                dic["success"] = false;
            }
            dic["success"] = true;

            return dic;
        }

        // 가입거절
        /// <summary>
        /// 클럽가입거절 (클럽장만 사용 가능)
        /// </summary>
        /// <remarks>
        /// 예제 입니다:
        /// 
        ///   {
        ///    "clubIdx" : 1,
        ///    "user" : 18
        ///    }
        ///  
        ///   
        /// </remarks>
        /// <returns></returns>
        [Route("ClubAdmins")]
        public object Delete([FromBody]ClubAdmin.Refuse cr)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            if (Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰 필요";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            int memberIdx = Convert.ToInt32(clsJWT1.isValidToken(jToken).Payload["id"]);
            int rtn = 0;
            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.CheckMaster(memberIdx, cr.clubIdx);
            }

            if (rtn == -1)
            {
                dic["success"] = false;
                dic["message"] = "권한이없습니다";
                return dic;
            }
            int rtn2 = 0;
            using(var biz2 = new Roacket_Api_Biz())
            {
                rtn2 = biz2.ClubJoinRefuse(memberIdx, cr.clubIdx, cr.user);
            }
            if (rtn2 == -1)
            {
                dic["success"] = false;
            }
            dic["success"] = true;

            return dic;
        }

    }

}
