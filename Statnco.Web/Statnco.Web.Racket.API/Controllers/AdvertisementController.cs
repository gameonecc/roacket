﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AdvertisementController : ApiController
    {
        public static string jToken = String.Empty;
        [Route("Advertisement")]
        public object Get(string locationCode)
        {   
            DataTable dt = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetAdvertisementWithLocation(locationCode);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }
            if(dt.Rows.Count == 0)
            {
                dic["success"] = false;
                dic["message"] = "데이터가 없습니다";
            }
            dic["success"] = true;
            dic["data"] = dt;
            return dic;
        }

        /// <summary>
        /// 광고 등록 (statnco 관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        ///     예제 : 
        ///     
        ///         {
        ///             "title" : "test1",
        ///             "locationCode": "main",
        ///             "linkUrl" : "www.test.com",
        ///             "srtDate" : "2022-02-25T06:38:30.248Z",
        ///             "endDate" : "2022-02-26T06:38:30.248Z",
        ///             "imgPath" : "dkk3jksnaf"
        ///         }
        /// </remarks>
        /// <returns></returns>
        [Route("Advertisement")]
        public object Post([FromBody]Advertisement adv)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            
            if(Request.Headers.Authorization == null)
            {
                dic["success"] = false;
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            string type = clsJWT.isValidToken(jToken).Payload["memberType"].ToString();
            if(type != "S")
            {
                dic["success"] = false;
                dic["message"] = "권한이 없습니다";
                return dic;
            }

            int rtn = 0;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetAdvertisement(adv.title,adv.locationCode, adv.linkUrl,
                                                Convert.ToDateTime(adv.srtDate), Convert.ToDateTime(adv.endDate),adv.imgPath);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            if (rtn == 0)
            {
                dic["success"] = true;
                dic["message"] = "처리완료";
            }
            else
            {
                dic["success"] = true;
                dic["message"] = "query error";
                dic["error_no"] = rtn;
            }
            return dic;
        }

        [Route("Advertisement/{id}")]
        public object Patch(int id, [FromBody] Advertisement adv)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["result"] = "fail";
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            string type = clsJWT.isValidToken(jToken).Payload["memberType"].ToString();
            if (type != "S")
            {
                dic["result"] = "fail";
                dic["message"] = "권한이 없습니다";
                return dic;
            }

            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.EditAdvertisement(id, adv.title, adv.locationCode, adv.linkUrl,
                                                 Convert.ToDateTime(adv.srtDate), Convert.ToDateTime(adv.endDate), adv.imgPath
                                                );
                }
            }
            catch (Exception exp)
            {
                dic["result"] = "error";
                dic["message"] = "Exception";
                return dic;
            }

            if (rtn == 0)
            {
                dic["result"] = "success";
                dic["message"] = "처리완료";
            }
            else
            {
                dic["result"] = "fail";
                dic["message"] = "query error";
                dic["error_no"] = rtn;
            }
            return dic;
        }

        [Route("Advertisement{id}")]
        public object Delete(int id)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (Request.Headers.Authorization == null)
            {
                dic["result"] = "fail";
                dic["message"] = "토큰이 없습니다";
                return dic;
            }
            jToken = Request.Headers.Authorization.ToString();
            string type = clsJWT.isValidToken(jToken).Payload["memberType"].ToString();
            if (type != "S")
            {
                dic["result"] = "fail";
                dic["message"] = "권한이 없습니다";
                return dic;
            }
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.DelAdvertisement(id);
                }
            }
             catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            if (rtn == 0)
            {
                dic["success"] = true;
                dic["message"] = "처리완료";
            }
            else
            {
                dic["success"] = false;
                dic["message"] = "query error";
                dic["error_no"] = rtn;
            }
            return dic;
        }



    }
}
