﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class IdCheckController : ApiController
    {
        [Route("IdCheck")]
        public object Post([FromBody] IdCheck ic)
        {
            int rtn = 0;
            Dictionary<string, object> dic = new Dictionary<string, object>();

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.IdCheck(ic.id);
                }
            }
            catch (Exception exp)
            {
                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }
            if (rtn != 0)
            {
                dic["success"] = true;
                dic["message"] = "사용 가능한 아이디입니다.";
            }else if(rtn == 0)
            {
                dic["success"] = false;
                dic["message"] = "중복된 아이디입니다.";
            }

            return dic;
        }
    }
}
