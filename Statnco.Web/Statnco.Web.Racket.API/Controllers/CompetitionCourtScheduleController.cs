﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionCourtScheduleController : ApiController
    {
        /// <summary>
        /// 경기장 스케줄
        /// </summary>
        /// <remarks>
        /// 
        ///       {
        ///         "competitionIdx" : "45",
        ///         "stadiumNo" : "1"
        ///        }
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>
        [Route("CompetitionCourtSchedule")]
        public DataTable Get(string filter)
        {
            JObject root = JObject.Parse(filter);

            int competitionIdx = Convert.ToInt32(root["competitionIdx"].ToString());
            byte stadiumNo = Convert.ToByte(root["stadiumNo"].ToString());

            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.GetCourtSchedule(competitionIdx, stadiumNo);
            }

            return dt;
        }


        [Route("CompetitionCourtSchedule/{id}")]
        public DataSet Get(int id, int competitionIdx, int groupIdx)
        {
            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.GetCourtScheduleDetail(competitionIdx, groupIdx, id);
            }

            return ds;
        }
    }
}
