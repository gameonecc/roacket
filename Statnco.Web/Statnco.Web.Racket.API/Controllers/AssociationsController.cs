﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Jwt;
using Statnco.Web.Roacket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AssociationsController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 협회  생성 (statnco관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 :
        ///    
        ///       {
        ///         "regionCode": 0,    (시도협회)
        ///         "areaCode": 0,      (시군구 협회   시도협회일 경우는 0으로 넣어주세요)
        ///         "memberIdx": 0,     (협회 관리자 memberIdx)
        ///        }
        ///         
        ///     
        /// </remarks>
        /// <returns></returns>
        //협회생성
        [Route("Associations")]
        public object Post([FromBody] Association asso)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            int dt = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.AssociationCreate(asso.regionCode, asso.areaCode, asso.memberIdx, statnco);

            }

            return dt;
        }
        /// <summary>
        /// 협회 검색 (statnco관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 :
        ///    
        ///       {
        ///         "si" : "14",
        ///         "gu" : "13"
        ///         
        ///         
        ///        }
        ///         
        ///     
        /// </remarks>
        /// <returns></returns>
        //협회 검색
        [Route("Associations")]
        public object Get(string filter = null)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);
            int si = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["region"]);
            int gu = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["area"]);


            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }

            if (memberIdx == statnco)
            {
                if (filter != null)
                {
                    JObject root = JObject.Parse(filter);

                    si = root["si"].ToString() == "" ? 0 : Convert.ToInt32(root["si"].ToString());
                    gu = root["gu"].ToString() == "" ? 0 : Convert.ToInt32(root["gu"].ToString());

                }
                else
                {
                    si = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["region"]);
                    gu = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["area"]);
                }

            }

            DataSet ds = null;

            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.AssociationList(si, gu);
            }
            DataTable dt = ds.Tables[0];

            Dictionary<string, object> dic = new Dictionary<string, object>();

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            dic["data"] = dt;

            return dic;

        }
        /// <summary>
        /// 협회 상세조회 (statnco관리자만 가능)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //협회 상세조회
        [Route("Associations/{id}")]
        public object Get(int id)
        {
            //statnco 관리자만 가능
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            DataSet ds = null;


            using (var biz = new Roacket_Api_Biz())
            {
                ds = biz.AssociationDetail(id);
            }
            DataTable dt = ds.Tables[0];

            if (dt.Rows.Count == 0)
            {
                return null;
            }

            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt.Rows[0].DataRowToJson();

            return dic;

        }
        /// <summary>
        /// 협회수정 (statnco관리자만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 :
        ///    
        ///       {
        ///         "memberIdx": 0,     (협회 관리자 memberIdx)
        ///        }
        ///         
        ///     
        /// </remarks>
        /// <returns></returns>
        //협회 수정
        [Route("Associations")]
        public object Patch([FromBody] AssociationEdit ae)
        {
            int statnco = 5;
            //token 정보 조회
            jToken = Request.Headers.Authorization.ToString();
            if (jToken == null)
            {
                return "회원정보가 필요합니다.";
            }
            int memberIdx = Convert.ToInt32(clsJWT.isValidToken(jToken).Payload["idx"]);

            if (memberIdx != statnco)
            {
                return "statnco 관리자만 확인 가능합니다.";

            }
            int rtn = 0;

            using (var biz = new Roacket_Api_Biz())
            {
                rtn = biz.SetAssociation(ae.assIdx, ae.memberIdx, statnco);
            }

            return rtn;
        }

    }
}
