﻿using Statnco.DA.Roacket.Api;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumRegionsController : ApiController
    {
        // GET: api/Region
        [Route("StadiumRegions")]
        public DataTable Get()
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.fnGetRegionList();
            }

            return dt;
        }

    }
}
