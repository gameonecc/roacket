﻿using Statnco.DA.Roacket.Api;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    [Route("StadiumAreas")]
    public class StadiumAreasController : ApiController
    {
        public DataTable Get(string regionCode)
        {
            DataTable dt = null;

            using (var biz = new Roacket_Api_Biz())
            {
                dt = biz.fnGetRegionList();
            }

            return dt;
        }
    }
}
