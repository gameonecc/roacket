﻿using Statnco.DA.Roacket.Api;
using Statnco.FW.Util;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class MinorController : ApiController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// 
        ///     예제 :
        ///     
        ///         {
        ///             "id" : "test567",
        ///             "pw" : "dlwnsgus12",
        ///             "name" : "name",
        ///             "gender" : "M",
        ///             "birth" : "1900-01-01",
        ///             "phone" : "0102345679",
        ///             "DI" : "d3ifn!andmflk3dna%@#!#",
        ///             "parentName" : "이이이",
        ///             "parentGender" : "M",
        ///             "parentBirth" : "1905-02-40",
        ///             "parentPhone" : "010232232323",
        ///             "isAss" : 0,
        ///             "region" : 14,
        ///             "area"  : 12,
        ///             "clubIdx" : 0
        ///          }
        /// </remarks>
        /// <returns></returns>
        [Route("Minor")]
        public object Post([FromBody] Minor mi)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetMemberParent(mi.id, Crypto.fnGetSHA512(mi.pw), mi.name, mi.phone,
                                              mi.gender, mi.birth, mi.DI, mi.isAss,
                                              mi.region, mi.area, mi.clubIdx, mi.parentName, mi.parentGender, mi.parentBirth,
                                              mi.parentPhone);
                }
            }
            catch (Exception exp)
            {

                return exp;
            }

            Dictionary<object, string> dic = new Dictionary<object, string>();

            if (rtn == 0)
            {
                dic["result"] = "success";
            }
            else
            {
                dic["result"] = "fail";
                dic["error_no"] = rtn.ToString();
            }

            return dic;

        }
    }
}
