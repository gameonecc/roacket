﻿using Statnco.DA.Roacket.Api;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class AdvertisementLoginController : ApiController
    {

    [Route("AdverLogin")]
    public object Get( int id)
        {
            DataTable dt = null;
            Dictionary<string, object> dic = new Dictionary<string, object>();

            try
            {
                 using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.AdvertisementLogin(id);
                }
            }
            catch (Exception exp)
            {

                dic["success"] = false;
                dic["message"] = "Exception";
                return dic;
            }

            if (dt.Rows.Count == 0)
            {

                dic["success"] = false;
                dic["message"] = "query error";
                return dic;
            }


            dic["success"] = true;
            dic["data"] = dt;


            return dic;

        }
    }
}
