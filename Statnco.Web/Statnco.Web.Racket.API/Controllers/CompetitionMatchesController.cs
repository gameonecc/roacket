﻿using Newtonsoft.Json.Linq;
using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionMatchesController : ApiController
    {

        /// <summary>
        /// 경기장 스케줄
        /// </summary>
        /// <remarks>
        /// 
        ///       { 
        ///         "type" : "l",
        ///         "competitionIdx" : "45",
        ///         "groupIdx" : "1"
        ///        }
        ///        
        ///     
        /// </remarks>
        /// <returns></returns>

        [Route("CompetitionMatches")]
        public object Get(string filter)
        {

            JObject root = JObject.Parse(filter);

            string type = root["type"].ToString();
            int competitionIdx = Convert.ToInt32(root["competitionIdx"].ToString());
            int groupIdx = Convert.ToInt32(root["groupIdx"].ToString());

            DataTable dt = null;

            var compRoot = new CompetitionMatches.Root();
            List<object> li = new List<object>();
            string[] a = null;
            string[] c = null;

            Dictionary<string, object> dic = new Dictionary<string, object>();

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionMatches(type, competitionIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw null;
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                compRoot.matchIdx = Convert.ToInt32(dt.Rows[i][0]);
                compRoot.parentMatchIdx1 = Convert.ToInt32(dt.Rows[i][1]);
                compRoot.parentMatchIdx2 = Convert.ToInt32(dt.Rows[i][2]);
                compRoot.teamNo1 = Convert.ToInt32(dt.Rows[i][3]);

                string club1 = "";
                string name1 = "";
                string club2 = "";
                string name2 = "";
                if (dt.Rows[i][4].ToString() != "") // userinfo1 값이 있을때
                {


                    // | 있으면
                    if (dt.Rows[i][4].ToString().Contains("|"))
                    {
                        a = dt.Rows[i][4].ToString().Split('|');
                        club1 = a[0].Split('_')[0].ToString();
                        name1 = a[0].Split('_')[1].ToString();
                        club2 = a[1].Split('_')[0].ToString();
                        name2 = a[1].Split('_')[1].ToString();
                    }
                    else // | 없을때
                    {
                        a[0] = dt.Rows[i][4].ToString();
                        a[1] = "";
                        club1 = a[0].Split('_')[0].ToString();
                        name1 = a[0].Split('_')[1].ToString();
                        club2 = a[1];
                        name2 = a[1];
                    }

                    compRoot.userinfo1 = new List<CompetitionMatches.Userinfo1>
                    {
                        new CompetitionMatches.Userinfo1{club = club1, name = name1 },
                        new CompetitionMatches.Userinfo1{club = club2, name = name2 },
                    };

                }
                else // userinfo1 값이 없을때
                {
                    compRoot.userinfo1 = new List<CompetitionMatches.Userinfo1>
                    {
                        new CompetitionMatches.Userinfo1{club = club1, name = name1 },
                        new CompetitionMatches.Userinfo1{club = club2, name = name2 },
                    };
                }


                if (dt.Rows[i][6].ToString() != "") // userinfo2 값이 있을때
                {


                    // | 있으면
                    if (dt.Rows[i][6].ToString().Contains("|"))
                    {
                        c = dt.Rows[i][6].ToString().Split('|');
                        club1 = c[0].Split('_')[0].ToString();
                        name1 = c[0].Split('_')[1].ToString();
                        club2 = c[1].Split('_')[0].ToString();
                        name2 = c[1].Split('_')[1].ToString();
                    }
                    else // | 없을때
                    {
                        c[0] = dt.Rows[i][6].ToString();
                        c[1] = "";
                        club1 = c[0].Split('_')[0].ToString();
                        name1 = c[0].Split('_')[1].ToString();
                        club2 = c[1];
                        name2 = c[1];
                    }

                    compRoot.userinfo2 = new List<CompetitionMatches.Userinfo2>
                    {
                        new CompetitionMatches.Userinfo2{club = club1, name = name1 },
                        new CompetitionMatches.Userinfo2{club = club2, name = name2 },
                    };

                }
                else // userinfo2 값이 없을때
                {
                    compRoot.userinfo2 = new List<CompetitionMatches.Userinfo2>
                    {
                        new CompetitionMatches.Userinfo2{club = club1, name = name1 },
                        new CompetitionMatches.Userinfo2{club = club2, name = name2 },
                    };
                }

                li.Add(compRoot);
                compRoot = new CompetitionMatches.Root();

            }

            Dictionary<string, object> di = new Dictionary<string, object>();
            di["data"] = li.ToArray();

            return di;

        }

        // GET: api/CompetitionMatches/5
        [Route("CompetitionMatches")]
        public int Post(int competitionIdx, int groupIdx)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.InitMatches(competitionIdx, groupIdx);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn;
        }


    }
}
