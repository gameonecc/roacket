﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class StadiumScheduleController : ApiController
    {
        [Route("StadiumSchedule")]
        public DataTable Get(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionStadiumSchedule(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt;
        }

        // PUT: api/StadiumSchedule/5
        [Route("StadiumSchedule")]
        public int Patch([FromBody] Stadiums.StadiumSchedule css)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.SetCompetitionStadiumSchedule(
                        css.competitionIdx, css.stadiumNo, css.timeType, css.time);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
