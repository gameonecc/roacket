﻿using Statnco.DA.Roacket.Api;
using Statnco.Web.Racket.API.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Http;

namespace Statnco.Web.Racket.API.Controllers
{
    public class CompetitionBoardsController : ApiController
    {
        public static string jToken = String.Empty;
        /// <summary>
        /// 대회게시판 조회 CRUD API (공지 작성은 협회관리자, statnco 계정만 가능)
        /// </summary>
        /// <remarks>
        /// 
        /// 예제 입니다.:
        /// 
        ///     {
        ///         "idx": 1,
        ///         "competitionIdx": 64,
        ///         "boardTitle": "공지사항",
        ///         "compType": "list",
        ///         "title": "test",
        ///         "contents": "test1",
        ///         "memberIdx": 123,
        ///         "regdate": "1900-01-01T00:00:00",
        ///         "editDate": "1900-01-01T00:00:00",
        ///         "editMember": 123
        ///     }
        /// </remarks>
        /// <returns></returns>
        [Route("CompetiotionBoard")]
        //대회게시판 리스트 조회
        public object Get()
        {

            DataTable dt = null;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionBoardsList();

                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic["data"] = dt;



            return dic;

        }

        //대회게시판 상세 조회
        [Route("CompetiotionBoard/{id}")]
        public object Get(int id)
        {

            DataTable dt = null;
            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    dt = biz.GetCompetitionBoardDetail(id);

                }
            }
            catch (Exception exp)
            {

                throw exp;
            }

            if (dt.Rows.Count == 0)
            {
                return null;
            }
            else
            {
                Dictionary<string, object> dic = new Dictionary<string, object>();
                dic["data"] = dt;

                return dic;
            }

        }

        //대회게시판 게시글 생성
        [Route("CompetiotionBoard")]
        public int Post([FromBody] CompetitionBoards cb)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.CreateCompetitionBoard(0, cb.boardTitle, cb.compType, cb.title,
                                                    cb.contents, cb.memberIdx);
                }

            }
            catch (Exception exp)
            {
                return -1;
            }


            return 0;
        }


        //대회게시판 게시글 수정
        [Route("CompetiotionBoard/{id}")]
        public int Patch(int id, [FromBody] CompetitionBoards cb)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.ModifyCompetitionBoard(id,
                                                     cb.title,
                                                     cb.contents,
                                                     Convert.ToDateTime(cb.editDate),
                                                     cb.isActive,
                                                     cb.isDel,
                                                     cb.memberIdx);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }

        //대회게시판 게시글 삭제
        [Route("CompetiotionBoard/{id}")]
        public int Delete(int id)
        {
            int rtn = 0;

            try
            {
                using (var biz = new Roacket_Api_Biz())
                {
                    rtn = biz.DeleteCompetitionBoard(id);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
    }
}
