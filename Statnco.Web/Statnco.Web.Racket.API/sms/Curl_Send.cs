﻿using System;
using System.Net.Http;

namespace WpfApp1.AligoSMSSample
{
    public class Curl_Send
    {
        public Curl_Send()
        { }
        public int Curl_SendEx(string name, string compName, string phone, string secret)
        {
            /**************** 문자전송하기 예제 ******************/
            /* "result_code":결과코드,"message":결과문구, */
            /* "msg_id":메세지ID,"error_cnt":에러갯수,"success_cnt":성공갯수 */
            /* 동일내용 > 전송용 입니다.  
            /******************** 인증정보 ********************/
            //문자보내기

            int rtn = 0;
            try
            {


                using (HttpClient client = new HttpClient())
                {
                    MultipartFormDataContent formData = new MultipartFormDataContent();
                    formData.Add(new StringContent("leah"), "user_id");                                             //SMS 아이디
                    formData.Add(new StringContent("h8fn51m5aiw6n517efzdmvu2hpgxkoin"), "key");                     //인증키
                    formData.Add(new StringContent("[롸켓]" + name + "님이 " + compName + " 복식참가신청을 하였습니다.\n" +
                                                    "수락하시겠습니까?\n" +
                                                    " https://www.roacket.com/double.aspx?agree=" + secret), "msg");
                    //formData.Add(new StringContent(""), "msg");                                     // 메세지 내용
                    //formData.Add(new StringContent("https://www.roacket.com/double.aspx?agree="+secret), "msg");                                                 // 링크
                    formData.Add(new StringContent(phone), "receiver");                                              // 수신번호
                    //formData.Add(new StringContent("01111111111|담당자,01111111112|홍길동"), "destination");        // 수신인 %고객명% 치환      
                    //formData.Add(new StringContent(""), "sender");                                                  // 발신번호
                    //formData.Add(new StringContent(""), "rdate");                                                   // 예약일자 - 20161004 : 2016-10-04일기준
                    //formData.Add(new StringContent(""), "rtime");                                                   // 예약시간 - 1930 : 오후 7시30분
                    formData.Add(new StringContent("N"), "testmode_yn");                                            // Y 인경우 실제문자 전송X , 자동취소(환불) 처리
                    //formData.Add(new StringContent("제목입력"), "title");                                           //  LMS, MMS 제목 (미입력시 본문중 44Byte 또는 엔터 구분자 첫라인)

                    client.DefaultRequestHeaders.Add("Accept", "*/*");

                    var response = client.PostAsync("https://apis.aligo.in/send/", formData).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        rtn = -1;
                    }
                    else
                    {
                        var content = response.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                        rtn = 0;
                    }
                }
            }
            catch (Exception ex)
            { Console.WriteLine(ex.Message); }

            return rtn;
        }
    }
}
