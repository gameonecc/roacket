﻿namespace Statnco.Web.Roacket.API.Models
{
    public class Association
    {
        public int regionCode { get; set; }
        public int areaCode { get; set; }
        public int memberIdx { get; set; }

    }

    public class AssociationEdit
    {
        public int assIdx { get; set; }
        public int memberIdx { get; set; }

    }


    public class AssociationMemberEdit
    {
        public int memberIdx { get; set; }
        public string userId { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public string birth { get; set; }
        public string phone { get; set; }
        public int associationId { get; set; }
    }

    public class AssociationMemberDel
    {
        public int memberIdx { get; set; }
    }

    public class AssociationMemberAdd
    {
        public string signupType { get; set; }
        public string userId { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string gender { get; set; }
        public string birth { get; set; }
        public string phone { get; set; }
        public int regionCode { get; set; }
        public int areaCode { get; set; }
        public int assId { get; set; }
        public int memberIdx { get; set; }
    }
}