﻿namespace Statnco.Web.Racket.API.Models
{
    public class MenuPower
    {
        public int menuIdx { get; set; }
        public int menuSubIdx { get; set; }
        public int memberIdx { get; set; }
        public string power { get; set; }
        public int adminIdx { get; set; }
    }
}