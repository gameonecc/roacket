﻿using System.Collections.Generic;

namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionMatches
    {
        public class SwapInfo
        {
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
            public int matchIdx1 { get; set; }
            public int matchIdx2 { get; set; }
            public byte team1Num { get; set; }
            public byte team2Num { get; set; }
        }

        public class Userinfo1
        {
            public string club { get; set; }
            public string name { get; set; }
        }

        public class Userinfo2
        {
            public string club { get; set; }
            public string name { get; set; }
        }

        public class Root
        {
            public int matchIdx { get; set; }
            public int parentMatchIdx1 { get; set; }
            public int parentMatchIdx2 { get; set; }
            public int teamNo1 { get; set; }
            public List<Userinfo1> userinfo1 { get; set; }
            public int teamNo2 { get; set; }
            public List<Userinfo2> userinfo2 { get; set; }
        }


    }
}