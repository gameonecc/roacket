﻿namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionEvents
    {

        public class EventsSearch
        {
            public int competitionIdx { get; set; }
        }
        public class Events
        {
            public int competitionIdx { get; set; }
            public int order { get; set; }
            public byte eventCode { get; set; }
            public byte ageCode { get; set; }
            public byte gradeCode { get; set; }
            public byte applicationTeamCount { get; set; }
            public int adminIdx { get; set; }
        }
        public class EventsDel
        {
            public int competitionIdx { get; set; }
            public string eventIdxSet { get; set; }
            public int adminIdx { get; set; }
        }
    }
}