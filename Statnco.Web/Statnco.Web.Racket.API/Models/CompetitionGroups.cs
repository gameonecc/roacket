﻿namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionGroups
    {
        public class GroupList
        {
            public int competitionIdx { get; set; }
            public byte eventCode { get; set; }
            public byte ageCode { get; set; }
            public byte gradeCode { get; set; }
        }

        public class GroupConfig
        {
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
            public byte competitionType { get; set; }
            public byte leagueGroupCount { get; set; }
            public byte tournementMinRank { get; set; }

            public int stadiumNo { get; set; }
        }
    }
}