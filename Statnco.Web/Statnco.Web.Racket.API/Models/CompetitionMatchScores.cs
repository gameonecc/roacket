﻿using System;

namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionMatchScores
    {
        public class MatchScore
        {
            public int matchIdx { get; set; }
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
            public byte setNo { get; set; }
            public String team1Score1 { get; set; }
            public String team1Score2 { get; set; }
            public String team2Score1 { get; set; }
            public String team2Score2 { get; set; }
            public DateTime? setSrtTime { get; set; }
            public DateTime? setEndTime { get; set; }
            public int shuttleCockCnt { get; set; }

        }
    }
}