﻿using System;

namespace Statnco.Web.Racket.API.Models
{
    public class Advertisement
    {
        public string title { get; set; }
        public string locationCode { get; set; }
        public string linkUrl { get; set; }
        public string srtDate { get; set; }
        public string endDate { get; set; }
        public string imgPath { get; set; }
    }
}