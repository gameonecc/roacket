﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Statnco.Web.Racket.API.Models
{
    public class ConmpetitionParticipation
    {
        public string competitionName { get; set;}

        public string applicationStrDate { get; set; }

        public string applicationEndDate { get; set; }

        public string strDate { get; set; }

        public string endDate { get; set; }
    }
}