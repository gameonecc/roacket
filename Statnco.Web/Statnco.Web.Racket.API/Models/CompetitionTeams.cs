﻿namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionTeams
    {
        public class TeamList
        {
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
        }
    }
}