﻿namespace Statnco.Web.Racket.API.Models
{
    public class Terms
    {
        public string title { get; set; }
        public string contents { get; set; }
        public bool isUse { get; set; }
    }
}