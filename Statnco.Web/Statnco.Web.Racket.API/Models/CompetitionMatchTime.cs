﻿using System;

namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionMatchTime
    {
        public class MatchTime
        {
            public int competitionIdx { get; set; }
            public int groupIdx { get; set; }
            public int matchIdx { get; set; }
            public string timeType { get; set; }
            public DateTime setTime { get; set; }
        }
    }
}