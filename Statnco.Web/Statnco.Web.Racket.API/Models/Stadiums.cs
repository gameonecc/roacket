﻿using System;

namespace Statnco.Web.Racket.API.Models
{
    public class Stadiums
    {
        public class CourtCnt
        {
            public int competitionIdx { get; set; }
            public int stadiumNo { get; set; }
            public int courtCount { get; set; }
        }

        public class StadiumSchedule
        {
            public int competitionIdx { get; set; }
            public int stadiumNo { get; set; }
            public string timeType { get; set; }
            public DateTime time { get; set; }

        }
    }
}