﻿namespace Statnco.Web.Racket.API.Models
{
    public class Boards
    {
        public string type { get; set; }
        public int clubIdx { get; set; }
        public string title { get; set; }
        public string contents { get; set; }
        public string ph1 { get; set; }
        public string ph2 { get; set; }
        public string ph3 { get; set; }
        public string ph4 { get; set; }
        public string ph5 { get; set; }

        public class updateBoard
        {
            public string title { get; set; }
            public string contents { get; set; }
            public string ph1 { get; set; }
            public string ph2 { get; set; }
            public string ph3 { get; set; }
            public string ph4 { get; set; }
            public string ph5 { get; set; }

        }
    }
}