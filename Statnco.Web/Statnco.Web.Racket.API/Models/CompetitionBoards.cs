﻿using System;

namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionBoards
    {
        public int idx { get; set; }

        public int competitonIdx { get; set; }

        public string boardTitle { get; set; }

        public string compType { get; set; }

        public string title { get; set; }

        public string contents { get; set; }

        public int memberIdx { get; set; }

        public DateTime regDate { get; set; }

        public DateTime editDate { get; set; }

        public byte isActive { get; set; }

        public byte isDel { get; set; }

        public int editMemeber { get; set; }

    }
}