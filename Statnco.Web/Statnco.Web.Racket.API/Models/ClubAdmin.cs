﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Statnco.Web.Racket.API.Models
{
    public class ClubAdmin
    {
        public int clubIdx { get; set; }
        public int grade { get; set; }
        public int user { get; set; }

        public class Refuse
        {
            public int clubIdx { get; set; }
            public int user { get; set; }
        }

    }
}