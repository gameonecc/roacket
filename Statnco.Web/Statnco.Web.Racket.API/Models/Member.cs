﻿namespace Statnco.Web.Racket.API.Models
{
    public class MemberRegionArea
    {
        public string si { get; set; }
        public string gu { get; set; }
    }

    public class MemberPhoneEdit
    {

        public string password { get; set; }
        public string phone { get; set; }

    }

    public class MemberPasswordEdit
    {
        public string checkPw { get; set; }
        public string newPw { get; set; }
       
    }
}