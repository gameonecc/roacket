﻿namespace Statnco.Web.Racket.API.Models
{
    public class Clubs
    {
        public int clubIdx { get; set; }
        public string clubName { get; set; }
        public int clubMaster { get; set; }
    }
}