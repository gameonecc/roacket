﻿namespace Statnco.Web.Racket.API.Models
{
    public class CompetitionRules
    {
        public class Rules
        {
            public int competitionIdx { get; set; }
            public int useLiveScore { get; set; }
            public string rankRule { get; set; }
            public byte thirdRankRule { get; set; }

            public byte subWinGameCount { get; set; }
            public byte subWinScores { get; set; }
            public byte subMaxScores { get; set; }
            public byte subIsDeuce { get; set; }
            public byte mainWinGameCount { get; set; }
            public byte mainWinScores { get; set; }
            public byte mainMaxScores { get; set; }
            public byte mainIsDeuce { get; set; }
            public byte timePerMatch { get; set; }
        }
    }
}