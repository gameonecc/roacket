﻿namespace Statnco.Web.Racket.API.Models
{
    public class AdminLogin
    {
        public string id { get; set; }
        public string password { get; set; }
    }
}