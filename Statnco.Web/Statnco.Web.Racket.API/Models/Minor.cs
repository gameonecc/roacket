﻿namespace Statnco.Web.Racket.API.Models
{
    public class Minor
    {

        public string id { get; set; }
        public string pw { get; set; }
        public string name { get; set; }

        public string gender { get; set; }
        public string birth { get; set; }
        public string phone { get; set; }
        public string DI { get; set; }

        public string parentName { get; set; }
        public string parentGender { get; set; }
        public string parentBirth { get; set; }
        public string parentPhone { get; set; }

        public int isAss { get; set; }
        public int region { get; set; }
        public int area { get; set; }
        public int clubIdx { get; set; }



    }
}