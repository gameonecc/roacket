﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="club_2.aspx.cs" Inherits="Statnco.Web.Admin.club.club_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">클럽회원리스트</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <h3 class="club_name_title"></h3>

                <select class="name_select_box search_select_box select_box" name="" id="selOption" aria-placeholder="이름">
                    <option value="1">이름</option>
                    <option value="2">아이디</option>
                    <option value="3">전화번호</option>
                </select>

                <section class="search_box">
                    <input type="text" class="search_input" id="txtValue">
                    <button type="button" class="search_btn" onclick="fnGetClubMember();">Search</button>
                </section>
            </section>

            <ul class="table_box club_user_list_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_check">
                            <input type="checkbox" id="chkAll" onclick="fnChkAll();">
                        </li>
                        <li class="table_li table_li_short">idx</li>
                        <li class="table_li table_li_middle">아이디</li>
                        <li class="table_li table_li_middle">이름</li>
                        <li class="table_li table_li_short">성별</li>
                        <li class="table_li table_li_long">휴대폰</li>
                        <li class="table_li table_li_middle">생년월일</li>
                        <li class="table_li table_li_middle">소속 협회</li>
                        <li class="table_li table_li_short">시도 급수</li>
                        <li class="table_li table_li_middle">시군구급수</li>
                        <li class="table_li table_li_middle">회원구분</li>
                        <li class="table_li table_li_short">회비납부</li>
                    </ul>
                </li>
            </ul>

            <button type="button" class="payment_btn" onclick="fnSetClubMemberTax();">시군구협회 회비일괄납부</button>

            <section class="post_list_btn_box">
                <button type="button" class="pre_btn">Previous</button>
                <button type="button" class="num_btn">1</button>
                <button type="button" class="num_btn select_num_btn">2</button>
                <button type="button" class="num_btn">3</button>
                <button type="button" class="next_btn">Next</button>
            </section>
        </section>
    </div>
</asp:Content>
