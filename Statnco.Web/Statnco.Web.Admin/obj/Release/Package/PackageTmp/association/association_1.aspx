﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="association_1.aspx.cs" Inherits="Statnco.Web.Admin.association.association_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">협회관리</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <select class="cities_provinces_select_box select_box" name="" id="selRegion" aria-placeholder="시도" onchange="fnGetAreaList();">
                </select>

                <select class="select_box" name="" id="selArea" aria-placeholder="군구">
                </select>

                <button class="club_plus_btn" id="ssoClubplusBtn" data-bs-toggle="modal" data-bs-target="#ssoClubplusModal">
                    협회추가
                    <svg class="plus_icon" viewBox="0 0 27 27" fill="none">
                        <path d="M21.6 2.7002H5.40001C3.91501 2.7002 2.70001 3.9152 2.70001 5.4002V21.6002C2.70001 23.0852 3.91501 24.3002 5.40001 24.3002H21.6C23.085 24.3002 24.3 23.0852 24.3 21.6002V5.4002C24.3 3.9152 23.085 2.7002 21.6 2.7002ZM20.25 14.8502H14.85V20.2502H12.15V14.8502H6.75001V12.1502H12.15V6.7502H14.85V12.1502H20.25V14.8502Z" fill="#009F59"/>
                    </svg>
                </button>
            </section>

            <ul class="table_box association_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_short">idx</li>
                        <li class="table_li table_li_middle">시도</li>
                        <li class="table_li table_li_middle">시군구</li>
                        <li class="table_li table_li_middle">관리자이름</li>
                        <li class="table_li table_li_long">관리자연락처</li>
                        <li class="table_li table_li_middle">삭제</li>
                    </ul>
                </li>
            </ul>
        </section>
    </div>
</asp:Content>
