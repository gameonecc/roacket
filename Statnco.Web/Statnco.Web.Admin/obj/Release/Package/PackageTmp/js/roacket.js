﻿// Ajax Json 구현
var fnAjaxSend = function (pUrl, param, pFunction) {
    var oXML = new XMLHttpRequest();

    oXML.responseJSON = null;
    oXML.open("post", pUrl, true);
    oXML.responseType = "text";
    oXML.setRequestHeader("Content-Type", "application/json");

    oXML.addEventListener("load", function () {
        oXML.responseJSON = JSON.parse(oXML.responseText);
        pFunction(oXML.responseJSON, oXML);
    });

    oXML.send(JSON.stringify(param));

    return oXML;
};

String.format = function () {
    let args = arguments;
    return args[0].replace(/{(\d+)}/g, function (match, num) {
        num = Number(num) + 1;
        return typeof (args[num]) != undefined ? args[num] : match;
    });
}

/**
 * 좌측문자열채우기
 * @params
 *  - str : 원 문자열
 *  - padLen : 최대 채우고자 하는 길이
 *  - padStr : 채우고자하는 문자(char)
 */
function lpad(str, padLen, padStr) {
    if (padStr.length > padLen) {
        console.log("오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다");
        return str;
    }
    str += ""; // 문자로
    padStr += ""; // 문자로
    while (str.length < padLen)
        str = padStr + str;
    str = str.length >= padLen ? str.substring(0, padLen) : str;
    return str;
}
console.log(lpad("01", 5, "0")); // 00001
console.log(lpad("01", 5, "01")); // 01010


/**
 * 우측문자열채우기
 * @params
 *  - str : 원 문자열
 *  - padLen : 최대 채우고자 하는 길이
 *  - padStr : 채우고자하는 문자(char)
 */
function rpad(str, padLen, padStr) {
    if (padStr.length > padLen) {
        console.log("오류 : 채우고자 하는 문자열이 요청 길이보다 큽니다");
        return str + "";
    }
    str += ""; // 문자로
    padStr += ""; // 문자로
    while (str.length < padLen)
        str += padStr;
    str = str.length >= padLen ? str.substring(0, padLen) : str;
    return str;
}

function getCookie(cookieName) {
    var cookieValue = null;
    if (document.cookie) {
        var array = document.cookie.split((escape(cookieName) + '='));
        if (array.length >= 2) {
            var arraySub = array[1].split(';');
            cookieValue = unescape(arraySub[0]);
        }
    }
    return cookieValue;
}

function deleteCookie(cookieName) {
    var temp = getCookie(cookieName);
    if (temp) {
        setCookie(cookieName, temp, (new Date(1)));
    }
}

function setCookie(c_name, value, exdays) {
    var exdate = new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
    document.cookie = c_name + "=" + c_value;
}

function fnLogout() {
    deleteCookie("memberIdx");
    location.href = "/";
}

function fnGetRegionList(useArea, idx) {
    var param = {

    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var optionStr = "<option value=\"0\">시도</option>"

        for (var i = 0; i < table.length; i++) {
            if (table[i]["regionName"] == "전국") {
                continue;
            }

            optionStr += String.format(
                "<option value=\"{0}\">{1}</option>",
                table[i]["regionCode"],
                table[i]["regionName"]
            );
        }

        if (idx != undefined) {
            $("#selRegion" + idx).html(optionStr);
        }
        else {
            $("#selRegion").html(optionStr);
        }

        if (useArea) {
            fnGetAreaList(idx);
        }
    }

    window.fnAjaxSend("/com/ws_common.asmx/GetRegionList", param, fnCallback);
}


function fnGetAreaList(idx) {
    var param = {
        regionCode: idx == undefined ? $("#selRegion").val() : $("#selRegion" + idx).val()
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var optionStr = "<option value=\"0\">시군구</option>"

        for (var i = 0; i < table.length; i++) {
            if (table[i]["areaName"] == "전체") {
                continue;
            }

            optionStr += String.format(
                "<option value=\"{0}\">{1}</option>",
                table[i]["areaCode"],
                table[i]["areaName"]
            );
        }

        if (idx != undefined) {
            $("#selArea" + idx).html(optionStr);
        }
        else {
            $("#selArea").html(optionStr);
        }
        
    }

    window.fnAjaxSend("/com/ws_common.asmx/GetAreaList", param, fnCallback);
}


//  페이징 처리 
function fnSetPageing() {
    const pageSize = 20;
    const pageCount = 10;

    const totalPage = Math.ceil(totalNo / pageSize);
    const pageGroup = Math.ceil(pageNo / pageCount);

    var last = pageGroup * pageCount;
    if (last > totalPage) {
        last = totalPage;
    }
    var first = last - (pageCount - 1);
    const next = last + 1;
    const prev = first - 1;

    if (totalPage < 1) {
        first = last;
    }
    const pages = $(".post_list_btn_box");
    pages.empty();

    var prevStr = "<button type=\"button\" class=\"pre_btn\" onclick=\"fnPrev();\">Previous</button>";
    var pageStr = "<button type=\"button\" class=\"num_btn\ {1}\" onclick=\"fnMove({0});\">{0}</button>";
    var nextStr = "<button type=\"button\" class=\"next_btn\" onclick=\"fnNext();\">Next</button>";

    pages.append(
        prevStr
    )

    for (var j = first; j <= last; j++) {
        if (pageNo == j) {
            pages.append(
                String.format(
                    pageStr,
                    j,
                    "select_num_btn"
                )
            );
        }
        else if (j > 0) {
            pages.append(
                String.format(
                    pageStr,
                    j,
                    ""
                )
            );
        }

    }

    pages.append(
        nextStr
    );
}

function fnMove(no) {
    pageNo = no;
    fnGetList();
}

function fnPrev() {
    if (pageNo == 1) {
        return;
    }
    else {
        pageNo--;
        fnGetList();
    }
}

function fnNext() {
    if (pageNo == totalNo) {
        return;
    }
    else {
        pageNo++;
        fnGetList();
    }
}

function httpRequest(valuename)    //javascript로 구현한 Request
{
    var rtnval = "";
    var nowAddress = unescape(location.href);
    var parameters = (nowAddress.slice(nowAddress.indexOf("?") + 1, nowAddress.length)).split("&");

    for (var i = 0; i < parameters.length; i++) {
        var varName = parameters[i].split("=")[0];
        if (varName.toUpperCase() == valuename.toUpperCase()) {
            rtnval = parameters[i].split("=")[1];
            break;
        }
    }
    return rtnval;
}

// JSON으로 Serialize 되어 있는 datetime을 String으로 변환
function parseJsondate(jsonDateTime, type) {
    var date = new Date(eval("new " + jsonDateTime.replace(/\//g, "") + ";"));

    if (type == "date") {
        return String.format(
            "{0}-{1}-{2}",
            date.getFullYear(),
            lpad(date.getMonth() + 1, 2, "0"),
            lpad(date.getDate(), 2, "0")
        );
    }
    else if (type == "time") {
        return String.format(
            "{0}:{1}:{2}",
            lpad(date.getHours(), 2, "0"),
            lpad(date.getMinutes(), 2, "0"),
            lpad(date.getSeconds(), 2, "0")
        );
    }
    else {
        return String.format(
            "{0}-{1}-{2} {3}:{4}:{5}",
            date.getFullYear(),
            lpad(date.getMonth() + 1, 2, "0"),
            lpad(date.getDate(), 2, "0"),
            lpad(date.getHours(), 2, "0"),
            lpad(date.getMinutes(), 2, "0"),
            lpad(date.getSeconds(), 2, "0")
        );
    }
}

function fnGetComCode(division, con) {
    var param = {
        "division": division
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        for (var i = 0; i < table.length; i++) {
            con.append(
                String.format(
                    "<option value=\"{0}\">{1}</option>",
                    table[i]["code"],
                    table[i]["codeName"]
                )
            );
        }
    };

    window.fnAjaxSend("/com/ws_common.asmx/GetComCodeList", param, fnCallback);
}