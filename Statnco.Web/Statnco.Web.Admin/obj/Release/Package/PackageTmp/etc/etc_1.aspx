﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="etc_1.aspx.cs" Inherits="Statnco.Web.Admin.etc.etc_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">광고관리</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <button class="ad_plus_btn">
                    광고생성
                    <svg class="plus_icon" viewBox="0 0 27 27" fill="none">
                        <path d="M21.6 2.7002H5.40001C3.91501 2.7002 2.70001 3.9152 2.70001 5.4002V21.6002C2.70001 23.0852 3.91501 24.3002 5.40001 24.3002H21.6C23.085 24.3002 24.3 23.0852 24.3 21.6002V5.4002C24.3 3.9152 23.085 2.7002 21.6 2.7002ZM20.25 14.8502H14.85V20.2502H12.15V14.8502H6.75001V12.1502H12.15V6.7502H14.85V12.1502H20.25V14.8502Z" fill="#009F59"/>
                    </svg>
                </button>
            </section>

            <ul class="table_box advertisement_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_very_short">idx</li>
                        <li class="table_li table_li_long">타이틀</li>
                        <li class="table_li table_li_short">위치</li>
                        <li class="table_li table_li_middle">게시시작일</li>
                        <li class="table_li table_li_middle">게시종료일</li>
                        <li class="table_li table_li_img">이미지뷰</li>
                        <li class="table_li table_li_middle">상태</li>
                        <li class="table_li table_li_very_short">삭제</li>
                    </ul>
                </li>
            </ul>
            <section class="post_list_btn_box">
            </section>
        </section>
    </div>
</asp:Content>
