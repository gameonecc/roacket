﻿var pageNo = 1;
var totalNo = 0;

$(function(){
    fnGetList();
});

function fnGetAdvertisementList() {
    var listStr = "<li class=\"table_row_wrap\">\
                        <ul class=\"table_row\">\
                            <li class=\"table_li table_li_very_short\">{0}</li>\
                            <li class=\"table_li table_li_long\">{1}</li>\
                            <li class=\"table_li table_li_short\">{2}</li>\
                            <li class=\"table_li table_li_middle\">{3}</li>\
                            <li class=\"table_li table_li_middle\">{4}</li>\
                            <li class=\"table_li table_li_img\">\
                                <img class=\"ad_img_01\" src=\"{5}\" alt=\"advertisement_img\">\
                            </li>\
                            <li class=\"table_li table_li_middle\">{6}</li>\
                            <li class=\"table_li table_li_very_short\">\
                                <button type=\"button\" class=\"del_btn\">\
                                    <svg class=\"del_icon\" viewBox=\"0 0 17 19\" fill=\"none\">\
                                        <path d=\"M6.18182 7.72732V13.9091C6.18182 14.1141 6.26324 14.3106 6.40815 14.4555C6.55306 14.6005 6.74961 14.6819 6.95455 14.6819C7.15949 14.6819 7.35604 14.6005 7.50095 14.4555C7.64587 14.3106 7.72728 14.1141 7.72728 13.9091V7.72732C7.72728 7.52238 7.64587 7.32583 7.50095 7.18092C7.35604 7.036 7.15949 6.95459 6.95455 6.95459C6.74961 6.95459 6.55306 7.036 6.40815 7.18092C6.26324 7.32583 6.18182 7.52238 6.18182 7.72732Z\" fill=\"black\"/>\
                                        <path d=\"M10.0454 6.95459C10.2504 6.95459 10.4469 7.036 10.5918 7.18092C10.7367 7.32583 10.8182 7.52238 10.8182 7.72732V13.9091C10.8182 14.1141 10.7367 14.3106 10.5918 14.4555C10.4469 14.6005 10.2504 14.6819 10.0454 14.6819C9.84049 14.6819 9.64395 14.6005 9.49903 14.4555C9.35412 14.3106 9.27271 14.1141 9.27271 13.9091V7.72732C9.27271 7.52238 9.35412 7.32583 9.49903 7.18092C9.64395 7.036 9.84049 6.95459 10.0454 6.95459Z\" fill=\"black\"/>\
                                        <path d=\"M11.5909 3.09091H16.2273C16.4322 3.09091 16.6288 3.17232 16.7737 3.31724C16.9186 3.46215 17 3.6587 17 3.86364C17 4.06858 16.9186 4.26512 16.7737 4.41004C16.6288 4.55495 16.4322 4.63636 16.2273 4.63636H15.3726L14.2105 15.1084C14.1055 16.0535 13.6556 16.9267 12.9471 17.5608C12.2385 18.195 11.3209 18.5456 10.37 18.5455H6.63C5.67908 18.5456 4.76152 18.195 4.05294 17.5608C3.34436 16.9267 2.89453 16.0535 2.78955 15.1084L1.62582 4.63636H0.772727C0.567787 4.63636 0.371241 4.55495 0.226327 4.41004C0.0814123 4.26512 0 4.06858 0 3.86364C0 3.6587 0.0814123 3.46215 0.226327 3.31724C0.371241 3.17232 0.567787 3.09091 0.772727 3.09091H5.40909C5.40909 2.27115 5.73474 1.48496 6.3144 0.905306C6.89405 0.325648 7.68024 0 8.5 0C9.31976 0 10.1059 0.325648 10.6856 0.905306C11.2653 1.48496 11.5909 2.27115 11.5909 3.09091ZM8.5 1.54545C8.09012 1.54545 7.69703 1.70828 7.4072 1.99811C7.11737 2.28794 6.95455 2.68103 6.95455 3.09091H10.0455C10.0455 2.68103 9.88263 2.28794 9.5928 1.99811C9.30297 1.70828 8.90988 1.54545 8.5 1.54545ZM3.18209 4.63636L4.32573 14.9384C4.38886 15.5053 4.65882 16.0291 5.08395 16.4095C5.50909 16.7898 6.05954 17.0001 6.63 17H10.37C10.9402 16.9997 11.4903 16.7893 11.9151 16.4089C12.3399 16.0286 12.6096 15.5051 12.6727 14.9384L13.8195 4.63636H3.18364H3.18209Z\" fill=\"black\"/>\
                                    </svg>\
                                </button>\
                            </li>\
                        </ul>\
                </li>";

    var param = {
        pageNo: pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        $(".table_box").children("li:gt(0)").remove();

        totalNo = table1[0]["totalCnt"];

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["advIdx"],
                        table[i]["title"],
                        table[i]["locationCode"],
                        parseJsondate(table[i]["srtDate"], "date"),
                        parseJsondate(table[i]["endDate"], "date"),
                        table[i]["imgPath"],
                        fnGetAdvertisementState(table[i]["srtDate"], table[i]["endDate"])
                    )
                );
            }

            fnSetPageing();
        }
    };

    window.fnAjaxSend("/etc/ws_etc.asmx/GetAdvertisementList", param, fnCallback);
}

function fnGetAdvertisementState(srtDate, endDate) {
    var today = new Date();
    var srtDate = new Date(srtDate);
    var endDate = new Date(endDate);

    if (today < srtDate) {
        return "게시전";
    }
    else if (today > srtDate && today < endDate) {
        return "게시중";
    }
    else {
        return "종료";
    }
}

function fnGetList() {
    fnGetAdvertisementList();
}