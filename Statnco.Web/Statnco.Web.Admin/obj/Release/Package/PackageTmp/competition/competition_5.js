﻿var competitionIdx;

$(function () {
    competitionIdx = httpRequest("competitionIdx");

    fnGetCompetitionStadiumList();
});

function fnGetCompetitionStadiumList() {
    var param = {
        competitionIdx: competitionIdx
    };

    var fnCallback = function (data) {
        var stadiumStr = " <li class=\"ground_table_row_wrap\" onclick=\"fnGetCompetitionStadiumSchedule({0});\">\
                                <ul class=\"ground_table_row\">\
                                    <li class=\"ground_table_li ground_no_li\">{0}</li>\
                                    <li class=\"ground_table_li ground_name_li\">{1}</li>\
                                    <li class=\"ground_table_li ground_add_li\">{2} {3}</li>\
                                    <li class=\"ground_table_li ground_court_li\">\
                                        <input class=\"input_court_num\" type=\"text\" placeholder=\"18\">\
                                    </li>\
                                </ul>\
                            </li>";

        var table = JSON.parse(data.d).Table;

        $("#ulStadiumList").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $("#ulStadiumList").append(
                    String.format(
                        stadiumStr,
                        table[i]["stadiumNo"],
                        table[i]["stadiumName"],
                        table[i]["stadiumAddr_1"],
                        table[i]["stadiumAddr_2"]
                    )
                );
            }
        }
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionStadiumList", param, fnCallback);
}

function fnGetCompetitionStadiumSchedule(stadiumNo) {
    $(".ground_name_table_wrap").show();

    var schedultList = "<li class=\"ground_table_row_wrap\">\
                        <ul class=\"ground_table_row\">\
                            <li class=\"ground_table_li time_title_li\">경기일정</li>\
                            <li class=\"ground_table_li day_li\">2021-12-11</li>\
                            <li class=\"ground_table_li input_time_li\">\
                                <input class=\"input_time\" type=\"text\" placeholder=\"경기시작시간\">\
                                <p class=\"time_center_line\">~</p>\
                                <input class=\"input_time\" type=\"text\" placeholder=\"경기종료시간\">\
                            </li>\
                            <li class=\"ground_table_li del_li\">\
                            </li>\
                        </ul>\
                    </li>";

    var param = {
        competitionIdx: competitionIdx,
        stadiumNo: stadiumNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        for (var i = 0; i < table.length; i++) {
            if (i == 0) {
                $(".ground_name").text(
                    table[i]["stadiumName"]
                );
            }
        }
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionStadiumSchedule", param, fnCallback);
}