﻿var competitionIdx;
var regionCode;

$(function () {
    competitionIdx = httpRequest("competitionIdx");

    $(".timepicker").timepicker({
        timeFormat: 'HH:mm:ss',
        interval: 30,
        startTime: '00:00',
        dynamic: false,
        dropdown: true,
        scrollbar: true
    });

    if (competitionIdx == "") {
        competitionIdx = 0;
    }
    else {
        fnGetRegionList2();

        if (competitionIdx != "0") {
            fnGetCompetitionDetail();
        }
    }

    
});

function fnGetCompetitionDetail() {
    var param = {
        "competitionIdx": competitionIdx
    };

    var fnCallback = function (data) {
        var row = JSON.parse(data.d).Table[0];
        var table1 = JSON.parse(data.d).Table1;
        var table2 = JSON.parse(data.d).Table2;

        // 대회명
        $("#txtCompetitionName").val(row["competitionName"]);

        // 대회 종류 
        $("input[name='si_gun_gu']:input[value='" + row["typeCode"] + "']").prop("checked", true);

        // 승급여부
        var isDivision = row["isDivision"] ? "1" : "0";
        $("input[name='division']:input[value='" + isDivision + "']").prop("checked", true);

        // 대회 접수 기간
        $("#txtAppSrtDate").val(parseJsondate(row["applicationSrtDate"], "date"));
        $("#txtAppSrtTime").val(parseJsondate(row["applicationSrtDate"], "time"));

        $("#txtAppEndDate").val(parseJsondate(row["applicationEndDate"], "date"));
        $("#txtAppEndTime").val(parseJsondate(row["applicationEndDate"], "time"));

        // 대회기간
        $("#txtSrtDate").val(parseJsondate(row["srtDate"], "date"));
        $("#txtEndDate").val(parseJsondate(row["endDate"], "date"));

        // 대진표 발표일
        $("#txtListDate").val(parseJsondate(row["listDate"], "date"));

        // 대회 참가 가능지역
        regionCode = table1[0]["regionCode"];
        fnGetArea2(regionCode, null);

        setTimeout(function () {
            $(".area_li[idx='" + regionCode + "']").addClass("select_area_li");

            for (var i = 0; i < table1.length; i++) {
                if (regionCode == table1[i]["regionCode"]) {
                    $("#chkArea_" + table1[i]["areaCode"]).prop("checked", true);
                }
                
            }
        }, 1000);


        // 대회장소 바인딩
        if (table2.length > 0) {
            for (ii = 0; ii < table2.length; ii++) {
                if (ii != 0) {
                    fnAddStadium(table2[ii]["stadiumNo"]);
                }

                $("#txtStadiumName" + table2[ii]["stadiumNo"]).val(table2[ii]["stadiumName"]);
                $("#txtAddr" + table2[ii]["stadiumNo"] + "_1").val(table2[ii]["stadiumAddr_1"]);
                $("#txtAddr" + table2[ii]["stadiumNo"] + "_2").val(table2[ii]["stadiumAddr_2"]);
            }
        }
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionDetail", param, fnCallback);
}

function fnAddStadium(stadiumNo) {
    var stadiumStr = " <article class=\"input_address_box\">\
                        <p class=\"address_num\">{0}</p>\
                        <input class=\"input_address_name\" type=\"text\" placeholder=\"공백포함 최대 20자\" id=\"txtStadiumName{0}\">\
                        <input class=\"input_address\" id=\"txtAddr{0}_1\" type=\"text\" value=\"\" disabled>\
                        <button class=\"post_num_btn\" type=\"button\" onclick=\"fnAdress({0});\">주소찾기</button>\
                        <input class=\"input_address\" id=\"txtAddr{0}_2\" type=\"text\" value=\"\">\
                        <button type=\"button\" class=\"del_btn\" onclick=\"fnDelStadium({0});\">\
                            <svg class=\"del_icon\" width=\"17\" height=\"19\" viewBox=\"0 0 17 19\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\
                                <path d=\"M6.18182 7.72683V13.9086C6.18182 14.1136 6.26324 14.3101 6.40815 14.455C6.55306 14.6 6.74961 14.6814 6.95455 14.6814C7.15949 14.6814 7.35604 14.6 7.50095 14.455C7.64587 14.3101 7.72728 14.1136 7.72728 13.9086V7.72683C7.72728 7.52189 7.64587 7.32534 7.50095 7.18043C7.35604 7.03551 7.15949 6.9541 6.95455 6.9541C6.74961 6.9541 6.55306 7.03551 6.40815 7.18043C6.26324 7.32534 6.18182 7.52189 6.18182 7.72683Z\" fill=\"black\"/>\
                                <path d=\"M10.0454 6.9541C10.2504 6.9541 10.4469 7.03551 10.5918 7.18043C10.7367 7.32534 10.8182 7.52189 10.8182 7.72683V13.9086C10.8182 14.1136 10.7367 14.3101 10.5918 14.455C10.4469 14.6 10.2504 14.6814 10.0454 14.6814C9.84049 14.6814 9.64395 14.6 9.49903 14.455C9.35412 14.3101 9.27271 14.1136 9.27271 13.9086V7.72683C9.27271 7.52189 9.35412 7.32534 9.49903 7.18043C9.64395 7.03551 9.84049 6.9541 10.0454 6.9541Z\" fill=\"black\"/>\
                                <path d=\"M11.5909 3.09091H16.2273C16.4322 3.09091 16.6288 3.17232 16.7737 3.31724C16.9186 3.46215 17 3.6587 17 3.86364C17 4.06858 16.9186 4.26512 16.7737 4.41004C16.6288 4.55495 16.4322 4.63636 16.2273 4.63636H15.3726L14.2105 15.1084C14.1055 16.0535 13.6556 16.9267 12.9471 17.5608C12.2385 18.195 11.3209 18.5456 10.37 18.5455H6.63C5.67908 18.5456 4.76152 18.195 4.05294 17.5608C3.34436 16.9267 2.89453 16.0535 2.78955 15.1084L1.62582 4.63636H0.772727C0.567787 4.63636 0.371241 4.55495 0.226327 4.41004C0.0814123 4.26512 0 4.06858 0 3.86364C0 3.6587 0.0814123 3.46215 0.226327 3.31724C0.371241 3.17232 0.567787 3.09091 0.772727 3.09091H5.40909C5.40909 2.27115 5.73474 1.48496 6.3144 0.905306C6.89405 0.325648 7.68024 0 8.5 0C9.31976 0 10.1059 0.325648 10.6856 0.905306C11.2653 1.48496 11.5909 2.27115 11.5909 3.09091ZM8.5 1.54545C8.09012 1.54545 7.69703 1.70828 7.4072 1.99811C7.11737 2.28794 6.95455 2.68103 6.95455 3.09091H10.0455C10.0455 2.68103 9.88263 2.28794 9.5928 1.99811C9.30297 1.70828 8.90988 1.54545 8.5 1.54545ZM3.18209 4.63636L4.32573 14.9384C4.38886 15.5053 4.65882 16.0291 5.08395 16.4095C5.50909 16.7898 6.05954 17.0001 6.63 17H10.37C10.9402 16.9997 11.4903 16.7893 11.9151 16.4089C12.3399 16.0286 12.6096 15.5051 12.6727 14.9384L13.8195 4.63636H3.18364H3.18209Z\" fill=\"black\"/>\
                            </svg>\
                        </button>\
                    </article>";

    if (stadiumNo == 0) {
        var stadiumLength = $(".address_num").length;

        $("#divStadium").append(
            String.format(
                stadiumStr,
                stadiumLength + 1
            )

        );
    }
    else {
        $("#divStadium").append(
            String.format(
                stadiumStr,
                stadiumNo
            )

        );
    }
}

function fnGetRegionList2() {
    var param = {

    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var listStr = "<li class=\"area_li\" idx=\"{0}\" onclick=\"fnGetArea2({0}, this);\">{1}</li>";

        for (var i = 0; i < table.length; i++) {
            $("#liRegion").append(
                String.format(
                    listStr,
                    table[i]["regionCode"],
                    table[i]["regionName"]
                )
            )
        }
    }

    window.fnAjaxSend("/com/ws_common.asmx/GetRegionList", param, fnCallback);
}

function fnGetArea2(idx, con) {
    regionCode = idx;

    if (con != null) {
        $(".area_li").removeClass("select_area_li");
        $(con).addClass("select_area_li");
    }

    var param = {
        regionCode: idx
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var listStr = "<li class=\"area_check_li\"><label><input class=\"check_box\" type=\"checkbox\" id=\"chkArea_{0}\" value=\"{0}\">{1}</label></li>";

        $("#liArea").empty();

        for (var i = 0; i < table.length; i++) {
            if (i == 0 && idx != 0) {
                $("#liArea").append(
                    String.format(
                        listStr,
                        "0",
                        "전체"
                    )
                );
            }

            $("#liArea").append(
                String.format(
                    listStr,
                    table[i]["areaCode"],
                    table[i]["areaName"]
                )
            );
            
        }
    }

    window.fnAjaxSend("/com/ws_common.asmx/GetAreaList", param, fnCallback);
}

function fnAdress(stadiumIdx) {
    new daum.Postcode({
        oncomplete: function (data) {
            // 내려오는 변수가 값이 없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
            var address1 = ''; // 주소 변수
            var extraAddress = ''; // 참고항목 변수


            //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
            if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                address1 = data.roadAddress;
            } else { // 사용자가 지번 주소를 선택했을 경우(J)
                address1 = data.jibunAddress;
            }

            // 사용자가 선택한 주소가 도로명 타입일때 참고항목을 조합한다.
            if (data.userSelectedType === 'R') {
                // 법정동명이 있을 경우 추가한다. (법정리는 제외)
                // 법정동의 경우 마지막 문자가 "동/로/가"로 끝난다.
                if (data.bname !== '' && /[동|로|가]$/g.test(data.bname)) {
                    extraAddress += data.bname;
                }
                // 건물명이 있고, 공동주택일 경우 추가한다.
                if (data.buildingName !== '' && data.apartment === 'Y') {
                    extraAddress += (extraAddress !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                if (extraAddress !== '') {
                    extraAddress = ' (' + extraAddress + ')';
                }

                address1 += extraAddress;
            }

            $("#txtAddr" + stadiumIdx + "_1").val(address1);
        }
    }).open();
}

function fnSetCompetition() {

    // 대회 참가 가능 지역 처리
    var areaArr = [];

    $(".check_box:checked").each(function () {
        areaArr.push(
            {
                regionCode: regionCode,
                areaCode: $(this).val()
            }
        );
    });

    // 대회 장소
    var stadiumArr = [];
    for (var i = 1; i <= $(".address_num").length; i++) {


        stadiumArr.push(
            {
                no: i,
                name: $("#txtStadiumName" + i).val(),
                addr1: $("#txtAddr" + i + "_1").val(),
                addr2: $("#txtAddr" + i + "_2").val()
            }
        );
    }

    var param = {
        competitionIdx: competitionIdx,
        competitionName: $("#txtCompetitionName").val(),
        typeCode: $("input[name='si_gun_gu']:checked").val(),
        isDivision: $("input[name='division']:checked").val() == "1",
        appSrtDate: String.format(
            "{0} {1}",
            $("#txtAppSrtDate").val(),
            $("#txtAppSrtTime").val()
        ),
        appEndDate: String.format(
            "{0} {1}",
            $("#txtAppEndDate").val(),
            $("#txtAppEndTime").val()
        ),
        srtDate: String.format(
            "{0} {1}",
            $("#txtSrtDate").val(),
            "00:00:00"
        ),
        endDate: String.format(
            "{0} {1}",
            $("#txtEndDate").val(),
            "00:00:00"
        ),
        listDate: String.format(
            "{0} {1}",
            $("#txtListDate").val(),
            "00:00:00"
        ),
        areas: JSON.stringify({
            areas: areaArr
        }),
        stadiums: JSON.stringify({
            stadiums: stadiumArr
        })
    };

    var fnCallback = function (data) {
        if (data.d == "0") {
            location.href = "/competition/competition_1.aspx";
        }
        else {
            alert("데이터 저장 중 에러가 발생하였습니다");
        }
    }

    window.fnAjaxSend("/competition/ws_competition.asmx/SetCompetition", param, fnCallback);
}