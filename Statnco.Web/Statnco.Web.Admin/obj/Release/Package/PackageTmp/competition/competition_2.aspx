﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="competition_2.aspx.cs" Inherits="Statnco.Web.Admin.competition.competition_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">대회생성/수정</h1>
        <section class="contest_make_main">
            <article class="contest_make_main_hd">
                <h3 class="contest_make_title">대회요강 정보 입력</h3>
                <button type="button" class="pdf_btn">대회요강 pdf 생성</button>
            </article>
            <section class="contest_make_form">
                <h6 class="input_title">대회명<span class="pink_star">&#42;</span></h6>
                <input class="contest_name_input" type="text" placeholder="공백 포함 최대 50글자까지 입력 가능합니다." id="txtCompetitionName">

                <h6 class="input_title">대회종류<span class="pink_star">&#42;</span></h6>
                <article class="input_box input_radio_box">
                    <label><input class="input_radio" type="radio" name="si_gun_gu" value="1"> 시/도</label>
                    <label><input class="input_radio" type="radio" name="si_gun_gu" value="2"> 시/군/구</label>
                </article>

                <h6 class="input_title">승급여부<span class="pink_star">&#42;</span></h6>
                <article class="input_box input_radio_box">
                    <label><input class="input_radio" type="radio" name="division" value="1"> 승급</label>
                    <label><input class="input_radio" type="radio" name="division" value="0"> 비승급</label>
                </article>

                <h6 class="input_title">대회 접수기간<span class="pink_star">&#42;</span></h6>
                <article class="input_box input_date">
                    <input class="input_date" type="date" id="txtAppSrtDate">
                    <input class="input_day timepicker" type="text" id="txtAppSrtTime">
                    <p class="date_center_line">-</p>
                    <input class="input_date" type="date" id="txtAppEndDate">
                    <input class="input_day timepicker" type="text" id="txtAppEndTime">
                </article>

                <h6 class="input_title">대회기간<span class="pink_star">&#42;</span></h6>
                <article class="input_box input_date">
                    <input class="input_date" type="date" id="txtSrtDate">
                    <p class="date_center_line">-</p>
                    <input class="input_date" type="date" id="txtEndDate">
                </article>

                <h6 class="input_title">대진표 발표일</h6>
                <article class="input_box input_date">
                    <input class="input_date" type="date" id="txtListDate">
                </article>

                <h6 class="input_title">대회참가 가능 지역<span class="pink_star">&#42;</span></h6>
                <p class="input_sub_title">• 체크 된 지역의 선수들만 참여 할 수 있습니다.</p>
                <article class="input_box">
                    <ul class="area_select_box" id="liRegion">
                        
                    </ul>
                    <ul class="area_check_box" id="liArea">
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 전체</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강남구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강북구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강서구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강동구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 광진구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강남구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강북구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강서구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 강동구</label></li>
                        <li class="area_check_li"><label><input class="check_box" type="checkbox"> 광진구</label></li>
                    </ul>
                </article>

                <h6 class="input_title">대회장소<span class="pink_star">&#42;</span></h6>
                <div id="divStadium">
                    <article class="input_address_box">
                        <p class="address_num">1</p>
                        <input class="input_address_name" type="text" placeholder="공백포함 최대 20자" id="txtStadiumName1">
                        <input class="input_address" id="txtAddr1_1" type="text" value="서울특별시 송파구 송파대로 167, 문정동" disabled>
                        <button class="post_num_btn" type="button" onclick="fnAdress(1);">주소찾기</button>
                        <input class="input_address" id="txtAddr1_2" type="text" value="">
                        <button type="button" class="del_btn" onclick="fnDelStadium(1);">
                            <svg class="del_icon" width="17" height="19" viewBox="0 0 17 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M6.18182 7.72683V13.9086C6.18182 14.1136 6.26324 14.3101 6.40815 14.455C6.55306 14.6 6.74961 14.6814 6.95455 14.6814C7.15949 14.6814 7.35604 14.6 7.50095 14.455C7.64587 14.3101 7.72728 14.1136 7.72728 13.9086V7.72683C7.72728 7.52189 7.64587 7.32534 7.50095 7.18043C7.35604 7.03551 7.15949 6.9541 6.95455 6.9541C6.74961 6.9541 6.55306 7.03551 6.40815 7.18043C6.26324 7.32534 6.18182 7.52189 6.18182 7.72683Z" fill="black"/>
                                <path d="M10.0454 6.9541C10.2504 6.9541 10.4469 7.03551 10.5918 7.18043C10.7367 7.32534 10.8182 7.52189 10.8182 7.72683V13.9086C10.8182 14.1136 10.7367 14.3101 10.5918 14.455C10.4469 14.6 10.2504 14.6814 10.0454 14.6814C9.84049 14.6814 9.64395 14.6 9.49903 14.455C9.35412 14.3101 9.27271 14.1136 9.27271 13.9086V7.72683C9.27271 7.52189 9.35412 7.32534 9.49903 7.18043C9.64395 7.03551 9.84049 6.9541 10.0454 6.9541Z" fill="black"/>
                                <path d="M11.5909 3.09091H16.2273C16.4322 3.09091 16.6288 3.17232 16.7737 3.31724C16.9186 3.46215 17 3.6587 17 3.86364C17 4.06858 16.9186 4.26512 16.7737 4.41004C16.6288 4.55495 16.4322 4.63636 16.2273 4.63636H15.3726L14.2105 15.1084C14.1055 16.0535 13.6556 16.9267 12.9471 17.5608C12.2385 18.195 11.3209 18.5456 10.37 18.5455H6.63C5.67908 18.5456 4.76152 18.195 4.05294 17.5608C3.34436 16.9267 2.89453 16.0535 2.78955 15.1084L1.62582 4.63636H0.772727C0.567787 4.63636 0.371241 4.55495 0.226327 4.41004C0.0814123 4.26512 0 4.06858 0 3.86364C0 3.6587 0.0814123 3.46215 0.226327 3.31724C0.371241 3.17232 0.567787 3.09091 0.772727 3.09091H5.40909C5.40909 2.27115 5.73474 1.48496 6.3144 0.905306C6.89405 0.325648 7.68024 0 8.5 0C9.31976 0 10.1059 0.325648 10.6856 0.905306C11.2653 1.48496 11.5909 2.27115 11.5909 3.09091ZM8.5 1.54545C8.09012 1.54545 7.69703 1.70828 7.4072 1.99811C7.11737 2.28794 6.95455 2.68103 6.95455 3.09091H10.0455C10.0455 2.68103 9.88263 2.28794 9.5928 1.99811C9.30297 1.70828 8.90988 1.54545 8.5 1.54545ZM3.18209 4.63636L4.32573 14.9384C4.38886 15.5053 4.65882 16.0291 5.08395 16.4095C5.50909 16.7898 6.05954 17.0001 6.63 17H10.37C10.9402 16.9997 11.4903 16.7893 11.9151 16.4089C12.3399 16.0286 12.6096 15.5051 12.6727 14.9384L13.8195 4.63636H3.18364H3.18209Z" fill="black"/>
                            </svg>
                        </button>
                    </article>
                </div>
                <button type="button" class="ground_plus_btn" onclick="fnAddStadium(0);">경기장 추가</button>

                <button type="button" class="save_btn" onclick="fnSetCompetition();">저장</button>
            </section>
        </section>
    </div>
</asp:Content>
