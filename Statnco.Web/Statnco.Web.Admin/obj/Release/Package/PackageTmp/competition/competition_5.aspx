﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="competition_5.aspx.cs" Inherits="Statnco.Web.Admin.competition.competition_5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <section class="ground_set_wrap">
            <section class="ground_set_hd">
                대회관리 / 대회생성 / <span class="now_page_font">경기장설정</span>
            </section>

            <h1 class="ground_set_title">경기장 설정</h1>

            <section class="ground_table_wrap">
                <h3 class="ground_set_title">경기장 별 코트 구성</h3>
                <ul class="ground_table" id="ulStadiumList">
                    <li class="ground_table_row_wrap ground_table_row_title">
                        <ul class="ground_table_row">
                            <li class="ground_table_li ground_no_li">No</li>
                            <li class="ground_table_li ground_name_li">경기장명</li>
                            <li class="ground_table_li ground_add_li">주소</li>
                            <li class="ground_table_li ground_court_li">경기장 코트수</li>
                        </ul>
                    </li>
                </ul>
            </section>

            <section class="ground_name_table_wrap" style="display:none;">
                <h5 class="ground_name"></h5>
                <ul class="ground_table">
                    <%--<li class="ground_table_row_wrap">
                        <ul class="ground_table_row">
                            <li class="ground_table_li time_title_li">경기일정</li>
                            <li class="ground_table_li day_li">2021-12-11</li>
                            <li class="ground_table_li input_time_li">
                                <input class="input_time" type="text" placeholder="경기시작시간">
                                <p class="time_center_line">~</p>
                                <input class="input_time" type="text" placeholder="경기종료시간">
                            </li>
                            <li class="ground_table_li del_li">
                                                
                            </li>
                        </ul>
                    </li>
                   
                    <li class="ground_table_row_wrap">
                        <ul class="ground_table_row">
                            <li class="ground_table_li time_title_li">경기불가시간</li>
                            <li class="ground_table_li day_li">
                                <select name="" class="day_select_box">
                                    <option value="">2021-12-11</option>
                                    <option value="">2021-12-12</option>
                                    <option value="">2021-12-13</option>
                                </select>
                            </li>
                            <li class="ground_table_li input_time_li">
                                <input class="input_time" type="text" placeholder="경기시작시간">
                                <p class="time_center_line">~</p>
                                <input class="input_time" type="text" placeholder="경기종료시간">
                            </li>
                            <li class="ground_table_li del_li">
                                <button type="button" class="del_btn">
                                    <svg class="del_icon" viewBox="0 0 16 20" fill="none">
                                        <path d="M5.7876 8.47683V14.6586C5.7876 14.8636 5.86382 15.0601 5.9995 15.205C6.13518 15.35 6.3192 15.4314 6.51108 15.4314C6.70296 15.4314 6.88698 15.35 7.02266 15.205C7.15833 15.0601 7.23456 14.8636 7.23456 14.6586V8.47683C7.23456 8.27189 7.15833 8.07534 7.02266 7.93043C6.88698 7.78551 6.70296 7.7041 6.51108 7.7041C6.3192 7.7041 6.13518 7.78551 5.9995 7.93043C5.86382 8.07534 5.7876 8.27189 5.7876 8.47683Z" fill="black"/>
                                        <path d="M9.40509 7.7041C9.59696 7.7041 9.78097 7.78551 9.91665 7.93043C10.0523 8.07534 10.1285 8.27189 10.1285 8.47683V14.6586C10.1285 14.8636 10.0523 15.0601 9.91665 15.205C9.78097 15.35 9.59696 15.4314 9.40509 15.4314C9.21322 15.4314 9.02921 15.35 8.89353 15.205C8.75786 15.0601 8.68164 14.8636 8.68164 14.6586V8.47683C8.68164 8.27189 8.75786 8.07534 8.89353 7.93043C9.02921 7.78551 9.21322 7.7041 9.40509 7.7041Z" fill="black"/>
                                        <path d="M10.852 3.84091H15.1928C15.3847 3.84091 15.5687 3.92232 15.7044 4.06724C15.84 4.21215 15.9163 4.4087 15.9163 4.61364C15.9163 4.81858 15.84 5.01512 15.7044 5.16004C15.5687 5.30495 15.3847 5.38636 15.1928 5.38636H14.3926L13.3045 15.8584C13.2063 16.8035 12.7851 17.6767 12.1217 18.3108C11.4583 18.945 10.5992 19.2956 9.70892 19.2955H6.20734C5.31704 19.2956 4.45798 18.945 3.79457 18.3108C3.13116 17.6767 2.71 16.8035 2.61171 15.8584L1.52217 5.38636H0.723466C0.531591 5.38636 0.347575 5.30495 0.211899 5.16004C0.0762223 5.01512 0 4.81858 0 4.61364C0 4.4087 0.0762223 4.21215 0.211899 4.06724C0.347575 3.92232 0.531591 3.84091 0.723466 3.84091H5.06426C5.06426 3.02115 5.36915 2.23496 5.91186 1.65531C6.45456 1.07565 7.19063 0.75 7.95813 0.75C8.72563 0.75 9.4617 1.07565 10.0044 1.65531C10.5471 2.23496 10.852 3.02115 10.852 3.84091ZM7.95813 2.29545C7.57438 2.29545 7.20635 2.45828 6.93499 2.74811C6.66364 3.03794 6.5112 3.43103 6.5112 3.84091H9.40506C9.40506 3.43103 9.25262 3.03794 8.98127 2.74811C8.70991 2.45828 8.34188 2.29545 7.95813 2.29545ZM2.97923 5.38636L4.04996 15.6884C4.10907 16.2553 4.36182 16.7791 4.75985 17.1595C5.15789 17.5398 5.67325 17.7501 6.20734 17.75H9.70892C10.2428 17.7497 10.7578 17.5393 11.1555 17.1589C11.5532 16.7786 11.8058 16.2551 11.8648 15.6884L12.9385 5.38636H2.98068H2.97923Z" fill="black"/>
                                    </svg>
                                </button>
                            </li>
                        </ul>
                    </li>
                    <li class="ground_table_row_wrap">
                        <ul class="ground_table_row">
                            <li class="ground_table_li time_title_li">
                                <button type="button" class="time_plus_btn">불가시간추가</button>
                            </li>
                            <li class="ground_table_li day_li">
                                <select name="" class="day_select_box">
                                    <option value="">2021-12-11</option>
                                    <option value="">2021-12-12</option>
                                    <option value="">2021-12-13</option>
                                </select>
                            </li>
                            <li class="ground_table_li input_time_li">
                                <input class="input_time" type="text" placeholder="경기시작시간">
                                <p class="time_center_line">~</p>
                                <input class="input_time" type="text" placeholder="경기종료시간">
                            </li>
                            <li class="ground_table_li del_li">
                                <button type="button" class="del_btn">
                                    <svg class="del_icon" viewBox="0 0 16 20" fill="none">
                                        <path d="M5.7876 8.47683V14.6586C5.7876 14.8636 5.86382 15.0601 5.9995 15.205C6.13518 15.35 6.3192 15.4314 6.51108 15.4314C6.70296 15.4314 6.88698 15.35 7.02266 15.205C7.15833 15.0601 7.23456 14.8636 7.23456 14.6586V8.47683C7.23456 8.27189 7.15833 8.07534 7.02266 7.93043C6.88698 7.78551 6.70296 7.7041 6.51108 7.7041C6.3192 7.7041 6.13518 7.78551 5.9995 7.93043C5.86382 8.07534 5.7876 8.27189 5.7876 8.47683Z" fill="black"/>
                                        <path d="M9.40509 7.7041C9.59696 7.7041 9.78097 7.78551 9.91665 7.93043C10.0523 8.07534 10.1285 8.27189 10.1285 8.47683V14.6586C10.1285 14.8636 10.0523 15.0601 9.91665 15.205C9.78097 15.35 9.59696 15.4314 9.40509 15.4314C9.21322 15.4314 9.02921 15.35 8.89353 15.205C8.75786 15.0601 8.68164 14.8636 8.68164 14.6586V8.47683C8.68164 8.27189 8.75786 8.07534 8.89353 7.93043C9.02921 7.78551 9.21322 7.7041 9.40509 7.7041Z" fill="black"/>
                                        <path d="M10.852 3.84091H15.1928C15.3847 3.84091 15.5687 3.92232 15.7044 4.06724C15.84 4.21215 15.9163 4.4087 15.9163 4.61364C15.9163 4.81858 15.84 5.01512 15.7044 5.16004C15.5687 5.30495 15.3847 5.38636 15.1928 5.38636H14.3926L13.3045 15.8584C13.2063 16.8035 12.7851 17.6767 12.1217 18.3108C11.4583 18.945 10.5992 19.2956 9.70892 19.2955H6.20734C5.31704 19.2956 4.45798 18.945 3.79457 18.3108C3.13116 17.6767 2.71 16.8035 2.61171 15.8584L1.52217 5.38636H0.723466C0.531591 5.38636 0.347575 5.30495 0.211899 5.16004C0.0762223 5.01512 0 4.81858 0 4.61364C0 4.4087 0.0762223 4.21215 0.211899 4.06724C0.347575 3.92232 0.531591 3.84091 0.723466 3.84091H5.06426C5.06426 3.02115 5.36915 2.23496 5.91186 1.65531C6.45456 1.07565 7.19063 0.75 7.95813 0.75C8.72563 0.75 9.4617 1.07565 10.0044 1.65531C10.5471 2.23496 10.852 3.02115 10.852 3.84091ZM7.95813 2.29545C7.57438 2.29545 7.20635 2.45828 6.93499 2.74811C6.66364 3.03794 6.5112 3.43103 6.5112 3.84091H9.40506C9.40506 3.43103 9.25262 3.03794 8.98127 2.74811C8.70991 2.45828 8.34188 2.29545 7.95813 2.29545ZM2.97923 5.38636L4.04996 15.6884C4.10907 16.2553 4.36182 16.7791 4.75985 17.1595C5.15789 17.5398 5.67325 17.7501 6.20734 17.75H9.70892C10.2428 17.7497 10.7578 17.5393 11.1555 17.1589C11.5532 16.7786 11.8058 16.2551 11.8648 15.6884L12.9385 5.38636H2.98068H2.97923Z" fill="black"/>
                                    </svg>
                                </button>
                            </li>
                        </ul>
                    </li>--%>
                </ul>
            </section>
                            
            <button type="button" class="save_btn">저장</button>
        </section>
    </div>
</asp:Content>
