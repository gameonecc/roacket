﻿var competitionIdx = 0;

$(function () {
    competitionIdx = httpRequest("competitionIdx");

    fnGetCompetitionRules();
});

function fnGetCompetitionRules() {
    var param = {
        competitionIdx: competitionIdx
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        if (table.length > 0) {
            var row = table[0];

            // 라이브 스코어 사용여부
            var useLiveScore = row["useLiveScore"] ? "1" : "0";
            $("input[name='useLiveScore']:input[value='" + useLiveScore + "']").prop("checked", true);

            // 리그 순위 기준
            var rankRuleArr = row["rankRule"].split('|');
            for (var i = 0; i < rankRuleArr.length - 1; i++) {
                $("#selRankRule" + i).val(rankRuleArr[i]);
            }

            // 토너먼트 공동 3위 설정
            $("input[name='thirdRankRule']:input[value='" + row["thirdRankRuleCode"] + "']").prop("checked", true);

            // 기권 처리 방식
            $("input[name='giveup']:input[value='" + row["giveupRuleCode"] + "']").prop("checked", true);

            // 실격 처리 방식
            $("input[name='disqualification']:input[value='" + row["disqualRuleCode"] + "']").prop("checked", true);

            // 매치당 소요시간
            $("#txtTimePerMatch").val(row["timePerMatch"]);

            // 예선 매치 승리 세트 수
            $("#selSubWinGameCnt").val(row["subWinGameCnt"]);

            // 예선 승리 점수
            $("#txtSubWinScores").val(row["subWinScores"]);

            // 예선 듀스 여부
            var subIsDeuce = row["subIsDeuce"] ? "1" : "0";
            $("#selSubIsDeuce").val(subIsDeuce);

            // 예선 최대 점수
            $("#txtSubMaxScores").val(row["subMaxScores"]);

            // 본선 매치 승리 세트 수
            $("#selMainWinGameCnt").val(row["mainWinGameCnt"]);

            // 본선 승리 점수
            $("#txtMainWinScores").val(row["mainWinScores"]);

            // 본선 듀스 여부
            var mainIsDeuce = row["mainIsDeuce"] ? "1" : "0";
            $("#selMainIsDeuce").val(mainIsDeuce);

            // 본선 최대 점수
            $("#txtMainMaxScores").val(row["mainMaxScores"]);
        }
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionRules", param, fnCallback);
}

function fnSetCompetitionRules() {
    var rankRule = "";
    $(".league_select_box").each(function () {
        rankRule += $(this).val() + "|";
    });

    var param = {
        competitionIdx: competitionIdx,
        useLiveScore: $("input[name='useLiveScore']:checked").val() == "1",
        rankRule: rankRule,
        thirdRankRule: $("input[name='thirdRankRule']:checked").val(),
        giveupRuleCode: $("input[name='giveup']:checked").val(),
        disqualRuleCode: $("input[name='disqualification']:checked").val(),
        timePerMatch: $("#txtTimePerMatch").val(),
        subWinGameCnt: $("#selSubWinGameCnt").val(),
        subWinScore: $("#txtSubWinScores").val(),
        subMaxScore: $("#txtSubMaxScores").val(),
        subIsDeuce: $("#selSubIsDeuce").val() == "1",
        mainWinGameCnt: $("#selMainWinGameCnt").val(),
        mainWinScore: $("#txtMainWinScores").val(),
        mainMaxScore: $("#txtMainMaxScores").val(),
        mainIsDeuce: $("#selMainIsDeuce").val() == "1"
    };

    var fnCallback = function (data) {
        if (data.d == "0") {
            location.href = "/competition/competition_1.aspx";
        }
        else {
            alert("데이터 처리 중 에러가 발생하였습니다");
        }
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/SetCompetitionRules", param, fnCallback);
}