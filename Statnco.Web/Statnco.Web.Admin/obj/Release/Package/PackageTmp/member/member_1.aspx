﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="member_1.aspx.cs" Inherits="Statnco.Web.Admin.member.member_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">회원리스트</h1>
        <div class="card mb-4">
            <div class="card-body">
                <section class="table_wrap">
                    <section class="table_hd">
                        <select class="cities_provinces_select_box select_box" id="selRegion" aria-placeholder="시도" onchange="fnGetAreaList();">
                        </select>

                        <select class="select_box" name="" id="selArea" aria-placeholder="군구">
                            <option value="">군구</option>
                        </select>

                        <select class="select_box" name="" id="selTax" aria-placeholder="납부여부">
                            <option value="">납부여부</option>
                            <option value="1">납부</option>
                            <option value="0">미납</option>
                        </select>

                        <select class="club_name_select_box search_select_box select_box" name="" id="selOption">
                            <option value="1">이름</option>
                            <option value="2">아이디</option>
                            <option value="3">전화번호</option>
                            <option value="4">클럽명</option>
                        </select>

                        <section class="search_box">
                            <input type="text" class="search_input" id="txtValue">
                            <button type="button" class="search_btn" onclick="fnGetMemberList();">Search</button>
                        </section>
                    </section>

                    <ul class="table_box user_list_table">
                        <li class="table_row_wrap">
                            <ul class="table_row table_row_title">
                                <li class="table_li table_li_check">
                                    <input type="checkbox">
                                </li>
                                <li class="table_li table_li_middle">아이디</li>
                                <li class="table_li table_li_middle">이름</li>
                                <li class="table_li table_li_short">성별</li>
                                <li class="table_li table_li_middle">휴대폰</li>
                                <li class="table_li table_li_long">소속클럽</li>
                                <li class="table_li table_li_middle">소속 협회</li>
                                <li class="table_li table_li_short">시도 급수</li>
                                <li class="table_li table_li_short">시군구 급수</li>
                                <li class="table_li table_li_short">납부 여부</li>
                            </ul>
                        </li>
                    </ul>

                    <section class="post_list_btn_box">
                        
                    </section>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="ssoUserinfoModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <section class="user_info_popup modal-content">
                <button type="button" class="popup_close_btn" id="ssoUserinfoCancle" data-bs-dismiss="modal">
                    <svg class="close_icon" viewBox="0 0 20 20" fill="none">
                        <path d="M4.40002 4.3999L15.6 15.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M15.6 4.3999L4.40002 15.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>
                <h1 class="user_info_popup_title">회원정보</h1>
                <section class="user_info_wrap">
                    <section class="user_info_box">
                        <article class="user_img_box">
                            
                        </article>
                        <article class="basics_info_box">
                            <h5 class="basics_title">기본정보</h5>
                            <ul class="basics_info_table">
                                <li class="basics_info_col">
                                    <ul class="basics_info_left">
                                        <li class="basics_info_li">아이디</li>
                                        <li class="basics_info_li">이름</li>
                                        <li class="basics_info_li">성별</li>
                                        <li class="basics_info_li">생년월일</li>
                                        <li class="basics_info_li">휴대폰번호</li>
                                        <li class="basics_info_li">소속협회</li>
                                        <li class="basics_info_li">시도협회급수</li>
                                        <li class="basics_info_li">시군구협회급수</li>
                                        <li class="basics_info_li">소속클럽</li>
                                        <li class="basics_info_li">상태</li>
                                    </ul>
                                </li>
                                <li class="basics_info_col">
                                    <ul class="basics_info_right">
                                        <li class="basics_info_li" id="liUserId"></li>
                                        <li class="basics_info_li" id="liUserName"></li>
                                        <li class="basics_info_li" id="liGender">남</li>
                                        <li class="basics_info_li" id="liBirth"></li>
                                        <li class="basics_info_li" id="liPhone"></li>
                                        <li class="basics_info_li" id="liAssociationName"></li>
                                        <li class="basics_info_li basics_info_li_row">
                                            <select name="" id="selSidoGrade" class="rating_select_box" onchange="fnSetMemberGrade(1);">
                                            </select>
                                        </li>
                                        <li class="basics_info_li basics_info_li_row">
                                            <select name="" id="selSigunguGrade" class="rating_select_box" onchange="fnSetMemberGrade(2);">
                                            </select>
                                        </li>
                                        <li class="basics_info_li" id="liClubName"></li>
                                        <li class="basics_info_li" id="liClubState"></li>
                                    </ul>
                                </li>
                            </ul>
                        </article>
                    </section>
                </section>
            </section>
        </div>
    </div>
</asp:Content>
