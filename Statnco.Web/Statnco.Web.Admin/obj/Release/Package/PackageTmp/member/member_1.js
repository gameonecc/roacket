﻿var pageNo = 1;
var totalNo = 0;
var memberIdx = 0;

$(function () {
    fnGetComCode("GRADE", $("#selSidoGrade"));

    fnGetComCode("GRADE", $("#selSigunguGrade"));

    fnGetRegionList(true);

    setTimeout(function () {
        fnGetMemberList();
    }, 1000);
});

// 시도 데이터 바인딩
function fnGetMemberList() {
    var listStr = "<li class=\"table_row_wrap\">\
                        <ul class=\"table_row\">\
                            <li class=\"table_li table_li_check\">\
                                <input type=\"checkbox\">\
                            </li>\
                            <li class=\"table_li table_li_middle\" onclick=\"fnUserPop({0});\">{1}</li>\
                            <li class=\"table_li table_li_middle\" onclick=\"fnUserPop({0});\">{2}</li>\
                            <li class=\"table_li table_li_short\">{3}</li>\
                            <li class=\"table_li table_li_middle\">{4}</li>\
                            <li class=\"table_li table_li_long\">{5}</li>\
                            <li class=\"table_li table_li_middle\">{6}</li>\
                            <li class=\"table_li table_li_short\">{7}</li>\
                            <li class=\"table_li table_li_short\">{8}</li>\
                            <li class=\"table_li table_li_short\">{9}</li>\
                        </ul>\
                    </li>";

    var param = {
        "regionCode": $("#selRegion").val(),
        "areaCode": $("#selArea").val(),
        "isTax": $("#selTax").val(),
        "option": $("#selOption").val(),
        "value": $("#txtValue").val(),
        "pageNo" : pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        totalNo = table1[0]["totalCnt"];

        $(".table_box").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["memberIdx"],
                        table[i]["userId"],
                        table[i]["userName"],
                        table[i]["gender"] == "M" ? "남" : "여",
                        table[i]["phone"],
                        table[i]["clubName"],
                        table[i]["assName"],
                        table[i]["sidoGrade"],
                        table[i]["sigunguGrade"],
                        table[i]["tax"] == "1" ? "납부" : "미납"
                    )
                );
            }

            fnSetPageing();
        }
    }

    window.fnAjaxSend("/member/ws_member.asmx/GetMemberList", param, fnCallback);
}

function fnUserPop(idx) {
    memberIdx = idx;

    var param = {
        memberIdx: memberIdx
    };

    var fnCallback = function (data) {
        var row = JSON.parse(data.d).Table[0];

        // 유저 프로필 사진
        if (row["profilePath"] == null) {
            $(".user_img_box").css("background-image", "url(/img/user-default.png)");
        }
        else {
            $(".user_img_box").css("background-image", "url(/img/user-default.png)");
        }

        // 유저 아이디
        $("#liUserId").text(row["userId"]);

        // 유저 이름
        $("#liUserName").text(row["userName"]);

        // 성별
        $("#liGender").text(row["gender"] == "1" ? "남" : "여");

        // 생년월일
        $("#liBirth").text(row["birth"]);

        // 휴대폰 번호
        $("#liPhone").text(row["phone"]);

        // 소속 협회 명
        $("#liAssociationName").text(row["associationName"]);

        // 시도 그레이드
        $("#selSidiGrade").val(row["sidoGrade"]);

        // 시군구 그레이드
        $("#selSigunguGrade").val(row["sigunguGrade"]);

        // 클럽이름
        $("#liClubName").text(row["clubName"]);

        // 클럽 멤버 상태
        $("#liClubState").text(row["memberGrade"] == "5" ? "승인대기" : "가입완료");

        $("#ssoUserinfoModal").modal("show");
    };

    window.fnAjaxSend("/member/ws_member.asmx/GetMemberDetail", param, fnCallback);
}

function fnSetMemberGrade(type) {
    var param = {
        memberIdx: memberIdx,
        gradeType: type,
        gradeCode: type == 1 ? $("#selSidoGrade").val() : $("#selSigunguGrade").val()
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다");
        }
    };

    window.fnAjaxSend("/member/ws_member.asmx/SetMemberGrade", param, fnCallback);
}


function fnGetList() {
    fnGetMemberList();
}