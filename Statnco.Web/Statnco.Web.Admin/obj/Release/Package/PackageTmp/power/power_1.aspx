﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="power_1.aspx.cs" Inherits="Statnco.Web.Admin.power.power_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">권한모듈관리</h1>
        <section class="authority_module_wrap">
            <section class="authority_module_check_box_wrap">
                <article class="checkbox_hd">
                    <select class="authority_select_box" name="" id="selModule">
                    </select>
                    <button type="button" class="authority_module_plus_btn" onclick="fnPopup();">
                        권한모듈추가
                        <svg class="plus_icon" viewBox="0 0 27 27" fill="none">
                            <path d="M21.6002 2.7002H5.4002C3.9152 2.7002 2.7002 3.9152 2.7002 5.4002V21.6002C2.7002 23.0852 3.9152 24.3002 5.4002 24.3002H21.6002C23.0852 24.3002 24.3002 23.0852 24.3002 21.6002V5.4002C24.3002 3.9152 23.0852 2.7002 21.6002 2.7002ZM20.2502 14.8502H14.8502V20.2502H12.1502V14.8502H6.7502V12.1502H12.1502V6.7502H14.8502V12.1502H20.2502V14.8502Z" fill="#009F59"/>
                        </svg>
                    </button>
                </article>

                <ul class="checkbox_table">
                    
                </ul>
            </section>
        </section>
    </div>
    <!-- 모듈추가 팝업 -->
    <div class="modal fade" id="ssoModuleplusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <section class="module_plus_popup modal-content">
                <button type="button" class="popup_close_btn" id="ssoModuleplusCancle" data-bs-dismiss="modal">
                    <svg class="close_icon" viewBox="0 0 20 20" fill="none">
                        <path d="M4.40039 4.3999L15.6004 15.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        <path d="M15.6004 4.3999L4.40039 15.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                    </svg>
                </button>
                <h5 class="module_plus_popup_title">권한모듈 추가</h5>
                <input type="text" class="module_plus_popup_input" placeholder="추가할 모듈명을 적어주세요." id="txtModuleName">
                <button type="button" class="registration_btn" onclick="fnSetMenuModule();">등록</button>
            </section>
        </div>
    </div>
</asp:Content>
