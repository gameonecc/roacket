﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="power_2.aspx.cs" Inherits="Statnco.Web.Admin.power.power_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">권한관리</h1>
        <section class="authority_wrap">
            <section class="search_box">
                <select class="manager_id_select_box" name="" id="selOption" aria-placeholder="관리자ID">
                    <option value="1">이름</option>
                    <option value="2">아이디</option>
                    <option value="3">전화번호</option>
                </select>
                <input type="text" class="input_id" id="txtValue">
                <button type="button" class="search_btn" onclick="fnGetManagerList();">Search</button>
            </section>
            <ul class="manager_list_table">
                <li class="manager_list_wrap">
                    <ul class="manager_list_row manager_list_title">
                        <li class="manager_li short_li">idx</li>
                        <li class="manager_li long_li">ID</li>
                        <li class="manager_li middle_li">이름</li>
                        <li class="manager_li long_li">구분</li>
                    </ul>
                </li>
            </ul>

            <section class="authority_check_box_wrap">
                <select class="check_box_title_select_box" name="" id="selModule" aria-placeholder="선택" onchange="fnSetMenuModuleToMember();">
                    <option value="">선택</option>
                </select>

                <ul class="checkbox_table">
                    
                </ul>
            </section>
            <svg class="check_box_wrap_icon" viewBox="0 0 41 64" fill="none">
                <g filter="url(#filter0_d_246_316)">
                <path d="M4 28L37 0V56L4 28Z" fill="white"/>
                </g>
                <defs>
                <filter id="filter0_d_246_316" x="0" y="0" width="41" height="64" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                <feFlood flood-opacity="0" result="BackgroundImageFix"/>
                <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                <feOffset dy="4"/>
                <feGaussianBlur stdDeviation="2"/>
                <feComposite in2="hardAlpha" operator="out"/>
                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"/>
                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_246_316"/>
                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_246_316" result="shape"/>
                </filter>
                </defs>
            </svg>
        </section>
    </div>
</asp:Content>
