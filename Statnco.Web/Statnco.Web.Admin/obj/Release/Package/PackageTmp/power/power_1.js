﻿$(function () {
    fnGetMenuModuleList();
});

function fnPopup() {
    $("#ssoModuleplusModal").modal("show");
}

function fnGetMenuModuleList() {
    var param = {};

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $("#selModule").append(
                    String.format(
                        "<option value=\"{0}\">{1}</option>",
                        table[i]["moduleIdx"],
                        table[i]["moduleName"]
                    )
                );
            }

            fnGetMenuModulePower();
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/GetMenuModuleList", param, fnCallback);
}

function fnGetMenuModulePower() {
    var param = {
        "moduleIdx": $("#selModule").val()
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        var mainStr = "<li class=\"checkbox_table_row\">\
                            <h5 class=\"checkbox_title\">{1}</h5>\
                            <ul class=\"checkbox_li_wrap\" id=\"sub_{0}\"></ul>\
                        </li > ";

        var subStr = "<li class=\"checkbox_li\">\
                            <label for=\"check_0{0}_{1}\">{2}</label>\
                            <input id=\"check_0{0}_{1}\" type=\"checkbox\" class=\"input_check_box\" onclick=\"fnSetMenuModulePower({0},{1});\">\
                        </li>";

        $(".checkbox_table").empty();

        var mainIdx = 0;

        for (var i = 0; i < table.length; i++) {
            if (table[i]["menuSubIdx"] == "0") {
                mainIdx = table[i]["menuIdx"];

                $(".checkbox_table").append(
                    String.format(
                        mainStr,
                        table[i]["menuIdx"],
                        table[i]["menuName"]
                    )
                );
            }
            else {
                $("#sub_" + table[i]["menuIdx"]).append(
                    String.format(
                        subStr,
                        table[i]["menuIdx"],
                        table[i]["menuSubIdx"],
                        table[i]["menuName"]
                    )
                );

                var sel = "#check_0" + table[i]["menuIdx"] + "_" + table[i]["menuSubIdx"];

                $(sel).prop("checked", table[i]["power"] == "W");
            }
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/GetMenuModulePower", param, fnCallback);
}

function fnSetMenuModule() {
    var param = {
        "moduleName": $("#txtModuleName").val()
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다.");
        }
        else {
            fnGetMenuModuleList();
            $("#ssoModuleplusModal").modal("hide");
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/SetMenuModule", param, fnCallback);
}

function fnSetMenuModulePower(menuIdx, menuSubIdx) {
    var param = {
        "moduleIdx": $("#selModule").val(),
        "menuIdx": menuIdx,
        "menuSubIdx": menuSubIdx,
        "power": $("#check_0" + menuIdx + "_" + menuSubIdx).is(":checked") ? "W" : "N"
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다.");
        }
        else {
            fnGetMenuModuleList();
            $("#ssoModuleplusModal").modal("hide");
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/SetMenuModulePower", param, fnCallback);
}