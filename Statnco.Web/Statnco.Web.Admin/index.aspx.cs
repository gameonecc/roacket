﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Data;

namespace Statnco.Web.Admin
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            //string url = "https://api.roacket.com/AdminLogin";
            //var request = (HttpWebRequest)WebRequest.Create(url);

            //var postData = "id=" + Uri.EscapeDataString(txtId.Text);
            //postData += "&password=" + Uri.EscapeDataString(txtPw.Text);

            //var data = Encoding.ASCII.GetBytes(postData);

            //request.Method = "POST";
            //request.ContentType = "application/x-www-form-urlencoded";
            //request.ContentLength = data.Length;

            //using (var stream = request.GetRequestStream())
            //{
            //    stream.Write(data, 0, data.Length);
            //}

            //var response = (HttpWebResponse)request.GetResponse();

            //Stream receiveStream = response.GetResponseStream();

            //StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

            //var returnJson = readStream.ReadToEnd();

            //Root loginInfo = JsonConvert.DeserializeObject<Root>(returnJson);

            //response.Close();
            //readStream.Close();

            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetAdminLoginInfo(txtId.Text, Crypto.fnGetSHA512(txtPw.Text));
                }
            }
            catch (Exception exp)
            {
                throw;
            }

            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];

                // 로그인 성공
                Response.Cookies["memberIdx"].Value = dr["memberIdx"].ToString();
                Response.Cookies["regionCode"].Value = dr["regionCode"].ToString();
                Response.Cookies["areaCode"].Value = dr["areaCode"].ToString();
                Response.Cookies["assIdx"].Value = dr["associationId"].ToString();
                Response.Cookies["memberType"].Value = dr["memberType"].ToString();

                Response.Redirect("/main.aspx");
            }
            else
            {

            }


        }

    }
}