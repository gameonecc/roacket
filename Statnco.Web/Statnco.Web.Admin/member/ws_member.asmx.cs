﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Data;
using System.Web.Services;

namespace Statnco.Web.Admin.member
{
    /// <summary>
    /// ws_member의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_member : System.Web.Services.WebService
    {
        #region 회원 리스트 조회
        [WebMethod]
        public string GetMemberList(int regionCode, int areaCode, string isTax,
            int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetMemberList(regionCode, areaCode, isTax, option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 회원 정보
        [WebMethod]
        public string GetMemberDetail(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetMemberDetail(memberIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt.ToJSONString();
        }
        #endregion

        #region 회원 등급 변경
        [WebMethod]
        public int SetMemberGrade(int memberIdx, byte gradeType, byte gradeCode)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetMemberGrade(memberIdx, gradeType, gradeCode);
                }
            }
            catch (Exception exp)
            {
                return rtn;
            }

            return rtn;
        }
        #endregion
    }
}
