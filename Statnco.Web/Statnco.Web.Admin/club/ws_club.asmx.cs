﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Data;
using System.Web;
using System.Web.Services;

namespace Statnco.Web.Admin.club
{
    /// <summary>
    /// ws_club의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_club : System.Web.Services.WebService
    {

        #region 클럽 리스트 조회
        [WebMethod]
        public string GetClubList(int regionCode, int areaCode, string clubName, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetClubList(regionCode, areaCode, clubName, pageNo);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 클럽 회원 리스트 조회
        [WebMethod]
        public string GetClubMemberList(int clubIdx, int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetClubMemberList(clubIdx, option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 클럽 회원 등급 변경
        [WebMethod]
        public int SetClubMemberGrade(int clubIdx, int memberIdx, int memberGrade)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetClubMemberGrade(clubIdx, memberIdx, memberGrade, editUser);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion

        #region 협회 회비 일괄 납부
        [WebMethod]
        public int SetClubMemberTax(int clubIdx, string memberList)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetClubMemberTax(clubIdx, memberList, editUser);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion

    }
}
