﻿var clubIdx = 0;
var pageNo = 1;
var totalNo = 0;

$(function () {
    clubIdx = httpRequest("clubIdx");

    fnGetClubMember();
});

function fnGetClubMember() {
    var gradeOption = "<select name=\"\" id=\"selGrade_{0}\" onchange=\"fnSetClubMemberGrade({0});\" class=\"club_select_box\" aria-placeholder=\"클럽장\">\
                                    <option value=\"99\">클럽장</option>\
                                    <option value=\"10\">일반회원</option>\
                                </select>";

    var listStr = "<li class=\"table_row_wrap\">\
                        <ul class=\"table_row\">\
                            <li class=\"table_li table_li_check\">\
                                <input type=\"checkbox\" name=\"chkMember\" idx=\"{0}\">\
                            </li>\
                            <li class=\"table_li table_li_short\">{0}</li>\
                            <li class=\"table_li table_li_middle\">{1}</li>\
                            <li class=\"table_li table_li_middle\">{2}</li>\
                            <li class=\"table_li table_li_short\">{3}</li>\
                            <li class=\"table_li table_li_long\">{4}</li>\
                            <li class=\"table_li table_li_middle\">{5}</li>\
                            <li class=\"table_li table_li_middle\">{6}</li>\
                            <li class=\"table_li table_li_short\">{7}</li>\
                            <li class=\"table_li table_li_middle\">{8}</li>\
                            <li class=\"table_li table_li_middle\">\
                                {9}\
                            </li>\
                            <li class=\"table_li table_li_short\">{10}</li>\
                        </ul>\
                    </li>";

    var param = {
        "clubIdx": clubIdx,
        "option": $("#selOption").val(),
        "value": $("#txtValue").val(),
        "pageNo": pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        totalNo = table1[0]["totalCnt"];

        $(".club_name_title").text(table1[0]["clubName"]);

        $(".table_box").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["memberIdx"],
                        table[i]["userId"],
                        table[i]["userName"],
                        table[i]["gender"] == "M" ? "남" : "여",
                        table[i]["phone"],
                        table[i]["birth"],
                        table[i]["associationName"],
                        table[i]["sidoGrade"],
                        table[i]["sigunguGrade"],
                        String.format(gradeOption, table[i]["memberIdx"]),
                        table[i]["isTax"] == "1" ? "납부" : "미납"
                    )
                );

                $("#selGrade_" + table[i]["memberIdx"]).val(table[i]["memberGrade"]);
            }
        }

        fnSetPageing();
    }

    window.fnAjaxSend("/club/ws_club.asmx/GetClubMemberList", param, fnCallback);
}

function fnSetClubMemberGrade(clubMemberIdx) {
    var param = {
        "clubIdx": clubIdx,
        "memberIdx": clubMemberIdx,
        "memberGrade": $("#selGrade_" + clubMemberIdx).val()
    };

    var fnCallback = function (data) {
        if (data.d == "0") {

        }
        else {
            alert("데이터 저장 중 에러가 발생하였습니다.");
        }
    }

    window.fnAjaxSend("/club/ws_club.asmx/SetClubMemberGrade", param, fnCallback);
}

function fnSetClubMemberTax() {
    var targetList = "";

    $("input[name='chkMember']:checked").each(function () {
        targetList += $(this).attr("idx") + "|";
    });

    var param = {
        "clubIdx": clubIdx,
        "memberList": targetList
    };

    var fnCallback = function (data) {
        if (data.d == "0") {
            fnGetClubMember();
        }
        else {
            alert("데이터 저장 중 에러가 발생하였습니다.");
        }
    }

    window.fnAjaxSend("/club/ws_club.asmx/SetClubMemberTax", param, fnCallback);
}

function fnChkAll() {
    $("input[name='chkMember']").prop("checked", $("#chkAll").is(":checked"));
}

function fnGetList() {
    fnGetClubMember();
}