﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="club_1.aspx.cs" Inherits="Statnco.Web.Admin.club.club_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">클럽관리</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <select class="cities_provinces_select_box select_box" name="" id="selRegion" aria-placeholder="시도" onchange="fnGetAreaList();">
                    <option value="">시도</option>
                </select>

                <select class="borough_select_box select_box" name="" id="selArea" >
                </select>

                <section class="search_box">
                    <input type="text" class="search_input" placeholder="클럽명" id="searchClubName">
                    <button type="button" class="search_btn" onclick="fnGetClubList();">Search</button>
                </section>

                <button type="button" class="club_plus_btn" id="ssoClubplusBtn" onclick="fnEditClub();">
                    클럽추가
                    <svg class="plus_icon" viewBox="0 0 27 27" fill="none">
                        <path d="M21.6 2.7002H5.40001C3.91501 2.7002 2.70001 3.9152 2.70001 5.4002V21.6002C2.70001 23.0852 3.91501 24.3002 5.40001 24.3002H21.6C23.085 24.3002 24.3 23.0852 24.3 21.6002V5.4002C24.3 3.9152 23.085 2.7002 21.6 2.7002ZM20.25 14.8502H14.85V20.2502H12.15V14.8502H6.75001V12.1502H12.15V6.7502H14.85V12.1502H20.25V14.8502Z" fill="#009F59"/>
                    </svg>
                </button>
            </section>

            <ul class="table_box club_list_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_short">idx</li>
                        <li class="table_li table_li_middle">클럽명</li>
                        <li class="table_li table_li_middle">시도</li>
                        <li class="table_li table_li_middle">시군구</li>
                        <li class="table_li table_li_middle">주요체육관</li>
                        <li class="table_li table_li_long">체육관주소(클럽주소)</li>
                        <li class="table_li table_li_middle">회원수(회비납부)</li>
                    </ul>
                </li>
            </ul>
        
            <section class="post_list_btn_box">
            </section>


        </section>
    </div>
    <!-- 클럽추가 팝업 -->
    <div class="modal fade" id="ssoClubplusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <section class="club_plus_popup modal-content">
                <section class="popup_hd">
                    <h3 class="popup_title">클럽정보</h3>
                    <button type="button" class="popup_close_btn" id="ssoClubplusCancle" data-bs-dismiss="modal">
                        <svg class="close_icon" viewBox="0 0 14 14" fill="none">
                            <path d="M1.40002 1.3999L12.6 12.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.6 1.3999L1.40002 12.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                </section>
                <section class="popup_main">
                    <ul class="club_input_table">
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">클럽명<span class="star_mark"> &#42;</span></h3>
                            <input type="text" class="club_input" id="txtClubName">
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">소속협회</h3>
                            <input type="text" class="club_input" onfocus="fnOpenAss();">
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">주요체육관</h3>
                            <input type="text" class="club_input" onfocus="fnOpenAddr();">
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">체육관주소</h3>
                            <input type="text" class="club_input" readonly="readonly">
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">클럽장성명</h3>
                            <input type="text" class="club_input" onfocus="fnOpenAdmin();" >
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">휴대폰번호</h3>
                            <input type="text" class="club_input" id="txtPhone">
                        </li>
                        <li class="club_input_table_li">
                            <h3 class="club_input_table_title">상태</h3>

                            <article class="club_input_row">
                                <p class="association_select_title">협회소속</p>

                                <select class="association_select_box" name="" id="" aria-placeholder="협회소속">
                                    <option value="">Y</option>
                                    <option value="">N</option>
                                </select>
                            </article>
                        </li>
                    </ul>

                    <section class="popup_btn_box">
                        <button type="button" class="cancle_btn">삭제</button>
                        <button type="button" class="save_btn">저장</button>
                    </section>
                </section>
            </section>
        </div>
    </div>
</asp:Content>
