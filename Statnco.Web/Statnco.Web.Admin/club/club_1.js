﻿var pageNo = 1;
var totalNo = 0;

$(function () {
    fnGetRegionList(true);
});

function fnGetClubList() {
    var listStr = "<li class=\"table_row_wrap\">\
                        <ul class=\"table_row\">\
                            <li class=\"table_li table_li_short\">{0}</li>\
                            <li class=\"table_li table_li_middle\">{1}</li>\
                            <li class=\"table_li table_li_middle\">{2}</li>\
                            <li class=\"table_li table_li_middle\">{3}</li>\
                            <li class=\"table_li table_li_middle\">{4}</li>\
                            <li class=\"table_li table_li_long\">{5}</li>\
                            <li class=\"table_li table_li_middle\" onclick=\"fnMoveClubMember({0});\">{6}({7})명</li>\
                        </ul>\
                    </li>";



    var param = {
        "regionCode": $("#selRegion").val(),
        "areaCode": $("#selArea").val(),
        "clubName": $("#searchClubName").val(),
        "pageNo": pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        totalNo = table1[0]["totalCnt"];


        $(".club_list_table").children("li:gt(0)").remove();
        
        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".club_list_table").append(
                    String.format(
                        listStr,
                        table[i]["clubIdx"],
                        table[i]["clubName"],
                        table[i]["regionName"],
                        table[i]["areaName"],
                        table[i]["stadiumName"],
                        table[i]["stadiumAddr"],
                        table[i]["clubMemberCnt"],
                        table[i]["taxMemberCnt"]
                    )
                );
            }
        }

        fnSetPageing();
    }

    window.fnAjaxSend("/club/ws_club.asmx/GetClubList", param, fnCallback);
}

function fnEditClub() {
    $("#ssoClubplusModal").modal("show");
}


function fnGetList() {
    fnGetClubList();
}

function fnMoveClubMember(idx) {
    location.href = "/club/club_2.aspx?clubIdx=" + idx;
}