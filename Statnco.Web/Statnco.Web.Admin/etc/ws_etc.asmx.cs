﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Statnco.Web.Admin.etc
{
    /// <summary>
    /// ws_etc의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_etc : System.Web.Services.WebService
    {

        #region 광고 리스트 조회
        [WebMethod]
        public string GetAdvertisementList(int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetAdvertisementList(pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return ds.ToJSONString();
        }
        #endregion

    }
}
