﻿using Statnco.DA.Roacket.Admin;
using System;
using System.Data;
using System.Text;

namespace Statnco.Web.Admin.master
{
    public partial class roacket : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Cookies["memberIdx"] != null)
            {
                int memberIdx = Convert.ToInt32(Request.Cookies["memberIdx"].Value);

                DataTable menuDt = null;

                using (var biz = new Roacket_Admin_Biz())
                {
                    menuDt = biz.GetMenuPower(memberIdx);
                }

                fnSetMenu(menuDt);
            }
            else
            {
                Response.Redirect("/index.aspx");
            }
        }

        protected void fnSetMenu(DataTable menuDt)
        {
            string menuStr = string.Empty;

            StringBuilder sbMain = new StringBuilder();
            sbMain.Append("<a class=\"nav-link\" href=\"{1}\" id=\"solo_{0}\">");
            sbMain.Append("    {2}");
            sbMain.Append("    <div class=\"sb-sidenav-collapse-arrow menu_icon_hidden\"><i class=\"fas fa-angle-down\"></i></div>");
            sbMain.Append("</a>");

            StringBuilder sbSubMain = new StringBuilder();
            sbSubMain.Append("<a class=\"nav-link collapsed\" id=\"menu{0}\" href=\"{1}\" data-bs-toggle=\"collapse\" data-bs-target=\"#collapse_{0}\" aria-expanded=\"false\" aria-controls=\"collapse_{0}\">");
            sbSubMain.Append("    {2}");
            sbSubMain.Append("    <div class=\"sb-sidenav-collapse-arrow\"><i class=\"fas fa-angle-down\"></i></div>");
            sbSubMain.Append("</a>");

            StringBuilder sbSub = new StringBuilder();
            sbSub.Append("<div class=\"collapse\" id=\"collapse_{0}\" aria-labelledby=\"heading_{0}\" data-bs-parent=\"#sidenavAccordion\">");
            sbSub.Append("    <nav class=\"sb-sidenav-menu-nested nav accordion\" id=\"sidenavAccordion_{0}\">");
            sbSub.Append("        <a class=\"nav-link collapsed\" href=\"{1}\" >");
            sbSub.Append("            {2}");
            sbSub.Append("        </a>");
            sbSub.Append("    </nav>");
            sbSub.Append("</div>");




            for (var i = 0; i < menuDt.Rows.Count; i++)
            {
                DataRow dr = menuDt.Rows[i];

                // 서브 메뉴 Idx 가 0이고(대메뉴란 이야기) 다음 메뉴가 전체 길이보다 작다면(다음에도 데이터가 있다는 이야기)
                if ((i + 1) < menuDt.Rows.Count && Convert.ToInt32(dr["menuSubIdx"]) == 0)
                {
                    // 다음메뉴도 대메뉴 일경우
                    if (Convert.ToInt32(menuDt.Rows[i + 1]["menuSubIdx"]) == 0)
                    {
                        // 단독 대메뉴 스트링 넣기
                        menuStr += String.Format(
                            sbMain.ToString(),
                            dr["menuIdx"].ToString(),
                            String.Format(
                                "/{0}/{0}_{1}.aspx", 
                                dr["menuString"].ToString(), 
                                dr["menuSubIdx"].ToString() == "0" ? "1" : dr["menuSubIdx"].ToString()
                            ),
                            dr["menuName"].ToString()
                        );
                    }
                    else
                    {
                        // 다음 메뉴가 소메뉴 일경우 접는 스트링 넣기
                        menuStr += String.Format(
                            sbSubMain.ToString(),
                            dr["menuIdx"].ToString(),
                            String.Format(
                                "/{0}/{0}_{1}.aspx",
                                dr["menuString"].ToString(),
                                dr["menuSubIdx"].ToString() == "0" ? "1" : dr["menuSubIdx"].ToString()
                            ),
                            dr["menuName"].ToString()
                        );
                    }
                }
                else
                {
                    if (Convert.ToInt32(dr["menuSubIdx"]) == 0)
                    {
                        menuStr += String.Format(
                            sbMain.ToString(),
                            dr["menuIdx"].ToString(),
                            String.Format(
                                "/{0}/{0}_{1}.aspx",
                                dr["menuString"].ToString(),
                                dr["menuSubIdx"].ToString() == "0" ? "1" : dr["menuSubIdx"].ToString()
                            ),
                            dr["menuName"].ToString()
                        );
                    }
                    else
                    {
                        menuStr += String.Format(
                            sbSub.ToString(),
                            dr["menuIdx"].ToString(),
                            String.Format(
                                "/{0}/{0}_{1}.aspx",
                                dr["menuString"].ToString(),
                                dr["menuSubIdx"].ToString() == "0" ? "1" : dr["menuSubIdx"].ToString()
                            ),
                            dr["menuName"].ToString()
                        );
                    }
                }


            }

            divMenu.InnerHtml = menuStr;
        }

        private string fnGetMenuString(int menuIdx, int menuSubIdx)
        {
            string menuStr = string.Empty;

            switch (menuIdx)
            {
                case 1:
                    menuStr = "link";
                    break;
                case 2:
                    menuStr = "member";
                    break;
                case 3:
                    menuStr = "club";
                    break;
                case 4:
                    menuStr = "competition";
                    break;
                case 5:
                    menuStr = "board";
                    break;
                case 6:
                    menuStr = "basic";
                    break;
                case 7:
                    menuStr = "etc";
                    break;
                case 8:
                    menuStr = "association";
                    break;
            }

            return String.Format("/{0}/{0}_{1}.aspx", menuStr, (menuSubIdx == 0) ? 1 : menuSubIdx);
        }

        protected void btnLogout_Click(object sender, EventArgs e)
        {
            Response.Cookies["memberIdx"].Expires = DateTime.Now.AddDays(-1);
            Response.Redirect("/");
        }
    }
}