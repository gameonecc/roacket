﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="competition_3.aspx.cs" Inherits="Statnco.Web.Admin.competition.competition_3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <section class="contest_way_wrap">
            <section class="contest_way_hd">
                대회관리 / 대회생성 / <span class="now_page_font">대회규칙</span>
            </section>

            <h1 class="contest_way_title">대회규칙</h1>

            <h6 class="input_title">라이브 스코어 사용여부</h6>
            <p class="input_sub_title">• 태블릿 등의 단말기를 통해 대회장에서 점수를 실시간으로 입력하고, 웹페이지에서 확인 할 수 있습니다.</p>
            <article class="radio_input_box">
                <label><input class="radio_btn" type="radio" name="useLiveScore" value="1">사용</label>
                <label><input class="radio_btn" type="radio" name="useLiveScore" value="0">미사용</label>
            </article>

            <h6 class="input_title">리그 순위 기준</h6>
            <article class="select_input_box">
                <select class="league_select_box" name="" id="selRankRule0" aria-placeholder="다승">
                    <option value="1">다승</option>
                    <option value="2">승자승</option>
                    <option value="3">득실차</option>
                    <option value="4">저실점</option>
                    <option value="5">연장자</option>
                </select>
                <svg class="next_icon" viewBox="0 0 24 24" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.96912 2.46936C7.03879 2.39952 7.12155 2.34411 7.21267 2.3063C7.30379 2.26849 7.40147 2.24902 7.50012 2.24902C7.59877 2.24902 7.69645 2.26849 7.78757 2.3063C7.87869 2.34411 7.96145 2.39952 8.03112 2.46936L17.0311 11.4694C17.101 11.539 17.1564 11.6218 17.1942 11.7129C17.232 11.804 17.2515 11.9017 17.2515 12.0004C17.2515 12.099 17.232 12.1967 17.1942 12.2878C17.1564 12.3789 17.101 12.4617 17.0311 12.5314L8.03112 21.5314C7.89029 21.6722 7.69928 21.7513 7.50012 21.7513C7.30096 21.7513 7.10995 21.6722 6.96912 21.5314C6.82829 21.3905 6.74917 21.1995 6.74917 21.0004C6.74917 20.8012 6.82829 20.6102 6.96912 20.4694L15.4396 12.0004L6.96912 3.53136C6.89927 3.4617 6.84386 3.37893 6.80605 3.28781C6.76824 3.1967 6.74878 3.09902 6.74878 3.00036C6.74878 2.90171 6.76824 2.80403 6.80605 2.71291C6.84386 2.6218 6.89927 2.53903 6.96912 2.46936Z" fill="black"/>
                </svg>
                <select class="league_select_box" name="" id="selRankRule1" aria-placeholder="승자승">
                    <option value="1">다승</option>
                    <option value="2">승자승</option>
                    <option value="3">득실차</option>
                    <option value="4">저실점</option>
                    <option value="5">연장자</option>
                </select>
                <svg class="next_icon" viewBox="0 0 24 24" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.96912 2.46936C7.03879 2.39952 7.12155 2.34411 7.21267 2.3063C7.30379 2.26849 7.40147 2.24902 7.50012 2.24902C7.59877 2.24902 7.69645 2.26849 7.78757 2.3063C7.87869 2.34411 7.96145 2.39952 8.03112 2.46936L17.0311 11.4694C17.101 11.539 17.1564 11.6218 17.1942 11.7129C17.232 11.804 17.2515 11.9017 17.2515 12.0004C17.2515 12.099 17.232 12.1967 17.1942 12.2878C17.1564 12.3789 17.101 12.4617 17.0311 12.5314L8.03112 21.5314C7.89029 21.6722 7.69928 21.7513 7.50012 21.7513C7.30096 21.7513 7.10995 21.6722 6.96912 21.5314C6.82829 21.3905 6.74917 21.1995 6.74917 21.0004C6.74917 20.8012 6.82829 20.6102 6.96912 20.4694L15.4396 12.0004L6.96912 3.53136C6.89927 3.4617 6.84386 3.37893 6.80605 3.28781C6.76824 3.1967 6.74878 3.09902 6.74878 3.00036C6.74878 2.90171 6.76824 2.80403 6.80605 2.71291C6.84386 2.6218 6.89927 2.53903 6.96912 2.46936Z" fill="black"/>
                </svg>
                <select class="league_select_box" name="" id="selRankRule2" aria-placeholder="득실차">
                    <option value="1">다승</option>
                    <option value="2">승자승</option>
                    <option value="3">득실차</option>
                    <option value="4">저실점</option>
                    <option value="5">연장자</option>
                </select>
                <svg class="next_icon" viewBox="0 0 24 24" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.96912 2.46936C7.03879 2.39952 7.12155 2.34411 7.21267 2.3063C7.30379 2.26849 7.40147 2.24902 7.50012 2.24902C7.59877 2.24902 7.69645 2.26849 7.78757 2.3063C7.87869 2.34411 7.96145 2.39952 8.03112 2.46936L17.0311 11.4694C17.101 11.539 17.1564 11.6218 17.1942 11.7129C17.232 11.804 17.2515 11.9017 17.2515 12.0004C17.2515 12.099 17.232 12.1967 17.1942 12.2878C17.1564 12.3789 17.101 12.4617 17.0311 12.5314L8.03112 21.5314C7.89029 21.6722 7.69928 21.7513 7.50012 21.7513C7.30096 21.7513 7.10995 21.6722 6.96912 21.5314C6.82829 21.3905 6.74917 21.1995 6.74917 21.0004C6.74917 20.8012 6.82829 20.6102 6.96912 20.4694L15.4396 12.0004L6.96912 3.53136C6.89927 3.4617 6.84386 3.37893 6.80605 3.28781C6.76824 3.1967 6.74878 3.09902 6.74878 3.00036C6.74878 2.90171 6.76824 2.80403 6.80605 2.71291C6.84386 2.6218 6.89927 2.53903 6.96912 2.46936Z" fill="black"/>
                </svg>
                <select class="league_select_box" name="" id="selRankRule3" aria-placeholder="저실점">
                    <option value="1">다승</option>
                    <option value="2">승자승</option>
                    <option value="3">득실차</option>
                    <option value="4">저실점</option>
                    <option value="5">연장자</option>
                </select>
                <svg class="next_icon" viewBox="0 0 24 24" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M6.96912 2.46936C7.03879 2.39952 7.12155 2.34411 7.21267 2.3063C7.30379 2.26849 7.40147 2.24902 7.50012 2.24902C7.59877 2.24902 7.69645 2.26849 7.78757 2.3063C7.87869 2.34411 7.96145 2.39952 8.03112 2.46936L17.0311 11.4694C17.101 11.539 17.1564 11.6218 17.1942 11.7129C17.232 11.804 17.2515 11.9017 17.2515 12.0004C17.2515 12.099 17.232 12.1967 17.1942 12.2878C17.1564 12.3789 17.101 12.4617 17.0311 12.5314L8.03112 21.5314C7.89029 21.6722 7.69928 21.7513 7.50012 21.7513C7.30096 21.7513 7.10995 21.6722 6.96912 21.5314C6.82829 21.3905 6.74917 21.1995 6.74917 21.0004C6.74917 20.8012 6.82829 20.6102 6.96912 20.4694L15.4396 12.0004L6.96912 3.53136C6.89927 3.4617 6.84386 3.37893 6.80605 3.28781C6.76824 3.1967 6.74878 3.09902 6.74878 3.00036C6.74878 2.90171 6.76824 2.80403 6.80605 2.71291C6.84386 2.6218 6.89927 2.53903 6.96912 2.46936Z" fill="black"/>
                </svg>
                <select class="league_select_box" name="" id="selRankRule4" aria-placeholder="연장자">
                    <option value="1">다승</option>
                    <option value="2">승자승</option>
                    <option value="3">득실차</option>
                    <option value="4">저실점</option>
                    <option value="5">연장자</option>
                </select>
            </article>

            <h6 class="input_title">토너먼트 공동 3위 설정</h6>
            <article class="radio_input_box">
                <label><input class="radio_btn" type="radio" name="thirdRankRule" value="1">우승팀에 패한 팀 자동 3위</label>
                <label><input class="radio_btn" type="radio" name="thirdRankRule" value="2">토너먼트 공동 3위</label>
                <label><input class="radio_btn" type="radio" name="thirdRankRule" value="3">3,4위전 진행</label>
            </article>

            <h6 class="input_title">기권 처리 방식</h6>
            <article class="radio_input_box">
                <label><input class="radio_btn" type="radio" name="giveup" value="1">기권 경기만 0점</label>
                <label><input class="radio_btn" type="radio" name="giveup" value="2">진행전, 진행중, 진행예정인 모든 경기 0점</label>
            </article>

            <h6 class="input_title">실격 처리 방식</h6>
            <article class="radio_input_box">
                <label><input class="radio_btn" type="radio" name="disqualification" value="1">실격 경기만 0점</label>
                <label><input class="radio_btn" type="radio" name="disqualification" value="2">진행전, 진행중, 진행예정인 모든 경기 0점</label>
            </article>

            <article class="input_content">
                <h5 class="input_title">매치 당 소요시간</h5>
                <input class="input_num" type="text" placeholder="0" id="txtTimePerMatch">
                <p class="input_box_text">분</p>
            </article>

            <h6 class="input_title">승리 조건 설정</h6>
            <section class="input_content_box">
                <article class="input_content">
                    <h4 class="input_content_title">예선</h4>
                    <p class="input_content_text">매치승리조건</p>
                    <select name="" class="win_select_box" id="selSubWinGameCnt" aria-placeholder="0전 0선승">
                        <option value="2">3전 2선승</option>
                        <option value="3">5전 3선승</option>
                        
                    </select>
                    <p class="input_box_text">제</p>
                </article>
                <article class="input_content input_content_p">
                    <p class="input_content_text">게임승리조건</p>
                    <input class="input_num" type="text" id="txtSubWinScores" placeholder="0">
                    <p class="input_box_text">점</p>
                    <p class="input_content_text">듀스여부</p>
                    <select name="" class="deuce_select_box" id="selSubIsDeuce" aria-placeholder="있음">
                        <option value="1">있음</option>
                        <option value="0">없음</option>
                    </select>
                    <p class="input_content_text">최대 점수</p>
                    <input class="input_num" type="text" id="txtSubMaxScores" placeholder="0">
                </article>
                <article class="input_content input_content_main">
                    <h4 class="input_content_title">본선</h4>
                    <p class="input_content_text">매치승리조건</p>
                    <select name="" class="win_select_box" id="selMainWinGameCnt" aria-placeholder="0전 0선승">
                        <option value="2">3전 2선승</option>
                        <option value="3">5전 3선승</option>
                    </select>
                    <p class="input_box_text">제</p>
                </article>
                <article class="input_content input_content_p">
                    <p class="input_content_text">게임승리조건</p>
                    <input class="input_num" type="text" id="txtMainWinScores" placeholder="0">
                    <p class="input_box_text">점</p>
                    <p class="input_content_text">듀스여부</p>
                    <select name="" class="deuce_select_box" id="selMainIsDeuce" aria-placeholder="있음">
                        <option value="1">있음</option>
                        <option value="0">없음</option>
                    </select>
                    <p class="input_content_text">최대 점수</p>
                    <input class="input_num" type="text" id="txtMainMaxScores" placeholder="0">
                </article>
            </section>
                            
            <button type="button" class="save_btn" onclick="fnSetCompetitionRules();">저장</button>
        </section>
    </div>
</asp:Content>
