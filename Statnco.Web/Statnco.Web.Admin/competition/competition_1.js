﻿var pageNo = 1;
var totalNo = 0;


$(function () {
    $("#menu7").collapse("show");

    fnGetRegionList(false);

    fnGetCompetitionList();
});

function fnGetCompetitionList() {
    var listStr = "<li class=\"table_row_wrap\">\
                            <ul class=\"table_row\">\
                                <li class=\"table_li table_li_short\">{0}</li>\
                                <li class=\"table_li table_li_short\">{1}</li>\
                                <li class=\"table_li table_li_long\" onclick=\"fnGetDetail({0});\">{2}</li>\
                                <li class=\"table_li table_li_middle\">{3} ~<br>{4}</li>\
                                <li class=\"table_li table_li_middle\">{5} ~<br>{6}</li>\
                                <li class=\"table_li table_li_short\">{7}</li>\
                                <li class=\"table_li table_li_middle\">{8}</li>\
                                <li class=\"table_li table_li_long\">\
                                    <ul class=\"table_btn_li\">\
                                        <li class=\"btn_list\">\
                                            <button type=\"button\" class=\"table_btns event_write_btn\" onclick=\"fnMovePage(3, {0});\">{9}</button>\
                                        </li>\
                                        <li class=\"btn_list\">\
                                            <button type=\"button\" class=\"table_btns manage_write_btn\" onclick=\"fnMovePage(4, {0});\">{10}</button>\
                                        </li>\
                                        <li class=\"btn_list\">\
                                            <button type=\"button\" class=\"table_btns ground_write_btn\" onclick=\"fnMovePage(5, {0});\">미작성</button>\
                                        </li>\
                                        <li class=\"btn_list\">\
                                            <button type=\"button\" class=\"table_btns participation_btn\">접수현황</button>\
                                        </li>\
                                        <li class=\"btn_list\">\
                                            <button type=\"button\" class=\"table_btns contest_result_btn\">결과보기</button>\
                                        </li>\
                                    </ul>\
                                </li>\
                            </ul>\
                        </li>";

    var param = {
        "regionCode": $("#selRegion").val(),
        pageNo: pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        $(".table_box").children("li:gt(0)").remove();

        totalNo = table1[0]["totalCnt"];

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["competitionIdx"],
                        fnGetState(
                            table[i]["applicationSrtDate"],
                            table[i]["applicationEndDate"],
                            table[i]["srtDate"],
                            table[i]["endDate"]
                        ),
                        table[i]["competitionName"],
                        table[i]["applicationSrtDate"],
                        table[i]["applicationEndDate"],
                        table[i]["srtDate"],
                        table[i]["endDate"],
                        table[i]["regionName"],
                        table[i]["stadium"],
                        table[i]["rules"] == "1" ? "수정" : "미작성",
                        table[i]["groups"] == "1" ? "수정" : "미작성"
                    )
                );
            }

            fnSetPageing();
        }

        
    }

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionList", param, fnCallback);
}

function fnGetDetail(idx) {
    location.href = "competition_2.aspx?competitionIdx=" + idx;
}

function fnGetState(appSrtDate, appEndDate, srtDate, endDate) {
    var today = new Date();
    appSrtDate = new Date(appSrtDate);
    appEndDate = new Date(appEndDate);
    srtDate = new Date(srtDate);
    endDate = new Date(endDate);

    if (today < appSrtDate) {
        return "접수준비";
    }
    else if (today > appSrtDate && today < appEndDate) {
        return "접수중";
    }
    else if (today > appEndDate && today < srtDate) {
        return "접수종료";
    }
    else if (today > srtDate && today < endDate) {
        return "대회중";
    }
    else {
        return "대회종료";
    }
}

function fnMovePage(no, idx) {
    var url = String.format(
        "/competition/competition_{0}.aspx?competitionIdx={1}",
        no,
        idx
    );

    location.href = url;
}