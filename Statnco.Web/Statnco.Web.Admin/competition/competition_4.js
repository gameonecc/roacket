﻿var competitionIdx;
var pageNo = 1;
var totalNo = 0;

$(function () {
    competitionIdx = httpRequest("competitionIdx");

    fnGetComCode("event", $("#selEvent"));

    fnGetComCode("age", $("#selAge"));

    fnGetComCode("grade", $("#selGrade"));

    fnGetList();
});

function fnGetCompetitionGroupList() {
    var listStr = "<li class=\"receipt_event_row_wrap\">\
                    <ul class=\"receipt_event_row\">\
                        <li class=\"receipt_event_li receipt_event_check_li\"><input type=\"checkbox\" name=\"chkGroup\"></li>\
                        <li class=\"receipt_event_li\">{0}</li>\
                        <li class=\"receipt_event_li\">\
                            {1} \
                        </li>\
                        <li class=\"receipt_event_li receipt_event_li_long\">\
                            {2} \
                        </li>\
                        <li class=\"receipt_event_li\">\
                            {3} \
                        </li>\
                        <li class=\"receipt_event_li\">\
                            {4} \
                        </li>\
                        <li class=\"receipt_event_li receipt_team_li\">{5}</li>\
                    </ul>\
                </li>";

    var param = {
        competitionIdx: competitionIdx,
        eventCode: $("#selEvent").val(),
        ageCode: $("#selAge").val(),
        gradeCode: $("#selGrade").val(),
        pageNo: pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;
        var table1 = JSON.parse(data.d).Table1;

        totalNo = table1[0]["totalCnt"];

        $(".receipt_event_table").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".receipt_event_table").append(
                    String.format(
                        listStr,
                        table[i]["groupIdx"],
                        table[i]["groupIdx"],
                        table[i]["eventName"],
                        table[i]["ageName"],
                        table[i]["gradeName"],
                        table[i]["acceptTeamsCnt"]
                    )
                );
            }

            fnSetPageing();
        }
        
    };

    window.fnAjaxSend("/competition/ws_competition.asmx/GetCompetitionGroupList", param, fnCallback);
}

function fnGetList() {
    fnGetCompetitionGroupList();
}

function fnChkAll() {
    $("input[name='chkGroup']").prop("checked", $("#chkAll").is(":checked"));
}
