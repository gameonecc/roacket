﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using Newtonsoft.Json;

namespace Statnco.Web.Admin.competition
{
    /// <summary>
    /// ws_competition의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_competition : System.Web.Services.WebService
    {
        #region 대회 리스트 조회
        [WebMethod]
        public string GetCompetitionList(int regionCode, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetCompetitionList(regionCode, pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 대회 상세 조회
        [WebMethod]
        public string GetCompetitionDetail(int competitionIdx)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetCompetitionDetail(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 대회 추가/수정
        [WebMethod]
        public int SetCompetition(int competitionIdx, string competitionName, byte typeCode, bool isDivision,
            DateTime appSrtDate, DateTime appEndDate, DateTime srtDate, DateTime endDate, DateTime listDate,
            string areas, string stadiums
        )
        {
            int rtn = 0;

            AreaList al = JsonConvert.DeserializeObject<AreaList>(areas);
            StadiumList sl = JsonConvert.DeserializeObject<StadiumList>(stadiums);

            DataTable areasDt = al.areas.ToDataTable();
            DataTable stadiumsDt = sl.stadiums.ToDataTable();

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    // 대회 추가/수정
                    rtn = biz.SetCompetition(competitionIdx, competitionName, typeCode, isDivision, appSrtDate, appEndDate, srtDate, endDate, listDate, editUser);

                    int tempCompetitionIdx = rtn;

                    if (rtn > 0) {
                        // 대회 장소 추가/수정
                        rtn = biz.SetCompetitionAreas(competitionIdx, areasDt);
                    }
                    else
                    {
                        return -1;
                    }

                    if(rtn == 0)
                    {
                        // 대회 경기장 추가/수정
                        rtn = biz.SetCompetitionStadiums(competitionIdx, stadiumsDt);
                    }
                }
            }
            catch (Exception exp)
            {
                return rtn;
            }
            finally
            {
                areasDt.Dispose();
                stadiumsDt.Dispose();
            }

            return rtn;
        }
        #endregion

        #region 대회 규칙 조회
        [WebMethod]
        public string GetCompetitionRules(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetCompetitionRules(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 대회 규칙 추가/수정
        [WebMethod]
        public int SetCompetitionRules(int competitionIdx, bool useLiveScore, string rankRule,
            byte thirdRankRule, byte giveupRuleCode, byte disqualRuleCode, byte timePerMatch,
            byte subWinGameCnt, byte subWinScore, byte subMaxScore, bool subIsDeuce,
            byte mainWinGameCnt, byte mainWinScore, byte mainMaxScore, bool mainIsDeuce
        )
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetCompetitionRules(competitionIdx, useLiveScore, rankRule,
                        thirdRankRule, giveupRuleCode, disqualRuleCode, timePerMatch,
                        subWinGameCnt, subWinScore, subMaxScore, subIsDeuce, 
                        mainWinGameCnt, mainWinScore, mainMaxScore, mainIsDeuce
                    );
                }
            }
            catch (Exception exp)
            {
                return rtn;
            }

            return rtn;
        }
        #endregion

        #region 대회 그룹 리스트 조회 
        [WebMethod]
        public string GetCompetitionGroupList(int competitionIdx, byte eventCode, byte ageCode, byte gradeCode, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetCompetitionGroupList(competitionIdx, eventCode, ageCode, gradeCode, pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 대회 경기장 리스트 조회
        [WebMethod]
        public string GetCompetitionStadiumList(int competitionIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetCompetitionStadiumList(competitionIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 대회 경기장 스케쥴 조회
        [WebMethod]
        public string GetCompetitionStadiumSchedule(int competitionIdx, int stadiumNo)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetCompetitionStadiumSchedule(competitionIdx, stadiumNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return dt.ToJSONString();
        }
        #endregion

        #region Class

        public class Area
        {
            public int regionCode { get; set; }
            public string areaCode { get; set; }
        }

        public class AreaList
        {
            public List<Area> areas { get; set; }
        }

        public class Stadium
        {
            public int no { get; set; }
            public string name { get; set; }
            public string addr1 { get; set; }
            public string addr2 { get; set; }
        }

        public class StadiumList
        {
            public List<Stadium> stadiums { get; set; }
        }

        #endregion
    }
}
