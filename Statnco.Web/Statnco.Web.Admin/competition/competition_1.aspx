﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="competition_1.aspx.cs" Inherits="Statnco.Web.Admin.competition.competition_1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">전체대회리스트</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <select class="select_box" name="" id="" aria-placeholder="상태별">
                    <option value="0">상태별</option>
                    <option value="1">접수대기</option>
                    <option value="2">접수중</option>
                    <option value="3">접수종료</option>
                    <option value="4">대회중</option>
                    <option value="5">대회종료</option>
                </select>

                <select class="select_box" name="" id="selRegion" aria-placeholder="지역별">
                    <option value="0">지역별</option>
                </select>

                <section class="search_box">
                    <input type="text" class="search_input search_input_contest_name" placeholder="대회명을 입력하세요">
                    <button type="button" class="search_btn">Search</button>
                </section>

                <button type="button" class="new_contest_make_btn">신규 대회 생성</button>
            </section>

            <ul class="table_box contest_list_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_short">No</li>
                        <li class="table_li table_li_short">상태</li>
                        <li class="table_li table_li_long">대회명</li>
                        <li class="table_li table_li_middle">접수기간</li>
                        <li class="table_li table_li_middle">대회기간</li>
                        <li class="table_li table_li_short">참가지역</li>
                        <li class="table_li table_li_middle">대회장소</li>
                        <li class="table_li table_li_long">
                            <ul class="table_btn_li">
                                <li class="btn_list">대회규칙</li>
                                <li class="btn_list">대회종목</li>
                                <li class="btn_list">대회일정</li>
                                <li class="btn_list">참가종합</li>
                                <li class="btn_list">대회결과</li>
                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
            <section class="post_list_btn_box">
            </section>
        </section>
    </div>
</asp:Content>
