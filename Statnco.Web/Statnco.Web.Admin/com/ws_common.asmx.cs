﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Data;
using System.Web.Services;

namespace Statnco.Web.Admin.com
{
    /// <summary>
    /// ws_common의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_common : System.Web.Services.WebService
    {
        #region 지역 코드 조회
        [WebMethod]
        public string GetRegionList()
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetRegionList();
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 세부 지역 코드 조회 
        [WebMethod]
        public string GetAreaList(int regionCode)
        {
            DataTable dt = null;

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetAreaList(regionCode);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 공통 코드 조회
        [WebMethod]
        public string GetComCodeList(string division)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetComCodeList(division);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion
    }
}
