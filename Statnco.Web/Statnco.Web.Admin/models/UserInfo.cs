﻿using System.Collections.Generic;

namespace Statnco.Web.Admin.models
{
    public class UserInfo
    {
        public class MemberInfo
        {
            public int memberIdx { get; set; }
            public string userId { get; set; }
            public string userName { get; set; }
            public string memberType { get; set; }
            public int associationId { get; set; }
            public int regionCode { get; set; }
            public int areaCode { get; set; }
        }

        public class MenuInfo
        {
            public int menuIdx { get; set; }
            public int menuSubIdx { get; set; }
            public string menuName { get; set; }
            public string power { get; set; }
        }

        public class Root
        {
            public MemberInfo memberInfo { get; set; }
            public string memberToken { get; set; }
            public List<MenuInfo> menuInfo { get; set; }
        }
    }
}