﻿var pageNo = 1;
var totalNo = 0;
var isChkId = false;
var memberIdx = 0;

$(function () {
    fnGetRegionList(true, 2);

    fnGetList();
});

function fnGetManagerList() {
    var listStr = "<li class=\"table_row_wrap\">\
                    <ul class=\"table_row\">\
                        <li class=\"table_li table_li_short\">{0}</li>\
                        <li class=\"table_li table_li_middle\" onclick=\"fnEditPop({0});\">{1}</li>\
                        <li class=\"table_li table_li_middle\" onclick=\"fnEditPop({0});\">{2}</li>\
                        <li class=\"table_li table_li_short\">{3}</li>\
                        <li class=\"table_li table_li_middle\">{4}</li>\
                        <li class=\"table_li table_li_middle\">{5}</li>\
                        <li class=\"table_li table_li_middle\">{6}</li>\
                        <li class=\"table_li table_li_short\">\
                            <button type=\"button\" class=\"del_btn\" onclick=\"fnDelManager({0});\">\
                                <svg class=\"del_icon\" viewBox=\"0 0 17 19\" fill=\"none\">\
                                    <path d=\"M6.18164 7.72732V13.9091C6.18164 14.1141 6.26305 14.3106 6.40797 14.4555C6.55288 14.6005 6.74943 14.6819 6.95437 14.6819C7.15931 14.6819 7.35585 14.6005 7.50077 14.4555C7.64568 14.3106 7.7271 14.1141 7.7271 13.9091V7.72732C7.7271 7.52238 7.64568 7.32583 7.50077 7.18092C7.35585 7.036 7.15931 6.95459 6.95437 6.95459C6.74943 6.95459 6.55288 7.036 6.40797 7.18092C6.26305 7.32583 6.18164 7.52238 6.18164 7.72732Z\" fill=\"black\"/>\
                                    <path d=\"M10.0452 6.95459C10.2501 6.95459 10.4467 7.036 10.5916 7.18092C10.7365 7.32583 10.8179 7.52238 10.8179 7.72732V13.9091C10.8179 14.1141 10.7365 14.3106 10.5916 14.4555C10.4467 14.6005 10.2501 14.6819 10.0452 14.6819C9.84025 14.6819 9.6437 14.6005 9.49879 14.4555C9.35387 14.3106 9.27246 14.1141 9.27246 13.9091V7.72732C9.27246 7.52238 9.35387 7.32583 9.49879 7.18092C9.6437 7.036 9.84025 6.95459 10.0452 6.95459Z\" fill=\"black\"/>\
                                    <path d=\"M11.5909 3.09091H16.2273C16.4322 3.09091 16.6288 3.17232 16.7737 3.31724C16.9186 3.46215 17 3.6587 17 3.86364C17 4.06858 16.9186 4.26512 16.7737 4.41004C16.6288 4.55495 16.4322 4.63636 16.2273 4.63636H15.3726L14.2105 15.1084C14.1055 16.0535 13.6556 16.9267 12.9471 17.5608C12.2385 18.195 11.3209 18.5456 10.37 18.5455H6.63C5.67908 18.5456 4.76152 18.195 4.05294 17.5608C3.34436 16.9267 2.89453 16.0535 2.78955 15.1084L1.62582 4.63636H0.772727C0.567787 4.63636 0.371241 4.55495 0.226327 4.41004C0.0814123 4.26512 0 4.06858 0 3.86364C0 3.6587 0.0814123 3.46215 0.226327 3.31724C0.371241 3.17232 0.567787 3.09091 0.772727 3.09091H5.40909C5.40909 2.27115 5.73474 1.48496 6.3144 0.905306C6.89405 0.325648 7.68024 0 8.5 0C9.31976 0 10.1059 0.325648 10.6856 0.905306C11.2653 1.48496 11.5909 2.27115 11.5909 3.09091ZM8.5 1.54545C8.09012 1.54545 7.69703 1.70828 7.4072 1.99811C7.11737 2.28794 6.95455 2.68103 6.95455 3.09091H10.0455C10.0455 2.68103 9.88263 2.28794 9.5928 1.99811C9.30297 1.70828 8.90988 1.54545 8.5 1.54545ZM3.18209 4.63636L4.32573 14.9384C4.38886 15.5053 4.65882 16.0291 5.08395 16.4095C5.50909 16.7898 6.05954 17.0001 6.63 17H10.37C10.9402 16.9997 11.4903 16.7893 11.9151 16.4089C12.3399 16.0286 12.6096 15.5051 12.6727 14.9384L13.8195 4.63636H3.18364H3.18209Z\" fill=\"black\"/>\
                                </svg>\
                            </button>\
                        </li>\
                    </ul>\
                </li>";



    var param = {
        "option": $("#selOption").val(),
        "value": $("#txtValue").val(),
        "pageNo": pageNo
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        $(".table_box").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["memberIdx"],
                        table[i]["userId"],
                        table[i]["userName"],
                        table[i]["gender"] == "M" ? "남" : "여",
                        table[i]["phone"],
                        table[i]["birth"],
                        table[i]["associationName"]
                    )
                );
            }

            fnSetPageing();
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/GetManagerList", param, fnCallback);
}

function fnDelManager(idx) {
    var param = {
        "memberIdx": idx
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다.");
        }
        else {
            fnGetList();
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/DelManager", param, fnCallback);
}

function fnGetList() {
    fnGetManagerList();
}

function fnPopup() {
    // 팝업리셋
    memberIdx = 0;

    $("#txtUserId").val("");
    $("#txtPassword").val("");
    $("#txtUserName").val("");
    $("#txtYear").val("");
    $("#txtDay").val("");
    $("#txtPhone").val("");
    $("#divAssInfo").val("");
    $("#divAssInfo").hide();
    $("#divAss").show();

    $("#ssoManagerplusModal").modal("show");
}

function fnChkUserId() {
    var param = {
        "userId": $("#txtUserId").val()
    };

    var fnCallback = function (data) {
        if (data.d == 0) {
            isChkId = true;
            alert("사용 가능한 아이디 입니다");
        }
        else {
            isChkId = false;
            alert("이미 존재하는 아이디 입니다");
            $("#txtUserId").focus();
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/ChkUserId", param, fnCallback);
}

function fnResetChkId(event) {
    isChkId = false;

    const regExp = /[^0-9a-zA-Z]/g;
    const ele = event.target;
    if (regExp.test(ele.value)) {
        ele.value = ele.value.replace(regExp, '');
    }
}

function fnSavePopup() {
    if (!isChkId) {
        alert("아이디 중복체크클 먼저 진행해주세요");
        $("#txtUserId").focus();
        return;
    }

    if ($("#txtUserId").val() == "") {
        alert("아이디를 입력해주세요");
        return;
    }

    if ($("#txtUserName").val() == "") {
        alert("이름을 입력해주세요");
        return;
    }

    if ($("#txtYear").val().length < 4) {
        alert("년도를 입력해주세요");
        return;
    }

    if ($("#selMonth").val() == "") {
        alert("월을 선택해주세요");
        return;
    }

    if ($("#txtDay").val() == "") {
        alert("일을 입력해주세요");
        return;
    }

    if ($("#txtPhone").val() == "") {
        alert("휴대폰 번호를 입력해주세요");
        return;
    }

    var assType = $("input:radio[name='sigungu_radio']:checked").val();

    if (assType == "1") {
        // 시도 협회
        if ($("#selRegion").val() == "" || $("#selRegion").val() == "0") {
            alert("시도를 선택해주세요");
            return;
        }
    }
    else {
        // 시도 협회
        if ($("#selRegion2").val() == "" || $("#selRegion2").val() == "0") {
            alert("시도를 선택해주세요");
            return;
        }

        if ($("#selArea2").val() == "" || $("#selArea2").val() == "0") {
            alert("시군구를 선택해주세요");
            return;
        }
    }

    var param = {
        "memberIdx": memberIdx,
        "userId": $("#txtUserId").val(),
        "userPw": $("#txtPassword").val(),
        "userName": $("#txtUserName").val(),
        "gender": $("#selGender").val(),
        "birth": String.format(
            "{0}-{1}-{2}",
            $("#txtYear").val(),
            lpad($("#selMonth").val(), 2, "0"),
            lpad($("#txtDay").val(), 2, "0")
        ),
        "phone": $("#txtPhone").val(),
        "assType": assType,
        "regionCode": assType == "1" ? $("#selRegion").val() : $("#selRegion2").val(),
        "areaCode": assType == "1" ? "0" : $("#selArea2").val()
    };

    var fnCallback = function (data) {
        if (data.d == 0) {
            isChkId = false;

            $("#ssoManagerplusModal").modal("hide");

            fnGetList();
        }
        else {
            if (data.d == "-99") {

            }
            else {
                alert("데이터 처리 중 에러가 발생하였습니다");
            }

            
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/SetManager", param, fnCallback);
}

function fnEditPop(idx) {
    memberIdx = idx;

    // 상세 정보 조회
    var param = {
        "memberIdx": memberIdx
    };

    var fnCallback = function (data) {
        var row = JSON.parse(data.d).Table[0];

        isChkId = true;
        $("#txtUserId").val(row["userId"]);
        $("#txtUserName").val(row["userName"]);
        $("#selGender").val(row["gender"]);

        if (row["birth"].split('-').length == 3) {
            var birthArr = row["birth"].split('-');
            $("#txtYear").val(birthArr[0]);
            $("#selMonth").val(Number(birthArr[1]));
            $("#txtDay").val(Number(birthArr[2]));
        }

        $("#txtPhone").val(row["phone"]);
        $("#txtAssName").val(row["associationName"]);

        $("#divAssInfo").show();
        $("#divAss").hide();
    }

    window.fnAjaxSend("/association/ws_association.asmx/GetManagerDetail", param, fnCallback);


    $("#ssoManagerplusModal").modal("show");
}

function fnAssRadio() {
    if ($("input:radio[name='sigungu_radio']:checked").val() == "1") {
        $("#divAss1").show();
        $("#divAss2").hide();
    }
    else {
        $("#divAss1").hide();
        $("#divAss2").show();
    }
}