﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Statnco.Web.Admin.association
{
    /// <summary>
    /// ws_association의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_association : System.Web.Services.WebService
    {

        #region 협회 리스트 조회
        [WebMethod]
        public string GetAssociationList(int regionCode, int areaCode)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetAssociationList(regionCode, areaCode);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 관리자 리스트 조회
        [WebMethod]
        public string GetManagerList(int option, string value, int pageNo)
        {
            DataSet ds = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    ds = biz.GetManagerList(option, value, pageNo);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(ds);
        }
        #endregion

        #region 관리자 상세 조회 
        [WebMethod]
        public string GetManagerDetail(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetManagerDetail(memberIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 관리자 추가/수정
        [WebMethod]
        public string SetManager(int memberIdx, string userId, string userPw, string userName, string gender, string birth, string phone,
            int regionCode, int areaCode)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetManager(memberIdx, userId, Crypto.fnGetSHA512(userPw), userName, gender, birth, phone, regionCode, areaCode, editUser);
                }
            }
            catch (Exception exp)
            {
                throw exp;
            }

            return rtn.ToString();
        }
        #endregion

        #region 관리자 삭제
        [WebMethod]
        public int DelManager(int memberIdx)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.DelManager(memberIdx, editUser);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion

        #region 협회 삭제
        [WebMethod]
        public int DelAssociation(int associationIdx)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.DelAssociation(associationIdx, editUser);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion

        #region 아이디 중복 체크
        [WebMethod]
        public int ChkUserId(string userId)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.ChkUserId(userId);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion
    }
}
