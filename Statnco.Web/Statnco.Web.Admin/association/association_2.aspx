﻿<%@ Page Title="" Language="C#" MasterPageFile="~/master/roacket.Master" AutoEventWireup="true" CodeBehind="association_2.aspx.cs" Inherits="Statnco.Web.Admin.association.association_2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container-fluid px-4">
        <h1 class="mt-4">관리자계정관리</h1>
        <section class="table_wrap">
            <section class="table_hd">
                <select class="manager_select_box select_box" name="" id="selOption" aria-placeholder="관리자ID">
                    <option value="1">이름</option>
                    <option value="2">아이디</option>
                    <option value="3">전화번호</option>
                </select>

                <section class="search_box">
                    <input type="text" class="search_input" id="txtValue">
                    <button type="button" class="search_btn">Search</button>
                </section>

                <button type="button" class="club_plus_btn" id="ssoManagerplusBtn" onclick="fnPopup();">
                    관리자계정추가
                    <svg class="plus_icon" viewBox="0 0 27 27" fill="none">
                        <path d="M21.6 2.7002H5.40001C3.91501 2.7002 2.70001 3.9152 2.70001 5.4002V21.6002C2.70001 23.0852 3.91501 24.3002 5.40001 24.3002H21.6C23.085 24.3002 24.3 23.0852 24.3 21.6002V5.4002C24.3 3.9152 23.085 2.7002 21.6 2.7002ZM20.25 14.8502H14.85V20.2502H12.15V14.8502H6.75001V12.1502H12.15V6.7502H14.85V12.1502H20.25V14.8502Z" fill="#009F59"/>
                    </svg>
                </button>
            </section>

            <ul class="table_box manager_account_table">
                <li class="table_row_wrap">
                    <ul class="table_row table_row_title">
                        <li class="table_li table_li_short">idx</li>
                        <li class="table_li table_li_middle">아이디</li>
                        <li class="table_li table_li_middle">사용자명</li>
                        <li class="table_li table_li_short">성별</li>
                        <li class="table_li table_li_middle">휴대폰</li>
                        <li class="table_li table_li_middle">생년월일</li>
                        <li class="table_li table_li_middle">소속협회</li>
                        <li class="table_li table_li_short">삭제</li>
                    </ul>
                </li>
            </ul>
            <section class="post_list_btn_box">

            </section>
        </section>
    </div>
    <div class="modal fade" id="ssoManagerplusModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <section class="manager_plus_popup modal-content">
                <section class="popup_hd">
                    <button type="button" class="popup_close_btn" id="ssoManagerplusCancle" data-bs-dismiss="modal">
                        <svg class="close_icon" viewBox="0 0 14 14" fill="none">
                            <path d="M1.40002 1.3999L12.6 12.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                            <path d="M12.6 1.3999L1.40002 12.5999" stroke="#333333" stroke-width="2" stroke-miterlimit="10" stroke-linecap="round" stroke-linejoin="round"/>
                        </svg>
                    </button>
                    <h3 class="popup_title">관리자계정추가/수정</h3>
                </section>
                <section class="popup_main">
                    <h6 class="input_title">아이디<span class="pink_star">&#42;</span></h6>
                    <article class="input_box">
                        <input class="input_content" type="text" id="txtUserId" onkeyup="fnResetChkId(event);"/>
                        <button type="button" class="overlap_test_btn" onclick="fnChkUserId();">중복확인</button>
                    </article>

                    <h6 class="input_title">비밀번호<span class="pink_star">&#42;</span></h6>
                    <input class="input_content" type="password" id="txtPassword">

                    <h6 class="input_title">이름<span class="pink_star">&#42;</span></h6>
                    <input class="input_content" type="text" id="txtUserName">

                    <h6 class="input_title">성별<span class="pink_star">&#42;</span></h6>
                    <select class="input_select_box" name="" id="selGender" aria-placeholder="성별">
                        <option value="M">남</option>
                        <option value="F">여</option>
                    </select>

                    <h6 class="input_title">생년월일<span class="pink_star">&#42;</span></h6>
                    <article class="input_box input_day_box">
                        <input class="input_content" type="text" placeholder="생년(4자리)" maxlength="4" id="txtYear">
                        <select class="input_select_box" name="" id="selMonth" aria-placeholder="월">
                            <option value="">월</option>
                            <option value="1">1월</option>
                            <option value="2">2월</option>
                            <option value="3">3월</option>
                            <option value="4">4월</option>
                            <option value="5">5월</option>
                            <option value="6">6월</option>
                            <option value="7">7월</option>
                            <option value="8">8월</option>
                            <option value="9">9월</option>
                            <option value="10">10월</option>
                            <option value="11">11월</option>
                            <option value="12">12월</option>
                        </select>
                        <input class="input_content input_day" type="text" id="txtDay" placeholder="일">
                    </article>

                    <h6 class="input_title">휴대폰번호<span class="pink_star">&#42;</span></h6>
                    <input class="input_content" type="text" id="txtPhone" placeholder="-없이 숫자만 입력">

                    <div id="divAssInfo" style="display:none;"> 
                        <h6 class="input_title">소속협회<span class="pink_star">&#42;</span></h6>
                        <input class="input_content" type="text" id="txtAssName" readonly="readonly"/>
                    </div>

                    <div id="divAss">
                        <h6 class="input_title input_sigungu_title">소속협회</h6>
                        <article class="input_box input_radio_box">
                            <label><input class="input_radio" type="radio" name="sigungu_radio" onclick="fnAssRadio();" value="1" checked> 시도협회</label>
                            <label><input class="input_radio" type="radio" name="sigungu_radio" onclick="fnAssRadio();" value="2"> 시군구협회</label>
                        </article>

                        <div id="divAss1">
                            <h6 class="input_title">시도협회</h6>
                            <select class="input_select_box" name="" id="selRegion" aria-placeholder="서울">
                            
                            </select>
                        </div>

                        <div id="divAss2" style="display:none;">
                            <h6 class="input_title">시군구협회</h6>
                            <article class="input_box input_sigungu_box">
                                <select class="input_select_box" name="" id="selRegion2" aria-placeholder="서울" onchange="fnGetAreaList(2);">
                                
                                </select>
                                <select class="input_select_box" name="" id="selArea2" aria-placeholder="서울시 강남구">
                                
                                </select>
                            </article>
                        </div>
                    </div>
                    
                    

                    <button type="button" class="save_btn" onclick="fnSavePopup();">저장</button>
                </section>
            </section>
        </div>
    </div>
</asp:Content>
