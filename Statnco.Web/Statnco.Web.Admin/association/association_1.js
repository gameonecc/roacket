﻿var pageNo = 1;
var totalNo = 0;

$(function () {
    fnGetRegionList(true);

    setTimeout(function () {
        fnGetList();
    }, 1000);
});

function fnGetAssociationList() {
    var listStr = "<li class=\"table_row_wrap\">\
                        <ul class=\"table_row\">\
                            <li class=\"table_li table_li_short\">{0}</li>\
                            <li class=\"table_li table_li_middle\">{1}</li>\
                            <li class=\"table_li table_li_middle\">{2}</li>\
                            <li class=\"table_li table_li_middle\">{3}</li>\
                            <li class=\"table_li table_li_long\">{4}</li>\
                            <li class=\"table_li table_li_middle\">\
                                <button type=\"button\" class=\"del_btn\" onclick=\"fnDelAssociation({0});\">\
                                    <svg class=\"del_icon\" viewBox=\"0 0 17 19\" fill=\"none\">\
                                        <path d=\"M6.18164 7.72732V13.9091C6.18164 14.1141 6.26305 14.3106 6.40797 14.4555C6.55288 14.6005 6.74943 14.6819 6.95437 14.6819C7.15931 14.6819 7.35585 14.6005 7.50077 14.4555C7.64568 14.3106 7.7271 14.1141 7.7271 13.9091V7.72732C7.7271 7.52238 7.64568 7.32583 7.50077 7.18092C7.35585 7.036 7.15931 6.95459 6.95437 6.95459C6.74943 6.95459 6.55288 7.036 6.40797 7.18092C6.26305 7.32583 6.18164 7.52238 6.18164 7.72732Z\" fill=\"black\"/>\
                                        <path d=\"M10.0457 6.95459C10.2506 6.95459 10.4472 7.036 10.5921 7.18092C10.737 7.32583 10.8184 7.52238 10.8184 7.72732V13.9091C10.8184 14.1141 10.737 14.3106 10.5921 14.4555C10.4472 14.6005 10.2506 14.6819 10.0457 14.6819C9.84074 14.6819 9.64419 14.6005 9.49928 14.4555C9.35436 14.3106 9.27295 14.1141 9.27295 13.9091V7.72732C9.27295 7.52238 9.35436 7.32583 9.49928 7.18092C9.64419 7.036 9.84074 6.95459 10.0457 6.95459Z\" fill=\"black\"/>\
                                        <path d=\"M11.5909 3.09091H16.2273C16.4322 3.09091 16.6288 3.17232 16.7737 3.31724C16.9186 3.46215 17 3.6587 17 3.86364C17 4.06858 16.9186 4.26512 16.7737 4.41004C16.6288 4.55495 16.4322 4.63636 16.2273 4.63636H15.3726L14.2105 15.1084C14.1055 16.0535 13.6556 16.9267 12.9471 17.5608C12.2385 18.195 11.3209 18.5456 10.37 18.5455H6.63C5.67908 18.5456 4.76152 18.195 4.05294 17.5608C3.34436 16.9267 2.89453 16.0535 2.78955 15.1084L1.62582 4.63636H0.772727C0.567787 4.63636 0.371241 4.55495 0.226327 4.41004C0.0814123 4.26512 0 4.06858 0 3.86364C0 3.6587 0.0814123 3.46215 0.226327 3.31724C0.371241 3.17232 0.567787 3.09091 0.772727 3.09091H5.40909C5.40909 2.27115 5.73474 1.48496 6.3144 0.905306C6.89405 0.325648 7.68024 0 8.5 0C9.31976 0 10.1059 0.325648 10.6856 0.905306C11.2653 1.48496 11.5909 2.27115 11.5909 3.09091ZM8.5 1.54545C8.09012 1.54545 7.69703 1.70828 7.4072 1.99811C7.11737 2.28794 6.95455 2.68103 6.95455 3.09091H10.0455C10.0455 2.68103 9.88263 2.28794 9.5928 1.99811C9.30297 1.70828 8.90988 1.54545 8.5 1.54545ZM3.18209 4.63636L4.32573 14.9384C4.38886 15.5053 4.65882 16.0291 5.08395 16.4095C5.50909 16.7898 6.05954 17.0001 6.63 17H10.37C10.9402 16.9997 11.4903 16.7893 11.9151 16.4089C12.3399 16.0286 12.6096 15.5051 12.6727 14.9384L13.8195 4.63636H3.18364H3.18209Z\" fill=\"black\"/>\
                                    </svg>\
                                </button>\
                            </li>\
                        </ul>\
                    </li>";



    var param = {
        "regionCode": $("#selRegion").val(),
        "areaCode": $("#selArea").val()
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        $(".table_box").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".table_box").append(
                    String.format(
                        listStr,
                        table[i]["assIdx"],
                        table[i]["regionName"],
                        table[i]["areaName"],
                        table[i]["managerName"],
                        table[i]["phone"]
                        
                    )
                );
            }
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/GetAssociationList", param, fnCallback);
}

function fnDelAssociation(idx) {
    var param = {
        "associationIdx": idx
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다.");
        }
        else {
            fnGetList();
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/DelAssociation", param, fnCallback);
}

function fnGetList() {
    fnGetAssociationList();
}