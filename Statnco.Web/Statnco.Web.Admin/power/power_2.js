﻿var memberIdx = 0;

$(function () {
    fnGetMenuModuleList();

    fnGetManagerList();
});

function fnGetMenuModuleList() {
    var param = {};

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $("#selModule").append(
                    String.format(
                        "<option value=\"{0}\">{1}</option>",
                        table[i]["moduleIdx"],
                        table[i]["moduleName"]
                    )
                );
            }
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/GetMenuModuleList", param, fnCallback);
}

function fnSetMenuModuleToMember() {
    if ($("#selModule").val() == "") {
        return;
    }

    if (memberIdx == 0) {
        return;
    }

    var param = {
        "moduleIdx": $("#selModule").val(),
        "memberIdx": memberIdx
    };

    var fnCallback = function (data) {
        if (data.d == "0") {
            fnGetMenuPower(memberIdx);
        }
        else {
            alert("데이터 처리 중 에러가 발생하였습니다");
        }
    };

    window.fnAjaxSend("/power/ws_power.asmx/SetMenuModuleToMember", param, fnCallback);
}

function fnGetManagerList() {
    var listStr = "<li class=\"manager_list_wrap\" onclick=\"fnGetMenuPower({0}, this);\">\
                    <ul class=\"manager_list_row\">\
                        <li class=\"manager_li short_li\">{0}</li>\
                        <li class=\"manager_li long_li\">{1}</li>\
                        <li class=\"manager_li middle_li\">{2}</li>\
                        <li class=\"manager_li long_li\">{3}</li>\
                    </ul>\
                </li>";

    var param = {
        "option": $("#selOption").val(),
        "value": $("#txtValue").val(),
        "pageNo": 0
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        $(".manager_list_table").children("li:gt(0)").remove();

        if (table.length > 0) {
            for (var i = 0; i < table.length; i++) {
                $(".manager_list_table").append(
                    String.format(
                        listStr,
                        table[i]["memberIdx"],
                        table[i]["userId"],
                        table[i]["userName"],
                        table[i]["memberType"] == "A" ? "협회관리자" : "스탯앤코관리자"
                    )
                );
            }
        }
    }

    window.fnAjaxSend("/association/ws_association.asmx/GetManagerList", param, fnCallback);
}

function fnGetMenuPower(idx, con) {
    if (con != null) {
        $(".manager_list_wrap").removeClass("manager_list_select ");
        $(con).addClass("manager_list_select ");
    }

    memberIdx = idx;

    var param = {
        "memberIdx": memberIdx
    };

    var fnCallback = function (data) {
        var table = JSON.parse(data.d).Table;

        var mainStr = "<li class=\"checkbox_table_row\">\
                            <h5 class=\"checkbox_title\">{1}</h5>\
                            <ul class=\"checkbox_li_wrap\" id=\"sub_{0}\"></ul>\
                        </li > ";

        var subStr = "<li class=\"checkbox_li\">\
                            <label for=\"check_0{0}_{1}\">{2}</label>\
                            <input id=\"check_0{0}_{1}\" type=\"checkbox\" class=\"input_check_box\" onclick=\"fnSetMenuPower({0},{1});\">\
                        </li>";

        $(".checkbox_table").empty();

        var mainIdx = 0;

        for (var i = 0; i < table.length; i++) {
            if (table[i]["menuSubIdx"] == "0") {
                mainIdx = table[i]["menuIdx"];

                $(".checkbox_table").append(
                    String.format(
                        mainStr,
                        table[i]["menuIdx"],
                        table[i]["menuName"]
                    )
                );
            }
            else {
                $("#sub_" + table[i]["menuIdx"]).append(
                    String.format(
                        subStr,
                        table[i]["menuIdx"],
                        table[i]["menuSubIdx"],
                        table[i]["menuName"]
                    )
                );

                var sel = "#check_0" + table[i]["menuIdx"] + "_" + table[i]["menuSubIdx"];

                $(sel).prop("checked", table[i]["power"] == "W");
            }
        }
    };


    window.fnAjaxSend("/power/ws_power.asmx/GetMenuPower", param, fnCallback);
}

function fnSetMenuPower(menuIdx, menuSubIdx) {
    var param = {
        "memberIdx": memberIdx,
        "menuIdx": menuIdx,
        "menuSubIdx": menuSubIdx,
        "power": $("#check_0" + menuIdx + "_" + menuSubIdx).is(":checked") ? "W" : "N"
    };

    var fnCallback = function (data) {
        if (data.d != "0") {
            alert("데이터 처리 중 에러가 발생하였습니다.");
        }
        else {
            fnGetMenuModuleList();
            $("#ssoModuleplusModal").modal("hide");
        }
    }

    window.fnAjaxSend("/power/ws_power.asmx/SetMenuPower", param, fnCallback);
}