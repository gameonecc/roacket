﻿using Statnco.DA.Roacket.Admin;
using Statnco.FW.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Statnco.Web.Admin.power
{
    /// <summary>
    /// ws_power의 요약 설명입니다.
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // ASP.NET AJAX를 사용하여 스크립트에서 이 웹 서비스를 호출하려면 다음 줄의 주석 처리를 제거합니다. 
    [System.Web.Script.Services.ScriptService]
    public class ws_power : System.Web.Services.WebService
    {
        #region 메뉴 모듈 리스트 조회
        [WebMethod]
        public string GetMenuModuleList()
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetMenuModuleList();
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 메뉴 모듈 생성
        [WebMethod]
        public string SetMenuModule(string moduleName)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetMenuModule(moduleName);
                }
            }
            catch (Exception exp)
            {
                return "-1";
            }

            return rtn.ToString();
        }
        #endregion

        #region 메뉴 모듈 권한 조회
        [WebMethod]
        public string GetMenuModulePower(int moduleIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetMenuModulePower(moduleIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 메뉴 모듈 권한 세팅
        [WebMethod]
        public string SetMenuModulePower(int moduleIdx, int menuIdx, int menuSubIdx, string power)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetMenuModulePower(moduleIdx, menuIdx, menuSubIdx, power);
                }
            }
            catch (Exception exp)
            {
                return "-1";
            }

            return rtn.ToString();
        }
        #endregion

        #region 메뉴 권한 조회
        [WebMethod]
        public string GetMenuPower(int memberIdx)
        {
            DataTable dt = null;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    dt = biz.GetMenuPower(memberIdx);
                }
            }
            catch (Exception exp)
            {
                return null;
            }

            return Extends.GetJSONString(dt);
        }
        #endregion

        #region 메뉴 모듈 유저 복사
        [WebMethod]
        public int SetMenuModuleToMember(int moduleIdx, int memberIdx)
        {
            int rtn = 0;

            int editUser = Convert.ToInt32(HttpContext.Current.Request.Cookies["memberIdx"].Value);

            try
            {
                using (var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetMenuModuleToMember(moduleIdx, memberIdx, editUser);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion

        #region 메뉴 권한 저장
        [WebMethod]
        public int SetMenuPower(int memberIdx, int menuIdx, int menuSubIdx, string power)
        {
            int rtn = 0;

            try
            {
                using(var biz = new Roacket_Admin_Biz())
                {
                    rtn = biz.SetMenuPower(memberIdx, menuIdx, menuSubIdx, power);
                }
            }
            catch (Exception exp)
            {
                return -1;
            }

            return rtn;
        }
        #endregion
    }
}
